const browsersync = require('browser-sync').create();
const del = require('del');
const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const sass = require('gulp-dart-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const submodule = require('gulp-git-submodule');
const install = require('gulp-install');
const fs = require('fs');
const exec = require('child_process').exec;

submodule.registerTasks(gulp);

const paths = {
    base: {
        base: {
            dir: './'
        },
        node: {
            dir: './node_modules'
        },
        packageLock: {
            files: './package-lock.json'
        }
    },
    docs: {
        user: {
            bookdir: './docs/user/book',
            booksrc: './docs/user/src',
        },
        dev: {
            bookdir: './docs/dev/book',
            booksrc: './docs/dev/src',
        },
    },
    dist: {
        base: {
            dir: './Templates/dist',
            files: './Templates/dist/**/*'
        },
        css: {
            dir: './Templates/dist/assets/css',
        },
        js: {
            dir: './Templates/dist/assets/js',
            files: './Templates/dist/assets/js/pages',
        },
    },
    src: {
        base: {
            dir: './Templates/src',
            files: './Templates/src/**/*'
        },
        css: {
            dir: './Templates/src/assets/css',
            files: './Templates/src/assets/css/**/*'
        },
        img: {
            dir: './Templates/src/assets/images',
            files: './Templates/src/assets/images/**/*',
        },
        js: {
            dir: './Templates/src/assets/js',
            pages: './Templates/src/assets/js/pages',
            files: './Templates/src/assets/js/pages/*.js',
            main: './Templates/src/assets/js/*.js',
        },
        partials: {
            dir: './Templates/src/partials',
            files: './Templates/src/partials/**/*'
        },
        scss: {
            dir: './Templates/src/assets/scss',
            files: './Templates/src/assets/scss/**/*',
            main: './Templates/src/assets/scss/*.scss'
        }
    },
    plugins: {
        dir: './plugins'
    },
    external_dependencies: {
        composer: {
            main: 'composer.json',
            lock: 'composer.lock',
            folder: 'vendor'
        },
        npm: {
            main: 'package.json',
            lock: 'package-lock.json',
            folder: 'node_modules'
        },
    }
};

const corePath = paths.base.dir;
const plugins = fs.readdirSync(paths.plugins.dir);

/**
 * Genera la documentación de desarrollo.
 */
gulp.task('devDoc', function (callback) {
    exec('mdbook build docs/dev', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
});

/**
 * Genera la documentación de clases de PHP.
 */
gulp.task('devPhpDoc', function (callback) {
    // No funciona bien con todas las versiones de PHP
    exec('php72 -d memory_limit=2G bin/sami.phar update --force bin/sami_documentation.php', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
});

/**
 * Genera la documentación de desarrollo.
 */
gulp.task('userDoc', function (callback) {
    exec('mdbook build docs/user', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
});

/**
 * Instala las dependencias de composer y npm para los plugins que lo requieren.
 */
gulp.task('coreDependencies', function (callback) {
    if (fs.existsSync(corePath + '/' + paths.external_dependencies.composer.lock)) {
        del.sync(corePath + '/' + paths.external_dependencies.composer.lock);
    }
    if (fs.existsSync(corePath + '/' + paths.external_dependencies.composer.folder)) {
        del.sync(corePath + '/' + paths.external_dependencies.composer.folder);
    }
    if (fs.existsSync(corePath + '/' + paths.external_dependencies.composer.main)) {
        gulp
            .src(corePath + '/' + paths.external_dependencies.composer.main)
            .pipe(gulp.dest(corePath))
            .pipe(install());
    }

    if (fs.existsSync(corePath + '/' + paths.external_dependencies.npm.lock)) {
        del.sync(corePath + '/' + paths.external_dependencies.npm.lock);
    }
    if (fs.existsSync(corePath + '/' + paths.external_dependencies.npm.folder)) {
        del.sync(corePath + '/' + paths.external_dependencies.npm.folder);
    }
    if (fs.existsSync(corePath + '/' + paths.external_dependencies.npm.main)) {
        gulp
            .src(corePath + '/' + paths.external_dependencies.npm.main)
            .pipe(gulp.dest(corePath))
            .pipe(install());
    }
    callback();
});

/**
 * Instala las dependencias de composer y npm para los plugins que lo requieren.
 */
gulp.task('pluginsDependencies', function (callback) {
    let pluginPath = '';
    plugins.forEach(function (pluginName) {
        pluginPath = paths.plugins.dir + '/' + pluginName;
        if (fs.existsSync(pluginPath + '/' + paths.external_dependencies.composer.lock)) {
            del.sync(pluginPath + '/' + paths.external_dependencies.composer.lock);
        }
        if (fs.existsSync(pluginPath + '/' + paths.external_dependencies.composer.folder)) {
            del.sync(pluginPath + '/' + paths.external_dependencies.composer.folder);
        }
        if (fs.existsSync(pluginPath + '/' + paths.external_dependencies.composer.main)) {
            gulp
                .src(pluginPath + '/' + paths.external_dependencies.composer.main)
                .pipe(gulp.dest(pluginPath))
                .pipe(install());
        }

        if (fs.existsSync(pluginPath + '/' + paths.external_dependencies.npm.lock)) {
            del.sync(pluginPath + '/' + paths.external_dependencies.npm.lock);
        }
        if (fs.existsSync(pluginPath + '/' + paths.external_dependencies.npm.folder)) {
            del.sync(pluginPath + '/' + paths.external_dependencies.npm.folder);
        }
        if (fs.existsSync(pluginPath + '/' + paths.external_dependencies.npm.main)) {
            gulp
                .src(pluginPath + '/' + paths.external_dependencies.npm.main)
                .pipe(gulp.dest(pluginPath))
                .pipe(install());
        }
    });
    callback();
});

/**
 * Crea un servidor local (atención: no es equivalente a Apache).
 */
gulp.task('browsersync', function (callback) {
    browsersync.init({
        server: {
            baseDir: [paths.dist.base.dir, paths.src.base.dir, paths.base.base.dir]
        },
    });
    callback();
});

/**
 * Se encarga de mantener sincronizado y reiniciar el servidor local.
 * Debe ser ejecutado desde otra tarea como watch.
 */
gulp.task('browsersyncReload', function (callback) {
    browsersync.reload();
    callback();
});

/**
 * Monitoriza los cambios en ciertos archivos para ejecutar comandos de forma automática al guardar cambios.
 */
gulp.task('watch', function () {
    gulp.watch(paths.src.scss.files, gulp.series('scss', 'browsersyncReload'));
    gulp.watch([paths.src.js.dir], gulp.series('js', 'browsersyncReload'));
    gulp.watch([paths.src.js.pages], gulp.series('jsPages', 'browsersyncReload'));
    gulp.watch([paths.base.base.dir], gulp.series('browsersyncReload'));
    gulp.watch([paths.docs.dev.booksrc], gulp.series('devDoc'));
    gulp.watch([paths.docs.user.booksrc], gulp.series('userDoc'));
});

/**
 * Minimifica los archivos JS en js.
 */
gulp.task('js', function () {
    return gulp
        .src(paths.src.js.main)
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.js.dir));
});

/**
 * Minimifica los archivos JS en pages.
 */
gulp.task('jsPages', function () {
    return gulp
        .src(paths.src.js.files)
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.js.files));
});

/**
 * Compila y minifica los archivos SCSS.
 */
gulp.task('scss', function () {
    // generate ltr
    return gulp
        .src(paths.src.scss.main)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(
            autoprefixer()
        )
        .pipe(gulp.dest(paths.dist.css.dir))
        .pipe(cleanCSS())
        .pipe(
            rename({
                suffix: ".min",
            })
        )
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(paths.dist.css.dir));
});

/**
 * Elimina el archivo package-lock.json
 */
gulp.task('clean:packageLock', function (callback) {
    del.sync(paths.base.packageLock.files);
    callback();
});

/**
 * Elimina la carpeta Templates/dist del núcleo
 */
gulp.task('clean:dist', function (callback) {
    del.sync(paths.dist.base.dir);
    callback();
});

/**
 * Copia los archivos finales de Templates/src a la carpeta Templates/dist.
 */
gulp.task('copy:all', function () {
    return gulp
        .src([
            paths.src.base.files,
            '!' + paths.src.partials.dir, '!' + paths.src.partials.files,
            '!' + paths.src.scss.dir, '!' + paths.src.scss.files,
            '!' + paths.src.js.dir, '!' + paths.src.js.files, '!' + paths.src.js.main,
        ])
        .pipe(gulp.dest(paths.dist.base.dir));
});

/**
 * Tarea de construcción, prepara todos los archivos necesarios.
 */
gulp.task('build', gulp.series(gulp.parallel('clean:packageLock', 'clean:dist', 'copy:all', 'coreDependencies', 'pluginsDependencies'), 'scss', 'js', 'jsPages'));

/**
 * Tarea de ejecución del servidor web y el observador, prepara todos los archivos necesarios.
 */
gulp.task('browser-watch', gulp.series(gulp.parallel('browsersync', 'watch')));

/**
 * Tarea de construcción, prepara todos los archivos necesarios y además se mantiene monitorizando cambios para agilizar su desarrollo.
 */
gulp.task('default', gulp.series(gulp.parallel('clean:packageLock', 'clean:dist', 'copy:all', 'coreDependencies', 'pluginsDependencies', 'scss', 'js', 'jsPages'), gulp.parallel('browser-watch')));
