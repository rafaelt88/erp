<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of fs_list_decoration
 */
class fs_list_decoration
{
    /**
     * Columnas a utilizar en el listado de datos.
     *
     * @var array
     */
    public $columns = [];

    /**
     * Lista de opciones para la decoración.
     *
     * @var array
     */
    public $options = [];

    /**
     * Lista de enlaces para los resultados a mostrar (contiene la ruta base, se concatena con el registro)
     *
     * @var array
     */
    public $urls = [];

    /**
     * Clase con utilidades para las divisas.
     *
     * @var fs_divisa_tools
     */
    protected $divisa_tools;

    /**
     * Listado de objetos para consultar a los datos de modelos.
     *
     * @var array
     */
    private $model_objects = [];

    /**
     * fs_list_decoration constructor.
     *
     * @param fs_list_decoration $old_decoration
     */
    public function __construct($old_decoration = null)
    {
        $this->divisa_tools = new fs_divisa_tools();
        if (!is_null($old_decoration)) {
            $this->columns = $old_decoration->columns;
            $this->options = $old_decoration->options;
            $this->urls = $old_decoration->urls;
        }
    }

    /**
     * Añade una columna con todos los detalles indicados.
     *
     * @param string $tab_name
     * @param string $col_name
     * @param string $type
     * @param string $title
     * @param string $class
     * @param string $base_url
     */
    public function add_column($tab_name, $col_name, $type = 'string', $title = '', $class = '', $base_url = '')
    {
        $this->columns[$tab_name][$col_name] = [
            'base_url' => $base_url,
            'class' => $class,
            'type' => $type,
            'title' => empty($title) ? $col_name : $title,
        ];
    }

    /**
     * Añade la opción para la pestaña.
     *
     * @param string $tab_name
     * @param string $col_name
     * @param mixed  $value
     * @param string $class
     */
    public function add_row_option($tab_name, $col_name, $value, $class)
    {
        $this->options[$tab_name][] = [
            'class' => $class,
            'col_name' => $col_name,
            'value' => $value,
        ];
    }

    /**
     * Añade la URL base para la pestaña.
     *
     * @param string $tab_name
     * @param string $base_url
     * @param string $col_name
     */
    public function add_row_url($tab_name, $base_url, $col_name)
    {
        $this->urls[$tab_name] = [
            'col_name' => $col_name,
            'base_url' => $base_url,
        ];
    }

    /**
     * Devuelve las columnas relacionadas con la pestaña.
     *
     * @param string $tab_name
     *
     * @return array
     */
    public function get_columns($tab_name)
    {
        return isset($this->columns[$tab_name]) ? $this->columns[$tab_name] : [];
    }

    /**
     * Devuelve los datos adicionales para la pestaña y la fila.
     *
     * @param string $tab_name
     * @param array  $row
     *
     * @return string
     */
    public function row_class($tab_name, $row)
    {
        $extra = '';
        if (isset($this->urls[$tab_name])) {
            $col_name = $this->urls[$tab_name]['col_name'];
            $extra .= ' clickableRow" href="' . $this->urls[$tab_name]['base_url'] . $row[$col_name] . '"';
        }

        if (!isset($this->options[$tab_name])) {
            return $extra;
        }

        foreach ($this->options[$tab_name] as $option) {
            $col2_name = $option['col_name'];
            if ($this->compare_values($row[$col2_name], $option['value'])) {
                return $option['class'] . $extra;
            }
        }

        return $extra;
    }

    /**
     * TODO: Este método es invocado desde la vista view/master/list_controller.html de la siguiente forma:
     *  {$fsc->decoration->show($key2, $value2, $value1)}
     * No parece que se invoce de otra forma diferente, por lo que $css_class siempre será []
     *
     * @param string $col_name
     * @param string $col_config
     * @param array  $row
     * @param array  $css_class
     *
     * @return string
     */
    public function show($col_name, $col_config, $row, $css_class = [])
    {
        /**
         * TODO: Este método es invocado desde la vista view/master/list_controller.html de la siguiente forma:
         *  {$fsc->decoration->show($key2, $value2, $value1)}
         * No parece que se invoce de otra forma diferente, por lo que $css_class siempre será []
         */
        if (constant('FS_DB_HISTORY')) {
            if (!empty($css_class)) {
                bdump($css_class);
                bdump(debug_backtrace());
                die('¿Desde qué otro sitio es invocado fs_list_decoration() que tiene 4 parámetros?');
            }
        }

        $value = isset($row[$col_name]) ? $row[$col_name] : '';
        switch ($col_config['type']) {
            case 'bool':
                $final_value = $this->str2bool($value) ? 'Si' : 'No';
                break;

            case 'i':
                $final_value = '<i class="' . $value . '" aria-hidden="true"></i>';
                $col_config['class'] = [];
                break;

            case 'icon':
                /**
                 * Cambiamos el normal funcionamiento del valor de class, que es un string,
                 * por un array que se le tiene que pasar un índice definido indicado más abajo,
                 * que deberá contener los índices icon y color.
                 *
                 * Ejemplo:
                 *
                 * 'eq0' => [
                 *     'icon' => 'fa-solid fa-user fa-fw',
                 *     'color' => '#f04124',
                 * ],
                 *
                 */
                if (!is_array($col_config['class'])) {
                    throw new Exception('This value must be an array on ' . get_called_class() . '.');
                }

                // TODO: Requiere optimización para reducir código a medida que se añaden nuevos indices
                $indexes = [
                    'eq0', // equal 0
                    'eq1', // equal 1
                    'gt0', // greater than 0
                    'gt1', // greater than 1
                ];
                if (empty($value) && isset($col_config['class']['eq0'])) {
                    $index = 'eq0';
                } elseif ($value > 0 && isset($col_config['class']['gt0'])) {
                    $index = 'gt0';
                } elseif ($value > 1 && isset($col_config['class']['gt1'])) {
                    $index = 'gt1';
                } else {
                    $index = 'eq1';
                }

                if (!isset($col_config['class'][$index]['icon'])) {
                    throw new Exception('Index ' . $index . ' have no icon value on ' . get_called_class() . '.');
                }
                if (!isset($col_config['class'][$index]['color'])) {
                    throw new Exception('Index ' . $index . ' have no color value on ' . get_called_class() . '.');
                }

                $final_value = '<i class="' . $col_config['class'][$index]['icon'] . '" aria-hidden="true"
                                    style="color: ' . $col_config['class'][$index]['color'] . '"
                                    title="' . $col_config['class'][$index]['tooltip'] . '"></i>';

                if (isset($col_config['class'][$index]['link_field'])) {
                    $col_config['base_url'] = '';
                    $final_value = '<a href="' . $row[$col_config['class'][$index]['link_field']] . '" class="cancel_clickable">' . $final_value . '</a>';
                }
                // El tipo array retorna en class los valores, así que hay que eliminar el contenido para que no rellene el class en list_controller con 'Array' al dar un fallo en el implode de abajo
                $col_config['class'] = [];
                break;

            case 'date':
                $final_value = empty($value) ? '-' : show_date($value);
                break;

            case 'timestamp':
            case 'datetime':
                $final_value = show_date($value, 'd-m-Y H:i:s');
                break;

            case 'money':
                $final_value = $this->divisa_tools->show_precio((float) $value);
                break;

            case 'number':
                $final_value = $this->divisa_tools->show_numero((float) $value);
                break;

            case 'class':
                // Almacenamos en $this->tmp_data el resultado para evitar consultas SQL repetidas por elemento en tabla y así acelerar el resultado.
                if (!isset($this->tmp_data[$col_config['class']]['value'][$value])) {
                    $new_class = new $col_config['class']();
                    if ($new_class instanceof fs_model) {
                        $object_class = $new_class->get($value);
                        if ($object_class && is_object($object_class) && $object_class instanceof fs_model) {
                            $this->tmp_data[$col_config['class']]['value'][$value] = $object_class->get_description();
                            $this->tmp_data[$col_config['class']]['base_url'][$value] = $object_class->url();
                        } else {
                            $this->tmp_data[$col_config['class']]['value'][$value] = '';
                            $this->tmp_data[$col_config['class']]['base_url'][$value] = '';
                        }
                    } else {
                        $final_value = $col_config['class'] . ' is not fs_model';
                        break;
                    }
                }

                $final_value = $this->tmp_data[$col_config['class']]['value'][$value];
                if (empty($col_config['base_url'])) {
                    $col_config['base_url'] = $this->tmp_data[$col_config['class']]['base_url'][$value];
                    $value = '';
                }

                break;
            case 'array':
                $final_value = '<em>Valor no definido</em>';
                if (isset($col_config['class'][$value])) {
                    $final_value = $col_config['class'][$value];
                }
                $col_config['base_url'] = '';

                // El tipo array retorna en class los valores, así que hay que eliminar el contenido para que no rellene el class en list_controller con 'Array' al dar un fallo en el implode de abajo
                $col_config['class'] = [];
                break;

            case 'color':
                $final_value = "<i title='Código de color " . $value . "' style='background-color:$value;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>";
                break;

            case 'right':
                $final_value = "<div class='text-end'>$value</div>";
                break;

            case 'link':
            case 'button':
            default:
                //$final_value = truncate_text(strip_tags($value), 100);
                $final_value = strip_tags($value);
        }

        if (!empty($col_config['class'])) {
            $css_class[] = $col_config['class'];
        }

        if ('bool' === $col_config['type']) {
            $css_class[] = $this->str2bool($value) ? 'success' : 'warning';
        } elseif (in_array($col_config['type'], ['money', 'number',]) && $value <= 0) {
            $css_class[] = 'warning';
        }

        if (!empty($col_config['base_url'])) {
            switch ($col_config['class']) {
                case 'button':
                    $link_class = 'btn btn-outline-secondary';
                    $final_value = '<a href="' . $col_config['base_url'] . $value . '" class="' . $link_class . '">' . $final_value . '</a>';
                    break;
                case 'button-expanded':
                    $link_class = 'btn btn-outline-secondary';
                    $final_value = '<div class="d-flex btn-group" role="group">'
                        . '<a href="' . $col_config['base_url'] . $value . '" class="' . $link_class . '">' . $final_value . '</a>'
                        . '</div>';
                    break;
                case 'link':
                default:
                    $link_class = 'link-primary';
                    $final_value = '<a href="' . $col_config['base_url'] . $value . '" class="' . $link_class . '">' . $final_value . '</a>';
                    break;
            }
        }

        if (is_array($css_class)) {
            $css_class = implode(' ', $css_class);
        }
        if (is_array($final_value)) {
            $css_class = implode(' ', $final_value);
        }

        return '<td class="' . $css_class . '">' . $final_value . '</td>';
    }

    /**
     * Devuelve el resultado de comparar si 2 valores son iguales.
     *
     * @param mixed $value1
     * @param mixed $value2
     *
     * @return bool
     */
    protected function compare_values($value1, $value2)
    {
        if (is_bool($value2)) {
            return $value2 === $this->str2bool($value1);
        }

        return $value1 == $value2;
    }

    /**
     * Devuelve el objeto del modelo para la clave primaria indicada.
     *
     * @param string $model_class_name
     * @param string $code
     *
     * @return mixed
     */
    protected function get_model_object($model_class_name, $code)
    {
        if (isset($this->model_objects[$model_class_name][$code])) {
            return $this->model_objects[$model_class_name][$code];
        }

        $model = new $model_class_name();
        $object = $model->get($code);
        if ($object) {
            $this->model_objects[$model_class_name][$code] = $object;
            return $object;
        }

        return $model;
    }

    /**
     * Esta función devuelve TRUE si el valor se corresponde con 't' o '1'.
     *
     * @param string $val
     *
     * @return bool
     */
    private function str2bool($val)
    {
        return ($val == 't' || $val == '1');
    }
}
