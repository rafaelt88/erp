<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_file_manager.php';
require_once constant('BASE_PATH') . '/src/Xnet/Model/Version.php';
require_once constant('BASE_PATH') . '/src/Xnet/Core/XnetPluginManager.php';
require_once constant('BASE_PATH') . '/model/fs_folder.php';

use Xnet\Core\XnetPluginManager;
use Xnet\Model\Version;

/**
 * Description of fs_plugin_manager
 */
class fs_plugin_manager extends XnetPluginManager
{
    /**
     * Contiene los parámetros posibles en mifactura.ini y su valor por defecto si no ha sido definido.
     */
    const INI_PARAMETERS = [
        'idplugin' => null,
        'name' => null,
        'description' => 'Descripción pendiente',
        'min_version' => 0,
        'update_url' => '',
        'version' => 1,
        'version_url' => '',
        'wizard' => '',
        'self-activating' => false,
        'deactivable' => true,
    ];

    /**
     * Contiene el valor definido en config.php o en su defecto FALSE.
     *
     * @var bool
     */
    public $disable_mod_plugins = false;

    /**
     * Contiene el valor definido en config.php o en su defecto FALSE.
     *
     * @var bool
     */
    public $disable_add_plugins = false;

    /**
     * Contiene el valor definido en config.php o en su defecto FALSE.
     *
     * @var bool
     */
    public $disable_rm_plugins = false;

    /**
     * Versión base del núcleo (se sobre-escribe con el valor en MiVersion
     *
     * @var float|int
     */
    public $version = 2022.0000;

    /**
     * Permite conectar e interactuar con memcache.
     *
     * @var fs_cache
     */
    private $cache;

    /**
     * Gestiona el log de todos los controladores, modelos y base de datos.
     *
     * @var fs_core_log
     */
    private $core_log;

    /**
     * Listado de descargas disponibles.
     *
     * @var array
     */
    private $download_list;

    /**
     * fs_plugin_manager constructor.
     */
    public function __construct()
    {
        $this->cache = new fs_cache();
        $this->core_log = new fs_core_log();

        parent::__construct();

        if (defined('FS_DISABLE_MOD_PLUGINS')) {
            $this->disable_mod_plugins = FS_DISABLE_MOD_PLUGINS;
            $this->disable_add_plugins = FS_DISABLE_MOD_PLUGINS;
            $this->disable_rm_plugins = FS_DISABLE_MOD_PLUGINS;
        }

        if (!$this->disable_mod_plugins) {
            if (defined('FS_DISABLE_ADD_PLUGINS')) {
                $this->disable_add_plugins = FS_DISABLE_ADD_PLUGINS;
            }

            if (defined('FS_DISABLE_RM_PLUGINS')) {
                $this->disable_rm_plugins = FS_DISABLE_RM_PLUGINS;
            }
        }

        if (file_exists('MiVersion')) {
            $this->version = (float) trim(file_get_contents(constant('FS_FOLDER') . '/MiVersion'));
        } elseif (file_exists('FsVersion')) {
            $this->version = (float) trim(file_get_contents(constant('FS_FOLDER') . '/FsVersion'));
        } elseif (file_exists('VERSION')) {
            $this->version = (float) trim(file_get_contents(constant('FS_FOLDER') . '/VERSION'));
        }

        // Si el orden no coincide con el de las prioridades, lo cambiamos
        if ($GLOBALS['plugins'] != $this->get_plugin_priorities()) {
            $this->save_by_plugin_priorities();
        }
    }

    /**
     *
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return array
     */
    public function get_plugin_priorities()
    {
        $plugins = $this->enabled();

        $new_orders['new1'] = $plugins;
        $new_orders['new2'] = $this->bubble_sort($plugins);
        $previous = $plugins;

        $count = 0;
        $equals = 0;
        do {
            $bubble_sort = $this->bubble_sort($plugins);
            if ($previous != $bubble_sort) {
                $previous = $new_orders['new2'];
                $new_orders['new2'] = $bubble_sort;
            }
            if ($previous == $bubble_sort) {
                $equals++;
            }
            $count++;
        } while ($count < round(count($plugins) / 2) && $equals < 3);

        $priorities = [
            'old' => $this->enabled(),
            'new1' => $new_orders['new1'],
            'new2' => $new_orders['new2'],
        ];

        // dump([
        //     'max_tries' => round(count($plugins) / 2),
        //     'count' => $count,
        //     'equals' => $equals,
        //     'updated' => $priorities['new1'] != $priorities['new2'],
        //     'priorities' => $priorities,
        // ]);

        return $priorities['new2'];
        // return $priorities;
    }

    /**
     * Devuelve los plugins activados.
     *
     * @return array
     */
    public function enabled()
    {
        return $GLOBALS['plugins'];
    }

    /**
     * Ordenación por burbuja, los pesos se asignan a través de compare_plugins($p1, $p2).
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @param array $array
     *
     * @return array
     */
    private function bubble_sort($array)
    {
        do {
            $swapped = false;
            for ($i = 0, $c = count($array) - 1; $i < $c; $i++) {
                if ($this->compare_plugins($array[$i], $array[$i + 1]) > 0) {
                    [$array[$i + 1], $array[$i],] = [$array[$i], $array[$i + 1],];
                    $swapped = true;
                }
            }
        } while ($swapped);
        return $array;
    }

    /**
     * Devuelve un valor mayor que 0, si $plugin1 es más prioritario que $plugin2.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @param string $plugin1
     * @param string $plugin2
     *
     * @return int
     */
    private function compare_plugins($plugin1, $plugin2)
    {
        $diff = 0;

        $info1 = $this->get_plugin_data($plugin1);
        $info2 = $this->get_plugin_data($plugin2);

        if (isset($info1['self-activating']) && isset($info2['self-activating'])) {
            if ($info1['self-activating'] > $info2['self-activating']) {
                $diff += 4;
            } elseif ($info1['self-activating'] < $info2['self-activating']) {
                $diff -= 4;
            }
        }

        if (isset($info1['require']) && isset($info2['require'])) {
            if (count($info1['require']) < count($info2['require'])) {
                $diff += 2;
            } else {
                $diff -= 2;
            }
        }

        if (isset($info1['idplugin']) && isset($info2['idplugin'])) {
            if ($info1['idplugin'] != null && $info2['idplugin'] != null) {
                if ($info1['idplugin'] < $info2['idplugin']) {
                    $diff -= 1;
                } elseif ($info1['idplugin'] > $info2['idplugin']) {
                    $diff += 1;
                }
            }
        }

        return $diff;
    }

    /**
     * Obtine los datos del plugin desde su archivo ini.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0824
     *
     * @param $plugin_name
     *
     * @return array|false
     */
    public function get_plugin_data($plugin_name)
    {
        $inifile = constant('FS_FOLDER') . '/plugins/' . $plugin_name . '/mifactura.ini';
        if (!file_exists($inifile)) {
            // TODO: Mantenido de momento por compatibilidad.
            // Esto debería de ser sustituído por un 'return false' y ya está.
            $inifile = constant('FS_FOLDER') . '/plugins/' . $plugin_name . '/facturascripts.ini';
            if (!file_exists($inifile)) {
                return false;
            }
        }

        $plugin = [
            'compatible' => false,
            'description' => 'Sin descripción.',
            'download2_url' => '',
            'enabled' => false,
            // 'error_msg' => 'Falta archivo mifactura.ini',
            'idplugin' => null,
            'min_version' => $this->version,
            'name' => $plugin_name,
            'prioridad' => '-',
            'require' => [],
            'update_url' => '',
            'version' => 1,
            'version_url' => '',
            'wizard' => false,
        ];

        $ini_file = parse_ini_file($inifile);
        $ini_file['name'] = $plugin_name;
        foreach (self::INI_PARAMETERS as $field => $default) {
            if (isset($ini_file[$field])) {
                $plugin[$field] = $ini_file[$field];
            } else {
                $plugin[$field] = $default;
            }
        }

        $_downloads = $this->downloads();
        $downloads = [];
        if (isset($_downloads) && $_downloads !== false) {
            foreach ($_downloads as $download) {
                $downloads[$download['id']] = $download;
            }
        }
        // echo 'lista_plugins<pre>' . print_r($plugin, true) . '</pre>';
        if (isset($downloads[$plugin['idplugin']])) {
            $url = $downloads[$plugin['idplugin']]['link'];
            // echo 'url<pre>' . print_r($url, true) . '</pre>';

            if ($plugin['version_url'] == '') {
                $plugin['version_url'] = $url;
            }

            if ($plugin['update_url'] == '') {
                $plugin['update_url'] = $url;
            }
        }

        $plugin['enabled'] = in_array($plugin_name, $this->enabled());
        $plugin['version'] = floatval($plugin['version']);
        $plugin['min_version'] = floatval($plugin['min_version']);

        if ($this->version >= $plugin['min_version']) {
            $plugin['compatible'] = true;
        } else {
            $plugin['error_msg'] = 'Requiere MiFactura.eu ' . $plugin['min_version'];
        }

        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin_name . '/description')) {
            $plugin['description'] = file_get_contents(constant('FS_FOLDER') . '/plugins/' . $plugin_name . '/description');
        }

        if (isset($ini_file['require']) && $ini_file['require'] != '') {
            $plugin['require'] = explode(',', $ini_file['require']);
        }

        if (!isset($ini_file['version_url']) && $this->downloads()) {
            foreach ($this->downloads() as $ditem) {
                if ($ditem['nombre'] != $plugin['name']) {
                    continue;
                }

                $plugin['idplugin'] = $ditem['id'];

                if (floatval($ditem['version']) > $plugin['version']) {
                    $plugin['download2_url'] = 'updater.php?idplugin=' . $plugin['idplugin'] . '&name=' . $plugin_name;
                }
                break;
            }
        }

        if ($plugin['enabled']) {
            foreach (array_reverse($this->enabled()) as $i => $value) {
                if ($value == $plugin_name) {
                    $plugin['prioridad'] = $i + 1;
                    break;
                }
            }
        }

        return $plugin;
    }

    /**
     * Retorna un array con la lista de plugins.
     *
     * @return array
     */
    public function downloads()
    {
        // echo '<pre>' . print_r($this->download_list, true) . '</pre>';

        if (isset($this->download_list)) {
            return $this->download_list;
        }

        /// buscamos en la cache
        $this->download_list = $this->cache->get('download_list');

        if ($this->download_list) {
            return $this->download_list;
        }

        /// lista de plugins de la comunidad, se descarga de Internet.
        $list = get_from_community('/plugins-list.json', 10);
        if (!empty($list) && $list != 'ERROR') {
            $json = json_encode($list);
            $this->download_list = json_decode($json, true);
            if (isset($this->download_list) && $this->download_list !== false) {
                foreach ($this->download_list as $key => $value) {
                    $activado = file_exists(constant('FS_FOLDER') . '/plugins/' . $value['nombre']);
                    $this->download_list[$key]['instalado'] = $activado;
                    // Si está desactivado y no instalado, lo eliminamos de la lista
                    if (!$activado && ($this->download_list[$key]['activado'] ?? '0') == '0') {
                        unset($this->download_list[$key]);
                    }
                }
            }
            $this->cache->set('download_list', $this->download_list);
            return $this->download_list;
        }

        $this->core_log->new_error('Error al descargar la lista de plugins.');
        $this->download_list = [];

        return $this->download_list;
    }

    /**
     * Guarda los plugins activados de forma automática con el orden de prioridades definido.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     */
    public function save_by_plugin_priorities()
    {
        /**
         * TODO: No se actualizan los cambios, pero se muestra lo que se haría.
         * El problema es que invocar a save, generaría un archivo enabled_plugins.list
         * No queremos generar el archivo, tendríamos que reordenarlos llamando a
         *
         *   Versions::checkPlugins($this->get_plugin_priorities());
         *
         * Pero no lo hago, porque de momento, no me parece necesario.
         */
        debug_message('Plugins actuales: ' . implode(',', $GLOBALS['plugins']));
        debug_message('Plugins ordenados: ' . implode(',', $this->get_plugin_priorities()));
        return;

        /// Asi es como lo tenía Francesc
        $GLOBALS['plugins'] = $this->get_plugin_priorities();
        if ($this->save()) {
            $this->core_log->new_advice('El orden de los plugins se ha optimizado en función del orden de dependencias entre ellos.');
        }
    }

    /**
     * Persiste los datos de los plugins activados.
     *
     * @return bool
     */
    private function save()
    {
        if (empty($GLOBALS['plugins'])) {
            return unlink(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list');
        }

        $string = implode(',', $GLOBALS['plugins']);
        if (false === file_put_contents(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list', $string)) {
            return false;
        }

        return true;
    }

    /**
     * Desactiva el plugin indicado.
     *
     * @param string $plugin_name
     *
     * @return bool
     */
    public function disable($plugin_name)
    {
        if (!in_array($plugin_name, $this->enabled())) {
            return true;
        }

        $plugin = $this->get_plugin_data($plugin_name);

        if ($plugin['name'] == $plugin_name && $plugin['deactivable'] !== true) {
            $this->core_log->new_error('No está permitido desactivar el plugin  <b>' . $plugin_name . '</b>.');
            return false;
        }

        $version = Version::getByName($plugin_name);
        if (isset($version) && !$version->disable()) {
            debug_message('No se ha podido desactivar el plugin ' . $plugin_name . ' en la tabla versions');
        }

        /*
        foreach ($GLOBALS['plugins'] as $i => $value) {
            if ($value == $plugin_name) {
                unset($GLOBALS['plugins'][$i]);
                break;
            }
        }

        if ($this->save()) {
            $this->core_log->new_message('Plugin <b>' . $plugin_name . '</b> desactivado correctamente.');
            $this->core_log->save('Plugin ' . $plugin_name . ' desactivado correctamente.', 'msg');
        } else {
            $this->core_log->new_error('Imposible desactivar el plugin <b>' . $plugin_name . '</b>.');
            return false;
        }
        */

        $GLOBALS['plugins'] = Version::getEnabledPluginsArray(true);
        $this->core_log->new_message('Plugin <b>' . $plugin_name . '</b> desactivado correctamente.');
        $this->core_log->save('Plugin ' . $plugin_name . ' desactivado correctamente.', 'msg');

        /*
         * Desactivamos las páginas que ya no existen
         */
        $this->disable_unnused_pages();

        /// desactivamos los plugins que dependan de este
        foreach ($this->installed() as $plug) {
            /**
             * Si el plugin que hemos desactivado, es requerido por el plugin
             * que estamos comprobando, lo desativamos también.
             */
            if (is_plugin_enabled($plug['name']) && in_array($plugin_name, $plug['require'])) {
                $this->disable($plug['name']);
            }
        }

        $this->clean_cache();
        return true;
    }

    /**
     * Desactiva las páginas que no existen.
     */
    private function disable_unnused_pages()
    {
        $eliminadas = [];
        $page_model = new fs_page();
        foreach ($page_model->all() as $page) {
            if (file_exists(constant('FS_FOLDER') . '/controller/' . $page->name . '.php')) {
                continue;
            }

            $encontrada = false;
            foreach ($this->enabled() as $plugin) {
                if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/controller/' . $page->name . '.php')) {
                    $encontrada = true;
                    break;
                }
            }

            if (!$encontrada && $page->delete()) {
                $eliminadas[] = $page->name;
            }
        }

        if (!empty($eliminadas)) {
            $this->core_log->new_message('Se han eliminado automáticamente las siguientes páginas: ' . implode(', ', $eliminadas));
        }
    }

    /**
     * Retorna la relación de plugins instalados (activados y no activados).
     * También procede a activar aquellos que sean auto-activables.
     * NOTA: Esta función retorna la lista de plugins que se muestra en Ajustes.
     *
     * @return array
     */
    public function installed($redirect = true)
    {
        $plugins = [];
        $enable = [];
        $disabled = $this->disabled();

        foreach (fs_file_manager::scan_folder(constant('FS_FOLDER') . '/plugins') as $file_name) {
            if (!is_dir(constant('FS_FOLDER') . '/plugins/' . $file_name) || in_array($file_name, $disabled)) {
                continue;
            }

            $plugin_data = $this->get_plugin_data($file_name);
            if ($plugin_data === false) {
                $this->core_log->new_error($file_name . ' no es un plugin válido (¿tiene archivo mifactura.ini?)');
                continue;
            }

            $plugin_name = $plugin_data['name'];

            if ($plugin_data['enabled'] == false && $plugin_data['self-activating'] == true) {
                $enable[] = $plugin_name;
            }

            if ($plugin_data === false) {
                if (constant('FS_DB_HISTORY')) {
                    $this->core_log->new_message('No se encuentra mifactura.ini para ' . $file_name);
                }
            } else {
                $plugins[] = $plugin_data;
            }
        }

        // Se activan los plugins auto-activables
        if (count($enable) > 0) {
            $version_model = new Version();
            foreach ($enable as $name) {
                $version = $version_model->getByName($name);
                if (isset($version) && !$version->enable()) {
                    $this->core_log->new_error('Imposible auto-activar el plugin ' . $name);
                }
                /*
                $list = [];
                if (file_exists(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list')) {
                    $list = explode(',', file_get_contents(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list'));
                }
                $list[] = $name;
                file_put_contents(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list', implode(',', $list));
                */
            }
            $this->clean_cache();
            if ($redirect) {
                header('Location: index.php?page=admin_home');
                die();
            }
        }
        return $plugins;
    }

    /**
     * Devuelve los plugins deshabilitados.
     *
     * @return array
     */
    public function disabled()
    {
        $disabled = [];
        if (defined('FS_DISABLED_PLUGINS')) {
            foreach (explode(',', FS_DISABLED_PLUGINS) as $aux) {
                $disabled[] = $aux;
            }
        }

        return $disabled;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->clean();
        fs_file_manager::clear_views_cache();
        fs_folder::regenerar();
    }

    /**
     * Convierte la puntuación del plugin en una representación visual con estrellas.
     *
     * @param int|float $rating
     *
     * @return string
     */
    public function get_stars($rating)
    {
        $cad = '';
        for ($i = 0; $i < 5; $i++) {
            $star = 'fa-solid fa-star fa-fw' . ($i < $rating ? '' : '-empty');
            $cad .= '<i class="' . $star . '"></i>';
        }

        return $cad;
    }

    /**
     * Descarga un plugin.
     *
     * @param int   $plugin_id
     * @param false $quiet
     *
     * @return bool
     */
    public function download($plugin_id, $quiet = false)
    {
        // Comprobamos que tiene permiso para descargar plugins, y que se puede escribir en la carpeta de plugins.
        if ($this->disable_mod_plugins) {
            $this->core_log->new_error('No tienes permiso para descargar plugins.');
            return false;
        }

        if (!is_writable(constant('FS_FOLDER') . '/plugins')) {
            $this->core_log->new_error('No se puede escribir en la carpeta que contiene los plugins');
            return false;
        }

        // Obtenemos la información del plugin
        $item = null;
        foreach ($this->downloads() as $plugin) {
            if ($plugin['id'] === $plugin_id) {
                $item = $plugin;
            }
        }

        if (!isset($item)) {
            $this->core_log->new_error('Descarga no encontrada para plugin con id ' . $plugin_id . '.');
            return false;
        }

        // Descargamos el plugin en la carpeta temporal
        $tmpdir = constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'plugins/';
        if (!@fs_file_download($item['link'], constant('FS_FOLDER') . '/download.zip')) {
            $this->core_log->new_error('Error al descargar el plugin  ' . $item['nombre'] . '. Inténtelo más tarde y contacte con el proveedor si el error persiste.');
            return false;
        }

        // Lo descomprimimos
        $zip = new ZipArchive();
        $res = $zip->open(constant('FS_FOLDER') . '/download.zip', ZipArchive::CHECKCONS);
        if ($res !== true) {
            $this->core_log->new_error('Error al abrir el ZIP del plugin ' . $item['nombre'] . '. Código: ' . $res);
            return false;
        }

        if (!is_dir($tmpdir) && !mkdir($tmpdir)) {
            $this->core_log->new_error('No se ha podido crear la carpeta temporal para el plugin ' . $item['nombre'] . '.');
            return false;
        }
        if (!$zip->extractTo($tmpdir)) {
            $this->core_log->new_error('No se ha podido extraer el plugin ' . $item['nombre'] . '.');
            return false;
        }
        $zip->close();
        unlink(constant('FS_FOLDER') . '/download.zip');

        // Se elimina el plugin antes de descomprimir el nuevo.
        $existe = is_dir(constant('FS_FOLDER') . '/plugins/' . $item['nombre']);
        if ($existe) {
            // Guarda la copia del plugin antes de actualizarlo, por si hay que recuperarlo.
            if (is_dir(constant('FS_FOLDER') . '/plugins/' . $item['nombre']) && !rename(constant('FS_FOLDER') . '/plugins/' . $item['nombre'], constant('FS_FOLDER') . '/plugins/' . $item['nombre'] . '.old')) {
                $this->core_log->new_error('No se ha podido renombrar la carpeta de plugin ' . $item['nombre'] . '.');
                return false;
            }
        }

        // Movemos el plugin de la carpeta temporal a la del plugin
        if (!rename($tmpdir . $item['nombre'], constant('FS_FOLDER') . '/plugins/' . $item['nombre'])) {
            // Si falla, recuperamos el original
            rename(constant('FS_FOLDER') . '/plugins/' . $item['nombre'] . '.old', constant('FS_FOLDER') . '/plugins/' . $item['nombre']);
            $this->core_log->new_error('No se ha podido restaurar la carpeta del nuevo plugin ' . $item['nombre'] . '.');
            return false;
        }

        // Eliminamos la carpeta del plugin original que ya ha sido actualizado
        if ($existe) {
            if (!fs_file_manager::del_tree(constant('FS_FOLDER') . '/plugins/' . $item['nombre'] . '.old') || !fs_file_manager::del_tree($tmpdir . $item['nombre'])) {
                $this->core_log->new_error('No se han podido eliminar los archivos temporales del plugin ' . $item['nombre'] . '.');
            }
        }

        if (!$quiet) {
            $this->core_log->new_message('Plugin  ' . $item['nombre'] . ' añadido correctamente.');
        }

        return $this->enable($item['nombre'], $quiet);
    }

    public function downloadByName($plugin_name, $quiet = false)
    {
        // Comprobamos que tiene permiso para descargar plugins, y que se puede escribir en la carpeta de plugins.
        if ($this->disable_mod_plugins) {
            $this->core_log->new_error('No tienes permiso para descargar plugins.');
            return false;
        }

        if (!is_writable(constant('FS_FOLDER') . '/plugins')) {
            $this->core_log->new_error('No se puede escribir en la carpeta que contiene los plugins');
            return false;
        }

        // Obtenemos la información del plugin
        $item = null;
        foreach ($this->downloads() as $plugin) {
            if ($plugin['nombre'] === $plugin_name) {
                $item = $plugin;
            }
        }

        if (!isset($item)) {
            $this->core_log->new_error('Descarga no encontrada para plugin ' . $plugin_name . '.');
            return false;
        }

        // Descargamos el plugin en la carpeta temporal
        $tmpdir = constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'plugins/';
        if (!@fs_file_download($item['link'], constant('FS_FOLDER') . '/download.zip')) {
            $this->core_log->new_error('Error al descargar el plugin  ' . $item['nombre'] . '. Inténtelo más tarde y contacte con el proveedor si el error persiste.');
            return false;
        }

        // Lo descomprimimos
        $zip = new ZipArchive();
        $res = $zip->open(constant('FS_FOLDER') . '/download.zip', ZipArchive::CHECKCONS);
        if ($res !== true) {
            $this->core_log->new_error('Error al abrir el ZIP del plugin ' . $item['nombre'] . '. Código: ' . $res);
            return false;
        }

        if (!is_dir($tmpdir) && !mkdir($tmpdir)) {
            $this->core_log->new_error('No se ha podido crear la carpeta temporal para el plugin ' . $item['nombre'] . '.');
            return false;
        }
        if (!$zip->extractTo($tmpdir)) {
            $this->core_log->new_error('No se ha podido extraer el plugin ' . $item['nombre'] . '.');
            return false;
        }
        $zip->close();
        unlink(constant('FS_FOLDER') . '/download.zip');

        // Se elimina el plugin antes de descomprimir el nuevo.
        $existe = is_dir(constant('FS_FOLDER') . '/plugins/' . $item['nombre']);
        if ($existe) {
            // Guarda la copia del plugin antes de actualizarlo, por si hay que recuperarlo.
            if (is_dir(constant('FS_FOLDER') . '/plugins/' . $item['nombre']) && !rename(constant('FS_FOLDER') . '/plugins/' . $item['nombre'], constant('FS_FOLDER') . '/plugins/' . $item['nombre'] . '.old')) {
                $this->core_log->new_error('No se ha podido renombrar la carpeta de plugin ' . $item['nombre'] . '.');
                return false;
            }
        }

        // Movemos el plugin de la carpeta temporal a la del plugin
        if (!rename($tmpdir . $item['nombre'], constant('FS_FOLDER') . '/plugins/' . $item['nombre'])) {
            // Si falla, recuperamos el original
            rename(constant('FS_FOLDER') . '/plugins/' . $item['nombre'] . '.old', constant('FS_FOLDER') . '/plugins/' . $item['nombre']);
            $this->core_log->new_error('No se ha podido restaurar la carpeta del nuevo plugin ' . $item['nombre'] . '.');
            return false;
        }

        // Eliminamos la carpeta del plugin original que ya ha sido actualizado
        if ($existe) {
            if (!fs_file_manager::del_tree(constant('FS_FOLDER') . '/plugins/' . $item['nombre'] . '.old') || !fs_file_manager::del_tree($tmpdir . $item['nombre'])) {
                $this->core_log->new_error('No se han podido eliminar los archivos temporales del plugin ' . $item['nombre'] . '.');
            }
        }

        if (!$quiet) {
            $this->core_log->new_message('Plugin  ' . $item['nombre'] . ' añadido correctamente.');
        }

        return $this->enable($item['nombre'], $quiet);
    }

    /**
     * Activa el plugin.
     * La versión old_enable, que era ésta anteriormente, trataba de activar
     * los plugins de los que dependía si estaban operativos, pero de tener wizard,
     * Lanzaba el wizard y luego no continuaba con la activación del siguiente.
     * Ahora sólo debe de activarlo si las dependencias se cumplen, y si no se cumplen
     * avisar de las que faltan.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0824
     *
     * @param $plugin_name
     * @param $quiet
     *
     * @return bool
     */
    public function enable($pluginName, $quiet = false)
    {
        if (is_plugin_enabled($pluginName)) {
            if (!$quiet) {
                $this->core_log->new_message('Plugin <b>' . $pluginName . '</b> ya activado.');
            }
            return true;
        }

        // Determinamos los plugins que son requeridos para comprobar que están activados
        $plugin = $this->get_plugin_data($pluginName);
        $required = $plugin['require'];
        $wizard = $plugin['wizard'];

        // Determinamos los plugins instalados
        $installed = Version::getEnabledPluginsArray();
        $requiredOk = true;
        foreach ($required as $value) {
            if (!in_array($value, $installed)) {
                $this->core_log->new_error("Dependencia no cumplida: <strong>$value</strong> es necesario para <strong>$pluginName</strong>.");
                $requiredOk = false;
            }
        }
        if (!$requiredOk) {
            return false;
        }

        $version = Version::getByName($pluginName);
        if ($version === null) {
            $version = new Version();
            $version->plugin = $pluginName;
        }
        if (!$version->enable()) {
            debug_message('No se ha podido desactivar el plugin ' . $pluginName . ' en la tabla versions');
        }

        $GLOBALS['plugins'] = Version::getEnabledPluginsArray(true);

        /*
        array_unshift($GLOBALS['plugins'], $pluginName);
        if (!$this->save()) {
            $this->core_log->new_error('Imposible activar el plugin <b>' . $pluginName . '</b>.');
            return false;
        }
        */

        require_all_models();

        if ($wizard) {
            $this->core_log->new_advice('Ya puedes <a href="index.php?page=' . $wizard . '">configurar el plugin</a>.');
            header('Location: index.php?page=' . $wizard);
            $this->clean_cache();
            return true;
        }

        $this->enable_plugin_controllers($pluginName);
        if (!$quiet) {
            $this->core_log->new_message('Plugin <b>' . $pluginName . '</b> activado correctamente.');
        }
        $this->core_log->save('Plugin ' . $pluginName . ' activado correctamente.', 'msg');
        $this->clean_cache();
    }

    public function old_enable($plugin_name, $quiet = false)
    {
        if (is_plugin_enabled($plugin_name)) {
            if (!$quiet) {
                $this->core_log->new_message('Plugin <b>' . $plugin_name . '</b> ya activado.');
            }
            return true;
        }

        $name = $this->rename_plugin($plugin_name);

        $plugins = [];
        foreach ($this->downloads() as $plugin) {
            $plugins[$plugin['id']] = $plugin;
        }

        /// comprobamos las dependencias instaladas para activarlas
        $install = true;
        $wizard = false;
        $required_plugins = [];
        foreach ($this->installed() as $pitem) {
            if ($pitem['name'] != $name) {
                continue;
            }
            $wizard = $pitem['wizard'];
            $required_plugins[] = $pitem['require'];
            foreach ($pitem['require'] as $required_plugin) {
                if (is_plugin_enabled($required_plugin)) {
                    continue;
                }

                /// comprobamos las dependencias pendientes de instalar
                if (is_plugin_enabled($required_plugin)) {
                    continue;
                }
                foreach ($this->installed() as $pitem) {
                    if ($pitem['name'] != $required_plugin) {
                        continue;
                    }
                    if (!in_array($required_plugin, $this->enabled())) {
                        $install = $this->enable($required_plugin, $quiet);
                        break;
                    }
                }

                foreach ($this->downloads() as $plugin) {
                    if ($plugin['nombre'] != $required_plugin) {
                        continue;
                    }

                    if ($this->download($plugin['id'], $quiet)) {
                        $txt = 'Instalada dependencia: <b>' . $required_plugin . '</b>';
                        $this->core_log->new_message($txt);
                        array_unshift($required_plugins, $plugin['nombre']);
                    } else {
                        $install = false;
                        $txt = 'Dependencias incumplidas: <b>' . $required_plugin . '</b>';
                        foreach ($this->downloads() as $value) {
                            if ($value['nombre'] == $required_plugin && !$this->disable_add_plugins) {
                                $txt .= '. Puedes descargar este plugin desde la <b>pestaña descargas</b>.';
                                break;
                            }
                        }

                        $this->core_log->new_error($txt);
                    }
                }
            }
            break;
        }

        foreach ($this->installed() as $pitem) {
            if ($pitem['name'] != $name) {
                continue;
            }

            $wizard = $pitem['wizard'];
            foreach ($pitem['require'] as $req) {
                if (is_plugin_enabled($req)) {
                    continue;
                }

                $install = false;
                $txt = 'Dependencias incumplidas: <b>' . $req . '</b>';
                foreach ($this->downloads() as $value) {
                    if ($value['nombre'] == $req && !$this->disable_add_plugins) {
                        $txt .= '. Puedes descargar este plugin desde la <b>pestaña descargas</b>.';
                        break;
                    }
                }

                $this->core_log->new_error($txt);
            }
            break;
        }

        if (!$install) {
            $this->core_log->new_error('Imposible activar el plugin <b>' . $name . '</b>.');
            return false;
        }

        $version = Version::getByName($plugin_name);
        if ($version === false) {
            $version = new Version();
            $version->plugin = $plugin_name;
        }
        if (!$version->enable()) {
            debug_message('No se ha podido desactivar el plugin ' . $plugin_name . ' en la tabla versions');
        }

        /*
        array_unshift($GLOBALS['plugins'], $name);
        if (!$this->save()) {
            $this->core_log->new_error('Imposible activar el plugin <b>' . $name . '</b>.');
            return false;
        }
        */

        require_all_models();

        if ($wizard) {
            $this->core_log->new_advice('Ya puedes <a href="index.php?page=' . $wizard . '">configurar el plugin</a>.');
            header('Location: index.php?page=' . $wizard);
            $this->clean_cache();
            return true;
        }

        $this->enable_plugin_controllers($name);
        if (!$quiet) {
            $this->core_log->new_message('Plugin <b>' . $name . '</b> activado correctamente.');
        }
        $this->core_log->save('Plugin ' . $name . ' activado correctamente.', 'msg');
        $this->clean_cache();
        return true;
    }

    /**
     * Renombra el plugin indicado, para
     *
     * @param string $name
     *
     * @return false|string
     */
    private function rename_plugin($name)
    {
        if (!rename(constant('FS_FOLDER') . '/plugins/' . $name, constant('FS_FOLDER') . '/plugins/' . $name)) {
            $this->core_log->new_error('Error al renombrar el plugin.');
        }

        return $name;
    }

    /**
     * Se encarga de activar todos los controladores del plugin.
     *
     * @param string $plugin_name
     */
    private function enable_plugin_controllers($plugin_name)
    {
        /// cargamos el archivo functions.php
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin_name . '/functions.php')) {
            require_once constant('BASE_PATH') . '/plugins/' . $plugin_name . '/functions.php';
        }

        /// buscamos controladores
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin_name . '/controller')) {
            $page_list = [];
            foreach (fs_file_manager::scan_files(constant('FS_FOLDER') . '/plugins/' . $plugin_name . '/controller', 'php') as $f) {
                $page_name = substr($f, 0, -4);
                $page_list[] = $page_name;

                require_once constant('BASE_PATH') . '/plugins/' . $plugin_name . '/controller/' . $f;
                $new_fsc = new $page_name();

                if (!$new_fsc->page->save()) {
                    $this->core_log->new_error("No ha sido posible guardar la página " . $page_name);
                }

                unset($new_fsc);
            }

            $this->core_log->new_message('Se han activado automáticamente las siguientes páginas: ' . implode(', ', $page_list) . '.');
        }
    }

    /**
     * Instala el plugin.
     *
     * @param string $path Ruta del archivo zip
     * @param string $name Nombre del plugin
     */
    public function install($path, $name)
    {
        if ($this->disable_add_plugins) {
            $this->core_log->new_error('La subida de plugins está desactivada. Contacta con tu proveedor de hosting.');
            return;
        }

        $zip = new ZipArchive();
        $res = $zip->open($path, ZipArchive::CHECKCONS);
        if ($res === true) {
            $zip->extractTo(constant('FS_FOLDER') . '/plugins/');
            $zip->close();

            $name = $this->rename_plugin(substr($name, 0, -4));
            $this->core_log->new_message('Plugin <b>' . $name . '</b> añadido correctamente. Ya puede activarlo.');
            $this->clean_cache();
        } else {
            $this->core_log->new_error('Error al abrir el archivo ZIP. Código: ' . $res);
        }
    }

    /**
     * Elimina el plugin indicado.
     * Si no se especifica $force a true, se verificará si tiene permisos para hacerlo.
     * Desde el updater, se puede forzar a la eliminación del plugin aunque no esté autorizado.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0912
     *
     * @param $plugin_name
     * @param $force
     *
     * @return bool
     */
    public function remove($plugin_name, $force = false)
    {
        if (!$force && $this->disable_rm_plugins) {
            $this->core_log->new_error('No tienes permiso para eliminar plugins.');
            return false;
        }

        if (!is_writable(constant('FS_FOLDER') . '/plugins/' . $plugin_name)) {
            $this->core_log->new_error('No tienes permisos de escritura sobre la carpeta plugins/' . $plugin_name);
            return false;
        }

        if (fs_file_manager::del_tree(constant('FS_FOLDER') . '/plugins/' . $plugin_name)) {
            $this->core_log->new_message('Plugin ' . $plugin_name . ' eliminado correctamente.');
            $this->core_log->save('Plugin ' . $plugin_name . ' eliminado correctamente.');
            $this->clean_cache();
            return true;
        }

        $this->core_log->new_error('Imposible eliminar el plugin ' . $plugin_name);
        return false;
    }
}
