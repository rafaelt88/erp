<?php

/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

require_once constant('BASE_PATH') . '/base/fs_list_controller.php';

/**
 * Class xfs_list_controller
 *
 * Ante la inconveniencia de modificar los archivos originales de FS, se procede a extender
 * la clase fs_extended_model como xfs_model, con idea de poder realizar adaptaciones de los
 * modelos para el uso particular de los plugins de XFS.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2021.04
 */
abstract class xfs_list_controller extends fs_list_controller
{
}
