<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

$constants = [
    'FS_TMP_NAME' => '',
    'FS_PATH' => '',
    'FS_MYDOCS' => '',
    'ENGINE' => 'blade', // 'raintpl' or 'blade'
    'THEME' => 'skote',
    'FS_DEBUG' => false,
    'FS_DB_HISTORY' => false,
    'FS_DEMO' => false,
    'FS_DISABLE_MOD_PLUGINS' => false,
    'FS_DISABLE_ADD_PLUGINS' => false,
    'FS_DISABLE_RM_PLUGINS' => false,
    'USE_BUG_TRACKER' => false,
];
foreach ($constants as $name => $value) {
    if (!defined($name)) {
        /**
         * Define la constante con valor por defecto
         */
        define($name, $value);
    }
}

$server_params = [
    'SERVER_NAME',
    'REQUEST_URI',
    'SERVER_ADDR',
];
foreach ($server_params as $server_param) {
    if (!isset($_SERVER[$server_param])) {
        $_SERVER[$server_param] = '';
    }
}

if (!defined('BASE_URL')) {
    if (isset($_SERVER['HTTPS'])) {
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    } else {
        $protocol = 'http';
    }
    $base_url = $protocol . "://" . $_SERVER['SERVER_NAME'] . dirname($_SERVER["REQUEST_URI"] . '?') . '/';

    /**
     * Define la constante con valor por defecto vacío
     */
    define('BASE_URL', $base_url);
}

if (!defined('FS_FOLDER')) {
    /**
     * Contiene la ruta principal del alojamiento.
     *
     * No está deprecated, no es la misma ruta cuando es una multi instalación
     */
    define('FS_FOLDER', __DIR__ . '/..');
}

if (constant('FS_TMP_NAME') != '' && !file_exists(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME'))) {
    if (!file_exists(constant('FS_FOLDER') . '/tmp') && mkdir(constant('FS_FOLDER') . '/tmp')) {
        file_put_contents(constant('FS_FOLDER') . '/tmp/index.php', "<?php\necho 'ACCESO DENEGADO';");
    }

    mkdir(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME'));
}

if (!defined('FS_COMMUNITY_URL')) {
    /**
     * URL de la comunidad.
     */
    define('FS_COMMUNITY_URL', 'https://community.mifactura.eu');
}

$GLOBALS['config2'] = [
    'zona_horaria' => 'Europe/Madrid',
    'nf0' => 2,
    'nf0_art' => 2,
    'nf1' => ',',
    'nf2' => ' ',
    'pos_divisa' => 'right',
    'factura' => 'factura',
    'facturas' => 'facturas',
    'factura_simplificada' => 'factura simplificada',
    'factura_rectificativa' => 'factura rectificativa',
    'albaran' => 'albarán',
    'albaranes' => 'albaranes',
    'pedido' => 'pedido',
    'pedidos' => 'pedidos',
    'presupuesto' => 'presupuesto',
    'presupuestos' => 'presupuestos',
    'provincia' => 'provincia',
    'apartado' => 'apartado',
    'cifnif' => 'CIF/NIF',
    'iva' => 'IVA',
    'irpf' => 'IRPF',
    'numero2' => 'número 2',
    'serie' => 'serie',
    'series' => 'series',
    'cost_is_average' => 1,
    'precio_compra' => 'coste',
    'homepage' => 'admin_home',
    'check_db_types' => 1,
    'stock_negativo' => 0,
    'ventas_sin_stock' => 0,
    'ip_whitelist' => '*',
    'libros_contables' => 1,
    'foreign_keys' => 1,
    'new_codigo' => 'new',
    'db_integer' => 'INTEGER',
];

if (file_exists(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'config2.ini')) {
    $ini_data = parse_ini_file(constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'config2.ini');
    foreach ($ini_data as $i => $value) {
        $GLOBALS['config2'][$i] = $value;
    }
}

foreach ($GLOBALS['config2'] as $i => $value) {
    if ($i == 'zona_horaria') {
        /**
         * Asignación de la zona horaria definida para la empresa.
         */
        date_default_timezone_set($value);
    } elseif ($i == 'check_db_types') {
        /**
         * Comprobación de tipos de datos de la base de datos, forzada a si siempre.
         * No podemos demorar correcciones en tipos de datos de las tablas.
         */
        define('FS_' . strtoupper('check_db_types'), 1);
    } else {
        /**
         * Definición de constantes desde las opciones visuales en config2
         */
        define('FS_' . strtoupper($i), $value);
    }
}

if (!file_exists('plugins')) {
    mkdir('plugins');
    chmod('plugins', octdec(777));
}

$GLOBALS['plugins'] = [];
function loadPluginsConfig2()
{
    $plugins = \Xnet\Model\Version::getPlugins();
    foreach ($plugins as $plugin) {
        $pluginName = $plugin['plugin'];
        if (file_exists('plugins/' . $pluginName)) {
            $GLOBALS['plugins'][] = $pluginName;
        }
    }
    $GLOBALS['plugins'] = array_reverse($GLOBALS['plugins']);

    /// cargamos las funciones de los plugins
    foreach ($GLOBALS['plugins'] as $plug) {
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plug . '/functions.php')) {
            require_once constant('BASE_PATH') . '/plugins/' . $plug . '/functions.php';
        }
    }
    require_all_models();
}
