<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use Xnet\Core\DBSchema;
use Xnet\Model\User;

require_once constant('BASE_PATH') . '/base/fs_app.php';
require_once constant('BASE_PATH') . '/base/fs_db2.php';
require_once constant('BASE_PATH') . '/base/fs_default_items.php';
require_once constant('BASE_PATH') . '/base/fs_extended_model.php';
require_once constant('BASE_PATH') . '/base/fs_login.php';
require_once constant('BASE_PATH') . '/base/fs_divisa_tools.php';

/**
 * Ruta al archivo para gestionar datos en periodo de mantenimiento
 */
//define('MAINTENANCE_FILE', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'in_maintenance');

/**
 * La clase principal de la que deben heredar todos los controladores (las páginas) de MiFactura.eu.
 */
class fs_controller extends fs_app
{
    /**
     * Valor cuando el wizard está en ejecución
     */
    const RUNNING = 1;

    /**
     * Valor cuando el wizard ha terminado la ejecución
     */
    const FINISHED = 2;

    /**
     * Páginas modales de finalización del código.
     */
    const BREAKING_PAGES = [
        'access_denied',
        'broken',
        'coming-soon',
        'maintenance',
    ];

    /**
     * TODO: De momento, asumimos todas las horas de Canarias.
     */
    const DEFAULT_TIMEZONE = 'Atlantic/Canary';

    /**
     * Guarda la ruta relativa del controlador.
     *
     * @var string
     */
    private $path;

    /**
     * Nombre del fichero de ayuda, si existe.
     *
     * @var string
     */
    public $helpfile;

    public $wizard_status;

    /**
     * Permite consultar los parámetros predeterminados para series, divisas, forma de pago, etc...
     *
     * @var fs_default_items
     */
    public $default_items;

    /**
     * La empresa
     *
     * @var empresa
     */
    public $empresa;

    /**
     * Listado de extensiones de la página
     *
     * @var array
     */
    public $extensions;

    /**
     * El elemento del menú de esta página
     *
     * @var fs_page
     */
    public $page;

    /**
     * Esta variable contiene el texto enviado como parámetro query por cualquier formulario,
     * es decir, se corresponde con $_REQUEST['query']
     *
     * @var string|bool
     */
    public $query;

    /**
     * Indica que archivo HTML hay que cargar
     *
     * @var string|false
     */
    public $template;

    /**
     * El usuario que ha hecho login
     *
     * @var fs_user
     */
    public $user;

    /**
     * Información de los resellers
     *
     * @var stdClass
     */
    public $reseller_data;

    /**
     * Debe ocultarse el titulo?
     *
     * @var bool
     */
    public $hide_title;

    /**
     * Debe ocultarse el pié de página?
     *
     * @var bool
     */
    public $hide_footer;

    /**
     * Contiene si debe usarse o no el menú lateral.
     *
     * @var bool
     */
    public $use_sidebar_menu;

    /**
     * Muestra el fondo desde Empresa -> Personalización.
     *
     * @var bool
     */
    public $show_background;

    /**
     * Nombre del controlador (lo utilizamos en lugar de __CLASS__ porque __CLASS__
     * en las funciones de la clase padre es el nombre de la clase padre).
     *
     * @var string
     */
    protected $class_name;

    /**
     * Este objeto permite acceso directo a la base de datos.
     *
     * @var fs_db2
     */
    protected $db;

    /**
     * Clase con utilidades para las divisas.
     *
     * @var fs_divisa_tools
     */
    protected $divisa_tools;

    /**
     * Contiene el menú de MiFactura.eu
     *
     * @var fs_page[]
     */
    protected $menu;

    /**
     * Contiene el menú multi-nivel de MiFactura.eu
     *
     * @var array
     */
    protected $array_menu;

    /**
     * Indica si MiFactura.eu está actualizado o no.
     *
     * @var bool
     */
    private $fs_updated;

    /**
     * Listado con los últimos cambios en documentos.
     *
     * @var array
     */
    private $last_changes;

    /**
     * Objeto para centralizar acciones relativas al login/logout
     *
     * @var fs_login
     */
    private $login_tools;

    /**
     * fs_controller constructor.
     *
     * @param string $description Descripción
     * @param string $title       es el título de la página, y el texto que aparecerá en el menú
     * @param array  $folder      es el menú dónde quieres colocar el acceso directo
     * @param bool   $admin       OBSOLETO
     * @param bool   $shmenu      debe ser TRUE si quieres añadir el acceso directo en el menú
     * @param bool   $important   debe ser TRUE si quieres que aparezca en el menú de destacado
     */
    public function __construct(string $description = __CLASS__, $title = 'home', $folder = ['Oculto'], $admin = false, $shmenu = true, $important = false, $favorite = false)
    {
        require_all_models();

        parent::__construct();

        $name = get_called_class();
        if (constant('FS_DEBUG')) {
            if ($name === $description) {
                debug_message('En lugar de __CLASS__, pase una descripción al instanciar el controlador ' . $name);
            }
            if (!is_array($folder)) {
                debug_message('El campo $folder no es un array de strings al instanciar el controlador ' . $name);
            }
        }

        $folder_array = [];
        if (is_array($folder)) {
            // Si se define como oculto, no se puede mostrar en el menú
            if ($shmenu && in_array('Oculto', $folder)) {
                debug_message("El controlador {$title} no puede ser 'Oculto' e indicar que se muestre en el menú.");
                $shmenu = false;
            }

            if (count($folder) > 4) {
                die('El campo $folder no puede tener más de 3 elementos en ' . $name);
            }
            $folder_array = $folder;
            $folder = mb_strtolower(implode('|', $folder));
        }

        $this->reseller_data = getResellerData();
        $this->show_background = false;
        $this->hide_title = fs_filter_input_req('hide_title_iframe', false);
        $this->hide_footer = fs_filter_input_req('hide_footer_iframe', false);
        $this->use_sidebar_menu = false;

        $this->class_name = $name;
        $this->db = new fs_db2();
        $this->extensions = [];

        if (!$this->db->connect()) {
            $this->template = 'no_db';
            $this->new_error_msg('¡Imposible conectar con la base de datos <b>' . fs_app::get_db_name() . '</b>!');
            return;
        }
        $this->user = new fs_user();
        $this->use_sidebar_menu = $this->user->use_sidebar_menu ?? false;

        $this->page = $this->check_fs_page($name, $title, $folder, $shmenu, $important, $favorite);

        $empresa_model = new empresa();
        $this->empresa = $empresa_model->get(1);
        // Si no existe la empresa, la crea
        if ($this->empresa === false) {
            $empresa_model->nombre = 'Empresa de pruebas';
            $empresa_model->id = 1;
            $empresa_model->save();
            $this->empresa = $empresa_model->get(1);
        }
        $this->default_items = new fs_default_items();
        $this->login_tools = new fs_login($this->empresa->get_xid());
        $this->divisa_tools = new fs_divisa_tools($this->empresa->coddivisa);

        if ($name == 'Xnet\Controllers\DatabaseUpdate') {
            // No hacemos nada, ejecutamos directamente para no requerir login mientras se actualizan
            // hay un cambio en la tabla fs_users que ha hecho necesaria esta excepción.
            $this->private_core();
            return true;
        }

        if (filter_input(INPUT_GET, 'logout')) {
            unset($_GET['logout']);
            $this->template = 'login/' . $this->empresa->plantilla_login;
            $this->login_tools->log_out();
            return true;
        }

        if (filter_input(INPUT_POST, 'new_password') && filter_input(INPUT_POST,
                'new_password2') && filter_input(INPUT_POST, 'user')) {
            $this->login_tools->change_user_passwd();
            $this->template = 'login/' . $this->empresa->plantilla_login;
            return true;
        }

        if (!$this->log_in()) {
            $this->template = 'login/' . $this->empresa->plantilla_login;
            $this->public_core();
            return true;
        }

        if ($this->user->have_access_to($this->page->name)) {
            if (!$this->user->codagente) {
                $this->new_error_msg("El usuario no tiene un empleado asignado, contacta con un administrador.");
                if (!$this->user->admin) {
                    $this->template = "errors/coming-soon";
                    return;
                }
            }
            $this->load_extensions();
            if ($name == __CLASS__) {
                $this->template = 'index';
            } else {
                $this->template = $name;

                // TODO: Lo de los wizard, muy bonito, pero ¿Cómo lo dejamos en mantenimiento?
                // Lo único que podemos hacer es que los wizard también se ejecuten desde el updater

                if (file_exists(constant('MAINTENANCE_FILE'))) {
                    $file_contents = json_decode(file_get_contents(constant('MAINTENANCE_FILE')), true);
                    // Sólo permitiremos ejecutar los wizards pendientes mientras esté en mantenimiento
                    if (isset($file_contents['wizards']) && !in_array($name, $file_contents['wizards'])) {
                        $this->template = 'maintenance_update';
                    }
                }

                // Si está activándose el módulo, salimos
                if (isset($_GET['caca']) && get_called_class() !== $this->page->name) {
                    return;
                }

                // TODO: sobrepasado, bloqueado,...
                if (file_exists(FS_MYDOCS . 'images/sobrepasado.true') && in_array($name, $this->reseller_data->locked_controllers)) {
                    $this->template = 'sobrepasado';
                }

                $this->set_default_items();
                $this->pre_private_core();

                // Si está activándose el módulo, salimos
                if ($name != 'admin_home' && isset($_GET['caca'])) {
                    return;
                }

                $this->private_core();
            }
            return;
        }

        if ($name == '') {
            $this->template = 'index';
        }

        $this->template = 'access_denied';
        $this->user->clean_cache(true);
        $this->empresa->clean_cache();
    }

    /**
     * Retorna la fecha y hora en la zona horaria del usuario.
     * TODO: De momento, como el dato de la zona horaria no lo tenemos,a sumimos Atlantic/Canary
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0707
     *
     * @param $gmdatetime
     * @param $format
     *
     * @return string
     * @throws Exception
     */
    public function get_user_datetime($gmdatetime, $format = 'Y-m-d H:i:s T')
    {
        $gmt = new DateTime($gmdatetime, new DateTimeZone('UTC'));
        $local = $gmt->setTimezone(new DateTimeZone(self::DEFAULT_TIMEZONE));
        return $local->format($format);
    }

    /**
     * Convierte un instante en formato local a UTC/GMT
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0707
     *
     * @param $datetime
     * @param $format
     *
     * @return false|string
     */
    public function get_utc_datetime($datetime, $format = 'Y-m-d H:i:s')
    {
        return gmdate($format, strtotime($datetime));
    }

    /**
     * Establece el path relativo del controlador.
     *
     * @param $path
     *
     * @return void
     */
    public function set_path($path)
    {
        $this->path = $path;
        $this->helpfile = 'docs/user/book' . $this->path . '/' . $this->class_name . '.html';
    }

    /**
     * Muestra un consejo al usuario
     *
     * @param string $msg el consejo a mostrar
     */
    public function new_advice($msg, $save = true, $tipo = 'advice', $alerta = false)
    {
        if ($this->class_name == $this->core_log->controller_name()) {
            /// solamente nos interesa mostrar los mensajes del controlador que inicia todo
            $this->core_log->new_advice($msg);
        }

        if ($save) {
            $this->core_log->save($msg, $tipo, $alerta);
        }
    }

    /**
     * Muestra al usuario un mensaje de error
     *
     * @param string $msg el mensaje a mostrar
     */
    public function new_error_msg($msg, $tipo = 'error', $alerta = false, $guardar = true)
    {
        if ($this->class_name == $this->core_log->controller_name()) {
            /// solamente nos interesa mostrar los mensajes del controlador que inicia todo
            $this->core_log->new_error($msg);
        }

        if ($guardar) {
            $this->core_log->save($msg, $tipo, $alerta);
        }
    }

    /**
     * Elimina la lista con los últimos cambios del usuario.
     */
    public function clean_last_changes()
    {
        $this->last_changes = [];
        $this->cache->delete('last_changes_' . $this->user->nick);
    }

    /**
     * Cierra la conexión con la base de datos.
     */
    public function close()
    {
        $this->db->close();
    }

    /**
     * Convierte un precio de la divisa_desde a la divisa especificada
     *
     * @param float  $precio
     * @param string $coddivisa_desde
     * @param string $coddivisa
     *
     * @return float
     */
    public function divisa_convert($precio, $coddivisa_desde, $coddivisa)
    {
        return $this->divisa_tools->divisa_convert($precio, $coddivisa_desde, $coddivisa);
    }

    /**
     * Convierte el precio en euros a la divisa preterminada de la empresa.
     * Por defecto usa las tasas de conversión actuales, pero si se especifica
     * coddivisa y tasaconv las usará.
     *
     * @param float  $precio
     * @param string $coddivisa
     * @param float  $tasaconv
     *
     * @return float
     */
    public function euro_convert($precio, $coddivisa = null, $tasaconv = null)
    {
        return $this->divisa_tools->euro_convert($precio, $coddivisa, $tasaconv);
    }

    /**
     * Devuelve la lista de menús
     *
     * @return array lista de menús
     */
    public function folders()
    {
        $folders = [];
        foreach ($this->menu as $m) {
            if ($m->folder != '' && $m->show_on_menu && !in_array($m->folder, $folders)) {
                $folders[] = $m->folder;
            }
        }
        return $folders;
    }

    /**
     * Retorna la lista de menús con multi-nivel.
     *
     * @return array
     */
    public function folders_array()
    {
        $folders = [];
        foreach ($this->array_menu as $key1 => $data1) {
            if (is_array($data1)) {
                foreach ($data1 as $key2 => $data2) {
                    if (is_array($data2)) {
                        foreach ($data2 as $key3 => $data3) {
                            if (is_array($data3)) {
                                foreach ($data3 as $key4 => $data4) {
                                    if (!is_array($data4)) {
                                        $folders[strtolower($key1)][strtolower($key1 . '|' . $key2)][strtolower($key1 . '|' . $key2 . '|' . $key3)][$data4->name] = $data4->alias;
                                    }
                                }
                            } else {
                                $folders[strtolower($key1)][strtolower($key1 . '|' . $key2)][$data3->name] = $data3->alias;
                            }
                        }
                    } else {
                        $folders[strtolower($key1)][$data2->name] = $data2->alias;
                    }
                }
            } else {
                $folders[$data1->name] = $data1->alias;
            }
        }
        return $folders;
    }

    /**
     * Añade un elemento a la lista de cambios del usuario.
     *
     * @param string $txt   texto descriptivo.
     * @param string $url   URL del elemento (albarán, factura, artículos...).
     * @param bool   $nuevo TRUE si el elemento es nuevo, FALSE si se ha modificado.
     */
    public function new_change($txt, $url, $nuevo = false)
    {
        $this->get_last_changes();
        if (count($this->last_changes) > 0) {
            if ($this->last_changes[0]['url'] == $url) {
                $this->last_changes[0]['nuevo'] = $nuevo;
            } else {
                array_unshift($this->last_changes, [
                    'texto' => ucfirst($txt),
                    'url' => $url,
                    'nuevo' => $nuevo,
                    'cambio' => date('Y-m-d H:i:s'),
                ]);
            }
        } else {
            array_unshift($this->last_changes, [
                'texto' => ucfirst($txt),
                'url' => $url,
                'nuevo' => $nuevo,
                'cambio' => date('Y-m-d H:i:s'),
            ]);
        }

        /// sólo queremos 10 elementos
        $num = 10;
        foreach ($this->last_changes as $i => $value) {
            if ($num > 0) {
                $num--;
            } else {
                unset($this->last_changes[$i]);
            }
        }

        $this->cache->set('last_changes_' . $this->user->nick, $this->last_changes);
    }

    /**
     * Devuelve la lista con los últimos cambios del usuario.
     *
     * @return array
     */
    public function get_last_changes()
    {
        if (!isset($this->last_changes)) {
            $this->last_changes = $this->cache->get_array('last_changes_' . $this->user->nick);
        }

        return $this->last_changes;
    }

    /**
     * Devuelve la lista de elementos de un menú seleccionado
     *
     * @param string $folder el menú seleccionado
     *
     * @return array lista de elementos del menú
     */
    public function pages($folder = '')
    {
        $pages = [];
        foreach ($this->menu as $page) {
            if ($folder == $page->folder && $page->show_on_menu && !in_array($page, $pages)) {
                $pages[] = $page;
            }
        }
        return $pages;
    }

    /**
     * Devuelve el número de consultas SQL (SELECT) que se han ejecutado
     *
     * @return int
     */
    public function selects()
    {
        return $this->db->get_selects();
    }

    /**
     * Redirecciona a la página predeterminada para el usuario
     */
    public function select_default_page()
    {
        if (!$this->db->connected() || !$this->user->logged_on) {
            return;
        }

        $page = $this->user->fs_page;
        if (is_null($page)) {
            /*
             * Cuando un usuario no tiene asignada una página por defecto,
             * se selecciona la primera página del menú.
             */
            $page = 'admin_home';
            foreach ($this->menu as $p) {
                if (!$p->show_on_menu) {
                    continue;
                }

                $page = $p->name;
                if ($p->important) {
                    break;
                }
            }
        }

        header('Location: index.php?page=' . $page);
    }

    /**
     * Devuelve un string con el número en el formato de número predeterminado.
     *
     * @param float $num
     * @param int   $decimales
     * @param bool  $js
     *
     * @return string
     */
    public function show_numero($num = 0, $decimales = FS_NF0, $js = false)
    {
        return $this->divisa_tools->show_numero($num, $decimales, $js);
    }

    /**
     * Devuelve un string con el precio en el formato predefinido y con la
     * divisa seleccionada (o la predeterminada).
     *
     * @param float  $precio
     * @param string $coddivisa
     * @param string $simbolo
     * @param int    $dec Número de decimales
     *
     * @return string
     */
    public function show_precio($precio = 0, $coddivisa = false, $simbolo = true, $dec = FS_NF0)
    {
        return $this->divisa_tools->show_precio($precio, $coddivisa, $simbolo, $dec);
    }

    /**
     * Devuelve el símbolo de divisa predeterminado
     * o bien el símbolo de la divisa seleccionada.
     *
     * @param string $coddivisa
     *
     * @return string
     */
    public function simbolo_divisa($coddivisa = false)
    {
        return $this->divisa_tools->simbolo_divisa($coddivisa);
    }

    /**
     * Devuelve información del sistema para el informe de errores
     *
     * @return string la información del sistema
     */
    public function system_info()
    {
        $txt = 'MiFactura: ' . $this->version() . "\n";

        if ($this->db->connected()) {
            if ($this->user->logged_on) {
                $txt .= 'os: ' . php_uname() . "\n";
                $txt .= 'php: ' . phpversion() . "\n";
                $txt .= 'database type: ' . constant('FS_DB_TYPE') . "\n";
                $txt .= 'database version: ' . $this->db->version() . "\n";

                if (constant('FS_FOREIGN_KEYS') == 0) {
                    $txt .= "foreign keys: NO\n";
                }

                if ($this->cache->connected()) {
                    $txt .= "memcache: YES\n";
                    $txt .= 'memcache version: ' . $this->cache->version() . "\n";
                } else {
                    $txt .= "memcache: NO\n";
                }

                if (function_exists('curl_init')) {
                    $txt .= "curl: YES\n";
                } else {
                    $txt .= "curl: NO\n";
                }

                $txt .= "max input vars: " . ini_get('max_input_vars') . "\n";

                $txt .= 'plugins: ' . join(',', $GLOBALS['plugins']) . "\n";

                if ($this->check_for_updates()) {
                    $txt .= "updated: NO\n";
                }

                if (filter_input(INPUT_SERVER, 'REQUEST_URI')) {
                    $txt .= 'url: ' . filter_input(INPUT_SERVER, 'REQUEST_URI') . "\n------";
                }
            }
        } else {
            $txt .= 'os: ' . php_uname() . "\n";
            $txt .= 'php: ' . phpversion() . "\n";
            $txt .= 'database type: ' . constant('FS_DB_TYPE') . "\n";
        }

        foreach ($this->get_errors() as $e) {
            $txt .= "\n" . $e;
        }

        return str_replace('"', "'", $txt);
    }

    /**
     * Devuelve TRUE si hay actualizaciones pendientes (sólo si eres admin).
     *
     * @return bool
     */
    public function check_for_updates()
    {
        if (isset($this->fs_updated)) {
            return $this->fs_updated;
        }

        $this->fs_updated = false;
        if ($this->user->admin) {
            $desactivado = defined('FS_DISABLE_MOD_PLUGINS') ? FS_DISABLE_MOD_PLUGINS : false;
            if ($desactivado) {
                $this->fs_updated = false;
            } else {
                $fsvar = new fs_var();
                $this->fs_updated = (bool) $fsvar->simple_get('updates');
            }
        }

        return $this->fs_updated;
    }

    /**
     * Devuelve el número de transacciones SQL que se han ejecutado
     *
     * @return int
     */
    public function transactions()
    {
        return $this->db->get_transactions();
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        return isset($this->page) ? $this->page->url() : '';
    }

    /**
     * Se encarga de devolver el código ifrome para comunicar con la comunidad.
     *
     * @return string
     */
    public function iframe_xid()
    {
        return "<div class='hidden'><iframe src='" . constant('FS_COMMUNITY_URL') . "/index.php?page=community_stats" . "&add=TRUE&version=" . $this->version() . "&xid=" . $this->empresa->xid . "&plugins=" . join(',',
                $GLOBALS['plugins']) . "'>"
            . "</iframe></div>";
    }

    /**
     * Indica que el wizard ha sido iniciado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    public function wizard_start()
    {
        $this->wizard_status = self::RUNNING;
    }

    /**
     * Indica que el wizard ha finalizado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    public function wizard_finish()
    {
        $this->wizard_status = self::FINISHED;
    }

    /**
     * Devuelve si ha finalizado o no el wizard.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function is_wizard_finished()
    {
        return $this->wizard_status == self::FINISHED;
    }

    /**
     * Código para indicar que el wizard ha terminado su ejecución.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @param string $message
     */
    public function wizard_finished($message = 'Wizard finalizado correctamente.')
    {
        $status = false;
        $wizard_name = get_called_class();

        // Lo que haya que hacer para saber que la ejecución de $wizard_name ha terminado
        if (file_exists(constant('MAINTENANCE_FILE')) && is_readable(constant('MAINTENANCE_FILE'))) {
            $file_contents = json_decode(file_get_contents(constant('MAINTENANCE_FILE')), true);
            if (isset($file_contents['wizards'])) {
                foreach ($file_contents['wizards'] as $pos => $wizard) {
                    if ($wizard == $wizard_name) {
                        unset($file_contents['wizards'][$pos]);
                        // Necesitamos reordenar los índices tras eliminar un elemento.
                        $file_contents['wizards'] = array_values($file_contents['wizards']);
                        $status = file_put_contents(constant('MAINTENANCE_FILE'), json_encode($file_contents));
                    }
                }
            }
            $this->new_message($message);
        }

        if ($status == false) {
            $this->core_log->save('El wizard ' . $wizard_name . ' ha finalizado correctamente y no ha podido notificarse . ');
        } else {
            $this->core_log->save('El wizard ' . $wizard_name . ' ha finalizado correctamente y se ha notificado . ',
                'advices');
        }
    }

    /**
     * Muestra un mensaje al usuario
     *
     * @param string $msg
     * @param bool   $save
     * @param string $tipo
     * @param bool   $alerta
     */
    public function new_message($msg, $save = false, $tipo = 'message', $alerta = false)
    {
        if ($this->class_name == $this->core_log->controller_name()) {
            /// solamente nos interesa mostrar los mensajes del controlador que inicia todo
            $this->core_log->new_message($msg);
        }

        if ($save) {
            $this->core_log->save($msg, $tipo, $alerta);
        }
    }

    /**
     * Propaga los archivos adjuntos de un documento a otro.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.09
     *
     * @param string $target_entity_type
     * @param int    $target_id
     *
     * @param string $source_entity_type
     * @param int    $source_id
     *
     * @return bool
     */
    public function copia_adjuntos($source_entity_type, $source_id, $target_entity_type, $target_id)
    {
        if (!is_plugin_enabled('archivos_adjuntos')) {
            return true;
        }

        $filename = constant('FS_FOLDER') . '/plugins/archivos_adjuntos/helper/attached_files.php';
        if (file_exists($filename)) {
            require_once $filename;
        }
        if (class_exists('attached_files') && $source_entity_type !== null && $source_id !== null && $target_entity_type !== null && $target_id !== null) {
            return attached_files::copy_attachments($source_entity_type, $source_id, $target_entity_type, $target_id);
        }
        return true;
    }

    /**
     * Ejecuta las consultas correspondientes.
     * Si no pudieran ejecutarse, se hará un rollback.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.10
     *
     * @param string[][] $fixes [
     *                          'table_name_1' => ['query1','query2' ],
     *                          'table_name_2' => ['query1','query2' ],
     *                          ]
     *
     * @return bool
     */
    public function exec_fix_queries($fixes = [])
    {
        $status = true;
        $sqls = [];
        foreach ($fixes as $table => $queries) {
            foreach ($queries as $query) {
                if ($this->db->table_exists($table)) {
                    $sqls[] = $query;
                }
            }
        }

        if (!empty($sqls)) {
            $this->db->begin_transaction();
            $status &= $this->db->exec(implode('', $sqls));
            if (!$this->db->commit()) {
                $this->db->rollback();
            }
        }

        return $status;
    }

    /**
     * Carga el menú
     *
     * @param bool $reload TRUE si quieres recargar
     */
    protected function load_menu($reload = false)
    {
        $this->menu = $this->user->get_menu($reload);
        $this->array_menu = $this->user->get_array_menu($reload);
    }

    /**
     * Función que se ejecuta si el usuario no ha hecho login
     */
    protected function public_core()
    {

    }

    /**
     * Código que se ejecutará en la parte privada, antes de ejecutar el private_core
     */
    protected function pre_private_core()
    {
        $this->query = fs_filter_input_req('query');

        /// quitamos extensiones de páginas a las que el usuario no tenga acceso
        foreach ($this->extensions as $i => $value) {
            if ($value->type != 'config' && !$this->user->have_access_to($value->from)) {
                unset($this->extensions[$i]);
            }
        }
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {

    }

    /**
     * Cargamos el menú en la base de datos, pero en varias pasadas.
     */
    protected function check_menu($dir)
    {
        if (!file_exists($dir)) {
            $this->new_error_msg('No se encuentra el directorio ' . $dir);
            return;
        }

        /// leemos todos los controladores del plugin
        $max = 25;
        $data = fs_file_manager::scan_files($dir, 'php');
        foreach ($data as $f) {
            if ($f == __CLASS__ . '.php') {
                continue;
            }

            /// obtenemos el nombre
            $page_name = substr($f, 0, -4);

            /// lo buscamos en el menú
            $encontrado = false;
            foreach ($this->menu as $m) {
                if ($m->name == $page_name) {
                    $encontrado = true;
                    break;
                }
            }

            if ($encontrado) {
                continue;
            }

            require_once $dir . '/' . $f;
            $new_fsc = new $page_name();
            if (!$new_fsc->page->save()) {
                $this->new_error_msg("No ha sido posible guardar la página " . $page_name);
            }

            unset($new_fsc);

            if ($max > 0) {
                $max--;
            } elseif (!$this->get_errors()) {
                $this->recargar = true;
                $this->new_message('Instalando el menú... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');
                break;
            }
        }

        $this->load_menu(true);
    }

    /**
     * Establece un almacén como predeterminado para este usuario.
     *
     * @param string $cod el código del almacén
     */
    protected function save_codalmacen($cod)
    {
        assign_cookie('default_almacen', $cod);
        $this->default_items->set_codalmacen($cod);
    }

    /**
     * Establece un impuesto (IVA) como predeterminado para este usuario.
     *
     * @param string $cod el código del impuesto
     */
    protected function save_codimpuesto($cod)
    {
        assign_cookie('default_impuesto', $cod);
        $this->default_items->set_codimpuesto($cod);
    }

    /**
     * Establece una forma de pago como predeterminada para este usuario.
     *
     * @param string $cod el código de la forma de pago
     */
    protected function save_codpago($cod)
    {
        assign_cookie('default_formapago', $cod);
        $this->default_items->set_codpago($cod);
    }

    /**
     * Procesa los datos de la página o entrada en el menú
     *
     * @param string $name
     * @param string $title
     * @param string $folder
     * @param bool   $shmenu
     * @param bool   $important
     * @param bool   $favorite
     *
     * @return fs_page
     */
    protected function check_fs_page($name = 'Sin nombre', $title = 'Sin título', $folder = ['Oculto'], $shmenu = false, $important = false, $favorite = false)
    {
        /// ahora debemos comprobar si guardar o no
        if ($name !== 'fs_controller') {
            $page = (new fs_page())->get($name);
            if ($page) {
                /// la página ya existe ¿Actualizamos?
                if ($page->title != $title || $page->folder != $folder || $page->show_on_menu != $shmenu || $page->important != $important) {
                    $page->title = $title;
                    $page->folder = $folder;
                    $page->show_on_menu = $shmenu;
                    $page->favorite = $favorite;
                    $page->important = $important;
                    $page->save();
                }
            } else {
                /// la página no existe, guardamos.
                /// cargamos los datos de la página o entrada del menú actual
                $page = new fs_page([
                    'name' => $name,
                    'title' => $title,
                    'folder' => $folder,
                    'version' => $this->version(),
                    'show_on_menu' => $shmenu,
                    'favorite' => $favorite,
                    'important' => $important,
                    'orden' => 100,
                ]);
                $page->save();
            }
        }

        return $page;
    }

    /**
     * Carga las extensiones para la clase en ejecución.
     */
    private function load_extensions()
    {
        // Obtenemos las páginas a las que tiene acceso el usuario
        $pages = [];
        foreach ($this->menu as $page) {
            $pages[] = $page->name;
        }

        $fsext = new fs_extension();
        foreach ($fsext->all() as $ext) {
            if ($ext->type == 'head') {
                $ext->delete();
            } else {
                /// Cargamos las extensiones para este controlador o para todos, y que además el usuario tenga acceso
                if (
                    in_array($ext->to, [null, $this->class_name,]) &&
                    in_array($ext->from, $pages)
                ) {
                    $this->extensions[] = $ext;
                }
            }
        }
    }

    /**
     * Devuelve TRUE si el usuario realmente tiene acceso a esta página
     *
     * @return bool
     */
    private function log_in()
    {
        $this->login_tools->log_in($this->user);
        if ($this->user->logged_on) {
            $this->core_log->set_user_nick($this->user->nick);
            $this->load_menu();
        }

        // Si existe la nueva tabla de usuarios, actualizamos el log_key
        if ($this->user->logged_on && DBSchema::tableExists('users')) {
            $user = User::getByCode($this->user->nick);
            if ($user) {
                $user->log_key = $this->user->log_key;
                $user->save();
            }
        }

        return $this->user->logged_on;
    }

    /**
     * Establecemos los elementos por defecto, pero no se guardan.
     * Para guardarlos hay que usar las funciones fs_controller::save_lo_que_sea().
     * La clase fs_default_items sólo se usa para indicar valores
     * por defecto a los modelos.
     */
    private function set_default_items()
    {
        /// gestionamos la página de inicio
        if (filter_input(INPUT_GET, 'default_page')) {
            if (filter_input(INPUT_GET, 'default_page') == 'FALSE') {
                $this->default_items->set_default_page(null);
                $this->user->fs_page = null;
            } else {
                $this->default_items->set_default_page($this->page->name);
                $this->user->fs_page = $this->page->name;
            }

            $this->user->save();
        } elseif (is_null($this->default_items->default_page())) {
            $this->default_items->set_default_page($this->user->fs_page);
        }

        if (is_null($this->default_items->showing_page())) {
            $this->default_items->set_showing_page($this->page->name);
        }

        $this->default_items->set_codejercicio($this->empresa->codejercicio);

        if (filter_input(INPUT_COOKIE, 'default_almacen')) {
            $this->default_items->set_codalmacen(filter_input(INPUT_COOKIE, 'default_almacen'));
        } else {
            $this->default_items->set_codalmacen($this->empresa->codalmacen);
        }

        if (filter_input(INPUT_COOKIE, 'default_formapago')) {
            $this->default_items->set_codpago(filter_input(INPUT_COOKIE, 'default_formapago'));
        } else {
            $this->default_items->set_codpago($this->empresa->codpago);
        }

        if (is_plugin_enabled('facturacion_base') && $this->empresa->codimpuesto == null) {
            $this->new_advice('No tienes un impuesto por defecto asignado, puedes dirigirte a <a href="index.php?page=admin_empresa#facturacion" target="_blank">los ajustes de facturación</a> y asignarselo.');
        }
        if (is_plugin_enabled('facturacion_base') && $this->empresa->codimpuesto_sinimpuestos == null) {
            $this->new_advice('No tienes un impuesto por defecto asignado para los exentos, puedes dirigirte a <a href="index.php?page=admin_empresa#facturacion" target="_blank">los ajustes de facturación</a> y asignarselo.');
        }

        $this->default_items->set_codimpuesto($this->empresa->codimpuesto);
        $this->default_items->set_codpais($this->empresa->codpais);
        $this->default_items->set_codserie($this->empresa->codserie);
        $this->default_items->set_coddivisa($this->empresa->coddivisa);
    }

    /**
     * Devuelve el nombre del controlador.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0426
     *
     * @return string
     */
    public function get_class_name()
    {
        return $this->class_name;
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-unknown fa-fw"></i>';
    }

    /**
     * Ejecuta una acción dependiente de una extensión.
     * La extensión invoca si está incluído el trait, y en ese caso, el método invocado estará disponible en el propio trait.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0627
     *
     * @param $extension
     * @param $action
     *
     * @return false
     */
    public function do_extension_action($extension, $action)
    {
        $action_method = $extension . 'Action';
        if (method_exists($this, $action_method)) {
            return $this->{$action_method}($action);
        }
        $this->new_error_msg('Extensión ' . $extension . ' no soportada');
        return false;
    }

    /**
     * Devuelve el texto truncado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.06
     *
     * @param string $text
     * @param int    $len
     *
     * @return string
     *
     */
    public function truncate_text($text = '', $len = 50)
    {
        return truncate_text($text, $len);
    }

    public function getMenu($reload = false)
    {
        return $this->user->get_menu($reload);
    }

    public function getArrayMenu($reload = false)
    {
        return $this->user->get_array_menu($reload);
    }

    public function getUserName()
    {
        return $this->user->nick ?? 'Usuario';
    }

    public function getUserUrl()
    {
        return $this->user->url();
    }

    public function getLogoutUrl()
    {
        return $this->url() . '&logout=TRUE';
    }

}
