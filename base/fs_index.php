<?php

use Xnet\Controllers\DatabaseUpdate;
use Xnet\Core\Blade;
use Xnet\Core\DB;

// Forzamos el acceso mediante HTTPS
if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit;
}

/**
 * TODO: El problema de ésto, es que pierde el POST que pudiese haber hecho.
 * TODO: La pantalla debe de tener un botón que permita eliminar el fichero si se es administrador
 * TODO: Copiar a XFS
 */

if (file_exists(constant('MAINTENANCE_FILE'))
    || file_exists(constant('UPDATING_DATABASE_FILE'))
    || (file_exists(constant('DATABASE_UPDATE_FLAG')) && !isset($_GET['updated']))) {
    require_once('view/in_maintenance.html');
    die();
}

// Inicializa la base de datos y se conecta
new DB();
if (!DB::connect()) {
    debug_message('Se ha producido un error al tratar de conectarse a la base de datos usando la clase DB');
}

// Actualiza la base de datos, si se ha realizado una actualización
if (file_exists(constant('DATABASE_UPDATE_FLAG'))) {
    $controller = new DatabaseUpdate();
    die('Fin de la actualización');
    // TODO: Quedaría pendiente ver qué hacer con los Wizard.
    unlink(constant('DATABASE_UPDATE_FLAG'));
}

// TODO: Copiar a XFS hasta aquí, y todo lo que haya usado retocando lo necesario.

// ¿Qué controlador usar?
$pagename = '';
if (filter_input(INPUT_GET, 'page')) {
    $pagename = trim(filter_input(INPUT_GET, 'page'));
} elseif (defined('FS_HOMEPAGE')) {
    $pagename = constant('FS_HOMEPAGE');
} else {
    $pagename = 'dashboard';
}

if (empty($pagename)) {
    die('FS_HOMEPAGE no definida!');
}

loadPluginsConfig2();

$route_data = find_controller_and_path($pagename);
$class_path = $route_data['controller'];
if (file_exists($class_path)) {
    require_once $class_path;
}

// ¿Nuevo sistema de controladores?
$withNamespace = ($route_data['namespace'] ?? '') . $pagename;
try {
    $fsc = null;
    if (isset($route_data['path'])) {
        $fsc = new $withNamespace();
    }
} catch (Exception $exc) {
    if (constant('FS_DEBUG')) {
        debug_message('Error al tratar de instanciar ' . $withNamespace);
        debug_message($exc);
        debug_message($route_data);
        // TODO: No pinta en la barra de depuración, lo que hay arriba.
        dump($exc);
    }
}

/**
 * Temporalmente, mientras que se desarrolla, para tratar de impedir afectar
 * a los clientes durante la migración, sólo se activará el nuevo núcleo, si
 * se define la variable NEW_CORE con true.
 */
if (defined('NEW_CORE') && constant('NEW_CORE')) {
    if (isset($route_data['namespace']) && is_subclass_of($fsc, 'Xnet\Core\XnetAppController', false)) {
        try {
            $fsc->setPath($route_data['path']);
            echo $fsc->view();
            // TODO: Hay que mejorar el tratamiento de errores.
        } catch (PDOException $e) {
            dump($e);
        } catch (Exception $exc) {
            echo "<h1>Error fatal</h1>"
                . "<ul>"
                . "<li><b>Código:</b> " . $exc->getCode() . "</li>"
                . "<li><b>Archivo:</b> " . $exc->getFile() . "</li>"
                . "<li><b>Línea:</b> " . $exc->getLine() . "</li>"
                . "<li><b>Mensage:</b> " . $exc->getMessage() . "</li>"
                . "</ul>";
            dump(debug_backtrace());
        }
        die();
    }
}

if (!isset($fsc) || !is_subclass_of($fsc, 'fs_controller', false)) {
    $fsc = new fs_controller();
}

// TODO: Sistema obsoleto de MiFactura a extinguir
try {
    if (constant('BASE_PATH') . '/base/fs_controller.php' === $class_path) {
        header("HTTP/1.0 404 Not Found");
        $fsc = new fs_controller();
        $fsc->set_path($route_data['path']);
    }
} catch (PDOException $e) {
    dump($e);
    die();
} catch (Exception $exc) {
    echo "<h1>Error fatal</h1>"
        . "<ul>"
        . "<li><b>Código:</b> " . $exc->getCode() . "</li>"
        . "<li><b>Archivo:</b> " . $exc->getFile() . "</li>"
        . "<li><b>Línea:</b> " . $exc->getLine() . "</li>"
        . "<li><b>Mensage:</b> " . $exc->getMessage() . "</li>"
        . "</ul>";
    dump(debug_backtrace());
    die();
}

/// guardamos los errores en el log
$log_manager = new fs_log_manager();
$log_manager->save();

/// redireccionamos a la página definida por el usuario
if (is_null(filter_input(INPUT_GET, 'page'))) {
    $fsc->select_default_page();
}

$engine = mb_strtolower(defined('ENGINE') ? constant('ENGINE') : 'raintpl');

if ($fsc->template) {
    /// ¿Se puede escribir sobre la carpeta temporal?
    if (!is_writable(constant('TMP_FOLDER'))) {
        die('<h1>No se puede escribir sobre la carpeta ' . constant('TMP_FOLDER') . ' de MiFactura.eu</h1>');
    }

    $vars = [];
    $vars['fsc'] = $fsc;

    // Cambio para que conserve los parámetros enviados por GET
    unset($_GET['nlogin']);
    $nurl = '';
    foreach ($_GET as $key => $value) {
        $nurl .= "&$key=$value";
    }
    // Los pasa en la variable $nurl
    $vars['nurl'] = $nurl;

    $xid = isset($fsc->empresa) ? $fsc->empresa->get_xid() : null;
    if (filter_input(INPUT_POST, 'user')) {
        $vars['nlogin'] = filter_input(INPUT_POST, 'user');
    } elseif (filter_input(INPUT_COOKIE, 'user_' . $xid)) {
        $vars['nlogin'] = filter_input(INPUT_COOKIE, 'user_' . $xid);
    } else {
        $vars['nlogin'] = '';
    }

    switch ($engine) {
        case 'blade':
            $blade = new Blade(Blade::getRoutes(), constant('TMP_FOLDER') . '/' . constant('FS_TMP_NAME'));
            if (method_exists($fsc, 'get_db_history')) {
                $fsc->get_db_history();
            }
            try {
                echo $blade->render($fsc->template, $vars);
            } catch (Exception $e) {
                if (constant('FS_DEBUG')) {
                    $fsc->new_error_msg($e->getMessage());
                    dump($e);
                } else {
                    $fsc->template = 'errors/coming-soon';
                }
                echo $blade->render($fsc->template, $vars);
            }
            break;
        case 'raintpl':
            /// configuramos rain.tpl
            raintpl::configure('base_url', null);
            raintpl::configure('tpl_dir', 'view/');
            raintpl::configure('path_replace', false);
            raintpl::configure('cache_dir', constant('TMP_FOLDER') . '/' . constant('FS_TMP_NAME'));

            $tpl = new RainTPL();

            foreach ($vars as $name => $var) {
                $tpl->assign($name, $var);
            }

            $tpl->draw($fsc->template);
            break;
        case 'twig':
        case 'Twig':
            /*
            $vars = [
                'template' => $fsc->template,
                'this' => $fsc,
                'nlogin' => $nlogin,
                'nurl' => $nurl,
            ];
            Skin::setDebugBar($fsc->debugBar);
            Skin::setTemplatesEngine('twig');
            //Skin::setTemplatesFolder('sbadmin2');
            // $page=Skin::render($vars);
            // trigger_error('<pre>'.$page.'</pre>');
            // die('here in index');

            echo Skin::render($vars);
            break;
            */
        default:
            dump("Engine '$engine' not found!");
    }
}

/// guardamos los errores en el log (los producidos durante la carga del template)
$log_manager->save();

/// cerramos las conexiones
if (isset($fsc)) {
    $fsc->close();
}
