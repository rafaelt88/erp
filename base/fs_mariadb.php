<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_mysql_type.php';

/**
 * Clase para conectar a MariaDB.
 * Atención: no es 100% identica a MySQL.
 */
class fs_mariadb extends fs_mysql_type
{
    /**
     * Devuelve el motor de base de datos y la versión.
     *
     * @return string
     */
    public function version()
    {
        return self::$link ? 'MariaDB ' . self::$link->server_version : false;
    }

    /**
     * Solo haremos el ADD, si no existe en la base de datos o si existe en la tabla, y no es igual que en el XML
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.11
     *
     * @param string $table_name
     * @param array  $db_con
     * @param array  $xml_con
     * @param string $sql
     *
     * @return string
     */
    protected function getSqlAlterConstraint(string $table_name, array $db_con, array $xml_con, string $sql): string
    {
        if (constant('FS_DEBUG')) {
            $data = [
                'function' => __FUNCTION__,
                'table_name' => $table_name,
                'db_con' => $db_con,
                'xml_con' => $xml_con,
            ];
            debug_message($data);
        }

        if (empty($db_con) || !$this->exists_index($table_name, $db_con['name'])) {
            $sql .= 'ALTER TABLE `' . $table_name . '` ADD CONSTRAINT `' . $xml_con['nombre'] . '` ' . $xml_con['consulta'] . ';';
            $this->delete_cached_constraints($table_name);
        }
        return $sql;
    }
}
