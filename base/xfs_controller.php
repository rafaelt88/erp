<?php

/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

require_once constant('BASE_PATH') . '/base/fs_controller.php';

/**
 * Class xfs_controller
 *
 * Ante la inconveniencia de modificar los archivos origianles de FS, se procede a extender
 * la clase fs_controller como xfs_controller, con idea de poder realizar adaptaciones de los
 * controladores para el uso particular de los plugins de XFS.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2021.04
 */
abstract class xfs_controller extends fs_controller
{
    /**
     * TRUE si el usuario tiene permisos para eliminar en la página.
     *
     * @var bool
     */
    public $allow_delete;

    /**
     * Contiene un array con los datos obtenidos de la consulta SQL.
     *
     * @var array;
     */
    public $resultados;

    /**
     * Retorna el nombre de la clase del controlador.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0510
     *
     * @return string
     */
    public function get_class_name()
    {
        return $this->class_name;
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        /// ¿El usuario tiene permiso para eliminar en esta página?
        $this->allow_delete = $this->user->allow_delete_on($this->class_name);

        if (constant('FS_DEBUG') && !isset($this->ajax_url)) {
            die('Se necesita especificar la ruta de ajax_url antes de invocar a private_core en ' . $this->class_name);
        }

        /// ¿Hay más de un almacén?
        // $fsvar = new fs_var();
        // $this->multi_almacen = (bool) $fsvar->simple_get('multi_almacen');

        $this->resultados = [];
    }

    /**
     * TODO: Éste método debería de estar en fs_controller.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0316
     *
     * @return bool|void
     */
    public function ajax_requests()
    {
        if (!isset($this->tab_extensions)) {
            return false;
        }

        foreach ($this->tab_extensions as $extension) {
            $field_name = 'ajaxData';
            $method_name = $extension . 'Ajax';
            if (isset($_REQUEST['extension']) && $_REQUEST['extension'] === $extension && isset($_REQUEST[$field_name])) {
                $this->{$method_name}($_REQUEST[$field_name]);
                return true;
            }
        }
    }

    public function do_action($action)
    {
        return false;
    }
}
