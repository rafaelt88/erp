# Facturación

El proceso de facturación, consiste en agrupar varios albaranes de la misma fábrica y fecha de vencimento, para generar
una factura con el total de comisiones de la misma.

> Para poder generar la factura, la fábrica tiene que estar asociada a un cliente del ERP que será a quién se facturará.

## Proceso de facturación

En primer lugar, habrá que seleccionar la fábrica en los filtros de búsqueda, y si se desea, también el cliente.

A continuación, se pulsa en **Aplicar filtros**, y ya podremos seleccionar en **Revisar facturas**.

## Revisión de los albaranes.

Nos aparecerá una lista con los albaranes que cumplen con el criterio seleccionado.

En la lista, se podrán seleccionar aquellos albaranes que van a incluirse en la factura. Conforme los vamos 
seleccionando, podemos comprobar la comisión y facturación acumulada. Una vez seleccionados todos, si existe algún
pequeño descuadre, se puede solucionar con tan sólo indicar el importe real de la factura y la comisión: La factura
incluirá una línea de regularización para corregir ese desfase y dejarlo reflejado.

En los datos de la factura, nos aparecerá la Fábrica, la fecha de vencimiento, las comisiones que se han generado y
el importe neto total de los albaranes. Si fuese necesario, los 3 datos podrían ser modificados en el apartado
*Introduzca los datos de la factura*, para hacer una regularización de los datos calculados con los facturados.
