# Clientes finales

Al acceder a la opción de *clientes finales*, tendremos un listado con todas los clientes finales existentes, dónde 
podremos ver y filtrar para posteriormente poder editarlos.

Además de la pestaña *Clientes*, dónde se puede ver dicho listado, existe otra pestaña llamada *Grupos*, que nos permite
llevar un mantenimiento de **Grupos de clientes finales**, que en un principio son sólo un criterio de clasificación,
para poder agrupar a los clientes que pertenezcan al mismo grupo.

Para crear un nuevo cliente final, basta con pulsar al botón **Nuevo**.

## Alta de un nuevo Cliente

Desde el apartado de **Clientes**, pulsando el botón **Nuevo**, aparece un formulario dónde se pueden introducir los
datos principales del Cliente y una dirección que se considerará la dirección principal.

Si se necesitan cumplimentar datos adicionales, bastará con editarlo como se explica en el siguiente apartado.

## Edición de un Cliente existente

Al seleccionar un Cliente, se pueden modificar la mayoría de sus datos, además quedan disponibles los siguientes 
botones de acción:
- Pulsar en el botón **<- Clientes** para retornar al listado de Clientes. Se se ha realizado algún cambio, será
necesario pulsar antes en guardar para no perder dichos cambios.
- Existe un botón **Dar de baja**, que permite marcar al Cliente como *de baja*; o bien, **Editar baja** si ya está de 
baja, y queremos reestablecerla.

Al dar de baja a un Cliente, desaparecerá de las búsquedas pero no del listado, para poder recuperarlo si fuese 
necesario. La baja no es inmediata, sino que aparece una pantalla emergente donde hay que marcar el *check*
para darlo de baja, o desmarcarlo para recuperarlo.

### Pestaña *Datos*

En la pestaña de datos, tenemos la información general del Cliente, como es el nombre, el CIF, los datos de contacto
(teléfonos, fax y email) o el número de socio.

### Pestaña *Direcciones*

Por defecto, al dar de alta el Cliente, se crea una dirección **Principal**. En esta pestaña, podemos asociar nuevas 
direcciones al Cliente, entre ellas, a las que servir mercancía.

De haber más de una dirección, las tarifas pueden ser diferentes para cada una de ella, y además, al hacer un pedido, 
habrá que indicar a qué dirección se hace.

### Pestaña *Bancos*

La pestaña de bancos, permite definir números de cuentas bancarias asociadas al Cliente.

El dato es meramente informativo y pueden aparecer en el pedido si fuese necesario.

### Pestaña *Contactos*

Podemos tener varios contactos del cliente con sus datos de contacto (dirección, cargo, 
teléfono, email, etc).

### Pestaña *Tarifas*

La pestaña de tarifas, nos permite añadir distintas tarifas del Cliente por cada fábrica y dirección.

En primer lugar, nos preguntará por el cliente y la dirección sobre la que queremos cargar una
tarifa. Una vez seleccionada la fábrica y dirección, el funcionamiento es similiar al de tarifa
de fábrica.

Aparecerá un botón **Cambiar de fábrica**, que nos devolverá a la pantalla de selección anterior.

Una vez seleccionada la fábrica y su dirección, se podrá:

- Seleccionar uno de los periodos disponibles que irá de fecha a fecha (o de fecha a indefinida, si no se ha 
especificado una fecha de finalización de la tarifa). Una vez marcado el periodo deseado, habrá que pulsar en el botón
**Seleccionar**.
- Sobre la tarifa seleccionada, se podrá pulsar en el botón **Importar** para cargar la tarifa de productos de la 
fábrica, que deberá de estar en formato CSV, separado por punto y coma (;).
- Se podrá crear en cualquier momento una nueva tarifa, pulsando en el botón **Nueva**. Al crear una nueva tarifa, se
deberá de seleccionar la fecha de comienzo, y opcionalmente la fecha de finalización. Si no se especifica una fecha de
finalización, se asumirá que es hasta nueva tarifa (indefinida).

Hay que tener las siguientes consideraciones:

- Una tarifa no puede solaparse con otra. Si se crea una tarifa cuya fecha coincida con las de otras, la nueva 
sustituirá a las existentes hasta el día antes de la anterior y el día siguiente del de finalización.
- Si la última tarifa no tiene fecha de finalización, al abrir una posterior, la última será cerrada el día antes a la 
fecha de inicio de la nueva.
- El archivo CSV tendrá varias columnas con los siguientes títulos:
  - referencia, que detalla las referencias de los productos, según la fábrica (ha de ser única en la tarifa).
  - nombre, es el nombre del producto.
  - comisión (opcional), indica la comisión que aporta la venta del producto. De dejarse en blanco, se toma la comisión 
  de la tarifa o la página de la fábrica.
  - descuento1 a descuento3 (opcionales), son los posibles descuentos en línea del producto. Si se dejan en blanco, se
  asumen los descuentos indicados en la tarifa o en la ficha de la fábrica.
