# Pedidos a fábrica

Al acceder a la opción de *pedidos*, tendremos un listado con todos los pedidos pendientes. Se puede cambiar la lista
de pedidos mostrados, seleccionando una fábrica, un cliente o un filtro de estado, y pulsando en el botón
**Aplicar filtros**.

Para dar de alta un nuevo pedido, bastará con pulsar en el botón **Nuevo**. Los detalles del proceso de alta están en un
apartado más adelante.

## Filtros de estado

Los filtros de estado posible son los siguientes:

- **Pedidos pendientes** nos mostraría, aquellos que tienen mercancía pendiente de servir (total o parcialmente), y que
no hayan sido cancelados por el usuario. No se mostrarían los presupuestos (*proformas*).
> **Revisar**: Es posible que sea recomendable, mostrar también las proformas no canceladas.
- **Todos los presupuestos** nos mostraría todos los pedidos que tengan marcado el check de proforma, independientemente
de su estado.
- **Todos los pedidos** nos mostraría todos los pedidos que no tengan marcado el check de proforma, independientemente
de su estado.
- **Todo** nos mostraría todos los presupuestos (proformas) y pedidos independientemente de su estado.

## La tabla de pedidos

En la tabla de pedidos se nos muestra la siguiente información:

- Los **identificadores** del documento que son:
  - El ID (de uso interno), que es un número autoincrementado.
  - El tipo de documento, si es un Presupuesto o un Pedido)
  - El número asignado por la aplicación en formato PEE-NNNNN, siendo:
    - EE el ejercicio, o sea, los dos últimos dígitos del año.
    - NNNNN un número autogenerado con el orden de creación dentro del ejercicio.
  - El número de documento asociado por el cliente (si lo tuviésemos)
  - El número de documento del pedido de fábrica.
- La **fecha y hora** de creación del documento.
- La **fábrica** y el **cliente** asociados al documento.
- El importe de la **comisión total del pedido**, en el caso en el que sea servido por completo.
- El importe **neto** del pedido.
- El importe neto de la mercancía ya **servida**.
- El porcentaje del importe de la mercancía servida respecto al total.
- El **estado** del pedido.

Sobre dicha tabla, podremos realizar una búsqueda, o exportarla a una tabla Excel. También podremos ordenarla pinchando
en los títulos de las columnas.

Pinchando sobre una fila, vamos directos a la **edición del pedido**. Las opciones de edición son las mismas que para el
alta de un nuevo pedido, con la salvedad de que no es necesario seleccionar fábrica y cliente final

## Alta de un nuevo Pedido

Pulsando el botón **Nuevo** desde el listado de **Pedidos**, accederemos a una pantalla que nos permitirá introducir
los datos de la fábrica y el cliente final que ha hecho el pedido. Si la fábrica o el cliente sólo tienen una dirección,
se asignará ésta automáticamente al ser seleccionado.

Existe un check que permitirá asociar directamente un cliente a una fábrica si dicha asociación, no existe en ese 
momento. Si ya existe, aparecerá marcado por defecto. Si no está marcado, no se podrá continuar.

Una vez seleccionados fábrica y cliente, y marcado el check, se podrá pulsar en **Seleccionar** para generar un nuevo 
pedido vacío, y el proceso será similar a la edición que se explica en el siguiente apartado.

Si tras haber hecho la selección, comprobamos que hay un error, podemos pulsar en **Borrar campos** para limpiar la
selección de fábrica y cliente final, antes de pulsar en seleccionar.

> Una vez seleccionado fábrica y cliente y pulsado en Seleccionar, el nuevo pedido es generado, y esos datos ya no se
pueden cambiar, ya que las tarifas están asociadas a una fábrica y, posiblemente al cliente, por lo que los artículos
que pudiese haber seleccionados, podrían no existir en la otra fábrica, o bien, no tener la misma referencia.

## Edición de un pedido

La edición de pedido, como ocurre con otros editores de documentos, tiene varias zonas definidas.

- Botones de acción:
  - Pulsando en **<- Pedidos** se retorna al listado de pedidos, descartando los cambios.
  - Pulsando en **Imprimir** se saca el documento de pedido de fábrica al cliente.
  - Pulsando en **Albaranar** se crea un albarán con toda la mercancía pendiente de recibir.
    - Posteriormente, podremos acceder al albarán para modificar las cantidades servidas. La cantidad no servida, volverá 
      al pedido que seguirá estando como pendiente. Si no hay nada pendiente se considerará como finalizado.
    - Un pedido pendiente puede cancelarse manualmente pulsando en el botón **Cerrar pedido**
  - El botón **Duplicar** abriría un modal, que nos permitiría sacar copias del pedido en curso, **en el mismo estado en
  el que fue guardado por última vez**. Las copias, no conservar el número de fábrica ni del cliente, ni las cantidades 
  servidas, y opcionalmente, tampoco el cliente.
  - El botón **Guardar** almacena los cambios que han sido realizados en el pedido.
- Cabecera del documento:
  - Campos no editables como son los datos de la fábrica y cliente (en el caso de cliente, editable una vez si se ha 
    duplicado para otros clientes), el número de documento y la fecha y hora de creación.
  - Un check para convertir el pedido en proforma y establecer la fecha del pedido.
  - Un número para poner el código de la fábrica y otro para el código del cliente final.
  - Un empleado asociado (que puede ser diferente al de creación).
  - Selección de tarifas y ofertas aplicables al pedido.
- Líneas del documento:
  - Lo normal es añadir líneas poniendo parte del código o del nombre en el campo que pone *Escriba aquí para buscar 
    artículos y añadirlos...*, y cuando aparezca en la selección, marcándolo con el ratón, o con las flechas de cursor
    arriba y abajo, y pulsando enter una vez seleccionado.
  - Una vez añadida la línea, puede cambiarse la cantidad, precio, etc.
  - Pulsando en **Nuevo**, se puede añadir un artículo sin referencia o para una nueva referencia, que se añadirá al 
    catálogo de la fábrica.

> Hay que tener en cuenta que antes de pulsar cualquier botón de acción (Imprimir, Albaranar, Recalcular, etc), es 
necesario pulsar en **Guardar** ya que de otro modo, la acción se hará sobre los últimos datos que fueron guardados.

Cuando un pedido ha sido *albaranado*, el albarán generado aparecerá en una tabla llamada **Documentos relacionados**, 
junto con el resto de albaranes dependientes del pedido en curso. Pinchando en cualquier albarán, se abandonará la
edición, e irá a dicho albarán.

## Duplicados del pedido

Una vez guardados los cambios, se pueden sacar **duplicados del pedido** pulsando en el botón **Duplicar**: 
- Nos pedirá el número de copias del documento actual.
    - Además del número de copias, se puede forzar a que los pedidos generados sean consecutivos marcando el check
      **Forzar consecutivos**. En este caso, los números de documento serán consecutivos, pudiéndo quedar
      algún hueco en la numeración.
    - Por defecto, el documento generado es para la misma fábrica y el mismo cliente, marcando el check **Duplicados
      para otros clientes**, se deja en blanco el cliente final para poder seleccionar uno distinto al entrar en la
      edición del pedido.
