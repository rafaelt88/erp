# Trading
Este plugin permite el mantenimiento de fábricas y clientes de esas fábricas, para gestionar los pedidos que los 
clientes hacen a las fábricas, y generar facturas con las comisiones cobradas por la gestión.

Cada fábrica dispone de un catálogo de productos con precio de venta y hasta 3 descuentos acumulables. Se ha diseñado un
gestor de **tarifas** y de **ofertas** aplicables a cada pedido.
