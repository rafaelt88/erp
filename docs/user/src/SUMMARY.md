# Summary

- [Chapter 1](./chapter_1.md)

- [Trading](./plugins/trading/inicio.md)
    - [Fábricas](./plugins/trading/fabrica_fabricas.md) 
    - [Clientes finales](./plugins/trading/fabrica_clientes.md) 
    - [Relación fábrica-cliente](./plugins/trading/fabrica_cliente_articulo.md) 
    - [Formas de pago](./plugins/trading/list_fabrica_forma_pago.md) 
    - [Artículos](./plugins/trading/fabrica_articulos.md) 
    - [Pedidos a fábrica](./plugins/trading/fabrica_pedidos.md) 
    - [Albaranes de fábrica](./plugins/trading/fabrica_albaranes.md) 
    - [Facturación](./plugins/trading/fabrica_facturar.md)
