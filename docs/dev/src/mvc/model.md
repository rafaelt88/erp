# Modelos

Todos los modelos heredan de las clases fs_model o fs_extended_model, que son las que se encargan de comprobar las
correspondientes tablas y hacer los ajustes que sean oportunos.

- **[fs_model](../../classes/fs_model.html)**: La clase de la que heredan todos los modelos, conecta a la base de datos,
  comprueba la estructura de la tabla y de ser necesario la crea o adapta.
- **[fs_extended_model](../../classes/fs_extended_model.html)**: La clase de la que heredan todos los modelos que a su
  vez están implicados con un fs_list_controller y/o fs_edit_controller.

La principal diferencia entre fs_model y fs_extended_model, es que esta última requiere un par de funciones adicionales
para poder funcionar con fs_list_controller y fs_edit_controller, además de tener implementado código necesario para
estos últimos en la versión extendida, como por ejemplo el guardado/inserción automático de cada uno de los campos para
evitar su re-definición particular para cada uno de los modelos.

Una parte común indistintamente de quien se extienda, es que la clase y el archivo deben llamarse igual.

Para que el modelo se encargue de crear la tabla, esta clase debe ser instanciada. Hasta que no se instancie, no se
verificará ni creará su tabla.

De forma adicional, si un modelo depende de otro, se complementó su funcionamiento mediante el método **
check_model_dependencies()** que permite realizar comprobaciones adicionales instanciando a otros modelos que se
requieran antes de instanciar a este.

## Herencia de modelos

Los modelos de los principales plugins ya permiten herencia de forma muy sencilla. Si por ejemplo deseas extender el
modelo presupuesto_cliente, copia el archivo plugins/facturacion_base/model/presupuesto_cliente.php a tu plugin. Este
archivo ya realiza la herencia al de la carpeta core, así que simplemente debes re-implementar la función o funciones
que desees.

```
<?php

require_once 'plugins/facturacion_base/model/core/presupuesto_cliente.php';

class presupuesto_cliente extends FacturaScripts\model\presupuesto_cliente
{
   /// modifica o añade las propiedades o funciones que desees.
}
```

## Modelo extendiendo de fs_extended_model

Por ejemplo para crear un modelo extiendo de esta clase, y tenemos 2 campos ID y nombre. Su modelo sería algo como esto:

```
<?php

class tipo_dato extends fs_extended_model
{
    /**
     * Clave primaria.
     *
     * @var int
     */
    public $id;

    /**
     * Atributo nombre.
     *
     * @var string
     */
    public $nombre;

    /**
     * tipo_dato constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = FALSE)
    {
        parent::__construct('tipos_datos', $data);
    }

    /**
     * Retorna el nombre de la clase modelo.
     *
     * @return string
     */
    public function model_class_name()
    {
        return __CLASS__;
    }

    /**
     * Devuelve la clave primaria de la tabla.
     *
     * @return string
     */
    public function primary_column()
    {
        return 'id';
    }
}
```

Por tanto, vemos que mediante la herencia, ya se implementan las funciones básicas para leer, escribir y eliminar datos,
entre algún método adicional más.

Mediante la pestaña Structure en PhpStorm podemos ver más detalles sobre las propiedades y funciones, así como que de
cuál clase se están obteniendo.

## Modelo extendiendo de fs_model

```
Pendiente escribir
```

## Opciones comunes con modelos

### Creación de un registro

```
$tipo_dato = new tipo_dato();
$tipo_dato->nombre = 'Nombre dato';
$tipo_dato->save();
```

### Cargar un registro

```
$tipo_dato = new tipo_dato();
if( $tipo_dato->load_from_code($id) ) {
    // Hacemos lo que necesitemos
}
```

### Modificación de un registro

```
    // Modificamos los atributos del modelo
    $tipo_dato->nombre = 'Esto es un test';

    // Guardamos los cambios (a nivel SQL se hará un insert o un update en base a su propia definición)
    $tipo_dato->save();
```

### Eliminación de un registro

```
$tipo_dato = new tipo_dato();
if( $tipo_dato->load_from_code($id) ) {
    if($tipo_dato->delete()) {
        // Se ha eliminado el registro
    } else {
        // No se ha podido eliminar el registro
    }
} else {
    // No se ha encontrado el registro
}
```

# Estructura de la tabla

La estructura se define en la carpeta **model/table** para generar y adaptar la estructura de las tablas.
Estos archivos son XML y tienen el siguiente estilo:

## Archivo XML

```
<?xml version='1.0' encoding='UTF-8'?>
<tabla>
   <columna>
      <nombre>id</nombre>
      <tipo>serial</tipo>
      <nulo>NO</nulo>
      <defecto>nextval('tipos_datos_id_seq'::regclass)</defecto>
   </columna>
   <columna>
      <nombre>nombre</nombre>
      <tipo>character varying(100)</tipo>
      <nulo>YES</nulo>
   </columna>
   <restriccion>
      <nombre>tipos_datos_pkey</nombre>
      <consulta>PRIMARY KEY (id)</consulta>
   </restriccion>
</tabla>
```
