# Controladores

Actualmente disponemos de varios controladores básicos en el núcleo:

- **[fs_controller](../../classes/fs_controller.html)**: Es la clase principal de la que deben heredar todos los
  controladores. Desde este controlador se dispone de total libertad para realizar cosas a medida.
- **[fs_list_controller](../../classes/fs_list_controller.html)**: Controlador específico para listados de datos, hereda
  de fs_controller, pero se enfoca en realizar una funcionalidad de forma sencilla y rápida.
- **[fs_edit_controller](../../classes/fs_edit_controller.html)**: Controlador específico para manipular un modelo
  simple concreto, hereda de fs_controller, pero se enfoca en realizar una funcionalidad de forma sencilla y rápida.
- **[fs_license_controller](../../classes/fs_license_controller.html)**: Controlador específico para gestionar las
  licencias en los plugins, de modo que podamos si es necesario centralizar ciertas operaciones particulares.

De forma adicional, estos controladores actualmente tienen las funcionalidades que tienen, pero nada impide que estos
controladores puedan ampliarse y/o complementarse con funcionalidades específicas. Por ejemplo, empezamos a ver
necesarios:

- Controladores genéricos para ser usados con DataTables para aprovechar todo su potencial vía AJAX.
- Controlador genérico para hacer búsquedas masivas para todos los modelos y listar sus resultados, la búsqueda podría
  solicitarse vía AJAX para cada uno de los modelos disponibles e ir mostrando los resultados en su correspondiente
  capa.

Solo por mencionarlos, algunos específicos en plugins:

- plugins/facturacion_base: [fbase_controller](../../classes/fbase_controller.html) TODO
- plugins/ayuda_soporte: [ayuda_soporte_controller](../../classes/ayuda_soporte_controller.html) TODO
- plugins/importador_datos: [migracion_controller](../../classes/migracion_controller.html) TODO
- plugins/distribucion: [distribucion_controller](../../classes/distribucion_controller.html) TODO
- plugins/fsdk: [fs_standard_controller](../../classes/fs_standard_controller.html) TODO
- plugins/nomina: [nomina_controller](../../classes/nomina_controller.html) TODO
- plugins/reservas: [reserva_controller](../../classes/reserva_controller.html) TODO
- plugins/residentes: [residentes_controller](../../classes/residentes_controller.html) TODO
- plugins/xnetcommonhelpers: [xfs_controller](../../classes/xfs_controller.html) TODO
