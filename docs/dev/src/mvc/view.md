# Vistas

Los detalles sobre como funcionan las vistas en Blade, se encuentran en su documentación específica enlazada desde la
correspondiente sección.

Actualmente todas las vistas extienden de **Templates/layouts/main.blade.php** quedando una plantilla base por página
parecida a la siguiente:

```
@extends('layouts/main')

@section('styles')
    @parent
    ...
@endsection

@section('javascripts')
    @parent
    ...
@endsection

@section('main-content')
    ...
@endsection
```

Donde las secciones de styles y javascripts son opcionales solo si hacen falta, y además pueden derivarse a una vista
específica en Templates/block/javascripts/mi_pagina.blade.php o Templates/block/style/mi_pagina.blade.php para separar
el código en porciones más legibles, y que serán cargadas automáticamente solo en caso de existir.

La sección **main-content** es la imprescindible para definir el contenido principal de la propia página.

A través de la propia herencia de plantillas, la plantilla **layouts/main** define que la página tiene una cabecera y un
pie de página.

Como todo controlador tiene asociado por defecto una plantilla con su mismo nombre (salvando determinadas excepciones
que deberían unificarse)
o con plantillas alternativas indicadas desde el propio controlador mediante la propiedad template, esto permite tener
comportamientos bastante más particulares en caso de que fuera necesario.

Aunque lo ideal para facilitar su mantenimiento es asociar 1 controlador a 1 vista, y en caso de ser necesario generar
sub-vistas o bloques para dividir, asociados a ese controlador.

Algunas de las excepciones indicadas anteriormente son los controladores que extienden de fs_list_controller y
fs_edit_controller, que parten de una vista específica, pero que podrían dejar de ser excepciones si se añadiera que en
caso de existir una vista concreta con el nombre del controlador se cargara esa y en su defecto la actual genérica. De
modo que permitiría una relativa personalización partiendo del mismo concepto.