# Definición de modelos

Se ha estado revisando la definición de modelos (*sólo probado en MySQL/MariaDB*).

Para empezar, se ha pasado la clase xfs_model al núcleo para evolucionar a partir de ella.

Se han añadido algunas mejoras simples a la creación de tablas, entre ellas, un nuevo tipo de índice más lógico y facilidades para que otras tablas se refieran a él.

## El archivo XML.

En el sistema original, los índices se definían con el tipo *serial*, que se definía como *integer*.

En el nuevo sistema, se recomienda el uso de *autoincrement* en lugar de *serial*, que lo define como un bigint(20) sin signo, al estilo de Laravel.

Cuando un campo de una tabla, está relacionado con un índice, su tipo debe de coincidir con el del índice de la tabla referenciada. Si se usa *increment* para definir el índice de la tabla referenciada, en las tablas que lo referencien, el campo deberá de definirse como *related*.

Los nombres de las restricciones y referencias, se definirán como *<nombre_tabla>_<nombre_campo>_foreign* o *<nombre_tabla>_<nombre_campo>_unique*.

Los campos relacionados, utilizarán el nombre de la tabla referenciada, seguida del campo índice en dicha tabla en formato snakecase (habitualmente id).

Todos los nombres, tanto de tablas como de campos, se irán creando en inglés, traspasándose los nombres antiguos a los nuevos conforme se vaya revisando el código mediante migraciones de datos.

Las tablas se nombrarán en plural, y los modelos en singular.

Los tiempos se declararán en formato datetime y se guardarán siempre en UTC, haciéndose la conversión adecuada al leerlos y al guardarlos.

NOTA: Guardar un tiempo en formato local puede ser un verdadero problema, teniendo en cuenta que las zonas horarias pueden tener inconsistencias los días que se haga cambio de horario. Si a las 3 son las 2, el periodo de 2 a 3 existe dos veces.