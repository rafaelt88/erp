# Aspecto visual

En cuanto al aspecto, principalmente se está utilizando:

Aspecto base

- [BootStrap v5.1.3](https://getbootstrap.com/docs/5.1/getting-started/introduction/)
- [Skote v3.3.2](https://themesbrand.com/skote/layouts/index.html)

Iconos

- [FontAwesome v6.1.1](https://fontawesome.com/icons)

Formularios

- [Formularios con componentes HTML5]()
    - Form Grid Layout (Skote)
- [Ajax Autocomplete for jQuery](https://github.com/devbridge/jQuery-Autocomplete)
- [TinyMCE v6.0.1](https://www.tiny.cloud/docs/tinymce/6/)
- [Select2 v4.1.0](https://select2.org/)
- Añadidos a futuro
    - [jQuery Validation v1.19.3](https://jqueryvalidation.org/documentation/)
    - [Input Mask v5.0.6](https://github.com/RobinHerbots/Inputmask)

Diálogos

- [BootBox v5.5.2](http://bootboxjs.com/documentation.html)

Calendarios

- [FullCalendar v5.10.1](https://fullcalendar.io/docs/#toc)

Tablas

- [DataTables v1.11.5](https://datatables.net/extensions/index)
    - Extensiones
        - [Buttons](https://datatables.net/extensions/buttons/)
        - [ColReorder](https://datatables.net/extensions/colreorder/)
        - [Datetime](https://datatables.net/extensions/datetime/)
        - [FixedHeader](https://datatables.net/extensions/fixedheader/)
        - [KeyTable](https://datatables.net/extensions/keytable/)
        - [Responsive](https://datatables.net/extensions/responsive/)
        - [RowGroup](https://datatables.net/extensions/rowgroup/)
        - [RowReorder](https://datatables.net/extensions/rowreorder/)
        - [Scroller](https://datatables.net/extensions/scroller/)
        - [Select](https://datatables.net/extensions/select/)
        - [StateRestore](https://datatables.net/extensions/staterestore/)

Puntuaciones

- [Star ratings v4.1.2](https://plugins.krajee.com/star-rating)

Gráficos

- [Apex Charts v3.33.0](https://apexcharts.com/)

Pueden verse más detalles mediante el archivo package.json.
