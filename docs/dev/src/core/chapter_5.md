# Extensiones

Para facilitar el uso de extensiones, en fs_controller se ha incluído un método llamado *do_extension_action*, que es ejecutado desde las extensiones, pasándosele el nombre de la extensión que lo llama y la acción a ejecutar.

A continuación, se detalla una forma de implementar *extensiones*, que podrá evolucionar y no ser la única.

## El controlador (trait)

En el controlador que vaya a usar la extensión, se hará un **use** del *trait* de la extensión.

Un mismo controlador puede utilizar varias extensiones.

El trait, definirá unos atributos y unos métodos propios de la extensión.

Un caso típico de extensión puede ser la extensión **Addressable**, que incluye direcciones postales a un modelo de un controlador.

En el controlador habría que hacer un...

```
use Addressable;
```

El trait añade los siguientes atributos:

- Para identificar el registro propietario de las direcciones (se pasan en addressableInit):
  - addressableTableName, que indica la tabla que se va a relacionar con la dirección (por ejemplo, si se añade en la ficha de clientes, pues será con la tabla de clientes).
  - addressableTableId, que contiene el id del registro de la tabla addressableTableName que se va a relacionar (el que esté cargado en ese momento).
- Los modelos necesarios para trabajar con las direcciones:
  - countryModel es una instancia del modelo de países.
  - addressTypeModel es una instancia al modelo de tipos de dirección.
  - addressModel es una instancia del modelo de direcciones.
  - addressAddressTypeModel es una instancia al modelo que relaciona una dirección con sus tipos de dirección asociados.
  - addressRelationshipModel es una instancia al modelo que relaciona la dirección con el modelo propietario.

Otras extensiones podrán tener más o menos atributos. Es buena idea que comiencen todos con *addressable*, para que los atributos no se solapen con otros traits o con el propio controlador que lo invoque.

Por otro lado, también contará con algunos métodos como:

- addressableGet, que obtiene los datos asociados a la tabla e id actuales.
- addressableAjax, que copia en los *input* de la vista, los datos del registro solicitado.
- addressableInit, que inicializa el módulo (básicamente se le pasa modelo y registro para saber a quién relacionar los datos leer y a quién asociar al guardar).
- addressableAction, que ejecuta las acciones pasadas por POST desde el formulario (típicamente 'save' o 'delete'). 
- addressableSave, que guarda los cambios recibidos por POST.
- addressableDelete, que elimina un registro de la tabla de direcciones.

## La vista

Se definirá un bloque (en el apartado **block**), con el mismo nombre que la extensión que será incluído allá donde sea necesario. Por lo general, se incluirá en un panel (pestaña) con un sencillo @include('block/addressable')

Como ejemplo, podemos observar el bloque addressable.blade.php dentro de soporte_empresas.

## El soporte Javascript

La mayoría de las extensiones, tendrán una parte de código JS, con la idea de poder modificar los datos de la extensión sin tener que recargar el controlador.

Básicamente, encontraremos funciones JS como:

- addressable_edit, que se encarga de pasar los datos recibidos por AJAX al formulario.
- addressable_delete, que permite eliminar, previa confirmación, un registro de la extensión.