# Definición de los menús

Las opciones de menú, se definen en dos partes:
- La estructura del menú, se define en los archivos <code>folders.php</code> que se encuentran en la carpeta <code>dataconfig</code> del núcleo y de cada plugin.
- Las opciones del menú, se definen en cada uno de los constructores de cada controlador.

## Definición de la estructura del menú.

Cada archivo <code>folders.php</code>, retorna elementos de un array, cuyo índice **sirve de referencia** para obtener la ruta, y cuyo contenido define *lo que se visualiza* del menú.

Cada opción de submenú, se separará con el carácter barra vertical o *pipeline*.

Por ejemplo:

```
return [
    'erp' => [
        'icon' => null,
        'icon_open' => null,
        'title' => 'X-Net ERP',
    ],
    'erp|tpv' => [
        'icon' => 'fa-solid fa-desktop fa-fw',
        'icon_open' => 'fa-solid fa-desktop fa-fw',
        'title' => 'TPV',
    ],
```

El primer elemento define una opción de primer nivel de menú, referenciada como <code>erp</code>, y que se mostrará sin iconos y con el texto <code>X-Net ERP</code>.
Para cambiar el texto mostrado o el icono, sólo habría que editar el archivo <code>folders.php</code> correspondiente, y cambiarlo. Posteriormente borrar la caché para refrescar el menú.

El segundo elemento, define una opción de segundo nivel de menú, con un icono fa-desktop y con el texto <code>TPV</code>. Dicha opción será un submenú de <code>X-Net ERP</code>.

El <code>icon</code> será el icono normal, y el <code>icon_open</code> será el que aparezca cuando la carpeta esté abierta.

Al limpiarse la caché, se leerán todos los archivos folders y la información quedará almacenada en la tabla <code>fs_folders</code>.

## Inclusión de un controlador en un submenú.

Las opciones actualizadas del constructor del controlador quedan así:

```
    parent::__construct(
        'Definición de terminales punto de venta y generación de arqueos de caja',
        'Arqueos y terminales', ['erp', 'tpv']
    );
```

El primer parámetro será una descripción de lo que hace el controlador, que se mostrará como opción por defecto en la pantalla de ajustes, si bien,
podrá ser editado por el administrador o por un usuario autorizado.

Las opciones utilizadas en el array <code>folders</code>, tienen que haber sido definidas previamente en las carpetas <code>folders.php</code>. En el caso de que no existan, en modo de depuración, se mostrará un mensaje de advertencia en la barra de depuración.

Por convención. No se usarán tildes, mayúsculas, ni símbolos para definir los índices del array. Sólo letras minúsculas y números.
