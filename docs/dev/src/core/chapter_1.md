# Núcleo

El núcleo de X-Net ERP está repartido de la siguiente forma:

- **base**: Aquí se encuentran todas las clases esenciales para el funcionamiento del entorno, principalmente clases que
  de por sí ya dan una funcionalidad completa, o por herencia aportan una funcionalidad particular.
- **bin**: Contienen algunos scripts auxiliares o adicionales de diferentes tipologías.
- **controller**: Contiene los controladores que estarán disponibles desde el entorno a través del parámetro page.
- **docker**: Define un entorno totalmente funcional a través de Docker para tener un entorno aislado en desarrollo.
- **docs**: Contiene las diferentes documentaciones del entorno, la de clases de PHP, la de nivel de desarrollo y la de
  nivel de usuario.
- **model**: Contiene los modelos que estarán disponibles desde el entorno a través de cualquiera de los controladores.
- **node_modules**: Contiene las dependencias de npm definidas en package.json.
- **plugins**: Aquí se encuentran plugins que complementan y/o modifican la funcionalidad original, requieren una
  estructura básica para su correcto funcionamiento.
- **raintpl**: Obsoleto, se ha migrado todo el código de RainTPL a Blade.
- **resellers**: Contiene información de algunos resellers del entorno.
- **resources**: Contiene archivos "comunes" con Alxarafe (integrado pero no usado).
- **src**: Contiene archivos "comunes" con Alxarafe (integrado pero no usado).
- **Templates**: En esta carpeta se encuentran todas las vistas.
- **updater**: Contiene el código del actualizador. Está diseñado para ser totalmente independiente del resto del código y compatible con XFS.
- **vendor**: Contiene las dependencias de composer definidas en composer.json.
- **view**: Obsoleto, se ha migrado todo el código de RainTPL a Blade (ahora las vistas están en Templates).
- **api.php**: Punto de entrada para la API basada en modelos (ATENCIÓN: que la lógica sea correcta depende totalmente
  del cliente)
- **CHANGELOG.md**: OPCIONAL Incluye un resumen de los cambios entre versiones.
- **composer.json**: Define todas las dependencias externas gestionadas por composer.
- **CONTRIBUTING.md**: Define una serie de reglas básicas para contribuir con cambios.
- **COPYING**: Contiene una copia de la licencia aplicada al proyecto.
- **cron.php**: Punto de entrada para la ejecución del CRON.
- **gulpfile.js**: Define tareas automatizadas, dispone de una sección dedicada.
- **index.html**: Carga una versión muy básica indicando los requisitos y enlazando a las documentaciones de desarrollo
  y de usuario.
- **index.php**: Punto de entrada para la ejecución habitual del entorno.
- **install.php**: Punto de entrada al que se redirige cuando no existe un config.php para la instalación.
- **miAJAX.php**: TODO
- **MiVersion**: Contiene la versión del núcleo.
- **package.json**: Define todas las dependencias externas gestionadas por npm.
- **README.md**: Contiene una archivo de explicación mínimo para el entorno, comúnmente utilizado por los repositorios
  GIT.
- **robots.txt**: Indica a los rastreadores de los buscadores a qué direcciones del sitio pueden acceder.
- **updater.php**: Página que se encarga de gestionar las actualizaciones del entorno desde la comunidad.
- **updater_ajax.php**: Sustituto de updater_core.php
- **updater_core.php**: Obsoleto. Página que se encarga de gestionar las actualizaciones del núcleo desde la comunidad.
- **updater_maintenance.php**: Obsoleto. Página que responde mediante JSON mientras el entorno está en mantenimiento por
  actualización, de modo que podemos ir consultando en que instante se encuentra.