# Plugins

Un plugin por diseño del propio entorno, puede añadir una nueva funcionalidad (que es la opción más recomendada) o bien,
reemplazar una funcionalidad previamente existente (que es la opción desaconsejada).

Los plugins son gestionados por el núcleo, partiendo de un orden de activación como sistema de prioridad de carga, que
con el tiempo se ha visto levemente mejorado para que este orden de activación pueda autoajustarse y no ser la prioridad
únicamente como el usuario ha decidido activar los plugins, sino también teniendo en cuenta la indicación de que un
plugin puede requerir a otro, teniendo así un sistema de ordenación basada en las dependencias de los plugins que han
ido siendo activados con el tiempo.

La estructura de un plugin puede ser como la que se indica a continuación:

- **nombre_plugin**
    - **controller**: Contiene los controladores del plugin.
    - **model**: Contiene los modelos del plugin.
    - **Templates**: Contiene las vistas del plugin en formato Blade.
    - **view**: Obsoleto, se ha migrado todo el código de RainTPL a Blade.
    - **CHANGELOG.md**: OPCIONAL Incluye un resumen de los cambios entre versiones.
    - **cron.php**: OPCIONAL Define procesos más largos o periódicos que serán ejecutados mediante CRON del sistema.
    - **functions.php**: OPCIONAL Define funciones adicionales que estarán disponibles en todo el entorno.
    - **description**: Descripción que será mostrada al usuario desde Panel de control como plugin instalado.
    - **LICENSE**: OPCIONAL Define una licencia aplicable al plugin, si carece de ella se entenderá que no hay
      restricciones a su respecto.
    - **mifactura.ini**: REQUERIDO Define datos esenciales del plugin.

El núcleo interpreta si el plugin es o no válido para la versión en curso basándose en la siguiente información:

- **mifactura.ini**
    - **name = mi_plugin_2**: REQUERIDO Nombre del plugin, debe llamarse igual que el nombre de la carpeta e igual que
      el repositorio.
    - **version = YYYY.MMDDR**: REQUERIDO Número de versión, basado en la fecha con el formato indicado (R: revisión o
      actualización menor para el mismo día).
    - **require = 'mi_plugin_1'**: OPCIONAL Si depende de otro/s plugin/s para su funcionalidad, será obligatorio
      indicar de quien depende.
    - **min_version = YYYY.MMDD**: REQUERIDO De que versión mínima del núcleo se depende.
    - **wizard = 'wizard_mi_plugin_2'**: OPCIONAL Se suele utilizar para mostrar una licencia al usuario y/o poder
      definir asistentes que serán ejecutados tras instalaciones nuevas y actualizaciones.
