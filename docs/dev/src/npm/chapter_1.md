# NPM

[NPM](https://www.npmjs.com/) es el sistema de gestión de paquetes por defecto para Node.js, un entorno de ejecución
para JavaScript.

Hay que tener en cuenta que npm puede actualizarse a si mismo, en ese caso este comando.

```
npm i npm@latest -g
```

*Recuerda:* En el caso que utilices docker, los comandos debes ejecutarlos tras este:

```
docker exec -ti xnet_php bash
```

Sus dependencias están definidas en package.json en la raíz del proyecto.

Adicionalmente soporta scripts, a través de los cuales realizamos una mayor integración con composer y gulp para
agilizar tareas.

- *npm run pluginsDependencies*: Para instalar las dependencias externas de todos los plugins
- *npm run scss*: Para generar los archivos css
