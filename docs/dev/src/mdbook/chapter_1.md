# mdBook

[mdBook](https://rust-lang.github.io/mdBook/) permite crear libros desde archivos markdown. Esta misma documentación
está creada con ello.

*Recuerda:* En el caso que utilices docker, los comandos debes ejecutarlos tras este:

```
docker exec -ti xnet_php bash
```

Su funcionamiento es tan sencillo como añadir una nueva página en
formato [markdown](https://rust-lang.github.io/mdBook/format/markdown.html) y añadir su enlace a SUMMARY.md, acto
seguido ejecutar *mdbook build* y ya tienes la nueva página generada.

Tanto la documentación de desarrollo como la de usuario se ha generado utilizando esta misma herramienta.
