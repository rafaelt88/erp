<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/controller/admin_home.php';

/**
 * Controlador para modificar el perfil del usuario.
 */
class admin_user extends fs_controller
{
    /**
     * Objeto agente.
     *
     * @var agente
     */
    public $agente;

    /**
     * Contiene si puede o no eliminar en este controlador.
     *
     * @var bool
     */
    public $allow_delete;

    /**
     * TODO: Missing documentation
     *
     * @var bool
     */
    public $allow_modify;

    /**
     * Lista de registros fs_log.
     *
     * @var fs_log[]
     */
    public $user_log;

    /**
     * Objeto fs_user.
     *
     * @var fs_user
     */
    public $suser;

    /**
     * admin_user constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Edición de usuario',
            'Usuario', ['admin', 'dashboard'], false, false
        );
        $this->share_extensions();
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    public function private_core()
    {
        $this->agente = new agente();

        /// ¿El usuario tiene permiso para eliminar en esta página?
        $this->allow_delete = $this->user->admin;

        /// ¿El usuario tiene permiso para modificar en esta página?
        $this->allow_modify = $this->user->admin;

        $this->suser = false;
        if (isset($_GET['snick'])) {
            $this->suser = $this->user->get(filter_input(INPUT_GET, 'snick'));
        }

        if ($this->suser) {
            /// ¿Estamos modificando nuestro usuario?
            if ($this->suser->nick == $this->user->nick) {
                $this->allow_modify = true;
                $this->allow_delete = false;
            }
            $action = fs_filter_input_req('action');
            switch ($action) {
                case 'update_theme_options':
                    $this->ajax_update_theme_options();
                    break;
                default:
                    if (isset($_POST['nnombre'])) {
                        $this->nuevo_empleado();
                    } elseif (isset($_POST['spassword']) || isset($_POST['scodagente']) || isset($_POST['sadmin'])) {
                        $this->modificar_user();
                    } elseif (fs_filter_input_req('senabled')) {
                        $this->desactivar_usuario();
                    }

                    /// ¿Estamos modificando nuestro usuario?
                    if ($this->suser->nick == $this->user->nick) {
                        $this->user = $this->suser;
                    }

                    /// si el usuario no tiene acceso a ninguna página, entonces hay que informar del problema.
                    if (!$this->suser->admin) {
                        $sin_paginas = true;
                        foreach ($this->all_pages() as $p) {
                            if ($p->enabled) {
                                $sin_paginas = false;
                                break;
                            }
                        }
                        if ($sin_paginas) {
                            $this->new_advice('No has autorizado a este usuario a acceder a ninguna' . ' página y por tanto no podrá hacer nada. Puedes darle acceso a alguna página' . ' desde la pestaña autorizar.');
                        }
                    }

                    $fslog = new fs_log();
                    $this->user_log = $fslog->all_from_usuario($this->suser->nick);
                    break;
            }
        } else {
            $this->new_error_msg("Usuario no encontrado.", 'error', false, false);
        }
    }

    /**
     * El método actualiza las opciones del tema del usuario
     *
     * @author  Daniel Marcelino Hernández Vieira
     * @version 2022.03
     */
    private function ajax_update_theme_options()
    {
        $this->suser->theme_topbar = fs_filter_input_req('theme_topbar', 'light');
        $this->suser->theme_sidebar = fs_filter_input_req('theme_sidebar', 'dark');
        $this->suser->theme_body = fs_filter_input_req('theme_body', 'light');
        $status = $this->suser->save();
        $this->template = false;
        $reply = [
            'updated' => $status,
            'user' => $this->suser,
        ];
        json_response($reply);
    }

    /**
     * Devuelve todas las páginas a las que el usuario tiene acceso.
     *
     * @return array
     */
    public function all_pages()
    {
        $returnlist = [];

        /// Obtenemos la lista de páginas. Todas
        foreach ($this->menu as $m) {
            $m->enabled = false;
            $m->allow_delete = false;
            $returnlist[] = $m;
        }

        /// Completamos con la lista de accesos del usuario
        $access = $this->suser->get_accesses();
        foreach ($returnlist as $i => $value) {
            // Se comprueba que no sea una de las páginas que siempre tiene que estar activa
            if (in_array($value->name, admin_home::SIEMPRE_ACTIVAS)) {
                $value->enabled = true;
                $value->allow_delete = true;
                $value->save();
            }
            foreach ($access as $a) {
                if ($value->name == $a->fs_page) {
                    $returnlist[$i]->enabled = true;
                    $returnlist[$i]->allow_delete = $a->allow_delete;
                    break;
                }
            }
        }

        /// ordenamos por nombre
        usort($returnlist, function ($val1, $val2) {
            return strcmp($val1->name, $val2->name);
        });

        return $returnlist;
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (!isset($this->suser)) {
            return parent::url();
        } elseif ($this->suser) {
            return $this->suser->url();
        }

        return $this->page->url();
    }

    /**
     * Función para añadir extensiones
     */
    private function share_extensions()
    {
        foreach ($this->extensions as $ext) {
            if ($ext->type == 'css') {
                if (!file_exists($ext->text)) {
                    $ext->delete();
                }
            }
        }

        $extensiones = [
            [
                'name' => 'Ninguno',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => '',
                'params' => '',
            ],
            [
                'name' => 'cerulean',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/cerulean/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'cosmo',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/cosmo/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'cyborg',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/cyborg/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'darkly',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/darkly/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'flatly',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/flatly/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'journal',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/journal/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'litera',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/litera/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'lumen',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/lumen/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'lux',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/lux/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'materia',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/materia/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'minty',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/minty/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'morph',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/morph/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'pulse',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/pulse/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'quartz',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/quartz/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'sandstone',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/sandstone/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'simplex',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/simplex/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'sketchy',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/sketchy/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'slate',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/slate/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'solar',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/solar/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'spacelab',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/spacelab/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'superhero',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/superhero/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'united',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/united/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'vapor',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/vapor/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'yeti',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/yeti/bootstrap.min.css',
                'params' => '',
            ],
            [
                'name' => 'zephyr',
                'page_from' => 'admin_user',
                'page_to' => 'admin_user',
                'type' => 'css',
                'text' => 'node_modules/bootswatch/dist/zephyr/bootstrap.min.css',
                'params' => '',
            ],
        ];
        foreach ($extensiones as $ext) {
            $fsext = new fs_extension($ext);
            if (!$fsext->save()) {
                $this->new_error_msg('No ha sido posible guardar los datos de la extensión ' . $ext['name'] . '.');
            }
        }
    }

    /**
     * Se encarga de dar de alta un nuevo empleado con los datos recibidos.
     */
    private function nuevo_empleado()
    {
        $age0 = new agente();
        $age0->codagente = $age0->get_new_codigo();
        $age0->nombre = filter_input(INPUT_POST, 'nnombre');
        $age0->apellidos = filter_input(INPUT_POST, 'napellidos');
        $age0->dnicif = filter_input(INPUT_POST, 'ndnicif');
        $age0->telefono = filter_input(INPUT_POST, 'ntelefono');
        $age0->email = mb_strtolower(filter_input(INPUT_POST, 'nemail'));

        if (!$this->user->admin) {
            $this->new_error_msg('Solamente un administrador puede crear y asignar un empleado desde aquí.');
        } elseif ($age0->save()) {
            $this->new_message("Empleado " . $age0->codagente . " guardado correctamente.");
            $this->suser->codagente = $age0->codagente;

            if ($this->suser->save()) {
                $this->new_message("Empleado " . $age0->codagente . " asignado correctamente.");
            } else {
                $this->new_error_msg("¡Imposible asignar el agente!");
            }
        } else {
            $this->new_error_msg("¡No ha sido posible guardar el agente!");
        }
    }

    /**
     * Se encarga de modificar un empleado con los datos recibidos.
     */
    private function modificar_user()
    {
        if (FS_DEMO && $this->user->nick != $this->suser->nick) {
            $this->new_error_msg('En el modo <b>demo</b> sólo puedes modificar los datos de TU usuario.
            Esto es así para evitar malas prácticas entre usuarios que prueban la demo.');
        } elseif (!$this->allow_modify) {
            $this->new_error_msg('No tienes permiso para modificar estos datos.');
        } else {
            $user_no_more_admin = false;
            $error = false;
            $spassword = filter_input(INPUT_POST, 'spassword');
            if ($spassword != '') {
                if ($spassword == filter_input(INPUT_POST, 'spassword2')) {
                    if ($this->suser->set_password($spassword)) {
                        $this->new_message('Se ha cambiado la contraseña del usuario ' . $this->suser->nick, true, 'login', true);
                    }
                } else {
                    $this->new_error_msg('Las contraseñas no coinciden.');
                    $error = true;
                }
            }

            if (isset($_POST['email'])) {
                $this->suser->email = mb_strtolower(filter_input(INPUT_POST, 'email'));
            }

            if (isset($_POST['scodagente'])) {
                $this->suser->codagente = null;
                if ($_POST['scodagente'] != '') {
                    $this->suser->codagente = filter_input(INPUT_POST, 'scodagente');
                }
            }

            /*
             * Propiedad admin: solamente un admin puede cambiarla.
             */
            if ($this->user->admin) {
                /*
                 * El propio usuario no puede decidir dejar de ser administrador.
                 */
                if ($this->user->nick != $this->suser->nick) {
                    /*
                     * Si un usuario es administrador y deja de serlo, hay que darle acceso
                     * a algunas páginas, en caso contrario no podrá continuar
                     */
                    if ($this->suser->admin && !isset($_POST['sadmin'])) {
                        $user_no_more_admin = true;
                    }
                    $this->suser->admin = !!fs_filter_input_post('sadmin', false);
                }
            }

            $this->suser->fs_page = null;
            if (isset($_POST['udpage'])) {
                $this->suser->fs_page = filter_input(INPUT_POST, 'udpage');
            }

            if (isset($_POST['css'])) {
                $this->suser->css = filter_input(INPUT_POST, 'css');
            }

            if (isset($_POST['use_sidebar_menu'])) {
                $this->suser->use_sidebar_menu = filter_input(INPUT_POST, 'use_sidebar_menu');
            }

            if ($error) {
                /// si se han producido errores, no hacemos nada más
            } elseif ($this->suser->save()) {
                if (!$this->user->admin) {
                    /// si no eres administrador, no puedes cambiar los permisos
                } elseif (!$this->suser->admin) {
                    /// para cada página, comprobamos si hay que darle acceso o no
                    foreach ($this->all_pages() as $p) {
                        /**
                         * Creamos un objeto fs_access con los datos del usuario y la página.
                         * Si tiene acceso guardamos, sinó eliminamos. Así no tenemos que comprobar uno a uno
                         * si ya estaba en la base de datos. Eso lo hace el modelo.
                         */
                        $a = new fs_access([
                            'fs_user' => $this->suser->nick,
                            'fs_page' => $p->name,
                            'allow_delete' => false,
                        ]);
                        if (isset($_POST['allow_delete'])) {
                            $a->allow_delete = in_array($p->name, $_POST['allow_delete']);
                        }

                        if ($user_no_more_admin) {
                            /*
                             * Si un usuario es administrador y deja de serlo, hay que darle acceso
                             * a algunas páginas, en caso contrario no podrá continuar.
                             */
                            $a->save();
                        } elseif (!isset($_POST['enabled'])) {
                            /**
                             * No se ha marcado ningún checkbox de autorizado, así que eliminamos el acceso
                             * a todas las páginas. Una a una.
                             */
                            if (in_array($p->name, admin_home::SIEMPRE_ACTIVAS)) {
                                $this->new_error_msg("No puedes desactivar esta página (" . $p->name . ").");
                            } else {
                                $a->delete();
                            }
                        } elseif (in_array($p->name, $_POST['enabled'])) {
                            /// la página ha sido marcada como autorizada.
                            $a->save();

                            /// si no hay una página de inicio para el usuario, usamos esta
                            if (is_null($this->suser->fs_page)) {
                                $this->suser->fs_page = 'dashboard';
                                $this->suser->save();
                            }
                        } else {
                            /// la página no está marcada como autorizada.
                            if (in_array($p->name, admin_home::SIEMPRE_ACTIVAS)) {
                                $this->new_error_msg("No puedes desactivar esta página (" . $p->name . ").");
                            } else {
                                $a->delete();
                            }
                        }
                    }
                }

                $this->new_message("Datos modificados correctamente.");
            } else {
                $this->new_error_msg("¡Imposible modificar los datos!");
            }
        }
    }

    /**
     * Se encara de desactivar a un usuario.
     */
    private function desactivar_usuario()
    {
        if (!$this->user->admin) {
            $this->new_error_msg('Solamente un administrador puede activar o desactivar a un Usuario.');
        } elseif ($this->user->nick == $this->suser->nick) {
            $this->new_error_msg('No se permite Activar/Desactivar a uno mismo.');
        } else {
            // Un usuario no se puede Activar/Desactivar a él mismo.
            $this->suser->enabled = (fs_filter_input_req('senabled') == 'TRUE');

            if ($this->suser->save()) {
                if ($this->suser->enabled) {
                    $this->new_message('Usuario activado correctamente.', true, 'login', true);
                } else {
                    $this->new_message('Usuario desactivado correctamente.', true, 'login', true);
                }
            } else {
                $this->new_error_msg('Error al Activar/Desactivar el Usuario');
            }
        }
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-person-half-dress fa-fw"></i>';
    }
}
