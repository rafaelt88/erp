<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use Xnet\Core\XnetPhpFileCache;

/**
 * Controlador de admin -> información del sistema.
 */
class admin_info extends fs_list_controller
{
    /**
     * Objeto fs_var.
     *
     * @var fs_var
     */
    private $fsvar;

    /**
     * admin_info constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Proporciona información general sobre el sistema',
            'Información del sistema', ['admin', 'dashboard'], true, true
        );
    }

    /**
     * Devuelve la versión de la caché.
     *
     * @return string
     */
    public function cache_version()
    {
        return $this->cache->version();
    }

    /**
     * Devuelve el nombre de la base de datos.
     *
     * @return string
     */
    public function fs_db_name()
    {
        return fs_app::get_db_name();
    }

    /**
     * Devuelve la versión de la base de datos.
     *
     * @return string
     */
    public function fs_db_version()
    {
        return $this->db->version();
    }

    /**
     * Devuelve los bloqueos.
     *
     * @return array
     */
    public function get_locks()
    {
        return $this->db->get_locks();
    }

    /**
     * Devuelve la versión de PHP.
     *
     * @return string
     */
    public function php_version()
    {
        return phpversion();
    }

    /**
     * Crea las pestañas del controlador
     */
    protected function create_tabs()
    {
        /// pestaña historial
        $this->add_tab('logs', 'Historial', (new fs_log())->table_name(), 'fa-solid fa-book fa-fw');
        $this->add_search_columns('logs', ['usuario', 'tipo', 'detalle', 'ip', 'controlador',]);
        $this->add_sort_option('logs', ['id'], 2);
        $this->add_sort_option('logs', ['fecha']);
        $this->add_button('logs', 'Borrar', $this->url() . '&action=remove-all', 'fa-solid fa-trash-can fa-fw', 'btn-danger');

        /// filtros
        $tipos = $this->sql_distinct('fs_logs', 'tipo');
        $this->add_filter_select('logs', 'tipo', 'tipo', $tipos);
        $this->add_filter_date('logs', 'fecha', 'desde', '>=');
        $this->add_filter_date('logs', 'fecha', 'hasta', '<=');
        $this->add_filter_checkbox('logs', 'alerta', 'alerta');

        /// decoración
        $this->decoration->add_column('logs', 'id', 'string', '#');
        $this->decoration->add_column('logs', 'fecha', 'datetime', 'Fecha', 'text-nowrap');
        $this->decoration->add_column('logs', 'alerta', 'bool', 'Alerta');
        $this->decoration->add_column('logs', 'usuario', 'string', 'Usuario');
        $this->decoration->add_column('logs', 'tipo', 'string', 'Tipo');
        $this->decoration->add_column('logs', 'detalle', 'string', 'Detalle');
        $this->decoration->add_column('logs', 'controlador', 'string', 'Página', 'text-nowrap', 'index.php?page=');
        $this->decoration->add_column('logs', 'ip', 'string', 'IP', 'text-nowrap text-end');

        $this->decoration->add_row_option('logs', 'alerta', true, 'danger');
        $this->decoration->add_row_option('logs', 'tipo', 'error', 'danger');
        $this->decoration->add_row_option('logs', 'tipo', 'msg', 'success');

        /// cargamos una plantilla propia para la parte de arriba
        $this->template_top = 'block/admin_info_top';
    }

    /**
     * Ejecuta el código en el método antes de realizar la acción.
     *
     * @param string $action
     *
     * @return bool
     */
    protected function exec_previous_action($action)
    {
        switch ($action) {
            case 'remove-all':
                return $this->remove_all_action();

            default:
                return parent::exec_previous_action($action);
        }
    }

    /**
     * Elimina todos los registros de la tabla logs.
     *
     * @return bool
     */
    protected function remove_all_action()
    {
        $sql = "DELETE FROM `fs_logs` ;";
        if ($this->db->exec($sql)) {
            $this->new_message('Historial borrado correctamente.', true);
        }

        return true;
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        parent::private_core();

        /**
         * Cargamos las variables del cron
         */
        $this->fsvar = new fs_var();
        $cron_vars = $this->fsvar->array_get(['cron_exists' => false, 'cron_lock' => false, 'cron_error' => false,]);

        if (isset($_GET['fix'])) {
            $cron_vars['cron_error'] = false;
            $cron_vars['cron_lock'] = false;
            $this->fsvar->array_save($cron_vars);
        } elseif (isset($_GET['clean_cache'])) {
            fs_file_manager::clear_views_cache();
            XnetPhpFileCache::clearYamlCache();
            if ($this->cache->clean()) {
                $this->new_message("Cache limpiada correctamente.");
            }
        } elseif (!$cron_vars['cron_exists']) {
            $this->new_advice('Nunca se ha ejecutado el' . ' <a href="' . constant('FS_COMMUNITY_URL') . '/doc/2/configuracion/en-cron" target="_blank">cron</a>,' . ' te perderás algunas características interesantes de MiFactura.eu.');
        } elseif ($cron_vars['cron_error']) {
            $this->new_error_msg('Parece que ha habido un error con el cron. Haz clic <a href="' . $this->url() . '&fix=TRUE">aquí</a> para corregirlo.');
        } elseif ($cron_vars['cron_lock']) {
            $this->new_advice('Se está ejecutando el cron.');
        }
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-circle-info fa-fw"></i>';
    }
}
