<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use Xnet\Core\XnetPhpFileCache;
use Xnet\Model\Version;

require_once constant('BASE_PATH') . '/base/fs_plugin_manager.php';
require_once constant('BASE_PATH') . '/base/fs_settings.php';

/**
 * Panel de control de MiFactura.eu.
 */
class admin_home extends fs_controller
{
    /**
     * Páginas que no se pueden desactivar y tienen que estar siempre activas.
     */
    const SIEMPRE_ACTIVAS = [
        'admin_home',
        'dashboard',
        'admin_notificacion',
    ];

    /**
     * Listado de fs_page.
     *
     * @var fs_page[]
     */
    public $paginas;

    /**
     * Clase para centralizar la gestión de plugins.
     *
     * @var fs_plugin_manager
     */
    public $plugin_manager;

    /**
     * Clase para centralizar la gestión de ajustes.
     *
     * @var fs_settings
     */
    public $settings;

    /**
     * Paso del asistente inicial.
     *
     * @var string
     */
    public $step;

    /**
     * Número máximo de minutos tras los cuales, se permite ejecutar un nuevo cron limpiando los datos del anterior.
     *
     * @var int
     */
    public $cron_maximo_minutos;

    /**
     * Objeto fs_var.
     *
     * @var fs_var
     */
    private $fs_var;

    /**
     * Contiene la acción.
     *
     * @var string
     */
    public $action;

    /**
     * admin_home constructor.
     */
    public function __construct()
    {
        $this->plugin_manager = new fs_plugin_manager();
        parent::__construct(
            'Ajustes de panel de control',
            'Ajustes',
            ['admin', 'dashboard']
        );
    }

    /**
     * Devuelve una lista de plugins activados.
     *
     * @return array
     */
    public function plugin_advanced_list()
    {
        /**
         * Si se produce alguna llamada a esta función, desactivamos todos los plugins,
         * porque debe haber alguno que está desactualizado, y un problema al cargar
         * está página será muy difícil de resolver para un novato.
         */
        foreach ($this->plugin_manager->enabled() as $plug) {
            $this->plugin_manager->disable($plug);
        }

        return [];
    }

    /**
     * Comprueba actualizaciones de los plugins y del núcleo.
     *
     * @return bool
     */
    public function check_for_updates2()
    {
        if (!$this->user->admin) {
            return false;
        }

        /// comprobamos actualizaciones en los plugins
        $updates = false;
        foreach ($this->plugin_manager->installed(false) as $plugin) {
            if ($plugin['version_url'] != '' && $plugin['update_url'] != '') {
                /// plugin con descarga gratuita
                $internet_ini = @parse_ini_string(@fs_file_get_contents($plugin['version_url']));
                if ($internet_ini && $plugin['version'] < intval($internet_ini['version'])) {
                    $updates = true;
                    break;
                }
            } elseif ($plugin['idplugin'] && $plugin['download2_url'] != '') {
                /// plugin de pago/oculto
                /// download2_url implica que hay actualización
                $updates = true;
                break;
            }
        }

        if (!$updates) {
            $internet_version = 0;
            /// comprobamos actualizaciones del núcleo
            $version = trim(@file_get_contents('MiVersion'));

            $cores = get_from_community('/core.json', 30);
            if ($cores == null) {
                $this->core_log->new_error('No se ha podido comunicar con el servidor para obtener las actualizaciones.');
                return false;
            }

            if (count($cores) > 0) {
                $new_core = $cores[0];
                $internet_version = $new_core->version;
            }

            if (floatval($version) < floatval($internet_version)) {
                $updates = true;
            }
        }

        if ($updates) {
            $this->fs_var->simple_save('updates', 'true');
            return true;
        }

        $this->fs_var->name = 'updates';
        $this->fs_var->delete();
        return false;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->clean();
        fs_file_manager::clear_views_cache();
        XnetPhpFileCache::clearYamlCache();
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        // Si es una petición de los datos de la página por AJAX...
        if (isset($_POST['page_name'])) {
            $p = new fs_page();
            $data = $p->get($_POST['page_name']);

            if (empty($data->alias)) {
                $data->alias = $data->title;
            }
            if (empty($data->description)) {
                $data->description = $data->title;
            }

            json_response([$data]);
        }

        if (isset($_REQUEST['action'])) {
            $this->action = $_REQUEST['action'];
            $models = $GLOBALS['models'];
            foreach ($models as $pos => $model) {
                $models[$pos] = str_replace('.php', '', $model);
            }
            sort($models);

            switch ($_REQUEST['action']) {
                case 'check_plugins':
                    $this->check_plugins();
                    break;
                case 'edit_page':
                    $p = (new fs_page())->get($_POST['name']);
                    $p->alias = $_POST['alias'];
                    $p->description = $_POST['description'];
                    if ($p->save()) {
                        $this->new_message($_POST['name'] . ' actualizado correctamente');
                    } else {
                        $this->new_error_msg('No se ha podido actualizar ' . $_POST['name']);
                    }
                    break;
                case 'ajax_models':
                    json_response([
                        'status' => true,
                        'message' => 'Listado de modelos activos.',
                        'models' => $models,
                    ]);
                    break;
                case 'ajax_instance_model':
                    $model = $_REQUEST['model'];
                    try {
                        if (in_array($model, $models)) {
                            new $model();
                            $status = [
                                'status' => true,
                                'message' => 'Modelo ' . $model . ' instanciado.',
                            ];
                        } else {
                            throw new Exception("El modelo '" . $model . "' no existe.");
                        }
                    } catch (Exception $exc) {
                        $status = [
                            'status' => false,
                            'error' => $exc->getMessage(),
                        ];
                    }
                    json_response($status);
                    break;
                default:
                    break;
            }
        }

        $this->fs_var = new fs_var();
        $this->plugin_manager = new fs_plugin_manager();
        $this->settings = new fs_settings();
        $this->step = (string) $this->fs_var->simple_get('install_step');

        $this->cron_maximo_minutos = 5;
        $cron_maximo_minutos = $this->fs_var->simple_get('cron_maximo_minutos', 5);
        if ($cron_maximo_minutos !== false) {
            $cron_maximo_minutos = $cron_maximo_minutos;
        }

        $this->exec_actions();
        $this->paginas = $this->all_pages();
        $this->load_menu(true);

        // Listado de plugins instalados
        // dump($this->plugin_manager->installed(false));
    }

    /**
     * Procesa las acciones del controlador.
     */
    private function exec_actions()
    {
        if (filter_input(INPUT_GET, 'check4updates')) {
            $this->action = 'check4updates';
            $this->template = false;
            if ($this->check_for_updates2()) {
                echo 'Hay actualizaciones disponibles.';
            } else {
                echo 'No hay actualizaciones.';
            }
            return;
        }

        if (filter_input(INPUT_GET, 'updated')) {
            $this->action = 'updated';
            /// el sistema ya se ha actualizado
            $this->fs_var->simple_delete('updates');
            $this->activar_comprobacion_columnas();
            $this->clean_cache();
            return;
        }

        $cron_maximo_minutos = filter_input(INPUT_POST, 'cron_maximo_minutos');
        if ($cron_maximo_minutos) {
            $this->cron_maximo_minutos = $cron_maximo_minutos;
            $this->fs_var->simple_save('cron_maximo_minutos', $cron_maximo_minutos);
        }

        if (FS_DEMO) {
            $this->new_advice('En el modo demo no se pueden hacer cambios en esta página.');
            $this->new_advice('Si te gusta MiFactura.eu y quieres saber más, consulta la ' . '<a href="' . constant('FS_COMMUNITY_URL') . '/doc/2">documentación</a>.');
            return;
        }

        if (!$this->user->admin) {
            $this->new_error_msg('Sólo un administrador puede hacer cambios en esta página.');
            return;
        }

        if (filter_input(INPUT_GET, 'skip')) {
            if ($this->step == '1') {
                $this->step = '2';
                $this->fs_var->simple_save('install_step', $this->step);
            }
            return;
        }

        if ($this->step == '' && $GLOBALS['config2']['homepage'] != 'dashboard') {
            $GLOBALS['config2']['homepage'] = 'dashboard';
            if ($this->settings->save()) {
                $this->new_message('Establecida configuración inicial por defecto.');
            } else {
                $this->new_message('No se ha podido establecer la configuración inicial.');
            }
        }

        if (filter_input(INPUT_POST, 'modpages')) {
            $this->action = 'modpages';
            /// activar/desactivas páginas del menú
            $this->enable_pages();
        } elseif (filter_input(INPUT_GET, 'enable')) {
            $this->action = 'enable';
            /// activar plugin
            $this->enable_plugin(filter_input(INPUT_GET, 'enable'));
        } elseif (filter_input(INPUT_GET, 'disable')) {
            $this->action = 'disable';
            /// desactivar plugin
            $this->disable_plugin(filter_input(INPUT_GET, 'disable'));
        } elseif (filter_input(INPUT_GET, 'delete_plugin')) {
            $this->action = 'delete_plugin';
            /// eliminar plugin
            $this->delete_plugin(filter_input(INPUT_GET, 'delete_plugin'));
        } elseif (filter_input(INPUT_POST, 'install')) {
            $this->action = 'install';
            /// instalar plugin (copiarlo y descomprimirlo)
            $this->install_plugin();
        } elseif (filter_input(INPUT_GET, 'download')) {
            $this->action = 'download';
            /// descargamos un plugin de la lista de la comunidad
            $this->download(filter_input(INPUT_GET, 'download'));
        } elseif (filter_input(INPUT_GET, 'reset')) {
            $this->action = 'reset';
            /// reseteamos la configuración avanzada
            $this->settings->reset();
            $this->new_message('Configuración reiniciada correctamente, pulsa <a href="' . $this->url() . '#avanzado">aquí</a> para continuar.', true);
            return;
        }

        fs_file_manager::check_htaccess();

        /// ¿Guardamos las opciones de la pestaña avanzado?
        $this->save_avanzado();
    }

    /**
     * Activamos/desactivamos aleatoriamente la comprobación de tipos de las columnas
     * de las tablas. ¿Por qué? Porque la comprobación es lenta y no merece la pena hacerla
     * siempre, pero tras las actualizaciones puede haber cambios en las columnas de las tablas.
     */
    private function activar_comprobacion_columnas()
    {
        if ($GLOBALS['config2']['check_db_types'] != 1) {
            $GLOBALS['config2']['check_db_types'] = mt_rand(0, 1);
        }
        $this->settings->save();
    }

    /**
     * Se encarga de activar las páginas.
     */
    private function enable_pages()
    {
        if (!$this->step) {
            $this->step = '1';
            $this->fs_var->simple_save('install_step', $this->step);
        }

        set_temporaly_unlimited_limits();

        $enabled = filter_input(INPUT_POST, 'enabled', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        foreach ($this->all_pages() as $p) {
            if (!$p->exists) { /// la página está en la base de datos pero ya no existe el controlador
                if ($p->delete()) {
                    $this->new_message('Se ha eliminado automáticamente la página ' . $p->name . ' ya que no tiene un controlador asociado en la carpeta controller.');
                }
            } elseif (!$enabled) {
                /// Ninguna página marcada
                $this->disable_page($p);
            } elseif (!$p->enabled && in_array($p->name, $enabled)) {
                /// Página no activa marcada para activar
                $this->enable_page($p);
            } elseif ($p->enabled && !in_array($p->name, $enabled)) {
                /// Página activa no marcada (desactivar)
                $this->disable_page($p);
            } elseif ($p->enabled) {
                // Actualizamos detalles de la página
                $this->update_page_details($p);
            }
        }

        $message = "Páginas activas guardadas correctamente.";
        if ($this->regenerar_menus()) {
            $message .= "<br />Menús regenerados correctamente";
        }
        if (XnetPhpFileCache::clearYamlCache()) {
            $message .= "<br />Caché de ficheros eliminada correctamente";
        } else {
            $this->new_error_msg('No se ha podido eliminar la caché de ficheros');
        }

        $this->new_message($message);
    }

    /**
     * Regenera los menús con la información de "dataconfig/folders.php".
     *
     * @doc     docs/dev/src/core/chapter_3.md
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0613
     *
     * @return bool
     */
    private function regenerar_menus()
    {
        $defined_folders = fs_folder::regenerar();
        $folders_model = new fs_folder();
        $folders_model->vaciar();
        $ok = true;
        foreach ($defined_folders as $item => $value) {
            $folder_data = $folders_model->get($item);
            // Esta situación no debería de darse nunca, porque los array se fusionan (array_merge)
            if ($folder_data !== false) {
                debug_message($item . ' duplicado. Revise los archivos fs_folder.php');
                continue;
            }
            $folder_data = clone $folders_model;
            $folder_data->folder = $item;
            $folder_data->name = $value['title'];
            $folder_data->icon = $value['icon'];
            $folder_data->icon_open = $value['icon_open'] ?? $folder_data->icon;
            $ok = $ok && $folder_data->save();
        }
        unset($folders_model);
        return $ok;
    }

    private function get_page($file_name)
    {
        $p = new fs_page();
        $p->name = substr($file_name, 0, -4);
        $p->title = '';
        $p->folder = ['Oculto'];
        $p->exists = true;
        $p->show_on_menu = false;
        return $p;
    }

    /**
     * Devuelve las páginas/controladores de los plugins activos.
     *
     * @return fs_page[]
     */
    private function all_pages()
    {
        $pages = [];
        $page_names = [];

        /// añadimos las páginas de los plugins
        foreach ($this->plugin_manager->enabled() as $plugin) {
            if (!file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/controller')) {
                continue;
            }

            foreach (fs_file_manager::scan_files(constant('FS_FOLDER') . '/plugins/' . $plugin . '/controller', 'php') as $file_name) {
                $p = $this->get_page($file_name);
                if (!in_array($p->name, $page_names)) {
                    $pages[] = $p;
                    $page_names[] = $p->name;
                }
            }
        }

        /// añadimos las páginas que están en el directorio controller
        foreach (fs_file_manager::scan_files(constant('FS_FOLDER') . '/controller', 'php') as $file_name) {
            $p = $this->get_page($file_name);
            if (!in_array($p->name, $page_names)) {
                $pages[] = $p;
                $page_names[] = $p->name;
            }
        }

        /// completamos los datos de las páginas con los datos de la base de datos
        foreach ($this->page->all() as $p) {
            $encontrada = false;
            foreach ($pages as $i => $value) {
                if ($p->name == $value->name) {
                    $pages[$i] = $p;
                    $pages[$i]->enabled = true;
                    $pages[$i]->exists = true;
                    $encontrada = true;
                    break;
                }
            }
            if (!$encontrada) {
                $p->enabled = true;
                $pages[] = $p;
            }
        }

        /// ordenamos
        usort($pages, function ($a, $b) {
            if ($a->name == $b->name) {
                return 0;
            } elseif ($a->name > $b->name) {
                return 1;
            }
            return -1;
        });

        return $pages;
    }

    /**
     * Desactiva una página/controlador.
     *
     * @param fs_page $page
     */
    private function disable_page($page)
    {
        if (in_array($page->name, self::SIEMPRE_ACTIVAS)) {
            $this->new_error_msg("No puedes desactivar esta página (" . $page->name . ").");
            return false;
        } elseif (!$page->delete()) {
            $this->new_error_msg('Imposible eliminar la página ' . $page->name . '.');
            return false;
        }

        return true;
    }

    /**
     * Activa una página/controlador.
     *
     * @param fs_page $page
     */
    private function enable_page($page)
    {
        try {
            $class_name = find_controller($page->name);
            /// ¿No se ha encontrado el controlador?
            if ('base/fs_controller.php' === $class_name) {
                $this->new_error_msg('Controlador <b>' . $page->name . '</b> no encontrado.');
                return false;
            }

            require_once $class_name;
            $new_fsc = new $page->name();
            if (!isset($new_fsc->page)) {
                $this->new_error_msg("Error al leer la página " . $page->name);
                return false;
            } elseif (!$new_fsc->page->save()) {
                $this->new_error_msg("No ha sido posible guardar la página " . $page->name);
                return false;
            }

            unset($new_fsc);
            return true;
        } catch (Exception $exc) {
            $error = '<pre>' . var_export($exc, true) . '</pre>';
            $this->core_log->new_error('No se ha podido instanciar ' . $page->name . ': ' . $error);
        }
        return false;
    }

    /**
     * Instancia la página.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0714
     *
     * @param fs_page $page
     *
     * @return fs_page
     */
    private function update_page_details($page)
    {
        return $this->check_fs_page($page->name, $page->title, $page->folder, $page->show_on_menu, $page->important, $page->favorite);
    }

    /**
     * Activa un plugin.
     *
     * @param string $name
     */
    private function enable_plugin($name)
    {
        if (!$this->plugin_manager->enable($name)) {
            return;
        }

        $this->load_menu(true);

        if ($this->step == '1') {
            $this->step = '2';
            $this->fs_var->simple_save('install_step', $this->step);
        }
    }

    /**
     * Desactiva un plugin.
     *
     * @param string $name
     */
    private function disable_plugin($name)
    {
        $this->plugin_manager->disable($name);
    }

    /**
     * Elimina el plugin del directorio.
     *
     * @param string $name
     */
    private function delete_plugin($name)
    {
        $this->plugin_manager->remove($name);
    }

    /**
     * Se encarga de instalar el plugin subido.
     */
    private function install_plugin()
    {
        if (is_uploaded_file($_FILES['fplugin']['tmp_name'])) {
            $this->plugin_manager->install($_FILES['fplugin']['tmp_name'], $_FILES['fplugin']['name']);
        } else {
            $this->new_error_msg('Archivo no encontrado. ¿Pesa más de ' . $this->get_max_file_upload() . ' MB? Ese es el límite que tienes' . ' configurado en tu servidor.');
        }
    }

    /**
     * Descarga un plugin de la lista dinámica de la comunidad.
     */
    private function download($plugin_id)
    {
        $this->plugin_manager->download($plugin_id);
    }

    /**
     * Se encarga de almacenar los datos en config2.
     */
    private function save_avanzado()
    {
        $guardar = false;
        foreach ($GLOBALS['config2'] as $i => $value) {
            if (filter_input(INPUT_POST, $i) !== null) {
                $GLOBALS['config2'][$i] = filter_input(INPUT_POST, $i);
                $guardar = true;
            }
        }

        if (!$guardar) {
            return;
        }

        if ($this->settings->save()) {
            $this->new_message('Datos guardados correctamente.');
        } else {
            $this->new_message('Error al guardar los datos.');
        }
    }

    /**
     * Retorna los parámetros de configuración de un plugin.
     * Si no existe, retorna un array vacío.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0909
     *
     * @param $plugin
     *
     * @return array
     */
    private function get_plugin_parameters($plugin): array
    {
        $result = [];

        $config_filename = constant('BASE_PATH') . '/plugins/' . $plugin . '/mifactura.ini';
        if (!file_exists($config_filename)) {
            $config_filename = constant('BASE_PATH') . '/plugins/' . $plugin . '/xfs.ini';
        }

        $parameters = parse_ini_file($config_filename);
        if ($parameters === false) {
            return $result;
        }

        return $parameters;
    }

    /**
     * TODO: Ésto igual debería de ir en XnetConfig o mejor aún en XnetPluginManager
     * Reestructurar una vez finalizado, porque habrá llamdadas que también irán mejor allí.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0909
     *
     */
    private function check_plugins()
    {
        $plugins = [];
        $enabledPlugins = Version::getEnabledPluginsArray();
        foreach ($enabledPlugins as $enabledPlugin) {
            $parameters = $this->get_plugin_parameters($enabledPlugin);
            $requireString = $parameters['require'];
            $requires = [];

            if (!empty($requireString)) {
                $requires = explode(',', $requireString);
            }

            foreach ($requires as $require) {
                // si ya está, seguimos
                if (in_array($require, $plugins)) {
                    continue;
                }

                // Si no está instalado
                $requireConfig = $this->get_plugin_parameters($require);
                if (empty($requireConfig)) {
                    if (!$this->plugin_manager->downloadByName($require)) {
                        $this->new_error_msg('Error al intentar descargar el plugin ' . $require);
                        return false;
                    }
                }

                // si ya está, pero por detrás, se pone delante.
                if (in_array($require, $enabledPlugins)) {
                    $plugins[] = $require;
                    continue;
                }

                // Está, pero no activado, se activa
                if (!$this->enable_plugin($require)) {
                    $this->new_error_msg('Error al intentar descargar el plugin ' . $require);
                }

                $plugins[] = $require;
            }

            if (!in_array($enabledPlugin, $plugins)) {
                $plugins[] = $enabledPlugin;
            }
        }

        if (!Version::reSequence($plugins)) {
            $this->new_error_msg('Se ha producido un error al tratar de reordenar el orden de activación de los plugins');
            return false;
        }

        return true;
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-solar-panel fa-fw"></i>';
    }
}
