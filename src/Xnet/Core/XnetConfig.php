<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

use Xnet\Model\Version;

/**
 * Clase para concectar e interactuar con la cache.
 */
class XnetConfig
{
    public const PHP_CACHE_FOLDER = 'config';

    private static $plugins;

    public static function config()
    {
        // Inicializa la base de datos y se conecta
        new DB();
        if (!DB::connect()) {
            debug_message('Se ha producido un error al tratar de conectarse a la base de datos usando la clase DB');
        }

        /**
         * Si no existe la tabla de versiones, se crea y se intenta migrar de la base de datos original.
         * Esto es necesario, pues sin ésto, no se puede saber qué plugins hay activos.
         */
        if (!DBSchema::tableExists('versions')) {
            Version::createTable();
            self::migrateVersion();
        }

        $result = [];
        $plugins = Version::getPlugins();
        foreach ($plugins as $plugin) {
            $cfgPlugin = constant('BASE_PATH') . '/plugins/' . $plugin['plugin'] . '/mifactura.ini';
            if (!file_exists($cfgPlugin)) {
                debug_message('No se ha encontrado archivo de configuración para el plugin ' . $plugin['plugin']);
                continue;
            }
            $result[] = $plugin['plugin'];
        }
        self::$plugins = $result;
    }

    private static function migrateVersion()
    {
        $db = new \fs_db2();
        if ($db->connect()) {
            $versions = new \version();
            $all = $versions->all();
            if ($all !== false) {
                foreach ($all as $value) {
                    $path = $value['plugin'] === 'core'
                        ? 'src/Xnet/'
                        : 'plugins/' . $value['plugin'] . '/';

                    $data = [
                        'plugin' => $value['plugin'],
                        'version' => $value['version'],
                        'db_version' => $value['db_version'],
                        'sequence' => !empty($value['sequence']) ? $value['sequence'] : 0,
                        'path' => $path,
                    ];
                    $result = Version::create($data);
                }
                return true;
            }
        }
    }

    /**
     *
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0902
     *
     * @return array
     *
     * @deprecated Utilice Version::getEnabledPluginsArray;
     */
    public static function getEnabledPlugins(): array
    {
        return Version::getEnabledPluginsArray();
    }

    public static function getTmpFolder(): string
    {
        return constant('BASE_PATH') . '/tmp';
    }

}
