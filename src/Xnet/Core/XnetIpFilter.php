<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

/**
 * Class XnetIpFilter
 *
 * TODO: Revisar si hace falta o si debe de hacer algo más.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0805
 *
 * @package Xnet\Core
 */
class XnetIpFilter
{
    /**
     * Segundos qure dura un baneo.
     */
    public const BAN_SECONDS = 600;

    /**
     * Intentos máximos a partir del que se realizará un baneo
     */
    public const MAX_ATTEMPTS = 5;

    /**
     * Ruta en la que se almacena el archivo de IPs baneadas.
     *
     * @var string
     */
    private $filePath;

    /**
     * Lista de IPs bloqueadas,
     *
     * @var array
     */
    private $ipList;

    /**
     * Lista de IPs seguras.
     *
     * @var array
     */
    private $whiteList;

    /**
     * fs_ip_filter constructor.
     */
    public function __construct()
    {
        $this->filePath = constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'ip.log';
        $this->ipList = [];

        if (file_exists($this->filePath)) {
            /// Read IP list file
            $file = fopen($this->filePath, 'rb');
            if ($file) {
                while (!feof($file)) {
                    $line = explode(';', trim(fgets($file)));
                    $this->readLine($line);
                }

                fclose($file);
            }
        }

        $whiteFilePath = constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'ip_white_list.log';
        if (file_exists($whiteFilePath)) {
            $this->whiteList = [];
            $file = fopen($this->filePath, 'rb');
            if ($file) {
                while (!feof($file)) {
                    $this->whiteList[] = trim(fgets($file));
                }
                fclose($file);
            }
        }
    }

    /**
     * Lee la línea indicada del archivo y pasa los datos a un array.
     *
     * @param array $line
     */
    private function readLine($line)
    {
        /// if not expired
        if (count($line) == 3 && intval($line[2]) > time()) {
            $this->ipList[] = [
                'ip' => $line[0],
                'count' => (int) $line[1],
                'expire' => (int) $line[2],
            ];
        }
    }

    /**
     * Clear IP list.
     */
    public function clear()
    {
        $this->ipList = [];
        $this->save();
    }

    /**
     * Guarda la información sobre los intentos de conexión y su expiración.
     */
    private function save()
    {
        $file = fopen($this->filePath, 'wb');
        if ($file) {
            foreach ($this->ipList as $line) {
                fwrite($file, $line['ip'] . ';' . $line['count'] . ';' . $line['expire'] . "\n");
            }

            fclose($file);
        }
    }

    /**
     * Devuelve si la IP está en la lista blanca.
     *
     * @param string $ip
     *
     * @return bool
     */
    public function inWhiteList($ip)
    {
        // Se mantiene la definición en FS_IP_WHITELIST de momento, por compatibilidad.
        if (FS_IP_WHITELIST === '*' || FS_IP_WHITELIST === '') {
            return true;
        }

        $aux = explode(',', FS_IP_WHITELIST);
        return in_array($ip, $aux) || in_array($ip, $this->whiteList);
    }

    /**
     * Devuelve si la IP está baneada o no.
     *
     * @param string $ip
     *
     * @return bool
     */
    public function isBanned($ip)
    {
        foreach ($this->ipList as $line) {
            if ($line['ip'] == $ip && $line['count'] > self::MAX_ATTEMPTS) {
                return true;
            }
        }

        return false;
    }

    /**
     * Asigna intento de baneo.
     *
     * @param string $ip
     */
    public function setAttempt($ip)
    {
        return;

        $found = false;
        foreach ($this->ipList as $key => $line) {
            if ($line['ip'] == $ip) {
                $this->ipList[$key]['count']++;
                $this->ipList[$key]['expire'] = time() + self::BAN_SECONDS;
                $found = true;
                break;
            }
        }

        if (!$found) {
            $this->ipList[] = [
                'ip' => $ip,
                'count' => 1,
                'expire' => time() + self::BAN_SECONDS,
            ];
        }

        $this->save();
    }
}
