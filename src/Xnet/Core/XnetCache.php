<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

/**
 * Clase para concectar e interactuar con la cache.
 */
class XnetCache
{
    /**
     * Objeto memcache
     *
     * @var \Memcache|null
     */
    private static $memcache;

    /**
     * Objeto caché mediante archivos en PHP
     *
     * @var XnetPhpFileCache
     */
    private static $XnetPhpFileCache;

    /**
     * Contiene si está conectada o no a la caché.
     *
     * @var bool
     */
    private static $connected;

    /**
     * Contiene si ha habido un error en la cache.
     *
     * @var bool
     */
    private static $error;

    /**
     * Contiene un mensaje de error si es que ha habido uno.
     *
     * @var string
     */
    private static $error_msg;

    /**
     * fs_cache constructor.
     */
    public function __construct()
    {
        if (!isset(self::$memcache)) {
            $cache_host = constant('FS_CACHE_HOST') ?? 'localhost';
            $cache_port = constant('FS_CACHE_PORT') ?? '11211';
            $cache_port = empty($cache_port) ? 11211 : (int) $cache_port;
            if (class_exists('Memcache') && !empty($cache_host)) {
                self::$memcache = new \Memcache();
                if (@self::$memcache->connect($cache_host, $cache_port)) {
                    self::$connected = true;
                    self::$error = false;
                    self::$error_msg = '';
                } else {
                    self::$connected = false;
                    self::$error = true;
                    self::$error_msg = 'Error al conectar al servidor Memcache.';
                }
            } else {
                self::$memcache = false;
                self::$connected = false;
                self::$error = true;
                self::$error_msg = 'Clase Memcache no encontrada. Debes
               <a target="_blank" href="//community.mifactura.eu/index.php?page=community_item&id=553">
               instalar Memcache</a> y activarlo en el php.ini';
            }
        }

        if (!isset(self::$XnetPhpFileCache)) {
            self::$XnetPhpFileCache = new XnetPhpFileCache();
        }
    }

    /**
     * Devuelve si ha habido error.
     *
     * @return bool
     */
    public function error()
    {
        return self::$error;
    }

    /**
     * Devuelve el error almacenado si es que lo ha habido.
     *
     * @return string
     */
    public function error_msg()
    {
        return self::$error_msg;
    }

    /**
     * Cierra la conexión con la caché.
     */
    public function close()
    {
        if (isset(self::$memcache) && self::$connected) {
            self::$memcache->close();
        }
    }

    /**
     * Asigna una clave y valor a la cache.
     *
     * @param        $key
     * @param object $object
     * @param int    $expire
     */
    public function set($key, $object, $expire = 0)
    {
        if (self::$connected) {
            self::$memcache->set(FS_CACHE_PREFIX . $key, $object, false, $expire);
        } else {
            self::$XnetPhpFileCache->put($key, $object);
        }
    }

    /**
     * Obtiene el valor de una clave de cache.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get($key)
    {
        if (self::$connected) {
            return self::$memcache->get(FS_CACHE_PREFIX . $key);
        }

        return self::$XnetPhpFileCache->get($key);
    }

    /**
     * Devuelve un array almacenado en cache.
     *
     * @param string $key
     *
     * @return array
     */
    public function get_array($key)
    {
        $items = [];

        if (self::$connected) {
            $item = self::$memcache->get(FS_CACHE_PREFIX . $key);
            if ($item) {
                $items = $item;
            }
        } else {
            $item = self::$XnetPhpFileCache->get($key);
            if ($item) {
                $items = $item;
            }
        }

        return $items;
    }

    /**
     * Devuelve un array almacenado en cache, tal y como get_[], pero con la diferencia
     * de que si no se encuentra en cache, se pone $error a true.
     *
     * @param string $key
     * @param bool   $error
     *
     * @return array
     */
    public function get_array2($key, &$error)
    {
        $items = [];
        $error = true;

        if (self::$connected) {
            $item = self::$memcache->get(FS_CACHE_PREFIX . $key);
            if (is_array($item)) {
                $items = $item;
                $error = false;
            }
        } else {
            $item = self::$XnetPhpFileCache->get($key);
            if (is_array($item)) {
                $items = $item;
                $error = false;
            }
        }

        return $items;
    }

    /**
     * Elimina una clave de la caché.
     *
     * @param string $key
     *
     * @return bool
     */
    public function delete($key)
    {
        if (self::$connected) {
            return self::$memcache->delete(FS_CACHE_PREFIX . $key);
        }

        return self::$XnetPhpFileCache->delete($key);
    }

    /**
     * Elimina multiples claves de la caché.
     *
     * @param string[] $keys
     *
     * @return bool
     */
    public function delete_multi($keys)
    {
        $done = false;

        if (self::$connected) {
            foreach ($keys as $i => $value) {
                $done = self::$memcache->delete(FS_CACHE_PREFIX . $value);
            }
        } else {
            foreach ($keys as $i => $value) {
                $done = self::$XnetPhpFileCache->delete($value);
            }
        }

        return $done;
    }

    /**
     * Vacía toda la caché.
     *
     * @return bool
     */
    public static function clean()
    {
        if (self::$connected) {
            return self::$memcache->flush();
        }

        return self::$XnetPhpFileCache->flush();
    }

    /**
     * Devuelve la versión de caché en uso.
     *
     * @return string
     */
    public function version()
    {
        if (self::$connected) {
            return 'Memcache ' . self::$memcache->getVersion();
        }

        return 'Files';
    }

    /**
     * Devuelve si está conectada a la caché.
     *
     * @return bool
     */
    public function connected()
    {
        return self::$connected;
    }
}
