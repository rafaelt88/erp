<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

/**
 * Description of fs_core_log
 */
class XnetLog
{
    const LOG_FILE = BASE_PATH . DIRECTORY_SEPARATOR . 'in_maintenance.log';

    /**
     * Nombre del controlador que inicia este log.
     *
     * @var string
     */
    private static $controller_name;

    /**
     * Array de mensajes.
     *
     * @var array
     */
    private static $data_log;

    /**
     * Usuario que ha iniciado sesión.
     *
     * @var string
     */
    private static $user_nick;

    /**
     * fs_core_log constructor.
     *
     * @param string|null $controller_name
     */
    public function __construct($controller_name = null)
    {
        if (isset($controller_name) && !isset(self::$data_log)) {
            self::$controller_name = $controller_name;
            self::$data_log = [];

            if ((constant('FS_DB_HISTORY') === true || constant('FS_DEBUG') === true)) {
                file_put_contents(self::LOG_FILE, '[' . date('Y-m-d H:i:s') . ']: Log iniciado' . PHP_EOL);
            }
        }
    }

    /**
     * Limpia todos los mensajes en el canal de advertencias.
     */
    public function clean_advices()
    {
        self::clean('advices');
    }

    /**
     * Limpia todos los mensajes en el canal de errores.
     */
    public function clean_errors()
    {
        self::clean('errors');
    }

    /**
     * Limpia todos los mensajes en el canal de mensajes.
     */
    public function clean_messages()
    {
        self::clean('messages');
    }

    /**
     * Limpia todos los mensajes en el canal de SQL.
     */
    public function clean_sql_history()
    {
        self::clean('sql');
    }

    /**
     * Limpia todos los mensajes en el canal de guardado.
     */
    public function clean_to_save()
    {
        self::clean('save');
    }

    /**
     * Devuelve el nombre del controlador asociado.
     *
     * @return string
     */
    public function controller_name()
    {
        return self::$controller_name;
    }

    /**
     * Devuelve el listado de consejos a mostrar al usuario.
     *
     * @return array
     */
    public function get_advices()
    {
        return self::read('advices');
    }

    /**
     * Devuelve el listado de errores a mostrar al usuario.
     *
     * @return array
     */
    public function get_errors()
    {
        return self::read('errors');
    }

    /**
     * Devuelve el listado de mensajes a mostrar al usuario.
     *
     * @return array
     */
    public function get_messages()
    {
        return self::read('messages');
    }

    /**
     * Devuelve el historial de consultas SQL.
     *
     * @return array
     */
    public function get_sql_history()
    {
        return self::read('sql');
    }

    /**
     * Devuelve la lista de mensajes a guardar.
     *
     * @return array
     */
    public function get_to_save()
    {
        return self::read('save', true);
    }

    /**
     * Añade un consejo al listado.
     *
     * @param string $msg
     * @param array  $context
     */
    public static function new_advice($msg, $context = [])
    {
        self::log($msg, 'advices', $context);
    }

    /**
     * Añade un mensaje de error al listado.
     *
     * @param string $msg
     * @param array  $context
     */
    public static function new_error($msg, $context = [])
    {
        self::log($msg, 'errors', $context);
    }

    /**
     * Añade un mensaje al listado.
     *
     * @param string $msg
     * @param array  $context
     */
    public static function new_message($msg, $context = [])
    {
        self::log($msg, 'messages', $context);
    }

    /**
     * Añade una consulta SQL al historial.
     *
     * @param string $sql
     */
    public static function new_sql($sql)
    {
        self::log($sql, 'sql');
    }

    /**
     * Añade un mensaje para guardar después con el fs_log_manager.
     *
     * @param string $msg
     * @param string $type
     * @param bool   $alert
     * @param array  $context
     */
    public function save($msg, $type = 'error', $alert = false, $context = [])
    {
        $context = [
            'alert' => $alert,
            'type' => $type,
        ];
        self::log($msg, 'save', $context);
    }

    /**
     * Añade un mensaje para guardar después con el fs_log_manager.
     *
     * @param string $msg
     * @param string $type
     * @param bool   $alert
     * @param array  $context
     */
    public static function debug($msg, $type = 'error', $alert = false, $context = [])
    {
        if (constant('FS_DEBUG')) {
            $context = [
                'alert' => $alert,
                'type' => $type,
            ];
            self::log($msg, 'debug', $context);
        }
    }

    /**
     * Asigna el nick indicado del usuario.
     *
     * @param string $nick
     */
    public static function set_user_nick($nick)
    {
        self::$user_nick = $nick;
    }

    /**
     * Devuelve el nick del usuario.
     *
     * @return string
     */
    public static function user_nick()
    {
        return self::$user_nick;
    }

    /**
     * Limpia todos los mensajes en el canal indicado.
     *
     * @param string $channel
     */
    private static function clean($channel)
    {
        foreach (self::$data_log as $key => $value) {
            if ($value['channel'] === $channel) {
                unset(self::$data_log[$key]);
            }
        }
    }

    /**
     * Devuelve la lista de mensajes en el canal indicado.
     *
     * @param string $channel
     * @param bool   $full
     *
     * @return array
     */
    private static function read($channel, $full = false)
    {
        $messages = [];
        foreach (self::$data_log as $data) {
            if ($data['channel'] === $channel) {
                $messages[] = $full ? $data : $data['message'];
            }
        }

        return $messages;
    }

    /**
     * Devuelve la lista de mensajes en todos los canales.
     *
     * @return array
     */
    public static function get_all()
    {
        return self::$data_log;
    }

    /**
     * Añade el mensaje al listado del canal.
     *
     * @param string $msg
     * @param string $channel
     * @param array  $context
     */
    private static function log($msg, $channel, $context = [])
    {
        self::$data_log[] = [
            'channel' => $channel,
            'context' => $context,
            'message' => $msg,
            'time' => time(),
        ];
        if ((constant('FS_DB_HISTORY') === true || constant('FS_DEBUG') === true) && $channel != 'sql') {
            self::add_log_message('[' . date('Y-m-d H:i:s') . '] ' . $channel . ': ' . $msg);
        }
    }

    private static function add_log_message($message)
    {
        file_put_contents(self::LOG_FILE, $message . PHP_EOL, FILE_APPEND);
    }
}
