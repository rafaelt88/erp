<?php
/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

namespace Xnet\Core;

use Xnet\Model\Controller;

/**
 * Class XnetAppController
 *
 * Este es el nivel de controlador más fundamental.
 * Tiene lo básico para que el controlador funcione.
 * Añade el soporte básico para utilizar los elementos generales (barra de depuración, caché, logs,...)
 * Aunque se puede utilizar, no registra la página en "Controllers", ni añade opción de
 * menú, ni requiere autenticación.
 * Las vistas no están preparadas para utilizar este controlador.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0724
 *
 * @package Xnet\Core
 */
abstract class XnetAppController
{
    const ACTION_PREFIX = 'action';

    /**
     * Páginas modales de finalización del código.
     */
    public const BREAKING_PAGES = [
        'access_denied',
        'broken',
        'coming-soon',
        'maintenance',
    ];

    /**
     * Nombre del controlador (lo utilizamos en lugar de __CLASS__ porque __CLASS__
     * en las funciones de la clase padre es el nombre de la clase padre).
     *
     * @var string
     */
    protected $class_name;

    /**
     * Permite calcular cuanto tarda en procesarse la página.
     *
     * @var string
     */
    private $uptime;

    /**
     * Barra de depuración.
     *
     * @var XnetDebugBar
     */
    public $debugBar;

    /**
     * Identificador para la caché.
     *
     * @var string
     */
    public $id_cache;

    /**
     * Este objeto permite interactuar con memcache
     *
     * @var XnetCache
     */
    protected $cache;

    /**
     * En desarrollo, puede interesar que la caché de ficheros YAML genere los ficheros
     * pero no los use, pues se van haciendo cambios en las estructuras y configuraciones
     * que pueden crear confusión si la caché está operativa.
     *
     * Esta propiedad se pondrá a TRUE si se define en la configuración la siguiente constante:
     * define('DISABLE_YAML_CACHE', TRUE);
     *
     * @var bool
     */
    protected $disableYamlCache;

    /**
     * Este objeto contiene los mensajes, errores y consejos volcados por controladores,
     * modelos y base de datos.
     *
     * @var XnetLog
     */
    protected $coreLog;

    /**
     * TODO: Missing documentation
     *
     * @var XnetLogManager
     */
    protected $logManager;

    /**
     * Guarda la ruta relativa del controlador.
     *
     * @var string
     */
    private $path;

    /**
     * Nombre del fichero de ayuda, si existe.
     *
     * @var string
     */
    public $helpfile;

    /**
     * Indica que archivo HTML hay que cargar
     *
     * @var string|false
     */
    public $template;

    /**
     * Indica si hay que ocultar el título (si está incrustada en un panel)
     *
     * @var bool
     */
    public $hide_title;

    public function __construct()
    {
        require_all_models();

        $tiempo = explode(' ', microtime());
        $this->uptime = $tiempo[1] + $tiempo[0];

        $this->template = "errors/maintenance";

        $this->disableYamlCache = XnetPolicy::yamlCacheDisabled();
        $fullname = get_called_class();
        $namearray = explode('\\', $fullname);
        $name = end($namearray);
        $this->class_name = $name;

        $this->debugBar = new XnetDebugBar();
        $this->cache = new XnetCache();
        $this->coreLog = new XnetLog($this->class_name);
        $this->logManager = new XnetLogManager();

        $this->id_cache = $this->get_id_cache();

        if ($this->disableYamlCache) {
            debug_message('DISABLE_YAML_CACHE. La caché de archivos YAML está desactivada. Los ficheros se usarán, pero se generarán previamente cada vez.');
        }
    }

    /**
     * Establece el path relativo del controlador.
     *
     * @param $path
     *
     * @return void
     */
    public function setPath($path)
    {
        $this->path = $path;
        $this->helpfile = 'docs/user/book' . $this->path . '/' . $this->class_name . '.html';
    }

    /**
     * Devuelve la lista de errores
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0726
     *
     * @return array
     */
    public function getErrors()
    {
        $result = $this->coreLog->get_errors();
        //        dump($result);
        return $result;
    }

    /**
     * Devuelve la lista de mensajes
     *
     * @return array lista de mensajes
     */
    public function getMessages()
    {
        $result = $this->coreLog->get_messages();
        //        dump($result);
        return $result;
    }

    /**
     * Devuelve la lista de consejos
     *
     * @return array lista de consejos
     */
    public function getAdvices()
    {
        $result = $this->coreLog->get_advices();
        //        dump($result);
        return $result;
    }

    /**
     * Devuelve la lista de errores
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0726
     *
     * @return array
     *
     * @deprecated Utilice getErrors()
     */
    public function get_errors()
    {
        return $this->getErrors();
    }

    /**
     * Devuelve la lista de mensajes
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0726
     *
     * @return array
     *
     * @deprecated Utilice getMessages()
     */
    public function get_messages()
    {
        return $this->getMessages();
    }

    /**
     * Devuelve la lista de avisos
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0726
     *
     * @return array
     *
     * @deprecated Utilice getAdvices()
     */
    public function get_advices()
    {
        return $this->getAdvices();
    }

    /**
     * Devuelve la versión de MiFactura.eu
     *
     * @return float versión de MiFactura.eu
     */
    public function version()
    {
        return (float) file_exists('MiVersion') ? trim(@file_get_contents('MiVersion')) : '0';
    }

    /**
     * Devuelve información sobre la comunidad configurada en la instalación.
     *
     * @return false|array|mixed|string|null
     */
    public function get_community_info()
    {
        $result = $this->cache->get('community_info');
        if (!isset($result)) {
            $result = false;
            $communityInfo = get_from_community('/info.json', 30);
            if (!empty($communityInfo)) {
                $this->cache->set('community_info', $communityInfo);
                $result = $communityInfo;
            }
            // No se está pudiendo contactar con la comunidad
        }
        return $result;
    }

    /**
     * Devuelve si la instalación en cuestión está en beta.
     *
     * @return false
     */
    public function is_beta()
    {
        $info = $this->get_community_info();
        return $info && isset($info->beta) ? $info->beta : false;
    }

    /**
     * He detectado que algunos navegadores, en algunos casos, envían varias veces la
     * misma petición del formulario. En consecuencia se crean varios modelos (asientos,
     * albaranes, etc...) con los mismos datos, es decir, duplicados.
     * Para solucionarlo añado al formulario un campo petition_id con una cadena
     * de texto aleatoria. Al llamar a esta función se comprueba si esa cadena
     * ya ha sido almacenada, de ser así devuelve TRUE, así no hay que gabar los datos,
     * si no, se almacena el ID y se devuelve FALSE.
     *
     * @param string $pid el identificador de la petición
     *
     * @return bool TRUE si la petición está duplicada
     */
    protected function duplicated_petition($pid)
    {
        $ids = $this->cache->get_array('petition_ids');
        if (in_array($pid, $ids)) {
            return true;
        }

        $ids[] = $pid;
        $this->cache->set('petition_ids', $ids, 300);
        return false;
    }

    /**
     * Busca en la lista de plugins activos, en orden inverso de prioridad
     * (el último plugin activo tiene más prioridad que el primero)
     * y nos devuelve la ruta del archivo javascript que le solicitamos.
     * Así usamos el archivo del plugin con mayor prioridad.
     *
     * @param string $filename
     *
     * @return string
     */
    public function get_js_location($filename)
    {
        foreach (XnetConfig::getEnabledPlugins() as $plugin) {
            switch (ENGINE) {
                case 'raintpl':
                    if (file_exists('plugins/' . $plugin . '/view/js/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/view/js/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
                case 'blade':
                    if (file_exists('plugins/' . $plugin . '/Templates/js/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/Templates/js/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
            }
        }

        /// si no está en los plugins estará en el núcleo
        switch (ENGINE) {
            case 'raintpl':
                if (file_exists('view/js/' . $filename)) {
                    return constant('FS_PATH') . 'view/js/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'view/js/' . $filename . '?idcache=' . $this->id_cache;
                break;
            case 'blade':
                if (file_exists('Templates/js/' . $filename)) {
                    return constant('FS_PATH') . 'Templates/js/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'Templates/js/' . $filename . '?idcache=' . $this->id_cache;
                break;
        }

        return '#NOTFOUND-' . $filename . '?idcache=' . $this->id_cache;
    }

    /**
     * Busca en la lista de plugins activos, en orden inverso de prioridad
     * (el último plugin activo tiene más prioridad que el primero)
     * y nos devuelve la ruta del archivo css que le solicitamos.
     * Así usamos el archivo del plugin con mayor prioridad.
     *
     * @param string $filename
     *
     * @return string
     */
    public function get_css_location($filename)
    {
        foreach (XnetConfig::getEnabledPlugins() as $plugin) {
            switch (ENGINE) {
                case 'raintpl':
                    if (file_exists('plugins/' . $plugin . '/view/css/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/view/csss/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
                case 'blade':
                    if (file_exists('plugins/' . $plugin . '/Templates/css/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/Templates/css/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
            }
        }

        /// si no está en los plugins estará en el núcleo
        switch (ENGINE) {
            case 'raintpl':
                if (file_exists('view/css/' . $filename)) {
                    return constant('FS_PATH') . 'view/css/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'view/css/' . $filename . '?idcache=' . $this->id_cache;

                break;
            case 'blade':
                if (file_exists('Templates/css/' . $filename)) {
                    return constant('FS_PATH') . 'Templates/css/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'Templates/css/' . $filename . '?idcache=' . $this->id_cache;

                break;
        }
        return '#NOTFOUND-' . $filename . '?idcache=' . $this->id_cache;
    }

    /**
     * Devuelve la duración de la ejecución de la página
     *
     * @return string
     */
    public function duration()
    {
        $tiempo = explode(" ", microtime());
        return (number_format($tiempo[1] + $tiempo[0] - $this->uptime, 3) . ' s');
    }

    /**
     * Devuelve el número de transacciones SQL que se han ejecutado
     *
     * @return int
     */
    public function transactions()
    {
        return DBSchema::getTransactions();
    }

    /**
     * Devuelve el número de consultas SQL (SELECT) que se han ejecutado
     *
     * @return int
     */
    public function selects()
    {
        return DBSchema::getSelects();
    }

    /**
     * Devuelve la versión de la app.
     * Si está en modo depuración, devuelve la que debería contener el archivo VERSION.
     *
     * @return string
     */
    public function get_app_version()
    {
        $version = (string) number_format((float) $this->version(), 4, '.', '');
        if ($this->is_beta()) {
            $version .= "-beta";
        }
        if (defined('FS_DEBUG') && constant('FS_DEBUG')) {
            $number = date('Y') . '.'
                . str_pad(date('m'), 2, '0', STR_PAD_LEFT)
                . str_pad(date('d'), 2, '0', STR_PAD_LEFT);
            $version_dev = (string) number_format((float) $number, 4, '.', '');
            if ((float) $version < (float) $version_dev) {
                $version .= ' (' . $version_dev . '-dev)';
            }
        }
        return $version;
    }

    /**
     * Este método es invocado desde fs_index tras cargar la clase.
     * Es posible que action tenga un valor distinto a view.
     * El view hay que hacerlo siempre, porque es el que pinta la página.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0829
     *
     * @return bool
     */
    public function actionView(): bool
    {
        if ($this->template) {
            $engine = 'blade';

            $tmpFolder = XnetConfig::getTmpFolder();

            /// ¿Se puede escribir sobre la carpeta temporal?
            if (!is_writable($tmpFolder)) {
                die("<h1>No se puede escribir sobre la carpeta '$tmpFolder' de MiFactura.eu</h1>");
            }

            $vars = [];
            $vars['fsc'] = $this;

            switch ($engine) {
                case 'blade':
                    $blade = new Blade(Blade::getRoutes(), $tmpFolder . '/' . constant('FS_TMP_NAME'));

                    if (method_exists($this, 'get_db_history')) {
                        $this->get_db_history();
                    }
                    try {
                        debug_message('Usando plantilla: ' . $this->template);
                        echo $blade->render($this->template, $vars);
                    } catch (Exception $e) {
                        if (constant('FS_DEBUG')) {
                            // $this->new_error_msg($e->getMessage());
                        }
                        echo $blade->render($this->template, $vars);
                    }
                    break;
                case 'raintpl':
                    /// configuramos rain.tpl
                    raintpl::configure('base_url', null);
                    raintpl::configure('tpl_dir', 'view/');
                    raintpl::configure('path_replace', false);
                    raintpl::configure('cache_dir', constant('TMP_FOLDER') . '/' . constant('FS_TMP_NAME'));

                    $tpl = new RainTPL();

                    foreach ($vars as $name => $var) {
                        $tpl->assign($name, $var);
                    }

                    $tpl->draw($this->template);
                    break;
                default:
                    dump("Engine '$engine' not found!");
            }
        }
        return true;
    }

    /**
     * Lo necesita la plantilla, pero en este punto, no tenemos información
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0724
     *
     * @param $coddivisa
     *
     * @return string
     */
    public function simbolo_divisa($coddivisa = false)
    {
        return '€';
    }

    /**
     * Lo necesita la plantilla, pero en este punto, no tenemos información
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0724
     *
     * @return false
     */
    public function check_for_updates()
    {
        return false;
    }

    public function get_icon_controller()
    {
        return '';
    }

    public function url()
    {
        return 'index.php?page=' . $this->class_name;
    }

    /**
     *
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @return mixed|string
     */
    private function get_id_cache()
    {
        /// necesitamos un id que se cambie al limpiar la caché
        $idcache = $this->cache->get('fs_idcache');
        if (!$idcache) {
            $idcache = random_string(10);
            $this->cache->set('fs_idcache', $idcache, 30 * 24 * 60 * 60);
        }
        return $idcache;
    }

    public function view()
    {
        $this->doAction('View');
    }

    public function canExecute($action)
    {
        return true;
    }

    /**
     * Ejecuta una acción.
     * Existen dos tipos de acciones según se defina el método.
     * - do{Action} se ejecuta siempre, no necesita controlar si tiene acceso.
     * - action{Action} se ejecuta sólo, si el usuario tiene acceso a dicha acción.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0809
     *
     * @return bool
     */
    protected function doAction($action): bool
    {
        if (empty($action)) {
            return true;
        }

        /**
         * Acciones que comienzan con do (no se controla acceso).
         * Acciones permitidas para todos.
         */
        $method = 'do' . $action;
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }

        /**
         * Acciones que comienzan por $prefix, sí se controla acceso.
         */
        $method = self::ACTION_PREFIX . $action;
        if (!method_exists($this, $method)) {
            $this->new_error_msg('Acción ' . $action . ' no definida');
            return false;
        }

        // Verificar si el usuario tiene permiso para ejecutar la acción.
        if (!$this->canExecute($action)) {
            // $this->template = 'access-denied';
            return false;
        }

        return $this->{$method}();
    }

    /**
     * Muestra un mensaje al usuario
     *
     * @param string $msg
     * @param bool   $save
     * @param string $tipo
     * @param bool   $alerta
     */
    public function new_message($msg, $save = false, $tipo = 'message', $alerta = false)
    {
        return $this->newMessage($msg, $save, $tipo, $alerta);
    }

    /**
     * Muestra un consejo al usuario
     *
     * @param string $msg el consejo a mostrar
     */
    public function new_advice($msg, $save = true, $tipo = 'advice', $alerta = false)
    {
        return $this->newAdvice($msg, $save, $tipo, $alerta);
    }

    public function newMessage($msg, $save = false, $tipo = 'message', $alerta = false)
    {
        if ($this->class_name == $this->coreLog->controller_name()) {
            /// solamente nos interesa mostrar los mensajes del controlador que inicia todo
            $this->coreLog->new_message($msg);
        }

        if ($save) {
            $this->coreLog->save($msg, $tipo, $alerta);
        }
    }

    public function newAdvice($msg, $save = true, $tipo = 'advice', $alerta = false)
    {
        if ($this->class_name == $this->coreLog->controller_name()) {
            /// solamente nos interesa mostrar los mensajes del controlador que inicia todo
            $this->coreLog->new_advice($msg);
        }

        if ($save) {
            $this->coreLog->save($msg, $tipo, $alerta);
        }
    }

    public function newError($msg, $tipo = 'error', $alerta = false, $guardar = true)
    {
        if ($this->class_name == $this->coreLog->controller_name()) {
            /// solamente nos interesa mostrar los mensajes del controlador que inicia todo
            $this->coreLog->new_error($msg);
        }

        if ($guardar) {
            $this->coreLog->save($msg, $tipo, $alerta);
        }
    }

    /**
     * Muestra al usuario un mensaje de error
     *
     * @param string $msg el mensaje a mostrar
     */
    public function new_error_msg($msg, $tipo = 'error', $alerta = false, $guardar = true)
    {
        return $this->newError($msg, $tipo, $alerta, $guardar);
    }

    public function getClassName(): string
    {
        return $this->class_name;
    }
}
