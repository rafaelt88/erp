<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

/**
 * Description of fs_log_manager
 */
class XnetLogManager
{
    /**
     * Gestiona el log de todos los controladores, modelos y base de datos.
     *
     * @var XnetLog
     */
    private $core_log;

    /**
     * fs_log_manager constructor.
     */
    public function __construct()
    {
        $this->core_log = new XnetLog();
    }

    /**
     * Guarda el mensaje en el log.
     */
    public function save()
    {
        foreach ($this->core_log->get_to_save() as $data) {
            $new_log = new fs_log();
            $new_log->alerta = $data['context']['alert'];
            $new_log->controlador = $this->core_log->controller_name();
            $new_log->detalle = $data['message'];
            $new_log->fecha = date('Y-m-d H:i:s', $data['time']);
            $new_log->ip = fs_get_ip();
            $new_log->tipo = $data['context']['type'];
            $new_log->usuario = $this->core_log->user_nick();
            $new_log->save();
        }

        $this->core_log->clean_to_save();
    }
}
