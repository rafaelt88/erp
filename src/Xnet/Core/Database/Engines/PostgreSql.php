<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core\Database\Engines;

use Xnet\Core\Database\Engine;

/**
 * Clase para conectar a PostgreSQL.
 */
class PostgreSql extends Engine
{
    /**
     * Debería realizar comprobaciones extra, pero en PostgreSQL no es necesario.
     *
     * @param string $table_name
     *
     * @return bool
     */
    public function check_table_aux($table_name)
    {
        return true;
    }

    /**
     * Compara dos arrays de columnas, devuelve una sentencia SQL en caso de encontrar diferencias.
     *
     * @param string $table_name
     * @param array  $xml_cols
     * @param array  $db_cols
     *
     * @return string
     */
    public function compare_columns($table_name, $xml_cols, $db_cols)
    {
        $sql = '';

        foreach ($xml_cols as $xml_col) {
            $db_col = $this->search_in_array($db_cols, 'name', $xml_col['nombre']);
            if (empty($db_col)) {
                /// columna no encontrada en $db_cols. La creamos
                $sql .= 'ALTER TABLE ' . $table_name . ' ADD COLUMN "' . $xml_col['nombre'] . '" ' . $xml_col['tipo'];
                $sql .= ($xml_col['defecto'] !== null) ? ' DEFAULT ' . $xml_col['defecto'] : '';
                $sql .= ($xml_col['nulo'] == 'NO') ? ' NOT NULL;' : ';';
                continue;
            }

            // TODO: Ésto no se ha probado en postgres.
            if (mb_strtolower($xml_col['tipo']) === 'relationship') {
                $xml_col['tipo'] = constant('FS_INDEX_TYPE');
            }

            /// columna ya presente en db_cols. La modificamos
            if (!$this->compare_data_types($db_col['type'], $xml_col['tipo'])) {
                $sql .= 'ALTER TABLE ' . $table_name . ' ALTER COLUMN "' . $xml_col['nombre'] . '" TYPE ' . $xml_col['tipo'] . ';';
            }

            if ($db_col['default'] == $xml_col['defecto']) {
                /// do nothing
            } elseif (is_null($xml_col['defecto'])) {
                $sql .= 'ALTER TABLE ' . $table_name . ' ALTER COLUMN "' . $xml_col['nombre'] . '" DROP DEFAULT;';
            } else {
                $this->default2check_sequence($table_name, $xml_col['defecto'], $xml_col['nombre']);
                $sql .= 'ALTER TABLE ' . $table_name . ' ALTER COLUMN "' . $xml_col['nombre'] . '" SET DEFAULT ' . $xml_col['defecto'] . ';';
            }

            if ($db_col['is_nullable'] == $xml_col['nulo']) {
                /// do nothing
            } elseif ($xml_col['nulo'] == 'YES') {
                $sql .= 'ALTER TABLE ' . $table_name . ' ALTER COLUMN "' . $xml_col['nombre'] . '" DROP NOT NULL;';
            } else {
                $sql .= 'ALTER TABLE ' . $table_name . ' ALTER COLUMN "' . $xml_col['nombre'] . '" SET NOT NULL;';
            }
        }

        return $sql;
    }

    /**
     * Ejecuta sentencias SQL sobre la base de datos (inserts, updates o deletes).
     * Para hacer selects, mejor usar select() o selec_limit().
     * Por defecto se inicia una transacción, se ejecutan las consultas, y si
     * sale bien, se guarda, sinó se deshace.
     * Se puede evitar este modo de transacción si se pone false
     * en el parámetro transaction.
     *
     * @param string $sql
     * @param bool   $transaction
     *
     * @return bool
     */
    public function _exec($sql, $transaction = true)
    {
        $result = false;

        if (self::$link) {
            /// añadimos la consulta sql al historial
            self::$core_log->new_sql($sql);

            if ($transaction) {
                $this->begin_transaction();
            }

            $aux = @pg_query(self::$link, $sql);
            if ($aux) {
                pg_free_result($aux);
                $result = true;
            } else {
                $errno = pg_result_error(self::$link);
                $posicion = count(self::$core_log->get_sql_history());
                // @doc https://www.postgresql.org/docs/7.4/errcodes-appendix.html
                switch ($errno) {
                    // case '23000': // Debe ser la equivalente a 1451 de MySQL
                    // case '40002': // Debe ser la equivalente a 1451 de MySQL
                    //     $error = '<b>No se puede eliminar el registro indicado. Está intentando eliminar un registro que ya tiene datos relacionados con él.</b>';
                    //     break;
                    default:
                        $error = pg_last_error(self::$link) . '. La secuencia ocupa la posición ' . $posicion;
                        break;
                }

                if (constant('FS_DB_HISTORY') || constant('FS_DEBUG')) {
                    $error .= '<br><br><b>DEBUG</b><pre>' . str_replace(';', ';' . PHP_EOL, self::$core_log->get_sql_history()[$posicion - 1]) . '</pre>';
                }

                self::$core_log->new_error($error);
                self::$core_log->save($error);
            }

            if ($transaction) {
                if ($result) {
                    $this->commit();
                } else {
                    $this->rollback();
                }
            }
        }

        return $result;
    }

    /**
     * Inicia una transacción SQL.
     *
     * @return bool
     */
    public function begin_transaction()
    {
        return self::$link ? (bool) pg_query(self::$link, 'BEGIN TRANSACTION;') : false;
    }

    /**
     * Guarda los cambios de una transacción SQL.
     *
     * @return bool
     */
    public function commit()
    {
        if (self::$link) {
            /// aumentamos el contador de selects realizados
            self::$t_transactions++;
            return (bool) pg_query(self::$link, 'COMMIT;');
        }

        return false;
    }

    /**
     * Deshace los cambios de una transacción SQL.
     *
     * @return bool
     */
    public function rollback()
    {
        return self::$link ? (bool) pg_query(self::$link, 'ROLLBACK;') : false;
    }

    /**
     * Conecta a la base de datos.
     *
     * @return bool
     */
    public static function _connect():bool
    {
        $connected = false;

        if (self::$link) {
            $connected = true;
        } elseif (function_exists('pg_connect')) {
            self::$link = pg_connect(
                'host = ' . constant('FS_DB_HOST') .
                ' dbname = ' . \fs_app::get_db_name() .
                ' port = ' . constant('FS_DB_PORT') .
                ' user = ' . constant('FS_DB_USER') .
                ' password = ' . constant('FS_DB_PASS')
            );
            if (self::$link) {
                $connected = true;

                /// establecemos el formato de fecha para la conexión
                pg_query(self::$link, "SET DATESTYLE TO ISO, DMY;");
            }
        } else {
            self::$core_log->new_error('No tienes instalada la extensión de PHP para PostgreSQL . ');
        }

        return $connected;
    }

    /**
     * Desconecta de la base de datos.
     *
     * @return bool
     */
    public function close()
    {
        if (self::$link) {
            $return = pg_close(self::$link);
            self::$link = null;
            return $return;
        }

        return true;
    }

    /**
     * Devuelve el estilo de fecha del motor de base de datos.
     *
     * @return string
     */
    public function date_style()
    {
        return 'Y-m-d';
    }

    /**
     * Escapa las comillas de la cadena de texto.
     *
     * @param string $str
     *
     * @return string
     */
    public function escape_string($str)
    {
        return self::$link ? pg_escape_string(self::$link, $str) : $str;
    }

    /**
     * Devuelve la sentencia SQL necesaria para crear una tabla con la estructura proporcionada.
     *
     * @param string $table_name
     * @param array  $xml_cols
     * @param array  $xml_cons
     *
     * @return string
     */
    public function generate_table($table_name, $xml_cols, $xml_cons)
    {
        $sql = 'CREATE TABLE ' . $table_name . ' (';

        $i = false;
        foreach ($xml_cols as $col) {
            /// añade la coma al final
            if ($i) {
                $sql .= ', ';
            } else {
                $i = true;
            }

            // TODO: No probado en postgre
            if (mb_strtolower($col['tipo']) === 'relationship') {
                $col['tipo'] = constant('DB_INDEX_TYPE');
            }

            $sql .= '"' . $col['nombre'] . '" ' . $col['tipo'];

            if ($col['nulo'] == 'NO') {
                $sql .= ' NOT null';
            }

            if ($col['defecto'] !== null && !in_array($col['tipo'], ['serial', 'bigserial',])) {
                $sql .= ' default ' . $col['defecto'];
            }
        }

        return $sql . ' ); ' . $this->compare_constraints($table_name, $xml_cons, false);
    }

    /**
     * Compara dos arrays de restricciones, devuelve una sentencia SQL en caso de encontrar diferencias.
     *
     * @param string $table_name
     * @param array  $xml_cons
     * @param array  $db_cons
     * @param bool   $delete_only
     *
     * @return string
     */
    public function compare_constraints($table_name, $xml_cons, $db_cons, $delete_only = false)
    {
        $sql = '';

        if (!empty($db_cons)) {
            /// comprobamos una a una las viejas
            foreach ($db_cons as $db_con) {
                $xml_con = $this->search_in_array($xml_cons, 'nombre', $db_con['name']);
                if (empty($xml_con)) {
                    /// eliminamos la restriccion
                    $sql .= "ALTER TABLE " . $table_name . " DROP CONSTRAINT " . $db_con['name'] . ";";
                }
            }
        }

        if (!empty($xml_cons) && !$delete_only) {
            /// comprobamos una a una las nuevas
            foreach ($xml_cons as $xml_con) {
                $db_con = $this->search_in_array($db_cons, 'name', $xml_con['nombre']);
                if (empty($db_con)) {
                    /// añadimos la restriccion
                    $sql .= "ALTER TABLE " . $table_name . " ADD CONSTRAINT " . $xml_con['nombre'] . " " . $xml_con['consulta'] . ";";
                }
            }
        }

        return $sql;
    }

    /**
     * Devuelve un array con las columnas de una tabla dada.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_columns($table_name)
    {
        $columns = [];
        $sql = "SELECT column_name as name, data_type as type, character_maximum_length, column_default as default, is_nullable"
            . " FROM information_schema.columns WHERE table_catalog = '" . \fs_app::get_db_name() . "' AND table_name = '" . $table_name . "'"
            . " ORDER BY name ASC;";

        $aux = $this->select($sql);
        if ($aux) {
            foreach ($aux as $d) {
                $d['extra'] = null;

                /// añadimos la longitud, si tiene
                if ($d['character_maximum_length']) {
                    $d['type'] .= '(' . $d['character_maximum_length'] . ')';
                    unset($d['character_maximum_length']);
                }

                $columns[] = $d;
            }
        }

        return $columns;
    }

    /**
     * Devuelve una array con las restricciones de una tabla dada:
     * clave primaria, claves ajenas, etc.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_constraints($table_name)
    {
        $constraints = [];
        $sql = "SELECT tc.constraint_name as name, tc.constraint_type as type"
            . " FROM information_schema.table_constraints AS tc"
            . " WHERE tc.table_name = '" . $table_name . "' AND tc.constraint_type IN"
            . " ('PRIMARY KEY','FOREIGN KEY','UNIQUE')"
            . " ORDER BY type DESC, name ASC;";

        $aux = $this->select($sql);
        if ($aux) {
            foreach ($aux as $a) {
                $constraints[] = $a;
            }
        }

        return $constraints;
    }

    /**
     * Devuelve una array con las restricciones de una tabla dada, pero aportando muchos más detalles.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_constraints_extended($table_name)
    {
        $constraints = [];
        $sql = "SELECT tc.constraint_name as name,
            tc.constraint_type as type,
            kcu.column_name,
            ccu.table_name AS foreign_table_name,
            ccu.column_name AS foreign_column_name,
            rc.update_rule AS on_update,
            rc.delete_rule AS on_delete
         FROM information_schema.table_constraints AS tc
         LEFT JOIN information_schema.key_column_usage AS kcu
            ON kcu.constraint_schema = tc.constraint_schema
            AND kcu.constraint_catalog = tc.constraint_catalog
            AND kcu.constraint_name = tc.constraint_name
         LEFT JOIN information_schema.constraint_column_usage AS ccu
            ON ccu.constraint_schema = tc.constraint_schema
            AND ccu.constraint_catalog = tc.constraint_catalog
            AND ccu.constraint_name = tc.constraint_name
            AND ccu.column_name = kcu.column_name
         LEFT JOIN information_schema.referential_constraints rc
            ON rc.constraint_schema = tc.constraint_schema
            AND rc.constraint_catalog = tc.constraint_catalog
            AND rc.constraint_name = tc.constraint_name
         WHERE tc.table_name = '" . $table_name . "' AND tc.constraint_type IN ('PRIMARY KEY','FOREIGN KEY','UNIQUE')
         ORDER BY type DESC, name ASC;";

        $aux = $this->select($sql);
        if ($aux) {
            foreach ($aux as $a) {
                $constraints[] = $a;
            }
        }

        return $constraints;
    }

    /**
     * Devuelve una array con los indices de una tabla dada.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_indexes($table_name)
    {
        $indexes = [];

        $aux = $this->select("SELECT indexname, indexdef FROM pg_indexes WHERE tablename = '" . $table_name . "';");
        if ($aux) {
            foreach ($aux as $a) {
                $indexes[] = [
                    'name' => $a['indexname'],
                    'table' => $table_name,
                    'column' => $a['indexdef'],
                ];
            }
        }

        return $indexes;
    }

    /**
     * Devuelve un array con los datos de bloqueos en la base de datos.
     *
     * @return array
     */
    public function get_locks()
    {
        $llist = [];
        $sql = "SELECT relname,pg_locks.* FROM pg_class,pg_locks WHERE relfilenode=relation AND NOT granted;";

        $aux = $this->select($sql);
        if ($aux) {
            foreach ($aux as $a) {
                $llist = $a;
            }
        }

        return $llist;
    }

    /**
     * Devuelve el último ID asignado al hacer un INSERT en la base de datos.
     *
     * @return int|false
     */
    public function lastval()
    {
        $aux = $this->select('SELECT lastval() as num;');
        return $aux ? $aux[0]['num'] : false;
    }

    /**
     * Devuelve un array con los nombres de las tablas de la base de datos.
     *
     * @return array
     */
    public function list_tables()
    {
        $tables = [];
        $sql = "SELECT *"
            . " FROM pg_catalog.pg_tables WHERE schemaname NOT IN "
            . "('pg_catalog','information_schema') ORDER BY tablename ASC;";

        $aux = $this->select($sql);
        if ($aux) {
            foreach ($aux as $a) {
                $tables[] = ['name' => $a['tablename']];
            }
        }

        return $tables;
    }

    /**
     * Ejecuta una sentencia SQL de tipo select, pero con paginación,
     * y devuelve un array con los resultados o false en caso de fallo.
     * Limit es el número de elementos que quieres que devuelva.
     * Offset es el número de resultado desde el que quieres que empiece.
     *
     * @param string $sql
     * @param int    $limit
     * @param int    $offset
     *
     * @return false|array
     */
    public function select_limit($sql, $limit = FS_ITEM_LIMIT, $offset = 0)
    {
        /// añadimos limit y offset a la consulta sql
        $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';
        return $this->select($sql);
    }

    /**
     * Devuelve el SQL necesario para convertir la columna a entero.
     *
     * @param string $col_name
     *
     * @return string
     */
    public function sql_to_int($col_name)
    {
        return $col_name . '::integer';
    }

    /**
     * Devuelve el motor de base de datos y la versión.
     *
     * @return bool
     */
    public function version()
    {
        if (self::$link) {
            $aux = pg_version(self::$link);
            return 'POSTGRESQL ' . $aux['server'];
        }

        return false;
    }

    /**
     * Ejecuta las SQL para el cambio de charset y collation y devuelve su resultado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function update_collation()
    {
        // No implementado
        return true;
    }

    /**
     * Devuelve los detalles de la constraint de una tabla.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @param $table_name
     * @param $name
     *
     * @return array
     */
    public function get_extended_constraint($table_name, $name)
    {
        $data = [];
        foreach ($this->get_constraints_extended($table_name) as $item) {
            if ($item['name'] == $name) {
                $data = $item;
                break;
            }
        }
        return $data;
    }

    /**
     * Compara los tipos de datos de una columna. Devuelve TRUE si son iguales.
     *
     * @param string $db_type
     * @param string $xml_type
     *
     * @return bool
     */
    private function compare_data_types($db_type, $xml_type)
    {
        if (FS_CHECK_DB_TYPES != 1) {
            /// si está desactivada la comprobación de tipos, devolvemos que son iguales.
            return true;
        } elseif ($db_type == $xml_type) {
            return true;
        } elseif (mb_strtolower($xml_type) == 'serial') {
            return true;
        } elseif (substr($db_type, 0, 4) == 'time' && substr($xml_type, 0, 4) == 'time') {
            return true;
        }

        return false;
    }

    /**
     * A partir del campo default del xml de una tabla
     * comprueba si se refiere a una secuencia, y si es así
     * comprueba la existencia de la secuencia. Si no la encuentra
     * la crea.
     *
     * @param string $table_name
     * @param string $default
     * @param string $colname
     */
    private function default2check_sequence($table_name, $default, $colname)
    {
        /// ¿Se refiere a una secuencia?
        if (mb_strtolower(substr($default, 0, 9)) == "nextval('") {
            $aux = explode("'", $default);

            /// ¿Existe esa secuencia?
            if (count($aux) == 3 && !$this->sequence_exists($aux[1])) {
                /// ¿En qué número debería empezar esta secuencia?
                $num = 1;
                $aux_num = $this->select("SELECT MAX(" . $colname . "::integer) as num FROM " . $table_name . ";");
                if ($aux_num) {
                    $num += intval($aux_num[0]['num']);
                }

                $this->exec("CREATE SEQUENCE " . $aux[1] . " START " . $num . ";");
            }
        }
    }

    /**
     * Devuelve TRUE si la secuancia solicitada existe.
     *
     * @param string $seq_name
     *
     * @return bool
     */
    private function sequence_exists($seq_name)
    {
        return (bool) $this->select("SELECT *"
            . " FROM pg_class where relname = '" . $seq_name . "';");
    }

    /**
     * Devuelve el SQL necesario para realizar una busqueda en columnas con acentos
     *
     * @param string $col_name
     * @param string $search
     * @param string $splitWord
     *
     * @return string
     */
    public function search_diacritic_insensitive($col_name, $search, $splitWord = '')
    {
        $search = $this->escape_string($search);
        $search = $splitWord ? explode($splitWord, $search) : [$search];
        $return = [];
        foreach ($search as $word) {
            $return[] = "{$col_name} like \"%{$word}%\"";
        }
        return "(" . implode(' AND ', $return) . ")";
    }
}
