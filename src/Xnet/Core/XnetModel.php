<?php
/**
 * Class XnetModel
 *
 * Es el nuevo sistema de modelos que sustituye a fs_extended_model y a fs_model
 *
 * De momento extiende de fs_model, pero la idea es sustituírlo por completo.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0708
 *
 */

namespace Xnet\Core;

use ReflectionClass;
use ReflectionException;
use stdClass;
use Xnet\Model\Version;

require_once BASE_PATH . '/base/fs_db2.php';

// TODO: Estos parámetros son necesarios en los test para tablas de tipo antiguo, por lo menos.
if (!defined('FS_DB_INTEGER')) {
    define('FS_DB_INTEGER', 'INTEGER');
    define('FS_FOREIGN_KEYS', 1);
    define('FS_CHECK_DB_TYPES', 1);
}

abstract class XnetModel
{
    public const ERROR = 'Error';
    public const OK = 'Ok';
    private const VALID_RESPONSES = ['status', 'errors', 'data', 'info'];
    private const ARRAY_RESPONSES = ['errors', 'data'];

    public const PHP_CACHE_FOLDER = 'models2';

    private static $response;
    private static $model_fields = [];
    private static $errors = [];

    /**
     * Directorio donde se encuentra el directorio table con
     * el XML con la estructura de la tabla.
     *
     * @var string[]
     */
    private static $xmlPaths;

    /**
     * Lista de tablas ya comprobadas.
     *
     * @var array
     */
    private static $checked_tables;

    /**
     * Gestiona el log de todos los controladores, modelos y base de datos.
     *
     * @var XnetLog
     */
    private static $coreLog;

    /**
     * Permite conectar e interactuar con memcache.
     *
     * @var XnetCache
     */
    protected $cache;

    /**
     * Proporciona acceso directo a la base de datos.
     * Implementa la clase fs_mysql o fs_postgresql.
     *
     * @var fs_db2
     *
     * @deprecated No es necesario. Se utilizan los métodos estáticos de la clase DB.
     */
    protected $db;

    /**
     * Clase que se utiliza para definir algunos valores por defecto:
     * codejercicio, codserie, coddivisa, etc...
     *
     * @var fs_default_items
     */
    // protected $default_items;

    /**
     * Nombre de la tabla en la base de datos.
     *
     * @var string
     *
     * @deprecated Utilice el método tablename()
     */
    protected $table_name;

    /**
     * True si la tabla tiene el campo created_at
     *
     * @var bool
     */
    public $use_created_at;

    /**
     * True si la tabla tiene el campo updated_at
     *
     * @var bool
     */
    public $use_updated_at;

    /**
     * True si la tabla tiene el campo deleted_at
     *
     * @var bool
     */
    public $use_deleted_at;

    protected $data;
    public $created_at;
    public $updated_at;
    public $deleted_at;
    static public $disableYamlCache;
    public $dataStructure;

    public function __construct($data = false)
    {
        if (!isset(self::$coreLog)) {
            self::$coreLog = new XnetLog();
        }

        $fileCacheExists = !XnetPolicy::yamlCacheDisabled() && file_exists(XnetPhpFileCache::getYamlFileName(self::PHP_CACHE_FOLDER, static::tablename()));
        if (!DBSchema::tableExists(static::tablename()) || !$fileCacheExists) {
            $this->check_table(static::tablename());
        }

        $this->data = [];

        // Revisar
        $this->cache = new XnetCache();

        static::$disableYamlCache = XnetPolicy::yamlCacheDisabled();
        $this->dataStructure = $this->getFields();

        if (constant('FS_DEBUG') && !DB::connected()) {
            echo 'Intento de instanciar el modelo de ' . $this->tablename() . ' (' . get_class_name($this) . '), antes de conectar con la base de datos.<br>';
            debug_message(debug_backtrace());
            die('Verifique que no se haya instanciado antes de llamar al constructor del controlador.');
        }

        $this->table_name = $this->tablename();
        // $this->default_items = new fs_default_items();

        // La primera vez que se entre en un modelo, no existirá checked_tables, así que se cargará
        if (!isset(self::$checked_tables)) {
            self::$xmlPaths = [];

            self::$checked_tables = $this->cache->get_array('fs_checked_tables');
            if (!empty(self::$checked_tables)) {
                /// nos aseguramos de que existan todas las tablas que se suponen comprobadas
                foreach (self::$checked_tables as $ckt => $value) {
                    if (!DBSchema::tableExists($ckt)) {
                        $this->clean_checked_tables();
                        break;
                    }
                }
            }
        }

        if (!isset(self::$xmlPaths[$this->tablename()])) {
            $this->get_base_dir($this->tablename());
        }

        // Sólo revisaremos si se ha limpiado caché.
        if (empty(self::$checked_tables)) {
            $this->check_collation();
        }

        // Si aún no se ha chequeado la tabla, o si se está verificando (porque se llame por segunda vez, o fallase la verificación)
        if (!$this->is_table_checked($this->tablename())) {
            // Se marca como que se está verificando. Así no se volverá a entrar aquí, y el proceso finalizará por su cuenta
            self::$checked_tables[$this->tablename()] = 'Verifying';
            if ($this->check_table($this->tablename())) {
                // Si éxito se da por verificada...
                self::$checked_tables[$this->tablename()] = 'Verified';
            }
            // Se guarda el resultado (verificada o no)
            $this->cache->set('fs_checked_tables', self::$checked_tables, 5400);
        }

        $this->checkDatetimeFields();

        if (empty($data)) {
            $this->clear();
        } else {
            $this->load_from_data($data);
        }

        $this->loadAssociatedData();
    }

    /**
     * Borra los datos de las tablas verificadas para proceder a una nueva verificación
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0706
     *
     */
    public static function clear_cache()
    {
        self::$checked_tables = [];
    }

    private static function getFieldsFromCache(): array
    {
        // Si la caché está desactivada, se retorna vacío para que se vuelva a regenerar.
        if (static::$disableYamlCache) {
            return [];
        }
        return XnetPhpFileCache::loadYamlFile(self::PHP_CACHE_FOLDER, static::tablename());
    }

    private static function getXml($tablename)
    {
        XnetModel::get_xml_table($tablename, $xml, $constraints);
        $result = [];
        foreach ($xml as $value) {
            $result[$value['nombre']] = $value;
        }
        return $result;
    }

    public static function getFields(): array
    {
        $tablename = static::tablename();

        $result = self::getFieldsFromCache();
        if (!empty($result)) {
            return $result;
        }

        $data = DBSchema::getColumnsAssociativeArray($tablename);
        $xml = static::getXml($tablename);

        foreach ($data as $key => $tableData) {
            $dataInfo = [];

            // El tipo boolean, es el único que se guarda con un nombre diferente tinyint(1)
            $xmlType = $xml[$key]['tipo'];
            if ($xmlType !== 'boolean') {
                $xmlType = $tableData['type'];
            }

            $type = DBSchema::splitType($xmlType);
            $genericType = DBSchema::getTypeOf($type['type']);
            $dataInfo['generictype'] = $genericType;
            $dataInfo['type'] = $type['type'];

            if (isset($value['required']) && $value['required']) {
                $dataInfo['required'] = true;
            }

            if (isset($xml[$key]['defecto'])) {
                $dataInfo['default'] = trim($xml[$key]['defecto'], " \"'`");
            }

            $othersParameters = ['pattern', 'placeholder', 'select', 'autocomplete'];
            foreach ($othersParameters as $parameter) {
                if (isset($value[$parameter])) {
                    $dataInfo[$parameter] = $value[$parameter];
                }
            }

            switch ($genericType) {
                case 'string':
                    if (!isset($value['maxlength']) || (int) $value['maxlength'] > (int) $type['length']) {
                        $value['maxlength'] = (int) $type['length'];
                    }
                    $dataInfo['maxlength'] = $type['length'];
                    break;
                case 'integer':
                    /**
                     * Lo primero es ver la capacidad física máxima según el tipo de dato.
                     */
                    $bytes = 4;
                    switch ($type['type']) {
                        case 'tinyint':
                            $bytes = 1;
                            break;
                        case 'smallint':
                            $bytes = 2;
                            break;
                        case 'mediumint':
                            $bytes = 3;
                            break;
                        case 'int':
                            $bytes = 4;
                            break;
                        case 'bigint':
                            $bytes = 8;
                            break;
                    }
                    $bits = 8 * (int) $bytes;
                    $physicalMaxLength = 2 ** $bits;

                    /**
                     * $minDataLength y $maxDataLength contendrán el mínimo y máximo valor que puede contener el campo.
                     */
                    $unsigned = isset($type['unsigned']) && $type['unsigned'] === 'yes';
                    $minDataLength = $unsigned ? 0 : -$physicalMaxLength / 2;
                    $maxDataLength = ($unsigned ? $physicalMaxLength : $physicalMaxLength / 2) - 1;

                    /**
                     * De momento, se asignan los límites máximos por el tipo de dato.
                     * En $min y $max, iremos arrastrando los límites conforme se vayan comprobando.
                     * $min nunca podrá ser menor que $minDataLength.
                     * $max nunca podrá ser mayor que $maxDataLength.
                     */
                    $min = $minDataLength;
                    $max = $maxDataLength;

                    /**
                     * Se puede hacer una limitación física Se puede haber definido en el xml un min y un max.
                     * A todos los efectos, lo definido en el XML como min o max se toma como limitación
                     * física del campo.
                     * TODO: Habría que controlar en el modelo que esos límites no se sobrepasen.
                     */
                    if (isset($xml[$key]['min'])) {
                        $minXmlLength = $xml[$key]['min'];
                        if ($minXmlLength > $minDataLength) {
                            $min = $minXmlLength;
                        } else {
                            debug_message("{$tablename} ({$key}): Se ha especificado un min {$minXmlLength} en el XML, pero por el tipo de datos, el mínimo es {$minDataLength}.");
                        }
                    }
                    if (isset($xml[$key]['max'])) {
                        $maxXmlLength = $xml[$key]['max'];
                        if ($maxXmlLength < $maxDataLength) {
                            $max = $maxXmlLength;
                        } else {
                            debug_message("{$tablename} ({$key}): Se ha especificado un min {$maxXmlLength} en el XML, pero por el tipo de datos, el máximo es {$maxDataLength}.");
                        }
                    }

                    /**
                     * Se puede hacer una limitación desde el controlador, pero no debe de sobreapasar
                     * los límites anteriores en ningún caso.
                     */
                    if (isset($value['min'])) {
                        $minControllerLength = $value['min'];
                        if ($minControllerLength > $min) {
                            $min = $minControllerLength;
                        } else {
                            if ($minControllerLength < $minDataLength) {
                                debug_message("{$tablename} ({$key}): El valor mínimo es de {$minDataLength}, pero en el controlador se limita a {$minControllerLength}.");
                            } elseif (isset($minXmlLength) && $minControllerLength < $minXmlLength) {
                                debug_message("{$tablename} ({$key}): Ha sido limitado en el XML a {$minXmlLength}, pero en el controlador se limita a {$minControllerLength}.");
                            }
                        }
                    }
                    if (isset($value['max'])) {
                        $maxControllerLength = $value['max'];
                        if ($maxControllerLength < $max) {
                            $max = $maxControllerLength;
                        } else {
                            if ($maxControllerLength > $maxDataLength) {
                                debug_message("{$tablename} ({$key}): El valor máximo es {$maxDataLength}, pero en el controlador se limita a {$maxControllerLength}.");
                            } elseif (isset($maxXmlLength) && $maxControllerLength > $maxXmlLength) {
                                debug_message("{$tablename} ({$key}): Ha sido limitado en el XML a {$maxXmlLength}, pero en el controlador se limita a {$maxControllerLength}.");
                            }
                        }
                    }

                    /**
                     * Length es el número que está entre paréntesis al definir el tipo:
                     * por ejemplo 2 en tinyint(2).
                     * MariaDB asigna un valor si no lo haces tú en el xml.
                     * Sólo se tendrá en cuenta, si recorta el tipo máximo.
                     * Se avisa si length sobrepasa la capacidad física.
                     *
                     * El dato enter paréntesis al definir un campo, sólo influye en la visualización
                     * del campo, no en el tamaño de su contenido, que depende del tipo.
                     * https://stackoverflow.com/questions/12839927/mysql-tinyint-2-vs-tinyint1-what-is-the-difference
                     *
                     * |     | (1) | (2) | (3) |
                     * +-----+-----+-----+-----+
                     * | 1   | 1   |  1  |   1 |
                     * | 10  | 10  | 10  |  10 |
                     * | 100 | 100 | 100 | 100 |
                     */
                    if (isset($type['length'])) {
                        $maxViewLength = 10 ** $type['length'] - 1;
                        if ($maxViewLength < $max) {
                            debug_message("{$tablename} ({$key}): Se ha definido como {$xmlType}, y el valor máximo es {$maxXmlLength}.");
                        }
                    }

                    /**
                     * Es posible que en el controlado se haya especificado un min y un max.
                     * Si existe alguno de ellos, se toma ese como mínimo o máximo.
                     */
                    if (isset($type['min'])) {
                        $min = $type['min'];
                    }
                    if (isset($type['max'])) {
                        $max = $type['max'];
                    }

                    $dataInfo['min'] = $min;
                    $dataInfo['max'] = $max;
                    break;
                default:
                    // ???
            }
            $result[$key] = $dataInfo;
        }

        if (!XnetPhpFileCache::saveYamlFile(self::PHP_CACHE_FOLDER, $tablename, $result)) {
            debug_message('Se ha producido un error al guardar el archivo ' . $tablename);
        }

        /**
         * Para evitar que pueda haber un error al utilizar siempre el dato calculado,
         * se fuerza a cargar el yaml generado y utilizarlo en lugar de dicho dato calculado.
         */
        if (self::$disableYamlCache) {
            $result = XnetPhpFileCache::loadYamlFile(self::PHP_CACHE_FOLDER, $tablename);
        }

        return $result;
    }

    private function checkDatetimeFields()
    {
        $this->use_created_at = false;
        $this->use_updated_at = false;
        $this->use_deleted_at = false;

        $columns = DBSchema::getColumns($this->tablename());
        foreach ($columns as $column) {
            if ($column['name'] === 'created_at') {
                $this->use_created_at = ($column['type'] === 'datetime');
                if (!$this->use_created_at) {
                    $this->new_error_msg('La columna created_at tiene que ser de tipo datetime, no de tipo ' . $column['type']);
                }
                continue;
            }
            if ($column['name'] === 'updated_at') {
                $this->use_updated_at = ($column['type'] === 'datetime');
                if (!$this->use_updated_at) {
                    $this->new_error_msg('La columna updated_at tiene que ser de tipo datetime, no de tipo ' . $column['type']);
                }
            }
            if ($column['name'] === 'deleted_at') {
                $this->use_deleted_at = ($column['type'] === 'datetime');
                if (!$this->use_deleted_at) {
                    $this->new_error_msg('La columna deleted_at tiene que ser de tipo datetime, no de tipo ' . $column['type']);
                }
            }
        }
    }

    /**
     * Retorna una cadena para la importación de registros.
     * Si existe un archivo CSV separado por punto y coma, genera la SQL para la importación de datos desde él.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0629
     *
     * @return string
     */
    public function install()
    {
        $result = '';
        $tablename = $this->tablename();
        $filename = self::$xmlPaths[$tablename] . 'seed/' . $tablename . '.csv';
        if (!file_exists($filename)) {
            return '';
        }

        $rows = 10; // Indicamos el número de registros que vamos a insertar de una vez
        $handle = fopen($filename, "r");
        if ($handle === false) {
            $this->new_error_msg('No ha sido posible abrir el archivo ' . $filename);
            return '';
        }

        // Asumimos que la primera fila es la cabecera...
        $header = fgetcsv($handle, 0, ';');
        if ($header === false) {
            $this->new_error_msg('No ha sido posible leer la primera línea del archivo ' . $filename);
            fclose($handle);
            return '';
        }

        $sqlHeader = "INSERT INTO `{$tablename}` (`" . implode('`, `', $header) . '`) VALUES ';
        $row = 0;
        $sqlData = [];
        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            // Entrecomillamos lo que no sea null.
            foreach ($data as $key => $datum) {
                if (mb_strtoupper($datum) !== 'NULL') {
                    $data[$key] = "'$datum'";
                }
            }

            if ($row % $rows === 0) {
                if (count($sqlData) > 0) {
                    $result .= ($sqlHeader . implode(', ', $sqlData) . ';' . PHP_EOL);
                }
                $sqlData = [];
            }
            $sqlData[] = '(' . implode(', ', $data) . ')';
            $row++;
        }
        if (count($sqlData) > 0) {
            $result .= ($sqlHeader . implode(', ', $sqlData) . ';' . PHP_EOL);
        }
        fclose($handle);

        return $result;
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        return true;
    }

    /**
     * Vacía la lista de errores de los modelos.
     */
    public function clean_errors()
    {
        self::$coreLog->clean_errors();
    }

    /**
     * Vacía la lista de mensajes de los modelos.
     */
    public function clean_messages()
    {
        self::$coreLog->clean_messages();
    }

    /**
     * Compara dos números en coma flotante con una precisión de $precision,
     * devuelve TRUE si son iguales, FALSE en caso contrario.
     *
     * @param float $f1
     * @param float $f2
     * @param int   $precision
     * @param bool  $round
     *
     * @return bool
     */
    public function floatcmp($f1, $f2, $precision = 10, $round = false)
    {
        if ($round || !function_exists('bccomp')) {
            return (abs($f1 - $f2) < 6 / pow(10, $precision + 1));
        }

        return (bccomp((string) $f1, (string) $f2, $precision) == 0);
    }

    /**
     * Devuelve la lista de mensajes de error de los modelos.
     *
     * @return array lista de errores.
     */
    public function get_errors()
    {
        return self::$coreLog->get_errors();
    }

    /**
     * Devuelve la lista de mensajes de los modelos.
     *
     * @return array
     */
    public function get_messages()
    {
        return self::$coreLog->get_messages();
    }

    /**
     * Devuelve el valor entero de la variable $s,
     * o NULL si es NULL. La función intval() del php devuelve 0 si es NULL.
     *
     * @param string $str
     *
     * @return int
     */
    public function intval($str)
    {
        return ($str === null) ? null : (int) $str;
    }

    /**
     * Esta función convierte:
     * < en &lt;
     * > en &gt;
     * " en &quot;
     * ' en &#39;
     *
     * No tengas la tentación de sustituirla por htmlentities o htmlspecialshars
     * porque te encontrarás con muchas sorpresas desagradables.
     *
     * TODO: Al utilizar el dato almacenado con este método, debe deshacerse con fs_fix_html(). Debería hacerse esto en el constructor para evitar problemas.
     *
     * @param string $txt
     *
     * @return string
     */
    public function no_html($txt)
    {
        $newt = str_replace(['<', '>', '"', "'",], ['&lt;', '&gt;', '&quot;', '&#39;',], $txt);

        return trim($newt);
    }

    /**
     * PostgreSQL guarda los valores TRUE como 't', MySQL como 1.
     * Esta función devuelve TRUE si el valor se corresponde con
     * alguno de los anteriores.
     *
     * @param string $val
     *
     * @return bool
     */
    public function str2bool($val)
    {
        return ($val == 't' || $val == '1');
    }

    /**
     * Transforma una variable en una cadena de texto válida para ser
     * utilizada en una consulta SQL.
     *
     * @param mixed $val
     *
     * @return string
     */
    public function var2str($val)
    {
        if (is_null($val)) {
            return 'NULL';
        } elseif (is_bool($val)) {
            return $val ? 'TRUE' : 'FALSE';
        } elseif (preg_match('/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/i', $val)) {
            /// es una fecha
            return "'" . date(DBSchema::dateStyle(), strtotime($val)) . "'";
        } elseif (preg_match('/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})$/i', $val)) {
            /// es una fecha+hora
            return "'" . date(DBSchema::dateStyle() . ' H:i:s', strtotime($val)) . "'";
        }

        return DBSchema::escapeString($val);
    }

    /**
     * Devuelve el campo descriptivo.
     *
     * @return string
     */
    public function get_description()
    {
        return get_called_class() . ' no tiene get_description';
    }

    /**
     * Devuelve el nombre de la tabla de este modelo.
     *
     * @return string
     *
     * @deprecated Utilice tablename()
     */
    public function table_name()
    {
        return static::tablename();
    }

    /**
     * Devuelve si la tabla está comprobada.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.10
     *
     * @param string $tablename
     *
     * @return bool
     */
    public function is_table_checked($tablename)
    {
        return isset(self::$checked_tables[$tablename]);
    }

    /**
     * Ejecuta las consultas correspondientes.
     * Si no pudieran ejecutarse, se hará un rollback.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.10
     *
     * @param string[][] $fixes [
     *                          'table_name_1' => ['query1','query2' ],
     *                          'table_name_2' => ['query1','query2' ],
     *                          ]
     *
     * @return bool
     */
    public function exec_fix_queries($fixes = [])
    {
        $status = true;
        $sqls = [];
        foreach ($fixes as $table => $queries) {
            if (DBSchema::tableExists($table)) {
                $constraints = [];
                foreach ($this->db->get_constraints($table) as $constraint) {
                    if ($constraint['type'] == "FOREIGN KEY") {
                        $constraints[] = $constraint['name'];
                    }
                }

                if (isset($constraint) && is_numeric($constraint)) {
                    $sqls[] = $query;
                } else {
                    foreach ($queries as $constraint => $query) {
                        if (!in_array($constraint, $constraints)) {
                            $sqls[] = $query;
                        }
                    }
                }
            }
        }

        if (!empty($sqls)) {
            DB::BeginTransaction();
            $status = DB::exec(implode('', $sqls));
            if ($status) {
                DB::commit();
            } else {
                DB::rollback();
            }
        }

        return $status;
    }

    /**
     * Devuelve los datos como array de objetos.
     *
     * @param string $sql
     * @param int    $offset
     * @param int    $limit
     *
     * @return static[]
     */
    public function all_from($sql, $offset = 0, $limit = FS_ITEM_LIMIT)
    {
        $sql = rtrim($sql, ';');
        $data = $limit > 0 ? DB::select_limit($sql, $limit, $offset) : DB::select($sql . ';');
        return $this->all_from_data($data);
    }

    /**
     * Limpia la lista de tablas comprobadas.
     */
    protected function clean_checked_tables()
    {
        self::$checked_tables = [];
        $this->cache->delete('fs_checked_tables');
    }

    /**
     * Comprueba si hay cambios de charset/collation y los aplica si son necesarios.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     */
    protected function check_collation()
    {
        DBSchema::updateCollation();
    }

    /**
     * Comprueba y actualiza la estructura de la tabla si es necesario
     *
     * @param string $tablename ()
     *
     * @return bool
     */
    public function check_table($tablename)
    {
        $sql = '';

        // Se instancian modelos dependientes y se ejecutan sus correspondientes fix_db()
        $this->check_model_dependencies();

        if (DBSchema::tableExists($tablename)) {
            $xml_cols = [];
            $xml_cons = [];

            if (!$this->get_xml_table($tablename, $xml_cols, $xml_cons)) {
                $this->new_error_msg('Error con el xml.');
                return false;
            }

            if (!DBSchema::checkTableAux($tablename)) {
                $this->new_error_msg('Error al convertir la tabla a InnoDB.');
            }

            /**
             * Si hay que hacer cambios en las restricciones, eliminamos todas las restricciones,
             * luego añadiremos las correctas. Lo hacemos así porque evita problemas en MySQL.
             */
            $db_cons = DBSchema::getConstraints($tablename);
            $sql2 = DBSchema::compareConstraints($tablename, $xml_cons, $db_cons, true);
            if ($sql2 != '') {
                if (!DB::exec($sql2)) {
                    $this->new_error_msg('Error al comprobar la tabla ' . $tablename);
                }

                /// leemos de nuevo las restricciones
                $db_cons = DBSchema::getConstraints($tablename);
            }

            /// comparamos las columnas
            $db_cols = DBSchema::getColumns($tablename);
            $sql .= DBSchema::compareColumns($tablename, $xml_cols, $db_cols);

            // Comprobamos si hay algún atributo que no haya sido definido en el modelo
            if (constant('FS_DEBUG')) {
                foreach ($db_cols as $col) {
                    $attribute = $col['name'];
                    if (!property_exists($this, $attribute)) {
                        debug_message("No se ha definido el atributo `$attribute` en {$this->tablename()}.");
                    }
                }
            }

            /// comparamos las restricciones
            $sql .= DBSchema::compareConstraints($tablename, $xml_cons, $db_cons);
        } else {
            /// generamos el sql para crear la tabla
            $sql .= DBSchema::generateTableSql($tablename);
            $sql .= $this->install();
        }

        // Probamos a corregir datos antes de aplicar cambios
        if (!$this->fix_model_table_before()) {
            $this->new_error_msg('Error al reparar datos de la tabla ' . $tablename . ' antes de actualizarla.');
        }

        if (!empty($sql) && !DB::exec($sql)) {
            $this->new_error_msg('Error al comprobar la tabla ' . $tablename . ' después de actualizarla.');
            return false;
        }

        // Probamos a corregir datos después de aplicar cambios
        if (!$this->fix_model_table_after()) {
            $this->new_error_msg('Error al reparar datos de la tabla ' . $tablename);
        }

        return true;
    }

    /**
     * Fusiona dos SimpleXmlElement retornando el resultado de la fusión
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0810
     *
     * @param \SimpleXMLElement $parent
     * @param \SimpleXMLElement $child
     *
     * @return \SimpleXMLElement
     */
    private static function mergeXml(\SimpleXMLElement $parent, \SimpleXMLElement $child): \SimpleXMLElement
    {
        foreach (['columna', 'restriccion'] as $toMerge) {
            foreach ($child->{$toMerge} as $item) {
                $childItem = $parent->addChild($toMerge, $item);
                foreach ($item->children() as $child) {
                    $childItem->addChild($child->getName(), reset($child));
                }
                // Si es una relación extendida, tiene que ser nullable para poder desactivar el plugin
                if (!isset($childItem->nulo) && reset($childItem->tipo) === 'relationship') {
                    $childItem->addChild('nulo', 'YES');
                }
            }
        }
        return $parent;
    }

    /**
     * Carga el archivo XML $filename, incluyendo sus dependencias de otros XML
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0810
     *
     * @param string $filename
     *
     * @return \SimpleXMLElement
     */
    private static function loadXmlFile(string $filename): \SimpleXMLElement
    {
        if (!file_exists($filename)) {
            self::new_error_msg('Archivo ' . $filename . ' no encontrado.');
            die('Archivo ' . $filename . ' no encontrado.');
        }

        $xml = simplexml_load_string(file_get_contents('./' . $filename, FILE_USE_INCLUDE_PATH));
        if (!$xml) {
            self::new_error_msg('Error al leer el archivo ' . $filename);
            die('Error al leer el archivo ' . $filename);
        }

        if (!isset($xml->incluye) || $xml->incluye->count() === 0) {
            return $xml;
        }

        // Si hay un apartado "incluye", hay que incluir las rutas
        foreach ($xml->incluye->children() as $item) {
            $includeFilename = './' . trim(reset($item));

            $xmlParent = simplexml_load_string(file_get_contents($includeFilename, FILE_USE_INCLUDE_PATH));
            if (!$xmlParent) {
                self::new_error_msg('Error al leer el archivo ' . $includeFilename . ' en ' . $filename);
                die('Error al leer el archivo ' . $includeFilename);
            }

            $xml = self::mergeXml($xmlParent, $xml);
        }

        return $xml;
    }

    /**
     * Obtiene las columnas y restricciones del fichero xml para una tabla
     *
     * @param string $tablename
     * @param array  $columns
     * @param array  $constraints
     *
     * @return bool
     */
    public static function get_xml_table($tablename, &$columns, &$constraints)
    {
        self::get_base_dir($tablename);

        $filename = self::$xmlPaths[$tablename] . 'table/' . $tablename . '.xml';
        $xml = self::loadXmlFile($filename);

        $return = false;
        if ($xml->columna) {
            $i = 0;
            foreach ($xml->columna as $col) {
                $columns[$i]['nombre'] = (string) $col->nombre;
                $columns[$i]['tipo'] = (string) $col->tipo;

                $columns[$i]['nulo'] = 'YES';
                if ($col->nulo && mb_strtolower($col->nulo) == 'no') {
                    $columns[$i]['nulo'] = 'NO';
                }

                if ($col->defecto == '') {
                    $columns[$i]['defecto'] = null;
                } else {
                    $columns[$i]['defecto'] = (string) $col->defecto;
                }

                /**
                 * Pueden existir otras definiciones de limitaciones físicas como min y max
                 * De existir, tienen que ser contempladas en el método test y tener mayor peso que
                 * la limitación en plantilla.
                 */
                foreach (['min', 'max'] as $field) {
                    if (isset($col->{$field})) {
                        $columns[$i][$field] = (string) $col->{$field};
                    }
                }

                if (isset($col->description)) {
                    debug_message('Cambie la etiqueta <description> por comentario en ' . $col->nombre . ' de ' . $tablename);
                    $columns[$i]['comentario'] = $col->description;
                } elseif (isset($col->comment)) {
                    debug_message('Cambie la etiqueta <comment> por comentario en ' . $col->nombre . ' de ' . $tablename);
                    $columns[$i]['comentario'] = $col->comment;
                } elseif (isset($col->comentario)) {
                    $columns[$i]['comentario'] = $col->comentario;
                }

                $i++;
            }

            /// debe de haber columnas, si no es un fallo
            $return = true;
        }

        if ($xml->restriccion) {
            $i = 0;
            foreach ($xml->restriccion as $col) {
                $constraints[$i]['nombre'] = (string) $col->nombre;
                $constraints[$i]['consulta'] = (string) $col->consulta;
                $i++;
            }
        }

        return $return;
    }

    /**
     * Muestra al usuario un mensaje de error
     *
     * @param string $msg mensaje de error
     */
    public static function new_error_msg($msg)
    {
        if (constant('FS_DEBUG')) {
            self::$coreLog->new_error('<b>Error en el modelo <em>' . get_called_class() . '</em>:</b> ' . $msg);
        }
        self::$coreLog->save($msg);
    }

    protected static function errorMessage($msg)
    {
        if (constant('FS_DEBUG')) {
            self::$coreLog->new_error('<b>Error en el modelo <em>' . get_called_class() . '</em>:</b> ' . $msg);
        }
        self::$coreLog->save($msg);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function check_model_dependencies()
    {

    }

    /**
     * Revisa si hay instantes de creación o actualización a null, poniéndolos al instante actual UTC si los hay.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0629
     *
     */
    private function updateCreationDatetimes()
    {
        if (!$this->use_created_at && !$this->use_updated_at) {
            return true;
        }

        $sql = '';
        if ($this->use_created_at) {
            $sql .= 'UPDATE ' . $this->table_name . ' SET created_at = "' . gmdate('Y-m-d H:i:s') . '" WHERE created_at IS NULL;';
        }
        if ($this->use_updated_at) {
            $sql .= 'UPDATE ' . $this->table_name . ' SET updated_at = "' . gmdate('Y-m-d H:i:s') . '" WHERE updated_at IS NULL;';
        }

        return DB::exec($sql);
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        return $this->fix_db();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0426
     *
     * @return bool
     */
    protected function fix_model_table_after()
    {
        return $this->updateCreationDatetimes();
    }

    /**
     * Convierte una variable con contenido binario a texto.
     * Lo hace en base64.
     *
     * @param mixed $val
     *
     * @return string
     */
    protected function bin2str($val)
    {
        return is_null($val) ? 'NULL' : "'" . base64_encode($val) . "'";
    }

    /**
     * Devuelve un array con todas las fechas entre $first y $last.
     *
     * @param string $first
     * @param string $last
     * @param string $step
     * @param string $format
     *
     * @return array
     */
    protected function date_range($first, $last, $step = '+1 day', $format = 'Y-m-d')
    {
        $dates = [];
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {
            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    /**
     * Escapa las comillas de una cadena de texto.
     *
     * @param string $str cadena de texto a escapar
     *
     * @return string cadena de texto resultante
     */
    protected function escape_string($str = '')
    {
        return $this->db->escape_string($str);
    }

    /**
     * Muestra al usuario un consejo.
     *
     * @param string $msg
     */
    protected function new_advice($msg)
    {
        if (constant('FS_DEBUG')) {
            self::$coreLog->new_advice('<b>Aviso en el modelo <em>' . get_called_class() . '</em>:</b> ' . $msg);
        }
        self::$coreLog->save($msg, 'advice');
    }

    /**
     * Muestra al usuario un mensaje.
     *
     * @param string $msg
     */
    protected function new_message($msg)
    {
        if (constant('FS_DEBUG')) {
            self::$coreLog->new_message('<b>Mensaje en el modelo <em>' . get_called_class() . '</em>:</b> ' . $msg);
        }
        self::$coreLog->save($msg, 'message');
    }

    /**
     * Devuelve una cadena de texto aleatorio de longitud $length
     *
     * @param int $length
     *
     * @return string
     */
    protected function random_string($length = 10)
    {
        return mb_substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    /**
     * Convierte un texto a binario.
     * Lo hace con base64.
     *
     * @param string $val
     *
     * @return null|string
     */
    protected function str2bin($val)
    {
        return is_null($val) ? null : base64_decode($val);
    }

    /**
     * Devuelve los datos como array de objetos haciendo uso de la caché.
     * IMPORTANTE: El uso de este método implica que el modelo final debe tener el método clean_cache definido.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.12
     *
     * @param string $name
     * @param string $sql
     * @param int    $offset
     * @param int    $limit
     *
     * @return static[]
     */
    protected function all_from_cached($name, $sql, $offset = 0, $limit = 0)
    {
        // Leemos esta lista de la caché
        $list = $this->cache->get_array($name);
        if (empty($list)) {
            // Si no está en caché, leemos de la base de datos
            $list = $this->all_from($sql, $offset, $limit);
            // Guardamos la lista en caché
            $this->cache->set($name, $list);
        }
        return $list;
    }

    /**
     * Devuelve los datos como objeto haciendo uso de la caché.
     * IMPORTANTE: El uso de este método implica que el modelo final debe tener el método clean_cache definido.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.12
     *
     * @param string $name
     * @param string $sql
     *
     * @return false|static
     */
    protected function get_from_cached_pk($name, $sql)
    {
        // Leemos el elemento de la caché
        $item = $this->cache->get($name);
        if (!empty($item)) {
            return new static($item);
        }
        if (empty($item)) {
            // Si no está en caché, leemos de la base de datos
            $data = DB::select($sql);
            if (isset($data[0])) {
                // Guardamos el elemento en caché
                $this->cache->set($name, $data[0]);
                return new static($data[0]);
            }
        }
        return false;
    }

    /**
     * Devuelve los datos como array de objetos.
     *
     * @param array $data
     *
     * @return static[]
     */
    protected function all_from_data(&$data)
    {
        $list = [];
        if ($data) {
            foreach ($data as $a) {
                $list[] = new static($a);
            }
        }
        return $list;
    }

    private static function getBasePath($tablename)
    {
        self::$xmlPaths[$tablename] = '';
        foreach (XnetConfig::getEnabledPlugins() as $plugin) {
            $path = 'plugins/' . $plugin . '/Model/';
            if (file_exists($path . 'table/' . $tablename . '.xml')) {
                self::$xmlPaths[$tablename] = $path;
                return true;
            }
        }
        // Ruta de los xml en formato nuevo
        $path = 'src/Xnet/Model/';
        if (file_exists($path . 'table/' . $tablename . '.xml')) {
            self::$xmlPaths[$tablename] = $path;
            return true;
        }
        // Ruta de los xml en formato antiguo
        $path = 'model/';
        if (file_exists($path . 'table/' . $tablename . '.xml')) {
            self::$xmlPaths[$tablename] = $path;
            return true;
        }
        return false;
    }

    /**
     * Buscamos el xml de la tabla en los plugins
     *
     * @param string $tablename ()
     */
    private static function get_base_dir($tablename)
    {
        self::$xmlPaths[$tablename] = '';

        foreach (Version::getEnabledPluginsArray() as $plugin) {
            $path = 'plugins/' . $plugin . '/Model/';
            if (file_exists($path . 'table/' . $tablename . '.xml')) {
                self::$xmlPaths[$tablename] = $path;
                return true;
            }
        }
        // Ruta de los xml en formato nuevo
        $path = 'src/Xnet/Model/';
        if (file_exists($path . 'table/' . $tablename . '.xml')) {
            self::$xmlPaths[$tablename] = $path;
            return true;
        }
        // Ruta de los xml en formato antiguo
        $path = 'Model/';
        if (file_exists($path . 'table/' . $tablename . '.xml')) {
            self::$xmlPaths[$tablename] = $path;
            return true;
        }
        return false;
    }

    /**
     * Asigna un valor a la respuesta estandarizada de la mayoría de los métodos.
     * Véase getResponse para más información.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0715
     *
     * @param $field
     * @param $value
     *
     * @return bool
     */
    public static function setResponse($field, $value)
    {
        if (!isset(self::$response) || ($field === 'status' && $value === XnetModel::OK)) {
            self::$response = new stdClass();
            self::$response->status = XnetModel::OK;
        }
        if (!in_array($field, self::VALID_RESPONSES)) {
            return false;
        }
        if (in_array($field, self::ARRAY_RESPONSES)) {
            self::$response->{$field}[] = $value;
        } else {
            self::$response->$field = $value;
        }
        return true;
    }

    /**
     * Limpia la variable self::$response retornando su valor previo como respuesta.
     * La respuesta es un stdClass que puede contener los siguientes atributos:
     * - status, puede ser XnetModel::OK o XnetModel::ERROR
     * - errors, es un array que puede contener varios errores generados durante el proceso
     * - data, contiene el resultado de la operación (registro guardado, creado, etc)
     * - info, contiene información adicional que se puede mostrar al usuario.
     *
     * Véase:
     *     private const VALID_RESPONSES = ['status', 'errors', 'data', 'info'];
     *     private const ARRAY_RESPONSES = ['errors', 'data'];
     *     public const ERROR = 'Error';
     *     public const OK = 'Ok';
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0718
     *
     * @return stdClass
     */
    public static function getResponse(): stdClass
    {
        $result = self::$response;
        self::$response = new stdClass();
        self::$response->status = XnetModel::OK;;
        return $result;
    }

    /**
     * Retorna un registro localizado por su identificador
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0714
     *
     * @param string $id
     *
     * @return XnetModel|null
     */
    public static function getById(string $id): ?XnetModel
    {
        $model = get_called_class();
        $return = (new $model())->get($id);
        if ($return) {
            return $return;
        }
        return null;
    }

    /**
     * Retorna el nombre de un registro, a partir de su ID
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0714
     *
     * @param string $id
     *
     * @return string
     */
    public static function getName(string $id): string
    {
        $record = self::getById($id);
        if ($record === false) {
            return 'Not found ' . $id;
        }
        return $record->get_name();
    }

    /**
     * Crea un nuevo registro, a partir de los datos que se le pasan en un array asociativo.
     * Retorna el resultado de la creación en formato de getResponse.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0714
     *
     * @param array $data
     *
     * @return XnetModel|null
     */
    public static function create(array $data): ?XnetModel
    {
        $model = get_called_class();

        $primaryKey = $model::primaryKey();
        $record = null;
        if (isset($data[$primaryKey])) {
            $record = $model::getById($data[$primaryKey]);
        }

        if (!isset($record)) {
            $record = new $model();
        }

        foreach ($data as $field => $value) {
            $record->{$field} = $value;
        }

        if (!$record->save()) {
            return null;
        }

        $response = $model::getResponse();
        if ($response->status === self::OK) {
            return reset($response->data) ?? null;
        }

        debug_message($response);
        return null;
    }

    /**
     * Carga los datos recibidos en el modelo
     * Sobreescribe al de fs_extended_model porque consideraba el tinyint como boolean.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0712
     *
     * @param $data
     */
    public function load_from_data($data)
    {
        $fields = $this->get_model_fields();
        foreach ($data as $key => $value) {
            if (!isset($fields[$key])) {
                $this->{$key} = $value;
                continue;
            }

            $field_type = $fields[$key];
            $type = (strpos($field_type, '(') === false) ? $field_type : substr(
                $field_type, 0,
                strpos($field_type, '('));
            switch ($type) {
                case 'boolean':
                    $this->{$key} = $this->str2bool($value);
                    break;

                case 'tinyint':
                case 'smallint':
                case 'mediumint':
                case 'integer':
                case 'int':
                case 'bigint':
                    $this->{$key} = $this->intval($value);
                    break;

                case 'decimal':
                case 'double':
                case 'double precision':
                case 'float':
                    $this->{$key} = empty($value) ? 0.00 : (float) $value;
                    break;

                case 'date':
                    $this->{$key} = empty($value) ? null : date('Y-m-d', strtotime($value));
                    break;

                case 'datetime':
                    $this->{$key} = empty($value) ? null : date('Y-m-d H:i:s', strtotime($value));
                    break;

                case 'char':
                case 'varchar':
                case 'mediumtext':
                    $this->{$key} = $value;
                    break;

                default:
                    if (constant('FS_DEBUG')) {
                        $this->new_error_msg('El tipo ' . $type . ' no está contemplado en load_from_data de XnetModel.');
                    }
                    $this->{$key} = $value;
            }
        }
    }

    /**
     * Carga información de otros modelos asociados a éste.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0708
     *
     */
    public function loadAssociatedData()
    {
        if (empty($this->id)) {
            return;
        }
        $this->data = $this->get_by([static::primaryKey() => $this->id]);
    }

    public function getShortClassName($name = null)
    {
        if ($name === null) {
            $name = get_called_class();
        }

        try {
            $name = (new ReflectionClass($name))->getShortName();
        } catch (ReflectionException $e) {
            $namespace = explode('\\', $name);
            $name = array_pop($namespace);
        }
        return $name;
    }

    /**
     * Guarda los cambios en el modelo actual
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0712
     *
     * @return stdClass
     */
    public function save(): bool
    {
        if (!$this->test()) {
            return false;
        }

        $response = self::getResponse();
        if ($response->status !== self::OK) {
            if (constant('FS_DEBUG')) {
                self::setResponse('errors', 'Test no pasado para los datos de ' . $this->getShortClassName());
                self::setResponse('data', $this->data);
            }
            return false;
        }

        if ($this->exists()) {
            return $this->updateTheRecord();
        }

        return $this->insertAnyRecord();
    }

    /**
     * Actualiza el registro en curso
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0712
     *
     * @return stdClass
     */
    protected function updateTheRecord(): bool
    {
        $previo = $this->get_by([static::primaryKey() => static::primaryKeyValue()]);
        $data = reset($previo);

        $sql = 'UPDATE `' . $this->tablename() . '`';
        $coma = ' SET ';

        $hay_datos = false;
        foreach ($data as $field => $value) {
            // La primary key y los que no han cambiado, nos los saltamos
            if ($field === static::primaryKey() || $value == $this->{$field}) {
                continue;
            }

            $hay_datos = true;
            $sql .= $coma . '`' . $field . '` = ' . $this->var2str($this->{$field});
            $coma = ', ';
        }

        if (!$hay_datos) {
            self::setResponse('info', 'No hay datos que actualizar (sin cambios en el registro)');
            self::setResponse('data', $this);
            return true;
        }

        $sql .= ' WHERE ' . static::primaryKey() . ' = ' . $this->var2str(static::primaryKeyValue())
            . ';';

        if (!DB::exec($sql)) {
            self::setResponse('status', XnetModel::ERROR);
            self::setResponse('info', $sql);
            self::setResponse('data', $this);
            return false;
        }

        self::setResponse('data', $this);
        return true;
    }

    /**
     * Crea un nuevo registro.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0712
     *
     * @return stdClass
     */
    protected function insertAnyRecord(): bool
    {
        $columns = [];
        $values = [];

        $fields = DBSchema::getColumnsAssociativeArray($this->table_name());
        if (empty($fields)) {
            $this->setResponse('status', XnetModel::ERROR);
            $this->setResponse('errors', 'Table ' . $this->tablename() . ' does not exists!');
            return false;
        }

        foreach ($fields as $field => $value) {
            // Nos saltamos el índice si es autoincremental
            if ($field === static::primaryKey() && $value['extra'] === 'auto_increment') {
                continue;
            }
            // Si no se le ha asignado valor
            if (!isset($this->{$field})) {
                continue;
            }
            $columns[] = $field;
            $values[] = $this->var2str($this->{$field});
        }

        if (count($columns) === 0) {
            return true;
        }

        $sql = 'INSERT INTO `' . $this->tablename() . '` (`' . implode('`, `', $columns) . '`) VALUES (' . implode(
                ', ',
                $values) . ');';

        if (!DB::exec($sql)) {
            self::setResponse('status', XnetModel::ERROR);
            self::setResponse('errors', $sql);
            self::setResponse('data', $this);
            return false;
        }

        if (null === static::primaryKeyValue()) {
            $this->{static::primaryKey()} = DBSchema::lastval();
        }

        self::setResponse('data', $this);
        return true;
    }

    /**
     * Crea un segmento de código SQL para filtrar un campo de fecha ($field) entre dos fechas.
     * Las fechas pueden ser false si se quiere filtrar sólo por una de ellas.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     *
     * @param string|false $fecha_hasta
     *
     * @param string       $field
     * @param string|false $fecha_desde
     *
     * @return string
     *
     */
    public static function filter_date($field, $fecha_desde, $fecha_hasta)
    {
        // Si no hay filtros, se retorna la cadena vacía (nada que añadir)
        if (!$fecha_desde && !$fecha_hasta) {
            return '';
        }

        if ($fecha_desde) {
            $fecha_desde = date('Y-m-d', strtotime($fecha_desde));
        }

        if ($fecha_hasta) {
            $fecha_hasta = date('Y-m-d', strtotime($fecha_hasta));
        }

        $sql = "date($field) ";

        if ($fecha_desde && $fecha_hasta) {
            $sql .= "BETWEEN date('$fecha_desde') AND date('$fecha_hasta')";
        } elseif ($fecha_desde) {
            $sql .= ">= date('$fecha_desde')";
        } else {
            $sql .= "<= date('$fecha_hasta')";
        }

        return $sql;
    }

    /**
     * Crea un segmento de código SQL para filtrar un campo de fecha ($field) entre dos fechas.
     * Las fechas pueden ser false si se quiere filtrar sólo por una de ellas.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     *
     * @param string|false $fecha_hasta
     *
     * @param string       $field
     * @param string|false $fecha_desde
     *
     * @return string
     *
     */
    public static function filter_datetime($field, $fecha_desde, $fecha_hasta)
    {
        // Si no hay filtros, se retorna la cadena vacía (nada que añadir)
        if (!$fecha_desde && !$fecha_hasta) {
            return '';
        }

        if ($fecha_desde) {
            $fecha_desde = date('Y-m-d H:i:s', strtotime($fecha_desde));
        }

        if ($fecha_hasta) {
            $fecha_hasta = date('Y-m-d 23:59:59', strtotime($fecha_hasta));
        }

        $sql = "$field ";

        if ($fecha_desde && $fecha_hasta) {
            $sql .= "BETWEEN '$fecha_desde' AND '$fecha_hasta'";
        } elseif ($fecha_desde) {
            $sql .= ">= '$fecha_desde'";
        } else {
            $sql .= "<= '$fecha_hasta'";
        }

        return $sql;
    }

    /**
     * Realiza una consulta, obteniendo un array con todos los registros que coincidan con el array asociativo
     * $key=>value proporcionado, siendo $key el campo y $value, el valor esperado para el campo. Los campos se
     * relacionan siempre con un 'AND'.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.05
     *
     * @param array $fields
     * @param array $orderby
     *
     * @return array
     *
     */
    public function get_by($fields, $orderby = [])
    {
        if (!is_array($fields)) {
            $this->new_error_msg('xfs_model::get_by necesita como parámetro un array asociativo');
        }

        $where_clause = '';
        $concat = '';
        foreach ($fields as $field => $_value) {
            switch ($_value) {
                case null:
                    $value = ' IS NULL';
                    break;
                default:
                    if (is_array($_value)) {
                        $value = ' IN(' . implode(',', $_value) . ')';
                    } else {
                        $value = "= '$_value'";
                    }
            }
            $where_clause .= "$concat {$field} {$value}";
            $concat = ' AND ';
        }

        if ($where_clause == '') {
            $where_clause = 'TRUE';
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE " . $where_clause;
        if (count($orderby) > 0) {
            $sql .= ' ORDER BY';
            $concat = ' ';
            foreach ($orderby as $order) {
                $sql .= $concat . $order;
                $concat = ', ';
            }
            $sql .= ';';
        }

        $data = DB::select($sql);
        if (empty($data)) {
            return [];
        }

        $index = static::primaryKey();
        $result = [];
        foreach ($data as $record) {
            $result[$record[$index]] = $record;
        }
        return $result;
    }

    public static function getBy($fields, $orderby = [])
    {
        if (!is_array($fields)) {
            self::errorMessage('XnetModel::getBy necesita como parámetro un array asociativo');
        }

        $where_clause = '';
        $concat = '';
        foreach ($fields as $field => $_value) {
            switch ($_value) {
                case null:
                    $value = $_value . ' IS NULL';
                    break;
                default:
                    if (is_array($_value)) {
                        $value = ' IN(' . implode(',', $_value) . ')';
                    } else {
                        $value = "= '$_value'";
                    }
            }
            $where_clause .= "$concat {$field} {$value}";
            $concat = ' AND ';
        }

        if ($where_clause == '') {
            $where_clause = 'TRUE';
        }

        $sql = "SELECT *"
            . " FROM `" . static::tablename() . "`"
            . " WHERE " . $where_clause;

        if (count($orderby) > 0) {
            $sql .= ' ORDER BY';
            $concat = ' ';
            foreach ($orderby as $order) {
                $sql .= $concat . $order;
                $concat = ', ';
            }
            $sql .= ';';
        }

        $data = DB::select($sql);
        if ($data === false) {
            self::setErrors(DB::$engine->getErrors());
            return false;
        }

        $index = static::primaryKey();
        $result = [];
        foreach ($data as $record) {
            $result[$record[$index]] = $record;
        }
        return $result;
    }

    public static function setError($error)
    {
        self::$errors[static::tablename()][] = $error;
    }

    public static function getErrors()
    {
        $errors = self::$errors[static::tablename()];
        self::$errors[static::tablename()] = [];
        return $errors;
    }

    /**
     * Obtiene un array con los registros cuyo campo $field, tenga el valor $value.
     * Opcionalmente, se puede pasar un campo $fields, con el listado de campos a retornar separados por coma.
     * El índice del array, será el ID del archivo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     *
     * @param string $fields
     *
     * @param string $field
     * @param mixed  $value
     *
     * @return array
     *
     */
    public function get_all($field, $value, $fields = '*')
    {
        $sql = "SELECT {$fields} FROM `" . $this->table_name() . "`"
            . " WHERE {$field} = " . $this->var2str($value);
        $data = DB::select($sql);
        if (empty($data)) {
            return [];
        }

        $index = static::primaryKey();
        $result = [];
        foreach ($data as $record) {
            $result[$record[$index]] = $record;
        }
        return $result;
    }

    /**
     * Retorna un array asociativo con id=>nombre
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return array
     *
     */
    public function get_all_names()
    {
        $pk = static::primaryKey();
        $todos = $this->all();
        $return = [];
        foreach ($todos as $value) {
            $return[$value->{$pk}] = $value->get_name();
        }
        return $return;
    }

    /**
     * Devuelve un array con todos las registros.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return static[]
     *
     */
    public function all($deleted = false)
    {
        $where = '';
        if (!$deleted && $this->use_deleted_at) {
            $where = ' WHERE deleted_at IS NULL';
        }

        $list = [];

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`" . $where
            . ";";

        $data = DB::select($sql);
        if ($data) {
            foreach ($data as $d) {
                $list[] = new static($d);
            }
        }
        return $list;
    }

    /**
     * Retorna un array con todos los elementos de una tabla según la selección ordenado por
     * la clave primaria.
     * - offset, indica el primer elemento a retornar, por defecto 0.
     * - limit, indica el número de elementos a retornar. Por defecto, todos (ver si
     *   utilizar un valor predefinido en la configuración).
     * - lastSyncDate sería una fecha en formato "YYYY-MM-DD HH:MM:SS", o false. Indica si
     *   sólo hay que retornar los actualizados a partir de determinada fecha (la fecha de
     *   última sincronización).
     * Se retorna un stdClass con 4 atributos:
     * - 'data', que contiene un array asociativo con los datos devueltos.
     * - 'total', que indica el número total de registros de la tabla.
     * - 'pagination', que indica cuántos se han pedido.
     * - 'served', que indica cuántos se han servido.
     * Para recorrer toda la tabla, basta con ir incrementando el offset en limit hasta que
     * lo servido sea menor que lo pedido, o 0.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0718
     *
     * @param int          $offset
     * @param int          $limit
     * @param string|false $lastSyncDate
     * @param bool         $returnDeleted
     *
     * @return stdClass
     */
    public static function getAll(int $offset = 0, int $limit = PHP_INT_MAX, $lastSyncDate = false, bool $returnDeleted = false): stdClass
    {
        $modelName = get_called_class();
        $model = new $modelName();
        $fieldId = $model->primaryKey();

        if ($lastSyncDate !== false && !$model->use_updated_at) {
            self::setResponse('status', XnetModel::ERROR);
            self::setResponse('errors', 'No tiene definido el campo use_updated_at, luego no puede usar el parámetro lastSyncDate');
            return self::getResponse();
        }

        // Se define la tabla de la que tomar los datos (FROM)
        $fromSql = "FROM {$model->tablename()} f ";

        // Se establecen los criterios de selección de registros (WHERE)
        $whereSql = '';
        $where = [];
        if ($lastSyncDate) {
            $where[] = "f.updated_at >= '$lastSyncDate'";
        }
        if ($model->use_deleted_at && !$returnDeleted) {
            $where[] = 'f.deleted_at IS NULL';
        }
        if (count($where) > 0) {
            $whereSql = 'WHERE ' . implode(' AND ', $where);
        }

        // Se realiza la consulta de los datos a retornar
        $selectSql = "SELECT f.* ";
        $limitSql = " ORDER BY f.{$fieldId} LIMIT {$limit} OFFSET {$offset};";

        $sql = $selectSql . $fromSql . $whereSql . $limitSql;

        $data = DB::select($sql);

        // Se calcula el total de registros afectados (retornados o no)
        $selectSql = "SELECT COUNT({$fieldId}) as total ";
        $limitSql = ';';

        $sql = $selectSql . $fromSql . $whereSql . $limitSql;
        $result = DB::select($sql);
        $total = (int) $result[0]['total'] ?? 0;

        // Se prepara la respuesta como un strClass con 4 campos
        $return = new stdClass();
        $return->data = $data;
        $return->total = $total;
        $return->pagination = $limit;
        $return->served = count($data);
        return $return;
    }

    /**
     * Define un método para obtener el nombre del registro actual
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return string
     *
     */
    abstract public function get_name();

    /**
     * Retorna un array asociativo con id=>registro
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return array
     *
     */
    public function getAllValues()
    {
        $pk = static::primaryKey();
        $todos = $this->all();
        $return = [];
        foreach ($todos as $value) {
            $return[$value->{$pk}] = $value;
        }
        return $return;
    }

    /**
     * Devuelve el numero total registros.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return int
     *
     */
    public function count()
    {
        $sql = "SELECT COUNT(id) AS total FROM " . $this->table_name()
            . ";";
        $data = DB::select($sql);
        return (empty($data) ? 0 : (int) $data[0]['total']);
    }

    /**
     * Retorna el valor de la clave primaria para el registro en curso
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0305
     *
     * @return mixed
     */
    public function get_id()
    {
        return $this->{static::primaryKey()};
    }

    /**
     * Comprueba los datos del modelo y retorna status OK si son correctos.
     * En caso de error, retorna status ERROR y en data un stdClass con los errores.
     *
     * @return bool
     */
    public function test()
    {
        self::setResponse('status', self::OK);

        foreach (self::getFields() as $field => $value) {
            switch ($value['generictype']) {
                case 'integer':
                    if (isset($this->{$field})) {
                        if (isset($value['min']) && $this->{$field} < $value['min']) {
                            $msg = static::tablename() . ' (' . $field . '): El valor ' . $this->{$field} . ' es inferior al mínimo permitido: ' . $value['min'];
                            self::setResponse('errors', $msg);
                            self::setResponse('status', self::ERROR);
                            debug_message($msg);
                        }
                        if (isset($value['max']) && $this->{$field} > $value['max']) {
                            $msg = static::tablename() . ' (' . $field . '): El valor ' . $this->{$field} . ' es superior al mínimo permitido: ' . $value['max'];
                            self::setResponse('errors', $msg);
                            self::setResponse('status', self::ERROR);
                            debug_message($msg);
                        }
                    }
                    break;
                case 'string':
                    if (isset($value['maxlength']) && strlen($this->{$field}) > $value['maxlength']) {
                        $msg = static::tablename() . ' (' . $field . '): El valor "' . $this->{$field} . '" excede el máximo de caracteres permtidos: ' . $value['maxlength'];
                        self::setResponse('errors', $msg);
                        self::setResponse('status', self::ERROR);
                        debug_message($msg);
                    }
                    break;
                default:
            }
        }

        if ($this->use_created_at && !isset($this->created_at)) {
            $this->created_at = gmdate('Y-m-d H:i:s');
        }

        if ($this->use_updated_at) {
            $this->updated_at = gmdate('Y-m-d H:i:s');
        }

        return self::$response->status === self::OK;
    }

    /**
     * Inserta o actualiza un registro.
     * Lo busca primero por los datos proporcionados en unique.
     * Si existe lo actualiza, y si no existe (o unique es un array vacío), lo crea.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0704
     *
     * @param $data
     * @param $unique
     */
    public function import_record($data, $unique = [])
    {
        $record = clone $this;
        if (!empty($unique)) {
            $search = $record->get_by($unique);
            if (count($search > 0)) {
                $datum = reset($search);
                $record = $record->get($datum[static::primaryKey()]);
            }
        }
        foreach ($data as $key => $datum) {
            $record->{$key} = $datum;
        }
        return $record->save();
    }

    /**
     * Realiza un borrado del registro actual.
     * Si no se especifica lo contrario, y es posible, hace un borrado suave.
     * Un borrado suave consiste en poner deleted_at en el instante actual.
     * Si no acepta borrado suave, o se especifica false en softDelete, hace un borrado físico.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0709
     *
     * @param $softDelete
     *
     * @return stdClass
     */
    public function delete($softDelete = true)
    {
        if ($softDelete === false || $this->use_deleted_at === false) {
            $sql = "DELETE FROM `" . $this->table_name() . "`"
                . " WHERE " . static::primaryKey() . " = '" . static::primaryKeyValue() . "'";
            return DB::exec($sql);
        }

        if ($this->deleted_at === null) {
            $this->deleted_at = gmdate('Y-m-d H:i:s');
            return $this->save();
        }

        self::setResponse('info', 'No hay datos que actualizar (sin cambios en el registro)');
        self::setResponse('data', $this);
        return self::getResponse();
    }

    /**
     * Realiza un borrado del registro especificado.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0718
     *
     * @param $id
     * @param $softDelete
     *
     * @return mixed|stdClass
     */
    public static function deleteRecord($id, $softDelete = true)
    {
        $record = self::getById($id);
        if ($record === false) {
            self::setResponse('info', 'Registro no encontrado');
            return self::getResponse();
        }
        return $record->delete($softDelete);
    }

    /**
     * Retorna un registro que relaciona un modelo con un registro de una tabla.
     * Por ejemplo, una dirección con una persona.
     * Véase su uso en ContactRelationship o AddressRelationship
     *
     * TODO: Reordenar el código que por cambios de última hora está hecho unos zorros.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0713
     *
     * @param XnetModel   $relationshipModel
     * @param array       $relation
     * @param string|null $name
     *
     * @return bool|fs_extended_model|XnetModel
     */
    public static function getRelationship(XnetModel $relationshipModel, array $relation, ?string $name = null)
    {
        $relationship = $relationshipModel->get_by($relation);

        // Si existe la relación, se retorna directamente
        if (!empty($relationship)) {
            $link = $relationshipModel->get(reset($relationship)['id']);
            if (isset($name)) {
                $link->name = $name;
            }
            if (!$link->save()) {
                debug_message("Imposible actualizar el nombre $name en " . $relationshipModel->table_name);
            }
            return $link;
        }
        $link = $relationshipModel;
        foreach ($relation as $field => $value) {
            $link->{$field} = $value;
        }
        if (isset($name)) {
            $link->name = $name;
        }
        if (!$link->save()) {
            debug_message("Imposible crear la relación de $name en " . $relationshipModel->table_name);
            return false;
        }

        return $link;
    }

    /**
     * TODO:
     *
     * A partir de aquí, los métodos son copiados de fs_extended_model y tendrán que ser
     * eliminados y sutituídos por los nuevos.
     *
     * Igual vamos a tener que hacer lo mismo con fs_model.
     */

    /**
     * Limpia los valores del modelo, asignando sus atributos a null, o a su valor por defecto.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0719
     *
     */
    public function clear()
    {
        // $columns=$this->db->get_columns($this->tablename());
        $columns = DBSchema::getColumns($this->tablename());
        foreach ($columns as $column) {
            $field = $column['name'];
            $value = $column['default'] ?? null;
            $this->{$field} = $value;
        }
    }

    /**
     * Devuelve los campos del modelo.
     *
     * @return array
     */
    public function get_model_fields()
    {
        $tablename = $this->tablename();
        if (!isset(self::$model_fields[$tablename])) {
            self::$model_fields[$tablename] = [];
            foreach (DBSchema::getColumns($tablename) as $column) {
                self::$model_fields[$tablename][$column['name']] = $column['type'];
            }
        }

        return self::$model_fields[$tablename];
    }

    /**
     * Devuelve la clave primaria de la tabla.
     *
     * @return string
     *
     * @deprecated Utilice primaryKey()
     */
    final public function primary_column()
    {
        return static::primaryKey();
    }

    /**
     * Retorna el campo que hace de clave primaria (generalmente 'id')
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0719
     *
     * @return mixed
     */
    abstract static public function primaryKey();

    /**
     * Retorna el nombre de la tabla.
     * El nombre de la tabla deberá de estar en minúsculas, en plural y en
     * formato snake_case.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0719
     *
     * @return mixed
     */
    abstract static public function tablename();

    /**
     * Devuelve el valor de la clave primaria del modelo.
     *
     * @return mixed
     *
     * @deprecated Utilice primaryKeyValue()
     */
    public function primary_column_value()
    {
        return static::primaryKeyValue();
    }

    public function primaryKeyValue()
    {
        if (!$this) {
            return 'No se ha cargado ningún registro';
        }
        return $this->{static::primaryKey()};
    }

    /**
     * Devuelve el objeto obtenido a partir del valor y clave indicados.
     * Por defecto la clave es la clave primaria del modelo.
     *
     * @param string $code
     * @param string $col_name
     *
     * @return XnetModel|bool
     */
    public function get($code, $col_name = '')
    {
        $column = empty($col_name) ? static::primaryKey() : $col_name;
        $sql = "SELECT *"
            . " FROM `" . $this->tablename() . "`"
            . " WHERE " . $column . " = " . $this->var2str($code)
            . ";";

        $data = DB::select($sql);
        if (empty($data)) {
            return false;
        }

        $model_class = get_class($this);
        return new $model_class($data[0]);
    }

    /**
     * Retorna el nombre de la clase modelo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0305
     *
     * @return string
     */
    public function model_class_name()
    {
        return get_class_name($this);
    }

    /**
     * Carga un registro a partir de su clave primaria.
     *
     * @param string $code Valor de la clave primaria
     *
     * @return bool
     */
    public function load_from_code($code)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->tablename() . "`"
            . " WHERE " . static::primaryKey() . " = " . $this->var2str($code)
            . ";";
        $data = DB::select($sql);
        if (empty($data)) {
            return false;
        }

        $this->load_from_data($data[0]);
        return true;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null(static::primaryKeyValue())) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE " . static::primaryKey() . " = " . $this->var2str(static::primaryKeyValue())
            . ";";
        return (bool) DB::select($sql);
    }

    /**
     * Retorna la URL de acceso al listado o ficha de consulta/edición si es un elemento
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0822
     *
     * @param string $controller
     *
     * @return string
     */
    final public function url(string $controller): string
    {
        $code = $this->get_id();

        if (is_null($code)) {
            return 'index.php?page=' . $controller;
        }

        return 'index.php?page=' . $controller . '&code=' . $code;
    }

    /**
     * Devuelve todos los registros de la tabla aplicando las cláusulas where, order, offset y limit indicadas
     * TODO: Este método podría sustituir perfectamente a all.
     *
     * Aquí hay algún tipo de inconsistencia, porque para formar los array se usan las clases getWhereValues y
     * getRequestArray y parece que no están funcionando bien.
     * El único ejemplo que he visto en el que se usa es en ApiModel, y retorna siempre un array vacío en ambos.
     *
     * TODO: Entiendo que este método tendría que leer los array y contatenar, pero podría estar mal implementado.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.10
     *
     * @param array $where
     * @param array $order
     * @param int   $offset
     * @param int   $limit
     *
     * @return static[]
     */
    public function all_query(array $where = [], array $order = [], int $offset = 0, int $limit = FS_ITEM_LIMIT)
    {
        $sql = 'SELECT *'
            . ' FROM ' . $this->table_name();
        if (!empty($where)) {
            $sql .= ' WHERE';
            $nexo = '';
            foreach ($where as $clause) {
                $sql .= $nexo . $clause;
                $nexo = ' AND ';
            }
        }
        if (!empty($order)) {
            $sql .= ' ORDER BY';
            $nexo = '';
            foreach ($order as $clause) {
                $sql .= $nexo . $clause;
                $nexo = ', ';
            }
        }
        $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';

        return $this->all_from($sql, $offset, $limit);
    }

    /**
     * Realiza la operación update para el save.
     *
     * @return bool
     */
    protected function save_update()
    {
        $sql = 'UPDATE `' . $this->table_name() . '`';
        $coma = ' SET ';
        foreach (array_keys($this->get_model_fields()) as $field) {
            if ($field == static::primaryKey()) {
                continue;
            }

            $sql .= $coma . $field . ' = ' . $this->var2str($this->{$field});
            $coma = ', ';
        }

        $sql .= ' WHERE ' . static::primaryKey() . ' = ' . $this->var2str(static::primaryKeyValue())
            . ';';
        return DB::exec($sql);
    }

    /**
     * Realiza la operación insert para el save.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0630
     *
     * @return bool
     */
    protected function save_insert()
    {
        $columns = [];
        $values = [];

        $fields = DBSchema::getColumns_details($this->table_name());
        foreach ($fields as $field => $value) {
            if ($field === static::primaryKey() && $value['extra'] === 'auto_increment') {
                continue;
            }
            $columns[] = $field;
            $values[] = $this->var2str($this->{$field});
        }

        $sql = 'INSERT INTO `' . $this->table_name() . '` (' . implode(', ', $columns) . ') VALUES (' . implode(
                ', ',
                $values) . ');';

        if (!DB::exec($sql)) {
            return false;
        }

        if (null === static::primaryKeyValue()) {
            $this->{static::primaryKey()} = DBSchema::lastval();
        }

        return true;
    }

    /**
     * Retorna un array asociativo con las propiedades del modelo.
     * Usado por la API e importado de Alxarafe.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0610
     *
     * @param $id
     *
     * @return array
     */
    final public function getDataArray($id = null)
    {
        $result = [];
        if (!isset($id) && !isset($this->id)) {
            $result;
        }

        $data = $this->getById($id ?? $this->id);
        foreach (get_object_vars($data) as $propiedad => $value) {
            if (is_object($value) || $propiedad === 'table_name') {
                continue;
            }
            $result[$propiedad] = $data->{$propiedad};
        }
        return $result;
    }

    public static function getByCode($code): ?XnetModel
    {
        $data = self::getBy(['code' => $code]);
        if (count($data) === 0) {
            return null;
        }
        return self::getById(reset($data)['id']);
    }

}
