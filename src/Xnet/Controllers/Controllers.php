<?php
/*
 * This file is part of facturacion_base for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Controllers;

use Xnet\Core\XnetEditController;
use Xnet\Core\XnetModel;
use Xnet\Model\Action;
use Xnet\Model\ActionRole;
use Xnet\Model\Controller;

/**
 * Class Countries
 */
class Controllers extends XnetEditController
{
    /**
     * admin_paises constructor.
     */
    public function __construct()
    {
        $this->title = 'Mantenimiento de controladores';
        parent::__construct([
            'folder' => 'admin|dashboard|config',
            'title' => 'Controladores',
            'alias' => 'Páginas',
            'description' => 'Mantenimiento de controladores',
            'show_on_menu' => true,
            'order' => 1000,
        ]);

        new ActionRole();
        ActionRole::addActionToRole(1, 1);
    }

    public function getHeaders(): array
    {
        return [
            'name' => 'Nombre',
            'title' => 'Título',
            'alias' => 'Alias',
            'description' => 'Descripción',
            'folder' => 'Carpeta',
            'show_on_menu' => 'Mostrar en menú',
            'order' => 'Orden',
        ];
    }

    public function getFields(): array
    {
        $fields = parent::getFields();

        if (empty($fields)) {
            $fields = self::setFields(
                $this->mainModel->tablename(),
                [
                    [
                        'name' => [
                            'class' => 'col-sm-4',
                        ],
                        'title' => [
                            'class' => 'col-sm-4',
                        ],
                        'alias' => [
                            'class' => 'col-sm-4',
                        ],
                    ],
                    [
                        'description' => [
                            'class' => 'col-sm-6',
                        ],
                        'folder' => [
                            'class' => 'col-sm-6',
                        ],
                    ],
                    [
                        'show_on_menu' => [
                            'class' => 'col-sm-4',
                        ],
                        'order' => [
                            'class' => 'col-sm-4',
                        ],
                    ],
                ]
            );
        }

        return $fields;
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-earth-europe fa-fw"></i>';
    }

    public static function import()
    {
        new Controller();
        new Action();
    }

    public function getMainModel(): XnetModel
    {
        return new Controller();
    }
}
