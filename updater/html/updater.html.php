<?php if (!defined('BASE_PATH')) {
    die(':)');
} ?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Actualizador de MiFactura</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="generator" content="MiFactura"/>
    <link rel="shortcut icon" href="updater/img/favicon.ico"/>
    <link rel="stylesheet" href="updater/css/bootstrap.min.css">
    <link rel="stylesheet" href="updater/css/all.min.css"/>
    <script type="text/javascript" src="updater/js/jquery.min.js"></script>
    <script type="text/javascript" src="updater/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="updater/js/updater.js"></script>
</head>
<body>
<div class="container-fluid mt-3">
    <div class="page-header">
        <h1 class="fw-light">
            <i class="fa-solid fa-upload fa-fw"></i> Actualizador de MiFactura
        </h1>
    </div>
    <div class="row">
        <div class="col-12">
            <a href="updater.php" class="btn btn-outline-secondary" title="Recargar">
                <i class="fa-solid fa-refresh fa-fw"></i>
            </a>
            <a href="index.php?page=admin_home&updated=TRUE" class="btn btn-outline-secondary">
                <i class="fa-solid fa-arrow-left fa-fw"></i>
                <span class="d-none d-sm-inline">Panel de control</span>
            </a>
            <button class="btn btn-outline-success" id="update_button" onclick="update_all();return false;">
                <i class="fa-solid fa-cloud-upload fa-fw"></i>
                <span class="d-none d-sm-inline">Actualizar todo</span>
            </button>
        </div>
    </div>
</div>
<div class="container-fluid mt-3">
    <input id="downloads" hidden>
    <input id="download_name" hidden>
    <div class="table-responsive mt-3">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th class="text-start">Nombre</th>
                <th class="text-start">Lista de cambios</th>
                <th class="text-end">Versión<br>Instalada</th>
                <th class="text-end">Versión<br>Disponible</th>
                <th class="text-end">Versión<br>BB.DD.</th>
                <th class="text-end">Estado</th>
            </tr>
            </thead>
            <?php foreach ($community_core as $plugin): ?>
                <?php
                echo printTableLine(
                    'Núcleo',
                    $plugin['changelog'],
                    $plugin['version'],
                    $plugin['new_version'],
                    $plugin['db_version']
                );
                ?>
            <?php endforeach; ?>
            <?php foreach ($community_plugins as $nombre => $plugin): ?>
                <?php
                echo printTableLine(
                    $nombre,
                    $plugin['changelog'],
                    $plugin['version'],
                    $plugin['new_version'],
                    $plugin['db_version']
                );
                ?>
            <?php endforeach; ?>
            <?php /* foreach ($community_availables as $nombre => $plugin): ?>
                    <?php
                    echo printTableLine(
                        $nombre,
                        $plugin['description'],
                        $plugin['version'],
                        $plugin['new_version'],
                        $plugin['db_version']
                    );
                    ?>
                <?php endforeach; */ ?>
        </table>
    </div>
</div>

<div class="modal" id="modallog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <i class="fa-solid fa-cloud-upload fa-fw"></i>
                    Proceso de instalación/actualización
                </h4>
            </div>
            <div class="modal-body">
                <div class="mt-3 col-sm-12">
                    <textarea class="form-control" id="messages" rows="10" style="width: 100%; resize: none" readonly></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <div class="mt-3 col-sm-12">
                    <a class="btn btn-outline-primary" id="close_button" href="index.php?page=admin_home&action=check_plugins#plugins-tab">
                        <i class="fa-solid fa-close fa-fw"></i>
                        <span class="d-none d-sm-inline">Cerrar ventana</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
