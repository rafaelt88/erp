<?php

if (!defined('BASE_PATH')) {
    die(':)');
}

require_once(constant('BASE_PATH') . '/updater/core/Model.php');

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class version
 *
 * Accede al listado de plugins instalados y versión en la base de datos
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0620
 *
 */
class version extends Model
{
    /**
     * Identificador único autoincremental.
     *
     * @var null|int
     */
    public $id;

    /**
     * Nombre del plugin.
     *
     * @var string
     */
    public $plugin;

    /**
     * Versión del plugin.
     *
     * @var float
     */
    public $version;

    /**
     * Versión de las tablas del plugin en la base de datos.
     *
     * @var float
     */
    public $db_version;

    /**
     * Orden de activación. Null es no activo. El 0 está reservado para el núcleo.
     *
     * @var integer|null
     */
    public $sequence;

    /**
     * versions constructor.
     *
     * @param false|array $data
     */
    public function __construct()
    {
        parent::__construct('versions');
    }

    /**
     * Retorna el registro con el nombre especificado, o false si no existe.
     *
     * @param string $name
     *
     * @return array
     */
    public function getByName($name)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->tableName . "`"
            . " WHERE plugin='" . $name . "';";

        $result = $this->select($sql);
        if (count($result) === 0) {
            $return = new static();
            $return->plugin = $name;
            return $return;
        }

        return $this->get(reset($result)['id']);
    }
}
