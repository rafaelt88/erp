<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Contiene la ruta principal del alojamiento.
 *
 * @deprecated Utilice en su lugar BASE_PATH
 */
define('PATH', constant('BASE_PATH'));

/**
 * Contiene la ruta principal del alojamiento.
 *
 * No está deprecated, no es la misma ruta cuando es una multi instalación
 */
define('FS_FOLDER', constant('BASE_PATH'));

/**
 * Ruta al archivo para gestionar datos en periodo de mantenimiento
 */
define('MAINTENANCE_FILE', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'in_maintenance');

/**
 * Ruta para logs de la ejecución
 */
define('LOG_FILE', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'in_maintenance.log');

ignore_user_abort(true);

$action = isset($_GET['action']) ? $_GET['action'] : 'check_maintenance';

$in_maintenance = file_exists(constant('MAINTENANCE_FILE'));

$default_data = ['error' => 'Sin registro de mensajes'];
if (file_exists(constant('LOG_FILE'))) {
    $default_data = ['error' => file_get_contents(constant('LOG_FILE'))];
}

$reply = $in_maintenance ? json_decode(file_get_contents(constant('MAINTENANCE_FILE')), true) : $default_data;

switch ($action) {
    case 'check_maintenance':
        json_response($reply, JSON_PRETTY_PRINT);
        break;
    case 'check_wizard':
        // TODO: Para poder autovalidar el procesamiento de wizards
        $reply = $reply['wizards'] ?? [];
        json_response($reply, JSON_PRETTY_PRINT);
        break;
}
