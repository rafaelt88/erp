/**
 * Variable global que contiene el número de núcleos pendientes de actualizar.
 *
 * @type {number}
 */
let cores = 0;

/**
 * Variable global que contiene el número de plugins pendientes de actualizar.
 *
 * @type {number}
 */
let plugins = 0;

/**
 * Listado de plugins.
 *
 * @type {*[]}
 */
let pluginsList = [];

/**
 * Añade una línea de texto en el textarea de estado.
 *
 * @param {string} linea
 * @param {boolean} clear
 */
function addLine(linea, clear = false) {
    let line = $('#messages');
    let current = line.val();
    if (clear) {
        current = '';
    }
    line.val(current + '- ' + linea + "\n");
    if (line.length) {
        line.scrollTop(line[0].scrollHeight - line.height());
    }
}

/**
 * Cierra la pantalla modal de actualización. Se invoca al pulsar el botón cerrar.
 */
function closeModal() {
    $('#messages').val('');
    $('#modallog').hide();
    document.location.reload();
}

/**
 * Elimina el semáforo de instalación para liberar el uso de la aplicación.
 */
function unsetUpdatingStatus() {
    $.ajax({
        url: "updater_ajax.php?action=unset_updating",
        dataType: "json",
        async: false,
    }).done(function (reply) {
        if (reply.status) {
            addLine(reply.message);
            console.log(reply.message);
        } else {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
        }
    }).fail(function (reply) {
        addLine('ERROR: ' + reply.message);
        console.log('ERROR: ' + reply.message);
    });
}

/**
 * Genera un semáforo de actualización de la base de datos.
 */
function setDatabaseUpdateFlag() {
    $.ajax({
        url: "updater_ajax.php?action=set_database_update_flag",
        dataType: "json",
        async: false,
        timeout: 0,
    }).done(function (reply) {
        if (reply.status) {
            addLine(reply.message);
            console.log(reply.message);
        } else {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
        }
    }).fail(function (reply) {
        addLine('ERROR: ' + reply.message);
        console.log('ERROR: ' + reply.message);
    });
}

/**
 * Elimina un semáforo de actualización de la base de datos.
 */
function unsetDatabaseUpdateFlag() {
    $.ajax({
        url: "updater_ajax.php?action=unset_database_update_flag",
        dataType: "json",
        async: false,
    }).done(function (reply) {
        if (reply.status) {
            addLine(reply.message);
            console.log(reply.message);
        } else {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
        }
    }).fail(function (reply) {
        addLine('ERROR: ' + reply.message);
        console.log('ERROR: ' + reply.message);
    });
}

/**
 * Descarga el ZIP del núcleo en la carpeta temporal.
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function downloadCore(name) {
    console.log('Descargando el núcleo');
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "updater_ajax.php?action=download_core&name=" + name,
            dataType: "json",
            timeout: 0,
        }).fail(function (reply) {
            console.log('Promise error in downloadCore');
            addLine('Error al tratar de descargar el núcleo');
            reject(reply.responseJSON);
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.responseJSON);
            } else {
                addLine('ERROR: ' + reply.message);
                console.log('ERROR:');
                console.log(reply);
                reject(reply.responseJSON);
            }
        });
    });
}

/**
 * Realiza una copia de seguirdad de las carpetas del núcleo.
 * TODO: Hay que mantener actualizada la lista de carpetas del núcleo.
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function backupCoreFiles(name) {
    console.log('Haciendo copia de seguridad del núcleo');
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "updater_ajax.php?action=backup_core&name=" + name,
            dataType: "json",
            timeout: 0,
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject(reply.responseJSON);
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.responseJSON);
            } else {
                addLine('ERROR: ' + reply.message);
                console.log('ERROR:');
                console.log(reply);
                reject(reply.responseJSON);
            }
        });
    });
}

/**
 * Descomprime el ZIP de la comunidad en una carpeta temporal.
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function decompressCoreFiles(name) {
    console.log('Instalando el núcleo')
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "updater_ajax.php?action=decompress_core&name=" + name,
            dataType: "json",
            timeout: 0,
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject(reply.responseJSON);
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.responseJSON);
            } else {
                addLine('ERROR: ' + reply.message);
                console.log('ERROR:');
                console.log(reply);
                reject(reply.responseJSON);
            }
        });
    });
}

/**
 * Mueve los archivos del núcleo de la carpeta temporal a la de instalación.
 * Una vez movidos los archivos, se elimina de la carpeta temporal
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function moveCoreFiles(name) {
    console.log('Instalando el núcleo');
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "updater_ajax.php?action=install_core&name=" + name,
            dataType: "json",
            timeout: 0,
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject(reply.responseJSON);
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.responseJSON);
            } else {
                addLine('ERROR: ' + reply.message);
                console.log('ERROR:');
                console.log(reply);
                reject(reply.responseJSON);
            }
        });
    });
}

/**
 * Descarga el ZIP del plugin name en la carpeta temporal
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function downloadPlugin(name) {
    console.log('Descargando ' + name);
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "updater_ajax.php?action=download_plugin&name=" + name,
            dataType: "json",
            timeout: 0,
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject(reply.responseJSON);
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.responseJSON);
            } else {
                // TODO: Un plugin es posible que no se pueda descargar por múltiples motivos
                // Por tanto, sólo lo avisamos y lo ignoramos hasta otra actualización.
                addLine('AVISO: ' + reply.message);
                console.log('AVISO: ' + reply.message);
                reject(reply.responseJSON);
            }
        });
    });
}

/**
 * Instala el plugin especificado en name.
 * La instalación consiste en descomprimir el archivo zip en la carpeta plugins.
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function decompressPlugin(name) {
    console.log('Instalando ' + name);
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "updater_ajax.php?action=decompress_plugin&name=" + name,
            dataType: "json",
            timeout: 0,
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject(reply.responseJSON);
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.responseJSON);
            } else {
                addLine('ERROR: ' + reply.message);
                console.log('ERROR:');
                console.log(reply);
                reject(reply.responseJSON);
            }
        });
    });
}

/**
 * Instala el plugin especificado en name.
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function installPlugin(name) {
    console.log('Instalando ' + name);
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "updater_ajax.php?action=install_plugin&name=" + name,
            dataType: "json",
            timeout: 0,
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject(reply.responseJSON);
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.responseJSON);
            } else {
                addLine('ERROR: ' + reply.message);
                console.log('ERROR:');
                console.log(reply);
                reject(reply.responseJSON);
            }
        });
    });
}

/**
 * Lanza la descarga de todos los plugins.
 *
 * @param data
 */
function downloadPlugins(data) {
    pluginsList = data.plugins;
    plugins = Object.entries(pluginsList).length;
    check_pending = setTimeout(checkPending, 500);
    if (plugins > 0) {
        if (plugins === 1) {
            addLine('Actualizando 1 plugin.');
        } else {
            addLine('Actualizando ' + plugins + ' plugins.');
        }

        Object.entries(pluginsList).forEach(entry => {
            setTimeout(function () {
                updatePlugin(entry[0]);
            }, 250);
        });
    } else {
        addLine('Todos los plugins están actualizados.');
    }
    setDatabaseUpdateFlag();
}

/**
 * Descarga un plugin.
 *
 * @param name
 */
function updatePlugin(name) {
    /**
     * Tras cumplirse la promesa de descarga y descompresión de cada uno de los plugins,
     * se procede a moverlos a la carpeta de plugins.
     */
    downloadPlugin(name).then(function (reply) {
        decompressPlugin(name).then(function (reply) {
            installPlugin(name).then(function (reply) {
                plugins--;  // Ya queda un plugin menos para instalar
                checkPending();
            });
        });
    }).catch(function (reply) {
        // TODO: Ignoramos el inconveniente, ya se avisó al usuario, pero continuamos el proceso
        plugins--;  // Ya queda un plugin menos para instalar
        checkPending();
    });
}

/**
 * Se lanza la descarga e instalación del núcleo si procede.
 * Lanza la instalación de los plugins.
 *
 * @param data
 */
function downloadCores(data) {
    cores = Object.entries(data.core).length;
    if (cores > 0) {
        addLine('Descargando el núcleo');
        let name = Object.entries(data.core)[0][0];
        downloadCore(name).then(function (reply) {
            backupCoreFiles(name).then(function (reply) {
                decompressCoreFiles(name).then(function (reply) {
                    moveCoreFiles(name).then(function (reply) {
                        cores--;
                        checkPending();
                    });
                });
            });
        });
    } else {
        addLine('El núcleo está actualizado.');
    }
    downloadPlugins(data);
}

/**
 * Se establece el semáforo de instalación, y posteriormente, comienza con la instalación del núcleo.
 */
function updateAll() {
    $.ajax({
        url: "updater_ajax.php?action=set_updating",
        dataType: "json",
    }).fail(function (reply) {
        addLine('ERROR: ' + reply.message);
        console.log('ERROR: ' + reply.message);
        reject(reply.responseJSON);
    }).done(function (reply) {
        if (reply.status) {
            addLine(reply.message);
            console.log(reply.message);
            downloadCores(reply.info);
        } else {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
        }
    });
}

function updateApplication() {
    addLine('Actualizando la base de datos de la aplicación');
    retval = undefined;
    $.ajax({
        url: "index.php?page=admin_home&updated=TRUE",
        dataType: "json",
        timeout: 0,
        async: false,
    }).done(function (reply) {
        addLine('Forzando comprobación de columnas.');
        console.log('Forzando comprobación de columnas.');
        console.log(reply);
        retval = reply;
    }).fail(function (reply) {
        console.log('ERROR:');
        console.log(reply);
        addLine('Se ha producido un error durante la actualización');
        addLine(reply.plugin + ' ' + reply.method + ': ' + reply.message);
    });

    console.log('*** retval ***');
    console.log(retval);

    $.each(retval, function (i, line) {
        $.each(line, function (j, item) {
            let texto = item.plugin + ' ' + item.method + ': ' + item.message;
            console.log(i);
            console.log(j);
            console.log(item);
            console.log(texto);
            addLine(texto);
        });
    });
}

/**
 * Limpiamos caché.
 * TODO: Esto no es una petición AJAX, y realmente no podemos estar seguros de que se haya ejecutado. Por ahora asumimos que si.
 */
function cleanCache() {
    $.ajax({
        url: "index.php?page=admin_info&clean_cache=TRUE",
        dataType: "html",
        async: false,
        timeout: 0,
    }).done(function (data) {
        addLine('Caché limpiada');
        console.log('Caché limpiada');
    });
}

/**
 * Lanzamos la comprobación de todos los modelos activados.
 *
 * @returns {Promise<unknown>}
 */
function checkModels() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "index.php?page=admin_home&action=ajax_models",
            dataType: "json",
            async: true,
            timeout: 0,
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply.models);
            } else {
                addLine(reply.message);
                console.log(reply.message);
                reject(reply.message);
            }
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject('ERROR: ' + reply.message);
        });
    });
}

/**
 * Instancia el modelo
 *
 * @param name
 * @returns {Promise<unknown>}
 */
function instanciandoModelo(name) {
    console.log('Instanciando ' + name);
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "index.php?page=admin_home&action=ajax_instance_model&model=" + name,
            dataType: "json",
            async: true,
            timeout: 0,
        }).done(function (reply) {
            if (reply.status) {
                addLine(reply.message);
                console.log(reply.message);
                resolve(reply);
            } else {
                addLine(reply.message);
                console.log(reply.message);
                reject(reply.message);
            }
        }).fail(function (reply) {
            addLine('ERROR: ' + reply.message);
            console.log('ERROR: ' + reply.message);
            reject('ERROR: ' + reply.message);
        });
    });
}

/**
 * Se invoca cada vez que se descuenta un plugin o núcleo.
 */
function checkPending() {
    if (cores === 0 && plugins === 0) {
        unsetUpdatingStatus();
        updateApplication();
        unsetDatabaseUpdateFlag();
        cleanCache();
        checkModels().then(function (data) {
            addLine('Solicitando modelos activos.');
            console.log('Solicitando modelos activos.');
            let promises = [];
            data.forEach(function (element, index, array) {
                promises[index] = instanciandoModelo(element);
            });

            Promise.allSettled(promises).then(function () {
                addLine('Modelos instanciados.');
                console.log('Modelos instanciados.');

                $('#close_button').prop('disabled', false);
                $('#update_button').prop('disabled', false);
                $('#close_button').show();
            });
        });
    }
}

/**
 * Esta función es la que pone la bola a rodar.
 * Activa el semáforo, actualiza y luego desactiva el semáforo.
 */
function update_all() {
    $('#update_button').prop('disabled', true);
    $('#modallog').show();
    $('#close_button').prop('disabled', true);
    $('#close_button').hide();

    cleanCache();
    updateAll();
}
