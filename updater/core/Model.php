<?php

if (!defined('BASE_PATH')) {
    die(':)');
}

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class Model
 *
 * Clase simple para la manipulación de modelos sin dependencia del núcleo.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0620
 *
 */
abstract class Model
{
    private static $handle = null;
    private $id;
    public $tableName;
    public $tableExists;

    public function __construct($tableName)
    {
        $this->tableName = $tableName;
        if (!isset(self::$handle)) {
            try {
                foreach (['NAME', 'HOST', 'USER', 'PASS'] as $const) {
                    ${'db' . strtolower($const)} = defined('XFS_DB_' . $const) ? constant('XFS_DB_' . $const) : constant('FS_DB_' . $const);
                }
                self::$handle = new PDO('mysql:dbname=' . $dbname . ';host=' . $dbhost, $dbuser, $dbpass);
            } catch (PDOException $e) {
                die('Falló la conexión: ' . $e->getMessage());
            }
        }
        $this->tableExists = $this->tableExists();
    }

    public function tableExists()
    {
        return (self::$handle->prepare("DESCRIBE `$this->tableName`"))->execute();
    }

    public function testdata()
    {
        return true;
    }

    private function getFields()
    {
        $result = [];
        foreach ($this->select('SHOW COLUMNS FROM ' . $this->tableName) as $row) {
            $result[$row['Field']] = $row['Default'] ?? null;
        }
        return $result;
    }

    private function insert()
    {
        $fields = $this->getFields();
        $names = [];
        $values = [];
        foreach ($fields as $name => $default) {
            $value = $this->{$name} ?? $default ?? null;
            if ($value !== null) {
                $names[] = $name;
                $values[] = "'$value'";
            }
        }

        if (count($names) > 0) {
            $nameStr = implode(',', $names);
            $valueStr = implode(',', $values);
            $sql = "INSERT INTO {$this->tableName} ($nameStr) VALUES ($valueStr);";
            return self::$handle->exec($sql);
        }
        return true;
    }

    private function update()
    {
        $record = $this->get($this->id);
        $data = [];
        foreach ($this->getFields() as $name => $default) {
            if ($this->{$name} !== $record->{$name}) {
                $data[$name] = $record->{$name};
            }
        }

        if (count($data) === 0) {
            return true;
        }

        $dataArray = [];
        foreach ($data as $name => $value) {
            $dataArray[] = "`$name` = '$value'";
        }

        $sql = "UPDATE {$this->tableName} SET " . implode(',', $dataArray);
        return self::$handle->exec($sql);
    }

    public function save()
    {
        if (!$this->testdata()) {
            echo 'Test no superado para:';
            var_dump($this);
            return false;
        }

        if (!isset($this->id)) {
            return $this->insert();
        }
        return ($this->update());
    }

    public function loaddata($data)
    {
        $record = new static();
        foreach ($data as $key => $value) {
            $record->{$key} = $value;
        }
        return $record;
    }

    public function get($id)
    {
        if (empty($id)) {
            return false;
        }

        $data = $this->select("SELECT * FROM `{$this->tableName}` WHERE id={$id};");
        if (!$data || empty($data)) {
            return false;
        }
        return ($this->loaddata(reset($data)));
    }

    private function _select($sql)
    {
        return self::$handle->query($sql, PDO::FETCH_ASSOC);
    }

    public function select($sql)
    {
        $data = [];
        if (!$this->tableExists) {
            return $data;
        }
        foreach ($this->_select($sql) as $row) {
            $data[] = $row;
        }
        return ($data);
    }
}
