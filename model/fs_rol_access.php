<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define los permisos individuales para cada página dentro de un rol de usuarios.
 */
class fs_rol_access extends fs_model
{
    /**
     * Código del rol.
     *
     * @var null|string
     */
    public $codrol;

    /**
     * Nombre de la página (nombre del controlador).
     *
     * @var null|string
     */
    public $fs_page;

    /**
     * TRUE si el usuario tiene permisos para eliminar en la página.
     *
     * @var bool
     */
    public $allow_delete;

    /**
     * fs_rol_access constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_roles_access');
        if ($data) {
            $this->codrol = $data['codrol'];
            $this->fs_page = $data['fs_page'];
            $this->allow_delete = $this->str2bool($data['allow_delete']);
        } else {
            $this->codrol = null;
            $this->fs_page = null;
            $this->allow_delete = false;
        }
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET allow_delete = " . $this->var2str($this->allow_delete)
                . " WHERE codrol = " . $this->var2str($this->codrol)
                . " AND fs_page = " . $this->var2str($this->fs_page)
                . ";";
        } else {
            $sql = "INSERT INTO `" . $this->table_name() . "` (codrol,fs_page,allow_delete) VALUES ("
                . $this->var2str($this->codrol)
                . ", " . $this->var2str($this->fs_page)
                . ", " . $this->var2str($this->allow_delete)
                . ");";
        }

        return $this->db->exec($sql);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codrol)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codrol = " . $this->var2str($this->codrol)
            . " AND fs_page = " . $this->var2str($this->fs_page)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codrol = " . $this->var2str($this->codrol)
            . " AND fs_page = " . $this->var2str($this->fs_page)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve todos los registros asociados al rol indicado.
     *
     * @param string $codrol
     *
     * @return static[]
     */
    public function all_from_rol($codrol)
    {
        $accesslist = [];

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codrol = " . $this->var2str($codrol)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            foreach ($data as $a) {
                $accesslist[] = new static($a);
            }
        }

        return $accesslist;
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        return '';
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new fs_rol())->fix_db();
        (new fs_page())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        $fixes = [
            $this->table_name() => [
                'fs_roles_access_fs_roles' => 'UPDATE `' . $this->table_name() . '` SET codrol = NULL WHERE codrol = "";',
                // 'fs_roles_access_fs_page' => 'UPDATE `' . $this->table_name() . '` SET fs_page = NULL WHERE fs_page = "";',
                'fs_roles_access_fs_page' => 'DELETE FROM `' . $this->table_name() . '` WHERE fs_page = "";',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }
}
