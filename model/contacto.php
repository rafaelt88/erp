<?php
/**
 * This file is part of trading for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once FS_FOLDER . '/base/xfs_model.php';

class contacto extends xfs_model
{

    /**
     * Clave primaria.
     *
     * @var null|int
     */
    public $id;

    /**
     * Es un identificador que determina la tabla con la que está asociada la cuenta.
     *
     * @var integer
     */
    public $filtro_tabla_1;

    /**
     * Es un identificador que determina el ID de la tabla con la que está asociada la cuenta.
     *
     * @var integer
     */
    public $filtro_valor_1;


    public $nombre;
    public $cargo;
    public $fecha;
    public $direccion;
    public $codpostal;
    public $ciudad;
    public $provincia;
    public $apartado;
    public $codpais;

    /**
     * contacto constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('contactos', $data);
    }

    /**
     * Comprueba los datos del modelo, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = parent::test();
        /*
        if (!isset($this->id_cliente_final)) {
            $this->new_error_msg('Tiene que haber un cliente asociado a la dirección');
            $status = false;
        }
        if (strlen($this->nombre) < 1 || strlen($this->nombre) > 50) {
            $this->new_error_msg("Nombre de contacto de cliente no válido: " . $this->nombre);
            $status = false;
        }
        if (strlen($this->provincia) > 15) {
            $this->new_error_msg("La provincia no puede exceder de los 15 caracteres: " . $this->provincia);
            $status = false;
        }
        */
        return $status;
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        // new cliente_final();

        return '';
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            /*
            'albaranescli' => [
                'ca_albaranescli_dirclientes' => 'UPDATE `albaranescli` SET coddir = NULL WHERE coddir NOT IN (SELECT id FROM `dirclientes`);',
            ],
            'facturascli' => [
                'ca_facturascli_dirclientes' => 'UPDATE `facturascli` SET coddir = NULL WHERE coddir NOT IN (SELECT id FROM `dirclientes`);',
            ],
            'pedidoscli' => [
                'ca_pedidoscli_dirclientes' => 'UPDATE `pedidoscli` SET coddir = NULL WHERE coddir NOT IN (SELECT id FROM `dirclientes`);',
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_dirclientes' => 'UPDATE `presupuestoscli` SET coddir = NULL WHERE coddir NOT IN (SELECT id FROM `dirclientes`);',
            ],
            'reciboscli' => [
                'ca_reciboscli_dirclientes' => 'UPDATE `reciboscli` SET coddir = NULL WHERE coddir NOT IN (SELECT id FROM `dirclientes`);',
            ],
            'servicioscli' => [
                'ca_servicioscli_dirclientes' => 'UPDATE `servicioscli` SET coddir = NULL WHERE coddir NOT IN (SELECT id FROM `dirclientes`);',
            ],
            'tpv_comandas' => [
                'ca_tpv_comandas_dirclientes' => 'UPDATE `tpv_comandas` SET coddir = NULL WHERE coddir NOT IN (SELECT id FROM `dirclientes`);',
            ],
            */
        ];

        return $this->exec_fix_queries($fixes);
    }

    public function primary_column()
    {
        return 'id';
    }

    public function get_name()
    {
        return $this->direccion;
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     */
    protected function check_model_dependencies()
    {
        parent::check_model_dependencies();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @return bool
     */
    protected function fix_model_table()
    {
        $fixes = [
            /*
            $this->table_name() => [
                'ca_dirclientes_clientes' => 'UPDATE `' . $this->table_name() . '` SET codcliente = NULL WHERE codcliente = "";',
                'ca_dirclientes_paises' => 'UPDATE `' . $this->table_name() . '` SET codpais = NULL WHERE codpais = "";',
            ],
            */
        ];

        return $this->exec_fix_queries($fixes);
    }

    public function _all_from_cliente($codcliente)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id_cliente_final = " . $this->var2str($codcliente)
            . " ORDER BY id DESC;";
        return $this->all_from($sql, 0, 0);
    }
}
