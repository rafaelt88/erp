<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Este modelo permite relacionar dos elementos de tablas distintas.
 */
class fs_relation extends fs_model
{
    /**
     * Clave primaria.
     *
     * @var null|int
     */
    public $id;

    /**
     * Nombre de la tabla 1.
     *
     * @var null|string
     */
    public $table1;

    /**
     * Campo para la tabla 1.
     *
     * @var null|int
     */
    public $id1;

    /**
     * Nombre de la tabla 2.
     *
     * @var null|string
     */
    public $table2;

    /**
     * Campo para la tabla 2.
     *
     * @var null|int
     */
    public $id2;

    /**
     * URL a devolver.
     *
     * @var null|string
     */
    public $return_url;

    /**
     * fs_relation constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_relations');
        if ($data) {
            $this->id = $this->intval($data['id']);
            $this->table1 = $data['table1'];
            $this->id1 = $data['id1'];
            $this->table2 = $data['table2'];
            $this->id2 = $data['id2'];
            $this->return_url = $data['return_url'];
        } else {
            $this->id = null;
            $this->table1 = null;
            $this->id1 = null;
            $this->table2 = null;
            $this->id2 = null;
            $this->return_url = null;
        }
    }

    /**
     * Devuelve el registro por ID o false si no se encuentra.
     *
     * @param int $id
     *
     * @return false|static
     */
    public function get($id)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($id)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "` SET "
                . "table1 = " . $this->var2str($this->table1)
                . ", id1 = " . $this->var2str($this->id1)
                . ", table2 = " . $this->var2str($this->table2)
                . ", id2 = " . $this->var2str($this->id2)
                . ", return_url = " . $this->var2str($this->return_url)
                . " WHERE id = " . $this->var2str($this->id)
                . ";";

            return $this->db->exec($sql);
        }

        $sql = "INSERT INTO `" . $this->table_name() . "` (table1,id1,table2,id2,return_url) VALUES ("
            . $this->var2str($this->table1)
            . ", " . $this->var2str($this->id1)
            . ", " . $this->var2str($this->table2)
            . ", " . $this->var2str($this->id2)
            . ", " . $this->var2str($this->return_url)
            . ");";

        if ($this->db->exec($sql)) {
            $this->id = $this->db->lastval();
            return true;
        }

        return false;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->id)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve el registro filtrado por una tabla y su ID.
     *
     * @param string $table
     * @param int    $id
     *
     * @return static[]
     */
    public function all_for($table, $id)
    {
        $rlist = [];
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE (table1 = " . $this->var2str($table)
            . " AND id1 = " . $this->var2str($id) . ") OR (table2 = " . $this->var2str($table)
            . " AND id2 = " . $this->var2str($id) . ") ORDER BY id DESC;";

        $data = $this->db->select($sql);
        if ($data) {
            foreach ($data as $d) {
                $rlist[] = new static($d);
            }
        }

        return $rlist;
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        return '';
    }
}
