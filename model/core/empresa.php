<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;
use fs_var;
use PHPMailer;

require_once constant('BASE_PATH') . '/vendor/autoload.php';

/**
 * Esta clase almacena los principales datos de la empresa.
 * Solamente se puede manejar una empresa en cada base de datos.
 */
class empresa extends fs_model
{
    /**
     * Devuelve los datos de email.
     *
     * @var array
     */
    private static $email_cache;

    /**
     * Clave primaria. Integer.
     *
     * @var null|int
     */
    public $id;

    /**
     * Identificador único para la empresa.
     *
     * @var string
     */
    public $xid;

    /**
     * Todavía sin uso.
     *
     * @var bool
     */
    public $stockpedidos;

    /**
     * TRUE -> activa la contabilidad integrada. Se genera el asiento correspondiente
     * cada vez que se crea/modifica una factura.
     *
     * @var bool
     */
    public $contintegrada;

    /**
     * TRUE -> activa el uso de recargo de equivalencia en los albaranes y facturas de compra.
     *
     * @var bool
     */
    public $recequivalencia;

    /**
     * Código de la serie por defecto.
     *
     * @var string
     */
    public $codserie;

    /**
     * Código del impuesto por defecto.
     *
     * @var string
     */
    public $codimpuesto;

    /**
     * Código del impuesto por defecto para sin impuestos.
     *
     * @var string
     */
    public $codimpuesto_sinimpuestos;

    /**
     * Código del almacén predeterminado.
     *
     * @var string
     */
    public $codalmacen;

    /**
     * Código de la forma de pago predeterminada.
     *
     * @var string
     */
    public $codpago;

    /**
     * Código de la divisa predeterminada.
     *
     * @var string
     */
    public $coddivisa;

    /**
     * Código del ejercicio predeterminado.
     *
     * @var string
     */
    public $codejercicio;

    /**
     * Web.
     *
     * @var string
     */
    public $web;

    /**
     * Dirección email.
     *
     * @var string
     */
    public $email;

    /**
     * Número de fax.
     *
     * @var string
     */
    public $fax;

    /**
     * Número de teléfono.
     *
     * @var string
     */
    public $telefono;

    /**
     * Código de pais.
     *
     * @var string
     */
    public $codpais;

    /**
     * Apartado de correos.
     *
     * @var string
     */
    public $apartado;

    /**
     * Provincia.
     *
     * @var string
     */
    public $provincia;

    /**
     * Ciudad.
     *
     * @var string
     */
    public $ciudad;

    /**
     * Código postal.
     *
     * @var string
     */
    public $codpostal;

    /**
     * Dirección.
     *
     * @var string
     */
    public $direccion;

    /**
     * Nombre del administrador de la empresa.
     *
     * @var string
     */
    public $administrador;

    /**
     * Actualmente sin uso.
     *
     * @var string
     */
    public $codedi;

    /**
     * Identificador fiscal de la empresa.
     *
     * @var string
     */
    public $cifnif;

    /**
     * Nombre.
     *
     * @var string
     */
    public $nombre;

    /**
     * Nombre corto de la empresa, para mostrar en el menú
     *
     * @var string Nombre a mostrar en el menú.
     */
    public $nombrecorto;

    /**
     * Lema de la empresa
     *
     * @var string
     */
    public $lema;

    /**
     * Horario de apertura
     *
     * @var string
     */
    public $horario;

    /**
     * Texto al pié de las facturas de venta.
     *
     * @var string
     */
    public $pie_factura;

    /**
     * Fecha de inicio de la actividad.
     *
     * @var string
     */
    public $inicio_actividad;

    /**
     * Régimen de IVA de la empresa.
     *
     * @var string
     */
    public $regimeniva;

    /**
     * Opciones de configuración del correo.
     *
     * @var array
     */
    public $email_config;

    /**
     * Nombre de la plantilla de login.
     *
     * @var string
     */
    public $plantilla_login;

    /**
     * Se ha habilitado el ancho para artículos.
     *
     * @var bool
     */
    public $articulo_habilitar_ancho;

    /**
     * Se ha habilitado el alto para artículos.
     *
     * @var bool
     */
    public $articulo_habilitar_alto;

    /**
     * Se ha habilitado el profundo para artículos.
     *
     * @var bool
     */
    public $articulo_habilitar_profundo;

    /**
     * Se ha habilitado la introducción rápida de líneas.
     *
     * @var bool
     */
    public $habilitar_linea_rapida;

    /**
     * empresa constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('empresa');

        if (!empty($data)) {
            $this->id = $this->intval($data['id']);
            $this->xid = $data['xid'];
            $this->stockpedidos = $this->str2bool($data['stockpedidos']);
            $this->contintegrada = $this->str2bool($data['contintegrada']);
            $this->recequivalencia = $this->str2bool($data['recequivalencia']);
            $this->codserie = $data['codserie'];
            $this->codimpuesto = $data['codimpuesto'];
            $this->codimpuesto_sinimpuestos = isset($data['codimpuesto_sinimpuestos']) ? $data['codimpuesto_sinimpuestos'] : null;
            $this->codalmacen = $data['codalmacen'];
            $this->codpago = $data['codpago'];
            $this->coddivisa = $data['coddivisa'];
            $this->codejercicio = $data['codejercicio'];
            $this->web = $data['web'];
            $this->email = $data['email'];
            $this->fax = $data['fax'];
            $this->telefono = $data['telefono'];
            $this->codpais = $data['codpais'];
            $this->apartado = $data['apartado'];
            $this->provincia = $data['provincia'];
            $this->ciudad = $data['ciudad'];
            $this->codpostal = $data['codpostal'];
            $this->direccion = $data['direccion'];
            $this->administrador = $data['administrador'];
            $this->codedi = $data['codedi'];
            $this->cifnif = $data['cifnif'];
            $this->nombre = $data['nombre'];
            $this->nombrecorto = $data['nombrecorto'];
            $this->lema = $data['lema'];
            $this->horario = $data['horario'];
            $this->pie_factura = $data['pie_factura'];
            $this->inicio_actividad = date('Y-m-d', strtotime($data['inicioact']));
            $this->regimeniva = $data['regimeniva'];
            $this->plantilla_login = isset($data['plantilla_login']) ? $data['plantilla_login'] : 'desplegable';
            $this->articulo_habilitar_ancho = $data['articulo_habilitar_ancho'] ?? false;
            $this->articulo_habilitar_alto = $data['articulo_habilitar_alto'] ?? false;
            $this->articulo_habilitar_profundo = $data['articulo_habilitar_profundo'] ?? false;
            $this->habilitar_linea_rapida = $data['habilitar_linea_rapida'] ?? false;
            $this->get_email_config();

            if (is_null($this->xid)) {
                $this->xid = $this->random_string(30);
                $this->save();
            }
        }
    }

    /**
     * Retorna los datos del contrato según por el id
     *
     * @param int $id
     *
     * @return false|static
     */
    public function get($id)
    {
        /// leemos los datos de la empresa de memcache o de la base de datos
        $data = $this->cache->get_array('empresa');
        if ($data) {
            return new static($data);
        } else {
            $sql = "SELECT *"
                . " FROM `" . $this->table_name() . "`"
                . " WHERE id='$id';";
            $data = $this->db->select($sql);
            if (isset($data[0])) {
                $this->cache->set('empresa', $data[0]);
                return new static($data[0]);
            }
        }

        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if (isset($this->email_config)) {
                /// guardamos la configuración de email
                $fsvar = new fs_var();
                $fsvar->array_save($this->email_config);
            }

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "nombre = " . $this->var2str($this->nombre)
                    . ", nombrecorto = " . $this->var2str($this->nombrecorto)
                    . ", cifnif = " . $this->var2str($this->cifnif)
                    . ", codedi = " . $this->var2str($this->codedi)
                    . ", administrador = " . $this->var2str($this->administrador)
                    . ", direccion = " . $this->var2str($this->direccion)
                    . ", codpostal = " . $this->var2str($this->codpostal)
                    . ", ciudad = " . $this->var2str($this->ciudad)
                    . ", provincia = " . $this->var2str($this->provincia)
                    . ", apartado = " . $this->var2str($this->apartado)
                    . ", codpais = " . $this->var2str($this->codpais)
                    . ", telefono = " . $this->var2str($this->telefono)
                    . ", fax = " . $this->var2str($this->fax)
                    . ", email = " . $this->var2str($this->email)
                    . ", web = " . $this->var2str($this->web)
                    . ", codejercicio = " . $this->var2str($this->codejercicio)
                    . ", coddivisa = " . $this->var2str($this->coddivisa)
                    . ", codpago = " . $this->var2str($this->codpago)
                    . ", codalmacen = " . $this->var2str($this->codalmacen)
                    . ", codserie = " . $this->var2str($this->codserie)
                    . ", codimpuesto = " . $this->var2str($this->codimpuesto)
                    . ", codimpuesto_sinimpuestos = " . $this->var2str($this->codimpuesto_sinimpuestos)
                    . ", recequivalencia = " . $this->var2str($this->recequivalencia)
                    . ", contintegrada = " . $this->var2str($this->contintegrada)
                    . ", stockpedidos = " . $this->var2str($this->stockpedidos)
                    . ", xid = " . $this->var2str($this->xid)
                    . ", lema = " . $this->var2str($this->lema)
                    . ", horario = " . $this->var2str($this->horario)
                    . ", pie_factura = " . $this->var2str($this->pie_factura)
                    . ", inicioact = " . $this->var2str($this->inicio_actividad)
                    . ", regimeniva = " . $this->var2str($this->regimeniva)
                    . ", plantilla_login = " . $this->var2str($this->plantilla_login)
                    . ", articulo_habilitar_ancho = " . $this->var2str($this->articulo_habilitar_ancho)
                    . ", articulo_habilitar_alto = " . $this->var2str($this->articulo_habilitar_alto)
                    . ", articulo_habilitar_profundo = " . $this->var2str($this->articulo_habilitar_profundo)
                    . ", habilitar_linea_rapida = " . $this->var2str($this->habilitar_linea_rapida)
                    . " WHERE id = " . $this->var2str($this->id)
                    . ";";

                return $this->db->exec($sql);
            }

            $sql = "INSERT INTO `" . $this->table_name() . "` (xid,stockpedidos,contintegrada,recequivalencia,codserie,
               codimpuesto,codimpuesto_sinimpuestos,codalmacen,codpago,coddivisa,codejercicio,web,email,fax,telefono,
               codpais,apartado,provincia,ciudad,codpostal,direccion,administrador,codedi,cifnif,nombre,
               nombrecorto,lema,horario,pie_factura,inicioact,regimeniva,articulo_habilitar_ancho,articulo_habilitar_alto,
               articulo_habilitar_profundo,habilitar_linea_rapida) VALUES ("
                . $this->var2str($this->get_xid())
                . ", " . $this->var2str($this->stockpedidos)
                . ", " . $this->var2str($this->contintegrada)
                . ", " . $this->var2str($this->recequivalencia)
                . ", " . $this->var2str($this->codserie)
                . ", " . $this->var2str($this->codimpuesto)
                . ", " . $this->var2str($this->codimpuesto_sinimpuestos)
                . ", " . $this->var2str($this->codalmacen)
                . ", " . $this->var2str($this->codpago)
                . ", " . $this->var2str($this->coddivisa)
                . ", " . $this->var2str($this->codejercicio)
                . ", " . $this->var2str($this->web)
                . ", " . $this->var2str($this->email)
                . ", " . $this->var2str($this->fax)
                . ", " . $this->var2str($this->telefono)
                . ", " . $this->var2str($this->codpais)
                . ", " . $this->var2str($this->apartado)
                . ", " . $this->var2str($this->provincia)
                . ", " . $this->var2str($this->ciudad)
                . ", " . $this->var2str($this->codpostal)
                . ", " . $this->var2str($this->direccion)
                . ", " . $this->var2str($this->administrador)
                . ", " . $this->var2str($this->codedi)
                . ", " . $this->var2str($this->cifnif)
                . ", " . $this->var2str($this->nombre)
                . ", " . $this->var2str($this->nombrecorto)
                . ", " . $this->var2str($this->lema)
                . ", " . $this->var2str($this->horario)
                . ", " . $this->var2str($this->pie_factura)
                . ", " . $this->var2str($this->inicio_actividad)
                . ", " . $this->var2str($this->regimeniva)
                . ", " . $this->var2str($this->articulo_habilitar_ancho)
                . ", " . $this->var2str($this->articulo_habilitar_alto)
                . ", " . $this->var2str($this->articulo_habilitar_profundo)
                . ", " . $this->var2str($this->habilitar_linea_rapida)
                . ");";

            if ($this->db->exec($sql)) {
                $this->id = $this->db->lastval();
                return true;
            }
        }

        return false;
    }

    /**
     * Comprueba los datos de la empresa, devuelve TRUE si es correcto
     *
     * @return bool
     */
    public function test()
    {
        $status = false;

        if (is_null($this->xid)) {
            $this->xid = $this->random_string(30);
            $this->save();
        }

        $this->nombre = $this->no_html($this->nombre);
        $this->nombrecorto = $this->no_html($this->nombrecorto);
        $this->administrador = $this->no_html($this->administrador);
        $this->apartado = $this->no_html($this->apartado);
        $this->cifnif = $this->no_html($this->cifnif);
        $this->ciudad = $this->no_html($this->ciudad);
        $this->codpostal = $this->no_html($this->codpostal);
        $this->direccion = $this->no_html($this->direccion);
        $this->email = $this->no_html($this->email);
        $this->fax = $this->no_html($this->fax);
        $this->horario = $this->no_html($this->horario);
        $this->lema = $this->no_html($this->lema);
        $this->pie_factura = $this->no_html($this->pie_factura);
        $this->provincia = $this->no_html($this->provincia);
        $this->telefono = $this->no_html($this->telefono);
        $this->web = $this->no_html($this->web);

        $this->articulo_habilitar_ancho = !!$this->articulo_habilitar_ancho;
        $this->articulo_habilitar_alto = !!$this->articulo_habilitar_alto;
        $this->articulo_habilitar_profundo = !!$this->articulo_habilitar_profundo;
        $this->habilitar_linea_rapida = !!$this->habilitar_linea_rapida;

        if (empty($this->plantilla_login)) {
            $this->plantilla_login = 'default';
        }

        $len_nombre = strlen($this->nombre);
        $len_nombre_corto = strlen($this->nombrecorto);
        if ($len_nombre < 1 || $len_nombre > 100) {
            $this->new_error_msg("Nombre de empresa no válido, longitud de " . strlen($this->nombre) . ".");
        } elseif ($len_nombre < $len_nombre_corto) {
            $this->new_error_msg("El Nombre Corto debe ser más corto que el Nombre.");
        } else {
            $status = true;
        }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete('empresa');
        self::$email_cache = null;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->id)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        return 'index.php?page=admin_empresa';
    }

    /**
     * Devuelve TRUE si están definidos el email y la contraseña
     *
     * @return bool
     */
    public function can_send_mail()
    {
        if ($this->email && $this->email_config['mail_password']) {
            return true;
        }

        return false;
    }

    /**
     * Devuelve un objeto PHPMailer con la configuración ya preparada.
     *
     * @return PHPMailer
     */
    public function new_mail()
    {
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->WordWrap = 50;
        $mail->Mailer = $this->email_config['mail_mailer'];
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = $this->email_config['mail_enc'];
        $mail->Host = $this->email_config['mail_host'];
        $mail->Port = intval($this->email_config['mail_port']);

        $mail->Username = $this->email;
        if ($this->email_config['mail_user'] != '') {
            $mail->Username = $this->email_config['mail_user'];
        }

        $mail->Password = $this->email_config['mail_password'];
        $mail->From = $this->email;
        $mail->FromName = $this->nombre;

        if ($this->email_config['mail_bcc']) {
            $mail->addBCC($this->email_config['mail_bcc']);
        }

        return $mail;
    }

    /**
     * Devuelve el estado de conexión con el correo.
     *
     * @param PHPMailer $mail
     *
     * @return bool
     */
    public function mail_connect(&$mail)
    {
        if ($this->email_config['mail_mailer'] == 'smtp') {
            return $mail->smtpConnect($this->smtp_options());
        }

        return true;
    }

    /**
     * Devuelve un array con las opciones para $mail->smtpConnect) de PHPMailer
     *
     * @return array
     */
    public function smtp_options()
    {
        $SMTPOptions = [];

        if ($this->email_config['mail_low_security']) {
            $SMTPOptions = [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ],
            ];
        }

        return $SMTPOptions;
    }

    /**
     * Función llamada al enviar correctamente un email
     *
     * @param PHPMailer $mail
     */
    public function save_mail($mail)
    {
        /// tu código aquí
        /// $mail es el email ya enviado (es un objeto PHPMailer)
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta
     *
     * @return false
     */
    public function delete()
    {
        /// no se puede borrar la empresa
        return false;
    }

    /**
     * Devuelve el xid de la empresa, sinó tiene uno ya, lo genera, lo guarda y lo devuelve.
     *
     * @return string
     */
    public function get_xid()
    {
        if (empty($this->xid)) {
            $this->xid = $this->random_string(30);
            $this->save();
        }
        return $this->xid;
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        $num = mt_rand(1, 9999);
        return "INSERT INTO `" . $this->table_name() . "` (xid,stockpedidos,contintegrada,recequivalencia,codserie,"
            . "codimpuesto,codimpuesto_sinimpuestos,codalmacen,codpago,coddivisa,codejercicio,web,email,fax,telefono,codpais,apartado,provincia,"
            . "ciudad,codpostal,direccion,administrador,codedi,cifnif,nombre,nombrecorto,lema,horario,plantilla_login,articulo_habilitar_ancho,articulo_habilitar_alto,articulo_habilitar_profundo)"
            . "VALUES ('" . $this->random_string(30) . "',NULL,FALSE,NULL,'A','IVA0','IVA0','ALG','CONT','EUR', '" . date('Y') . "','"
            . constant('FS_COMMUNITY_URL') . "',"
            . "null,null,null,'ESP',null,null,null,null,'C / Falsa, 123','',null,'00000014Z','Empresa " . $num . " S.L.',"
            . "'E - " . $num . "','','','default'," . $this->var2str(false) . "," . $this->var2str(false) . "," . $this->var2str(false) . ");";
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new impuesto())->fix_db();
        (new serie())->fix_db();
        (new almacen())->fix_db();
        (new divisa())->fix_db();
        (new ejercicio())->fix_db();
        (new forma_pago())->fix_db();
        (new pais())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        $fixes = [
            $this->table_name() => [
                'ca_empresa_impuestos' => 'UPDATE `' . $this->table_name() . '` SET codimpuesto = null WHERE codimpuesto = "";',
                'ca_empresa_impuestos2' => 'UPDATE `' . $this->table_name() . '` SET codimpuesto_sinimpuestos = null WHERE codimpuesto_sinimpuestos = "";',
                'ca_empresa_series' => 'UPDATE `' . $this->table_name() . '` SET codserie = null WHERE codserie = "";',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Asigna los datos de configuración de email.
     */
    private function get_email_config()
    {
        if (self::$email_cache === null) {
            /// cargamos las opciones de email por defecto
            self::$email_cache = [
                'mail_password' => '',
                'mail_bcc' => '',
                'mail_firma' => "\n---\nEnviado con MiFactura.eu",
                'mail_mailer' => 'smtp',
                'mail_host' => 'smtp.gmail.com',
                'mail_port' => '465',
                'mail_enc' => 'ssl',
                'mail_user' => '',
                'mail_low_security' => false,
            ];

            $fsvar = new fs_var();
            self::$email_cache = $fsvar->array_get(self::$email_cache, false);
        }

        $this->email_config = self::$email_cache;
    }
}
