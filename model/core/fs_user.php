<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use agente;
use fs_access;
use fs_model;
use fs_page;

/**
 * Usuario de MiFactura.eu. Puede estar asociado a un agente.
 */
class fs_user extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_fs_user_all';

    /**
     * Clave primaria. Varchar (12).
     *
     * @var null|string
     */
    public $nick;

    /**
     * Contraseña, en sha1
     *
     * @var string
     */
    public $password;

    /**
     * Email del usuario.
     *
     * @var string
     */
    public $email;

    /**
     * Clave de sesión. El cliente se la guarda en una cookie,
     * sirve para no tener que guardar la contraseña.
     * Se regenera cada vez que el cliente inicia sesión. Así se
     * impide que dos personas accedan con el mismo usuario.
     *
     * @var string
     */
    public $log_key;

    /**
     * TRUE -> el usuario ha iniciado sesión
     * No se guarda en la base de datos
     *
     * @var bool
     */
    public $logged_on;

    /**
     * Código del agente/empleado asociado
     *
     * @var string
     */
    public $codagente;

    /**
     * El objeto agente asignado. Hay que llamar previamente la función get_agente().
     *
     * @var agente
     */
    public $agente;

    /**
     * TRUE -> el usuario es un administrador
     *
     * @var bool
     */
    public $admin;

    /**
     * TRUE -> el usuario esta activo
     *
     * @var bool
     */
    public $enabled;

    /**
     * Fecha del último login.
     *
     * @var string
     */
    public $last_login;

    /**
     * Hora del último login.
     *
     * @var string
     */
    public $last_login_time;

    /**
     * Última IP usada
     *
     * @var string
     */
    public $last_ip;

    /**
     * Último identificador de navegador usado
     *
     * @var string
     */
    public $last_browser;

    /**
     * Página de inicio.
     *
     * @var string
     */
    public $fs_page;

    /**
     * Plantilla CSS predeterminada.
     *
     * @var string
     */
    public $css;

    /**
     * Indica si se va a usar el menú vertical en lugar del horizontal
     *
     * @var bool
     */
    public $use_sidebar_menu;

    /**
     * Listado de fs_page.
     *
     * @var fs_page[]
     */
    private $menu;

    /**
     * Contiene un array con el menú multinivel.
     *
     * @var array
     */
    private $array_menu;

    /**
     * Contiene el nombre del color del tema
     *
     * @var string
     */
    public $theme_topbar;

    /**
     * Contiene el nombre del color del tema
     *
     * @var string
     */
    public $theme_sidebar;

    /**
     * Contiene el nombre del color del tema
     *
     * @var string
     */
    public $theme_body;

    /**
     * fs_user constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_users');
        if ($data) {
            $this->nick = $data['nick'];
            $this->password = $data['password'];
            $this->email = $data['email'];
            $this->log_key = $data['log_key'];
            $this->codagente = isset($data['codagente']) ? $data['codagente'] : null;
            $this->admin = $this->str2bool($data['admin']);
            $this->theme_topbar = $data['theme_topbar'] ?? 'light';
            $this->theme_sidebar = $data['theme_sidebar'] ?? 'dark';
            $this->theme_body = $data['theme_body'] ?? 'light';
            $this->last_login = $data['last_login'] ? date('Y-m-d', strtotime($data['last_login'])) : null;
            $this->last_login_time = $data['last_login_time'] ? $data['last_login_time'] : null;
            $this->last_ip = $data['last_ip'];
            $this->last_browser = $data['last_browser'];
            $this->fs_page = $data['fs_page'] !== null ? $data['fs_page'] : 'dashboard';
            $this->css = isset($data['css']) ? $data['css'] : 'node_modules/bootswatch/dist/yeti/bootstrap.min.css';
            $this->use_sidebar_menu = isset($data['use_sidebar_menu']) ? $this->str2bool($data['use_sidebar_menu']) : false;
            $this->enabled = isset($data['enabled']) ? $this->str2bool($data['enabled']) : true;
        } else {
            $this->nick = null;
            $this->password = null;
            $this->email = null;
            $this->log_key = null;
            $this->codagente = null;
            $this->admin = false;
            $this->theme_topbar = 'light';
            $this->theme_sidebar = 'dark';
            $this->theme_body = 'light';
            $this->enabled = true;
            $this->last_login = null;
            $this->last_login_time = null;
            $this->last_ip = null;
            $this->last_browser = null;
            $this->fs_page = null;
            $this->css = 'node_modules/bootswatch/dist/yeti/bootstrap.min.css';
            $this->use_sidebar_menu = false;
        }

        $this->logged_on = false;
        $this->agente = null;
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->nick)) {
            return 'index.php?page=admin_users';
        }

        return 'index.php?page=admin_user&snick=' . $this->nick;
    }

    /**
     * Devuelve el nombre completo del emplado.
     *
     * @return string|null
     */
    public function get_agente_fullname()
    {
        $agente = $this->get_agente();
        if ($agente) {
            return $agente->get_fullname();
        }

        return $this->nick;
    }

    /**
     * Devuelve el agente/empleado asociado
     *
     * @return bool|agente
     */
    public function get_agente()
    {
        if (isset($this->agente)) {
            return $this->agente;
        } elseif (is_null($this->codagente)) {
            return false;
        }

        $agente_model = new \agente();
        $agente = $agente_model->get($this->codagente);
        if ($agente) {
            $this->agente = $agente;
            return $this->agente;
        }

        $this->codagente = null;
        $this->save();
        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "password = " . $this->var2str($this->password)
                    . ", email = " . $this->var2str($this->email)
                    . ", log_key = " . $this->var2str($this->log_key)
                    . ", codagente = " . $this->var2str($this->codagente)
                    . ", admin = " . $this->var2str($this->admin)
                    . ", enabled = " . $this->var2str($this->enabled)
                    . ", last_login = " . $this->var2str($this->last_login)
                    . ", last_ip = " . $this->var2str($this->last_ip)
                    . ", last_browser = " . $this->var2str($this->last_browser)
                    . ", last_login_time = " . $this->var2str($this->last_login_time)
                    . ", fs_page = " . $this->var2str($this->fs_page)
                    . ", css = " . $this->var2str($this->css)
                    . ", use_sidebar_menu = " . $this->var2str($this->use_sidebar_menu)
                    . ", theme_topbar = " . $this->var2str($this->theme_topbar)
                    . ", theme_sidebar = " . $this->var2str($this->theme_sidebar)
                    . ", theme_body = " . $this->var2str($this->theme_body)
                    . " WHERE nick = " . $this->var2str($this->nick)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (nick,password,email,log_key,codagente,admin,enabled,
               last_login,last_login_time,last_ip,last_browser,fs_page,css,use_sidebar_menu,theme_topbar,theme_sidebar,theme_body) VALUES ("
                    . $this->var2str($this->nick)
                    . ", " . $this->var2str($this->password)
                    . ", " . $this->var2str($this->email)
                    . ", " . $this->var2str($this->log_key)
                    . ", " . $this->var2str($this->codagente)
                    . ", " . $this->var2str($this->admin)
                    . ", " . $this->var2str($this->enabled)
                    . ", " . $this->var2str($this->last_login)
                    . ", " . $this->var2str($this->last_login_time)
                    . ", " . $this->var2str($this->last_ip)
                    . ", " . $this->var2str($this->last_browser)
                    . ", " . $this->var2str($this->fs_page)
                    . ", " . $this->var2str($this->css)
                    . ", " . $this->var2str($this->use_sidebar_menu)
                    . ", " . $this->var2str($this->theme_topbar)
                    . ", " . $this->var2str($this->theme_sidebar)
                    . ", " . $this->var2str($this->theme_body)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos del modelo, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $this->nick = trim($this->nick);
        $this->last_browser = $this->no_html($this->last_browser);

        if ($this->fs_page === null) {
            $this->fs_page = 'dashboard';
        }

        if (!preg_match("/^[A-Z0-9_\+\.\-]{3,12}$/i", $this->nick)) {
            $this->new_error_msg("Nick no válido. Debe tener entre 3 y 12 caracteres,
            valen números o letras, pero no la Ñ ni acentos.");
            return false;
        }

        return true;
    }

    /**
     * Limpia la caché
     *
     * @param bool $full
     */
    public function clean_cache($full = false)
    {
        $this->cache->delete(self::CACHE_KEY_ALL);

        if ($full) {
            $this->clean_checked_tables();
        }
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->nick)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE nick = " . $this->var2str($this->nick)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Devuelve la URL asociada al agennte.
     *
     * @return string
     */
    public function get_agente_url()
    {
        $agente = $this->get_agente();
        if ($agente) {
            return $agente->url();
        }

        return '#';
    }

    /**
     * Devuelve TRUE si el usuario tiene acceso a la página solicitada.
     *
     * @param string $page_name
     *
     * @return bool
     */
    public function have_access_to($page_name)
    {
        $status = false;
        foreach ($this->get_menu() as $m) {
            if ($m->name == $page_name) {
                $status = true;
                break;
            }
        }
        return $status;
    }

    /**
     * Obtiene el menú multi-nivel en forma de array
     *
     * @param $reload
     *
     * @return array
     */
    public function get_array_menu($reload = false)
    {
        if (!isset($this->array_menu) || $reload) {
            $page = new fs_page();
            if ($this->admin || FS_DEMO) {
                $menu = $page->all();
            } else {
                $access = new fs_access();
                $access_list = $access->all_from_nick($this->nick);
                foreach ($page->all() as $p) {
                    foreach ($access_list as $a) {
                        if ($p->name == $a->fs_page) {
                            $menu[] = $p;
                            break;
                        }
                    }
                }
            }

            $this->array_menu = [];
            foreach ($menu as $page) {
                if (!$page->show_on_menu) {
                    continue;
                }

                $levels = explode('|', $page->folder);
                if (count($levels) > 4) {
                    $this->new_error_msg('Número de niveles excedidos en ' . $page->name . ': ' . $page->folder);
                    continue;
                }
                /*
                if (isset($levels[2])) {
                    $this->array_menu[$levels[0]][$levels[1]][$levels[2]][] = $page;
                } elseif (isset($levels[1])) {
                    $this->array_menu[$levels[0]][$levels[1]][] = $page;
                } else {
                    $this->array_menu[$levels[0]][] = $page;
                }
                */
                if (isset($levels[2])) {
                    $this->array_menu[$levels[0]][$levels[0] . '|' . $levels[1]][$levels[0] . '|' . $levels[1] . '|' . $levels[2]][] = $page;
                } elseif (isset($levels[1])) {
                    $this->array_menu[$levels[0]][$levels[0] . '|' . $levels[1]][] = $page;
                } else {
                    $this->array_menu[$levels[0]][] = $page;
                }
            }
        }
        return $this->array_menu;
    }

    /**
     * Devuelve el menú del usuario, el conjunto de páginas a las que tiene acceso.
     *
     * @param bool $reload
     *
     * @return fs_page[]
     */
    public function get_menu($reload = false)
    {
        if (!isset($this->menu) || $reload) {
            $this->menu = [];
            $page = new fs_page();

            if ($this->admin || FS_DEMO) {
                $this->menu = $page->all();
            } else {
                $access = new fs_access();
                $access_list = $access->all_from_nick($this->nick);
                foreach ($page->all() as $p) {
                    foreach ($access_list as $a) {
                        if ($p->name == $a->fs_page) {
                            $this->menu[] = $p;
                            break;
                        }
                    }
                }
            }
        }
        return $this->menu;
    }

    /**
     * Devuelve TRUE si el usuario tiene permiso para eliminar elementos en la página solicitada.
     *
     * @param string $page_name
     *
     * @return bool
     */
    public function allow_delete_on($page_name)
    {
        if ($this->admin || FS_DEMO) {
            return true;
        }

        $status = false;
        foreach ($this->get_accesses() as $a) {
            if ($a->fs_page == $page_name) {
                $status = $a->allow_delete;
                break;
            }
        }
        return $status;
    }
    /*
     * Modifica y guarda la fecha de login si tiene una diferencia de más de 5 minutos
     * con la fecha guardada, así se evita guardar en cada consulta
     */

    /**
     * Devuelve la lista de accesos permitidos del usuario.
     *
     * @return array
     */
    public function get_accesses()
    {
        $access = new fs_access();
        return $access->all_from_nick($this->nick);
    }

    /**
     * Devuelve la última fecha y hora del último login.
     *
     * @return string
     */
    public function show_last_login()
    {
        if (is_null($this->last_login)) {
            return '-';
        }

        return date('Y-m-d', strtotime($this->last_login)) . ' ' . $this->last_login_time;
    }

    /**
     * Asigna una contraseña al usuario.
     *
     * @param string $pass
     *
     * @return bool
     */
    public function set_password($pass = '')
    {
        $pass = trim($pass);
        if (mb_strlen($pass) > 1 && mb_strlen($pass) <= 32) {
            $this->password = sha1($pass);
            return true;
        }

        $this->new_error_msg('La contraseña debe contener entre 1 y 32 caracteres.');
        return false;
    }

    /**
     * Actualiza el instante de login del usuario.
     */
    public function update_login()
    {
        $ltime = strtotime($this->last_login . ' ' . $this->last_login_time);
        if (time() - $ltime >= 300) {
            $this->last_login = date('Y-m-d');
            $this->last_login_time = date('H:i:s');
            $this->last_ip = fs_get_ip();
            $this->last_browser = $_SERVER['HTTP_USER_AGENT'];
            $this->save();
        }
    }

    /**
     * Genera una nueva clave de login, para usar en lugar de la contraseña (via cookie),
     * esto impide que dos o más personas utilicen el mismo usuario al mismo tiempo.
     */
    public function new_logkey()
    {
        if (is_null($this->log_key) || !FS_DEMO) {
            $this->log_key = sha1(strval(rand()));
        }

        $this->logged_on = true;
        $this->last_login = date('Y-m-d');
        $this->last_login_time = date('H:i:s');
        $this->last_ip = fs_get_ip();
        $this->last_browser = $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * Retorna los datos del modelo según el parámetro
     *
     * @param string $nick
     *
     * @return false|static
     */
    public function get($nick = '')
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE nick = " . $this->var2str($nick)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE nick = " . $this->var2str($this->nick)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve la lista completa de usuarios de MiFactura.eu.
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "` ORDER BY LOWER(nick) ASC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Devuelve la lista completa de usuarios activados de MiFactura.eu.
     *
     * @return static[]
     */
    public function all_enabled()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE enabled = TRUE ORDER BY LOWER(nick) ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Inserta valores por defecto a la tabla, en el proceso de creación de la misma.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache(true);

        $this->new_message('Se ha creado el usuario <b>admin</b> con la contraseña <b>admin</b>.');
        return "INSERT INTO `" . $this->table_name() . "` (nick,password,log_key,codagente,admin,enabled,fs_page)
            VALUES ('admin','" . sha1('admin') . "',NULL,'1',TRUE,TRUE,'admin_home');";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'albaranescli_detalles' => [
                'ca_albaranescli_detalles_fs_users' => "UPDATE `albaranescli_detalles` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'albaranesprov_detalles' => [
                'ca_albaranesprov_detalles_fs_users' => "UPDATE `albaranesprov_detalles` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'autoriza_descuento' => [
                'ca_autoriza_descuento_fs_users' => "UPDATE `autoriza_descuento` SET usuario = NULL WHERE usuario NOT IN (SELECT nick FROM `fs_users`);",
                'ca_autoriza_descuento_fs_users2' => "UPDATE `autoriza_descuento` SET usuario_creacion = NULL WHERE usuario_creacion NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'autorizaciones_descuento' => [
                'ca_autorizaciones_descuento_fs_users' => "UPDATE `autorizaciones_descuento` SET usuario = NULL WHERE usuario NOT IN (SELECT nick FROM `fs_users`);",
                'ca_autorizaciones_descuento_fs_users2' => "UPDATE `autorizaciones_descuento` SET usuario_creacion = NULL WHERE usuario_creacion NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'crm_calendario' => [
                'ca_crm_calendario_fs_users' => 'UPDATE `crm_calendario` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);',
            ],
            'crm_notas' => [
                'ca_crm_notas_fs_users' => 'UPDATE `crm_notas` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);',
            ],
            'detalles_servicios' => [
                'ca_detalle_usuario' => "UPDATE `detalles_servicios` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'documentos_adjuntos' => [
                'ca_documentos_adjuntos_fs_users' => 'UPDATE `documentos_adjuntos` SET usuario = null WHERE usuario NOT IN (SELECT nick FROM `fs_users`);',
            ],
            'documentosfac' => [
                'ca_documentosfac_fs_users' => "UPDATE `documentosfac` SET usuario = NULL WHERE usuario NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'facturascli_detalles' => [
                'ca_facturascli_detalles_fs_users' => "UPDATE `facturascli_detalles` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'facturasprov_detalles' => [
                'ca_facturasprov_detalles_fs_users' => "UPDATE `facturasprov_detalles` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'fs_access' => [
                // 'fs_access_user2' => "UPDATE `fs_access` SET fs_user = NULL WHERE fs_user NOT IN (SELECT nick FROM `fs_users`);",
                'fs_access_user2' => "DELETE FROM `fs_access` WHERE fs_user NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'fs_logs' => [
                'ca_fs_logs_fs_users' => "UPDATE `fs_logs` SET usuario = NULL WHERE usuario NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'fs_roles_users' => [
                // 'fs_roles_users_fs_users' => "UPDATE `fs_roles_users` SET fs_user = NULL WHERE fs_user NOT IN (SELECT nick FROM `fs_users`);",
                'fs_roles_users_fs_users' => "DELETE FROM `fs_roles_users` WHERE fs_user NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'lineasregstocks' => [
                'ca_lineasregstocks_fs_users' => "UPDATE `lineasregstocks` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'pedidoscli_detalles' => [
                'ca_pedidoscli_detalles_fs_users' => "UPDATE `pedidoscli_detalles` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'pedidosprov_detalles' => [
                'ca_pedidosprov_detalles_fs_users' => "UPDATE `pedidosprov_detalles` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'presupuestoscli_detalles' => [
                'ca_presupuestoscli_detalles_fs_users' => "UPDATE `presupuestoscli_detalles` SET nick = NULL WHERE nick NOT IN (SELECT nick FROM `fs_users`);",
            ],
            'transstock' => [
                'ca_transstock_fs_users' => "UPDATE `transstock` SET usuario = NULL WHERE usuario NOT IN (SELECT nick FROM `fs_users`);",
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new fs_page())->fix_db();
        (new agente())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        $fixes = [
            $this->table_name() => [
                // 'ca_fs_users_fs_pages' => 'UPDATE `' . $this->table_name() . '` SET fs_page = NULL WHERE fs_page = "";',
                'ca_fs_users_fs_pages' => 'DELETE FROM `' . $this->table_name() . '` WHERE fs_page = "";',
                'ca_fs_users_agentes' => 'UPDATE `' . $this->table_name() . '` SET codagente = NULL WHERE codagente = "";',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }
}
