<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Una clase genérica para consultar o almacenar en la base de datos pares clave/valor.
 */
class fs_var extends fs_model
{
    /**
     * Clave primaria. Varchar(35).
     *
     * @var string
     */
    public $name;

    /**
     * Valor almacenado. Text.
     *
     * @var string
     */
    public $varchar;

    /**
     * fs_var constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_vars');
        if ($data) {
            $this->name = $data['name'];
            $this->varchar = $data['varchar'];
        } else {
            $this->name = null;
            $this->varchar = null;
        }
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        $comillas = '';
        if (mb_strtolower(FS_DB_TYPE) === 'mysql') {
            $comillas = '`';
        }

        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET " . $comillas . "varchar" . $comillas . " = " . $this->var2str($this->varchar)
                . " WHERE name = " . $this->var2str($this->name)
                . ";";
        } else {
            $sql = "INSERT INTO `" . $this->table_name() . "` (name," . $comillas . "varchar" . $comillas . ") VALUES ("
                . $this->var2str($this->name)
                . ", " . $this->var2str($this->varchar)
                . ");";
        }

        return $this->db->exec($sql);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->name)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($this->name)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($this->name)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todos los elementos de la tabla.
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . ";";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Devuelve el valor de una clave dada.
     *
     * @param string $name
     *
     * @return string|bool
     */
    public function simple_get($name)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($name)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return $data[0]['varchar'];
        }

        return false;
    }

    /**
     * Rellena un array con los resultados de la base de datos para cada clave,
     * es decir, para el array('clave1' => false, 'clave2' => false) busca
     * en la tabla las claves clave1 y clave2 y asigna los valores almacenados
     * en la base de datos.
     *
     * Sustituye los valores por FALSE si no los encentra en la base de datos,
     * a menos que pongas FALSE en el segundo parámetro.
     *
     * @param array $array
     */
    public function array_get($array, $replace = true)
    {
        /// obtenemos todos los resultados y seleccionamos los que necesitamos
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`;";
        $data = $this->db->select($sql);
        if ($data) {
            foreach ($array as $i => $value) {
                $encontrado = false;
                foreach ($data as $d) {
                    if ($d['name'] == $i) {
                        $array[$i] = $d['varchar'];
                        $encontrado = true;
                        break;
                    }
                }

                if ($replace && !$encontrado) {
                    $array[$i] = false;
                }
            }
        }

        return $array;
    }

    /**
     * Guarda en la base de datos los pares clave, valor de un array simple.
     * ATENCIÓN: si el valor es FALSE, elimina la clave de la tabla.
     *
     * @param array $array
     */
    public function array_save($array)
    {
        $done = true;

        foreach ($array as $i => $value) {
            if ($value === false) {
                if (!$this->simple_delete($i)) {
                    $done = false;
                }
            } elseif (!$this->simple_save($i, $value)) {
                $done = false;
            }
        }

        return $done;
    }

    /**
     * Elimina de la base de datos la tupla con ese nombre.
     *
     * @param string $name
     *
     * @return bool
     */
    public function simple_delete($name)
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($name)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Almacena el par clave/valor proporcionado.
     *
     * @param string $name
     * @param string $value
     *
     * @return bool
     */
    public function simple_save($name, $value)
    {
        $comillas = '';
        if (mb_strtolower(FS_DB_TYPE) == 'mysql') {
            $comillas = '`';
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($name)
            . ";";
        if ($this->db->select($sql)) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET " . $comillas . "varchar" . $comillas . " = " . $this->var2str($value)
                . " WHERE name = " . $this->var2str($name)
                . ";";
        } else {
            $sql = "INSERT INTO `" . $this->table_name() . "` (name," . $comillas . "varchar" . $comillas . ") VALUES ("
                . $this->var2str($name)
                . ", " . $this->var2str($value)
                . ");";
        }

        return $this->db->exec($sql);
    }
}
