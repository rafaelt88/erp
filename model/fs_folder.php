<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Elemento del menú de MiFactura.eu, cada uno se corresponde con un controlador.
 */
class fs_folder extends fs_extended_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_fs_folder_all';

    /**
     * Clave primaria.
     *
     * Es la opción del menú hasta 4 niveles separados por barra vertical.
     *
     * @var string
     */
    public $folder;

    /**
     * Icono de la carpeta de último nivel.
     *
     * @var string
     */
    public $icon;

    /**
     * Icono de la carpeta de último nivel, cuando la carpeta está abierta
     *
     * @var string
     */
    public $icon_open;

    /**
     * Texto a mostrar para el último nivel de menú.
     *
     * @var string
     */
    public $name;

    /**
     * fs_page constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_folders');
        if ($data) {
            $this->folder = $data['folder'];
            $this->name = $data['name'];
            $this->icon = $data['icon'];
            $this->icon_open = $data['icon_open'];
        } else {
            $this->folder = null;
            $this->name = null;
            $this->icon = null;
            $this->icon_open = null;
        }
    }

    public function vaciar()
    {
        return $this->db->exec('TRUNCATE '.$this->table_name().';');
    }

    /**
     * Retorna un objeto con los iconos y el texto de la carpeta de menú.
     * Si el menú no ha sido definido, retorna false.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0608
     *
     * @param $folder
     *
     * @return bool|fs_extended_model
     */
    static public function get_folder($folder)
    {
        return (new fs_folder())->get($folder);
    }

    /**
     * Retorna las carpetas de un plugin (o del núcleo) dada la ruta del mismo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0608
     *
     * @param $folder
     *
     * @return array|mixed
     */
    static public function get_folders($folder)
    {
        $file = $folder . '/dataconfig/folders.php';
        if (!file_exists($file)) {
            return [];
        }
        return include($file);
    }

    /**
     * Retorna un array con toda la estructura de carpetas del menú.
     * Actualiza la información de dicha estructura en fs_folders.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0608
     *
     * @return array|mixed
     */
    static public function regenerar()
    {
        $plugins_activos = $GLOBALS['plugins'];
        $folders = self::get_folders(constant('FS_FOLDER'));
        foreach ($plugins_activos as $plugin) {
            $other_folder = self::get_folders(constant('FS_FOLDER') . '/plugins/' . $plugin);
            $folders = array_merge($folders, $other_folder);
        }
        return $folders;
    }

    public function primary_column()
    {
        return 'folder';
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Comprueba los datos de la opción del menú, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = true;

        if (strlen($this->folder) < 1 || strlen($this->folder) > 100) {
            $this->new_error_msg("Nombre de carpeta no válido, debe tener entre 1 y 100 caracteres.");
            $status = false;
        }

        if (isset($this->icon) && (strlen($this->icon) < 1 || strlen($this->icon) > 90)) {
            $this->new_error_msg("Nombre de icono no válido, debe tener entre 1 y 90 caracteres.");
            $status = false;
        }

        if (isset($this->icon_open) && (strlen($this->icon_open) < 1 || strlen($this->icon_open) > 90)) {
            $this->new_error_msg("Nombre de icono cuando el menú está bierto, no válido, debe tener entre 1 y 90 caracteres.");
            $status = false;
        }

        if (strlen($this->name) < 1 || strlen($this->name) > 90) {
            $this->new_error_msg("Nombre de página no válido, debe tener entre 1 y 90 caracteres.");
            $status = false;
        }

        return $status;
    }
}
