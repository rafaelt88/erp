<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-end">
            @if (!$fsc->plugin_manager->disable_add_plugins)
                <a href="#" class="btn btn-success flex-grow-1 flex-sm-grow-0 " data-bs-toggle="modal" data-bs-target="#modal_add_plugin">
                    <i class="fa-solid fa-plus fa-fw"></i>
                    <span>Añadir</span>
                </a>
            @endif
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="table-responsive-datatable">
            <table id="table_admin_home" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                <thead class="table-dark">
                <tr>
                    <th class="text-start">
                        Plugin
                    </th>
                    <th class="text-start">
                        Descripción
                    </th>
                    <th class="text-start">
                        Versión
                    </th>
                    <th class="text-center">
                        <i class="fa-solid fa-bolt fa-fw" title="Prioridad"></i>
                    </th>
                    <th class="text-center" style="width: 100px;">
                        Acciones
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($fsc->plugin_manager->installed(false) as $key_plugin => $plugin)
                    @php
                        $td_class = '';
                    @endphp
                    @if (!$plugin['compatible'])
                        @php
                            $td_class = '';
                        @endphp
                    @elseif($plugin['enabled'])
                        @php
                            $td_class = '';
                        @endphp
                    @endif
                    <tr {!! $td_class !!}>
                        <td class="{!! $td_class !!} text-start text-nowrap text-truncate">{!! $plugin['name'] !!}</td>
                        <td class="{!! $td_class !!} text-start text-wrap text-truncate" style="max-width: 300px">
                            {!! $plugin['description'] !!}
                        </td>
                        <td class="{!! $td_class !!} text-start">
                            <a href="{!! FS_COMMUNITY_URL !!}/index.php?page=community_changelog&plugin={!! $plugin['name'] !!}&version={!! $plugin['version'] !!}" target="_blank" class="link-primary">
                                {!! $plugin['version'] !!}
                            </a>
                        </td>
                        <td class="{!! $td_class !!} text-center text-nowrap text-truncate">{!! $plugin['prioridad'] !!}</td>
                        <td class="{!! $td_class !!} text-center text-nowrap">
                            @php
                                $params = [
                                    'buttons' => [],
                                    'type' => 'btn-group'
                                ];
                                if($plugin['wizard'] !='' && $plugin['enabled']){
                                    $button = [
                                        'type' => 'a',
                                        'onclick' => '',
                                        'link' => 'index.php?page=' . $plugin['wizard'],
                                        'class' => 'secondary ',
                                        'icon' => '<i class="fa-solid fa-cog fa-fw"></i>',
                                        'text' => '<span>Configurar</span>',
                                        'title' => 'Configurar',
                                    ];
                                    $params['buttons'][] = $button;
                                }
                                if ($plugin['enabled']){
                                    if ($plugin['deactivable']){
                                        $button = [
                                            'type' => 'a',
                                            'onclick' => '',
                                            'link' => $fsc->url() . '&disable=' . $plugin['name'] . '#plugins-tab',
                                            'class' => 'warning',
                                            'icon' => '<i class="fa-solid fa-times-circle fa-fw"></i>',
                                            'text' => '<span>Desactivar</span>',
                                            'title' => 'Desactivar',
                                        ];
                                        $params['buttons'][] = $button;
                                    }

                                } else{
                                    if (!$fsc->plugin_manager->disable_rm_plugins){
                                        $button = [
                                            'type' => 'a',
                                            'onclick' => "eliminar('". $plugin['name'] . "');false;",
                                            'link' => '#',
                                            'class' => 'danger',
                                            'icon' => '<i class="fa-solid fa-trash-can fa-fw"></i>',
                                            'text' => '<span>Eliminar</span>',
                                            'title' => 'Eliminar el plugin',
                                        ];
                                        $params['buttons'][] = $button;
                                    }
                                    if ($plugin['compatible']){
                                        $button = [
                                            'type' => 'a',
                                            'onclick' => '',
                                            'link' => $fsc->url() . '&caca=' . $fsc->random_string(4) . '&enable=' . $plugin['name'] . '#plugins-tab',
                                            'class' => 'success',
                                            'icon' => '<i class="fa-solid fa-check"></i>',
                                            'text' => '<span>Activar</span>',
                                            'title' => 'Activar',
                                        ];
                                        $params['buttons'][] = $button;
                                    } else {
                                        $button = [
                                            'type' => '',
                                            'onclick' => 'bootbox.alert({message: ' . $plugin['error_msg'] . ', title: "<b>Atención</b>"});',
                                            'link' => '#',
                                            'class' => 'warning',
                                            'icon' => '<i class="fa-solid fa-times-circle fa-fw"></i>',
                                            'text' => '<span>Incompatible</span>',
                                            'title' => 'Incompatible',
                                        ];
                                        $params['buttons'][] = $button;
                                    }

                                }
                            @endphp
                            @include('block/table_td_actions', $params)
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
