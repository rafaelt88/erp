<form class="form" action="{!! $fsc->url() !!}&caca={!! $fsc->random_string(4) !!}#avanzado" method="post">
    <div class="card border border-primary">
        <div class="card-header mt-2 bg-transparent border-primary text-primary text-truncate">
            <i class="fa-solid fa-wrench fa-fw"></i>
            Ajustes básicos
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Zona horaria
                        </label>
                        <select class="form-select select2" name="zona_horaria">
                            <option data-comment="Placeholder"></option>
                            @foreach ($fsc->settings->get_timezone_list() as $key1 => $value1)
                                @if ($value1['zone']==$GLOBALS['config2']['zona_horaria'])
                                    <option value="{!! $value1['zone'] !!}" selected="">{!! $value1['diff_from_GMT'] !!} - {!! $value1['zone'] !!}</option>
                                @else
                                    <option value="{!! $value1['zone'] !!}">{!! $value1['diff_from_GMT'] !!} - {!! $value1['zone'] !!}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Portada
                        </label>
                        <select name="homepage" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @foreach ($fsc->paginas as $key1 => $value1)
                                @if ($value1->name==$GLOBALS['config2']['homepage'])
                                    <option value="{!! $value1->name !!}" selected="">{!! $value1->name !!}</option>
                                @else
                                    <option value="{!! $value1->name !!}">{!! $value1->name !!}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Decimales de los totales
                        </label>
                        <select name="nf0" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @foreach ($fsc->settings->nf0() as $key1 => $value1)
                                @if ($value1==$GLOBALS['config2']['nf0'])
                                    <option value="{!! $value1 !!}" selected="">{!! $value1 !!}</option>
                                @else
                                    <option value="{!! $value1 !!}">{!! $value1 !!}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Decimales de los precios
                        </label>
                        <select name="nf0_art" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @foreach ($fsc->settings->nf0() as $key1 => $value1)
                                @if ($value1==$GLOBALS['config2']['nf0_art'])
                                    <option value="{!! $value1 !!}" selected="">{!! $value1 !!}</option>
                                @else
                                    <option value="{!! $value1 !!}">{!! $value1 !!}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Separador para los Decimales
                        </label>
                        <select name="nf1" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @foreach ($fsc->settings->nf1() as $key1 => $value1)
                                @if ($key1==$GLOBALS['config2']['nf1'])
                                    <option value="{!! $key1 !!}" selected="">{!! $value1 !!}</option>
                                @else
                                    <option value="{!! $key1 !!}">{!! $value1 !!}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Separador para los Millares
                        </label>
                        <select name="nf2" class="form-select select2">
                            <option value="">(Ninguno)</option>
                            @foreach ($fsc->settings->nf1() as $key1 => $value1)
                                @if ($key1==$GLOBALS['config2']['nf2'])
                                    <option value="{!! $key1 !!}" selected="">{!! $value1 !!}</option>
                                @else
                                    <option value="{!! $key1 !!}">{!! $value1 !!}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Símbolo Divisa
                        </label>
                        <select name="pos_divisa" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @if ($GLOBALS['config2']['pos_divisa']=='right')
                                <option value="right" selected="">123 {!! $fsc->simbolo_divisa() !!}</option>
                                <option value="left">{!! $fsc->simbolo_divisa() !!}123</option>
                            @else
                                <option value="right">123 {!! $fsc->simbolo_divisa() !!}</option>
                                <option value="left" selected="">{!! $fsc->simbolo_divisa() !!}123</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <p class="form-text">
                        <i class="fa-solid fa-circle-info fa-fw"></i>
                        La configuración de decimales y separadores de decimales y millares
                        se aplica únicamente a los listados y formatos de impresión. Para los
                        campos editables se utiliza la configuración del sistema operativo.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="card border border-success mt-3">
        <div class="card-header mt-2 bg-transparent border-success text-success text-truncate">
            <i class="fa-solid fa-language fa-fw"></i>
            Traducciones
        </div>
        <div class="card-body">
            <div class="row">
                <p class="form-text">
                    FACTURA y FACTURAS se traducen únicamente en los documentos de ventas.
                    FACTURA_SIMPLIFICADA se utiliza en los tickets.
                </p>
            </div>
            <div class="row">
                @foreach ($fsc->settings->traducciones() as $key1 => $value1)
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                        <div class="mb-2">
                            <span class="text-uppercase">{!! $value1['nombre'] !!}:</span>
                            <input class="form-control" type="text" name="{!! $value1['nombre'] !!}" value="{!! $value1['valor'] !!}" autocomplete="off"/>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="card border border-warning mt-3">
        <div class="card-header mt-2 bg-transparent border-warning text-warning text-truncate">
            <i class="fa-solid fa-exclamation-triangle fa-fw"></i>
            Desarrollo
        </div>
        <div class="card-body">
            <div class="row">
                <p class='form-text'>
                    Estos son parametros de configuración sensibles de MiFactura.eu.
                    <b>No los modifiques si no sabes lo que haces</b>.
                </p>
                {{--            <div class="col-md-3 col-sm-3">--}}
                {{--                <div class="mb-2">--}}
                {{--                    Comprobaciones en la base de datos: --}}
                {{--                    <select name="check_db_types" class="form-control select2">--}}
                {{--                       @if ($GLOBALS['config2']['check_db_types']==1) --}}
                {{--                        <option value="1" selected=''>Comprobar los tipos de las columnas de las tablas</option> --}}
                {{--                        <option value="0">No comprobar los tipos</option> --}}
                {{--                        @else --}}
                {{--                        <option value="1">Comprobar los tipos de las columnas de las tablas</option> --}}
                {{--                        <option value="0" selected=''>No comprobar los tipos</option> --}}
                {{--                        @endif --}}
                {{--                    </select> --}}
                {{--                    <p class="form-text">--}}
                {{--                        Tendrás que <a href="index.php?page=admin_info" class="link-primary">limpiar la caché</a> --}}
                {{--                        para que comiencen las comprobaciones. --}}
                {{--                    </p> --}}
                {{--                </div> --}}
                {{--            </div> --}}
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Tipo entero
                        </label>
                        <input class="form-control" type="text" name="db_integer" value="{!! $GLOBALS['config2']['db_integer'] !!}"/>
                        <p class="form-text">Tipo a usar en la base de datos (MySQL).</p>
                    </div>
                </div>
                <div class="ccol-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    @php
                        $form_class= '';
                    @endphp
                    @if ($GLOBALS['config2']['foreign_keys']==0)
                        @php
                            $form_class= ' has-warning';
                        @endphp
                    @endif
                    <div class="mb-2{!! $form_class !!}">
                        <label>
                            Comprobar claves ajenas
                        </label>
                        <select name="foreign_keys" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @if ($GLOBALS['config2']['foreign_keys']==1)
                                <option value="1" selected=''>Si</option>
                                <option value="0">No</option>
                            @else
                                <option value="1">Si</option>
                                <option value="0" selected=''>No</option>
                            @endif
                        </select>
                        <p class="form-text">
                            @if (mb_strtolower(FS_DB_TYPE)=='mysql')
                                <b>Peligro: no tocar si no sabes lo que haces</b>.
                            @else
                                Sólo se puede desactivar en MySQL.
                            @endif
                        </p>
                    </div>
                </div>
                <div class="ccol-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Permitir acceso desde estas IPs
                        </label>
                        <input class="form-control" type="text" name="ip_whitelist" value="{!! $GLOBALS['config2']['ip_whitelist'] !!}"/>
                        <p class="form-text">Los administradores pueden acceder desde cualquier IP.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Generar los libros contables
                        </label>
                        <select name="libros_contables" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @if ($GLOBALS['config2']['libros_contables']==1)
                                <option value="1" selected=''>Si</option>
                                <option value="0">No</option>
                            @else
                                <option value="1">Si</option>
                                <option value="0" selected=''>No</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <label>
                            Algoritmo de nuevo código
                        </label>
                        <select name="new_codigo" class="form-select select2">
                            <option data-comment="Placeholder"></option>
                            @foreach ($fsc->settings->new_codigo_options() as $key1 => $value1)
                                @if ($GLOBALS['config2']['new_codigo']==$key1)
                                    <option value="{!! $key1 !!}" selected=''>{!! $value1 !!}</option>
                                @else
                                    <option value="{!! $key1 !!}">{!! $value1 !!}</option>
                                @endif
                            @endforeach
                        </select>
                        <p class="form-text">
                            <b>Peligro: no modificar si ya tienes {!! FS_FACTURAS !!}.</b>
                        </p>
                    </div>
                </div>
                <!--

                 *
                 * Servicio SER2021JR898
                 *
                 * Se procede a eliminar la limitación de ejecución (que no es que cancele)
                 * Si el cron está en ejecución, no se lanza otro hasta que se de por finalizado.
                 *

                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="mb-2{!! $form_class !!}">
                        <label>
                            Máximo tiempo de ejecución del CRON
                        </label>
                        <input class="form-control" type="number" name="cron_maximo_minutos" value="{!! $fsc->cron_maximo_minutos !!}"/>
                        <p class="form-text">
                            Dejar el tiempo suficiente para que un cron finalice:
                            <b>Peligro: un valor bajo, podría permitir la ejecución concurrente de más de un cron</b>.
                        </p>
                    </div>
                </div>
                -->
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="d-flex justify-content-between">
            <button class="btn btn-warning flex-grow-1 flex-sm-grow-0 mx-1" type="button" onclick="window.location.href = '{!! $fsc->url() !!}&caca={!! $fsc->random_string(4) !!}&reset=TRUE#avanzado'">
                <i class="fa-solid fa-arrow-rotate-right fa-fw"></i>
                <span>Restablecer</span>
            </button>
            <button class="btn btn-primary flex-grow-1 flex-sm-grow-0 mx-1" type="submit" onclick="this.disabled = true;this.form.submit();">
                <i class="fa-solid fa-save fa-fw"></i>
                <span>Guardar</span>
            </button>
        </div>
    </div>
</form>
