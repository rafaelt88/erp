<!-- PhpStorm bug -->
{{--
USO:
@include('block/card_page_header')
--}}

<div class="card-header mt-2 text-truncate">
    {!! $fsc->get_icon_controller() !!}
    {!! $fsc->page->title !!}
</div>
