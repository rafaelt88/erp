<p class="form-text-description">
    <i class="fa-solid fa-user fa-fw"></i>
    Usuario <span class="fw-bold">{!! $fsc->suser->nick !!}</span>.
</p>
@if ($fsc->suser->admin)
    <p class="form-text-description">
        <span class="badge bg-warning">Los administradores tienen acceso a cualquier página.</span>
    </p>
@endif
@if (!$fsc->suser->enabled)
    <p class="form-text-description">
        <span class="badge bg-danger"><i class="fa-solid fa-lock fa-fw"></i>desactivado</span>
       el usuario está desactivado, no podrá acceder al sistema.
        Pulsa el <b>botón activar</b> si quieres activarlo de nuevo.
    </p>
@endif
