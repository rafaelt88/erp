@if ($fsc->notificacion)
    <span class="form-text-description">
        {!! $fsc->notificacion->get_tiempo_transcurrido() !!}
    </span>
@endif
