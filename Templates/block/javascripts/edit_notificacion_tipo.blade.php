<script type="text/javascript">
    function formatIcon(state) {
        return '<i class="' + state.id + '" aria-hidden="true"></i> &nbsp; ' + state.text;
    }

    function formatColor(state) {
        return '<i class="fa-solid fa-square fa-fw ' + state.id + '"></i> &nbsp; ' + state.text;
    }

    $(document).ready(function () {
        $('#icono').select2({
            theme: 'bootstrap-5',
            templateResult: formatIcon,
            templateSelection: formatIcon,
            escapeMarkup: function (m) {
                return m;
            }
        });
        $('#estilo').select2({
            theme: 'bootstrap-5',
            templateResult: formatColor,
            templateSelection: formatColor,
            escapeMarkup: function (m) {
                return m;
            }
        });
    });
</script>
