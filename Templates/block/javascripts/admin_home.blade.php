@include('block/javascripts/common/datatables')

@if ($fsc->action == 'enable' || $fsc->action == 'disable')
    <script type="text/javascript" src="{!! FS_PATH !!}updater/js/updater.js?idcache={!! $fsc->id_cache !!}"></script>
@endif

<script type="text/javascript">
    function fs_marcar_todo() {
        $('#f_enable_pages input:checkbox').prop('checked', true);
    }

    function editar_pagina(url, name) {
        $("#edit_page_modal").modal("show");
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: '&page_name=' + name,
            success: function (datos) {
                console.log(datos);
                $('#page_name').val(datos[0].name);
                $('#page_alias').val(datos[0].alias);
                $('#page_description').val(datos[0].description);
            },
            error: function () {
                bootbox.alert({
                    message: 'Se ha producido un error al obtener los datos.',
                    title: "<b>Atención</b>"
                });
            }
        });
        document.edit_page_form.alias.focus();
    }

    function eliminar(name) {
        bootbox.confirm({
            message: '¿Realmente desea eliminar este plugin?',
            title: '<b>Atención</b>',
            callback: function (result) {
                if (result) {
                    window.location.href = '{!! $fsc->url() !!}&delete_plugin=' + name + '#plugins-tab';
                }
            }
        });
    }

    function descargar_plugin_inestable(id) {
        bootbox.confirm({
            message: 'Este plugin está marcado como inestable. Significa que <b>no se recomienda su uso</b>, que todavía está en desarrollo y puede contener errores. ¿Estas seguro de que quieres descargarlo?',
            title: '<b>Atención</b>',
            callback: function (result) {
                if (result) {
                    window.location.href = '{!! $fsc->url() !!}&caca={!! $fsc->random_string(4) !!}&download=' + id + '#plugins-tab';
                }
            }
        });
    }

    function mostrar_info(id, nombre, descripcion) {
        bootbox.setLocale('es');
        bootbox.alert({
            title: id + ' ' + nombre,
            message: descripcion
        });
    }

    $(document).ready(function () {
        @if ($fsc->step=='1')
        $('#tab_panel a[href="#t_descargas"]').tab('show');
        @endif

        if (window.location.hash.substring(1) == 'paginas') {
            $('#tab_panel a[href="#t_paginas"]').tab('show');
        } else if (window.location.hash.substring(1) == 'plugins') {
            $('#tab_panel a[href="#t_plugins"]').tab('show');
        } else if (window.location.hash.substring(1) == 'descargas') {
            $('#tab_panel a[href="#t_descargas"]').tab('show');
        } else if (window.location.hash.substring(1) == 'avanzado') {
            $('#tab_panel a[href="#t_avanzado"]').tab('show');
        }

        $('#marcar_todo_enabled').click(function () {
            var checked = $(this).prop('checked');
            $("#f_enable_pages input[name='enabled[]']").prop('checked', checked);
        });

        @if ($fsc->action == 'enable' || $fsc->action == 'disable')
        $('#modallog').show();
        $('#close_button').prop('disabled', true);
        $('#close_button').hide();
        checkModels().then(function (data) {
            addLine('Solicitando modelos activos.');
            console.log('Solicitando modelos activos.');
            let promises = [];
            data.forEach(function (element, index, array) {
                promises[index] = instanciandoModelo(element);
            });

            Promise.allSettled(promises).then(function () {
                addLine('Modelos instanciados.');
                console.log('Modelos instanciados.');

                $('#close_button').prop('disabled', false);
                $('#update_button').prop('disabled', false);
                $('#close_button').show();
                $('#modallog').hide();
            });
        });
        @endif
    });
</script>
