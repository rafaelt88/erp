<!-- PhpStorm bug -->
{{--
    @include('block/javascripts/common/protect_close')
--}}

<script type="text/javascript">
    let canBeClose = true;

    function allowClose() {
        canBeClose = true;
    }

    function modifiedField() {
        canBeClose = false;
    }

    function iCanClose() {
        alert(canBeClose);
        if (!canBeClose) {
            return 'Para evitar perder cambios por error, se recomienda usar los botones Guardar o Salir';
        }
    }
</script>