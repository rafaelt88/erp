<!-- PhpStorm bug -->
{{-- @include('block/javascripts/common/datatables') --}}

{{-- LOCAL --}}
{{--<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-autofill/js/dataTables.autoFill.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-buttons/js/buttons.colVis.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-buttons/js/buttons.flash.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-buttons/js/buttons.html5.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-buttons/js/buttons.print.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-buttons/js/dataTables.buttons.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-colreorder/js/dataTables.colReorder.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-colreorder-bs4/js/colReorder.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-datetime/js/dataTables.dateTime.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-keytable/js/dataTables.keyTable.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-keytable-bs4/js/keyTable.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-responsive/js/dataTables.responsive.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-rowgroup/js/dataTables.rowGroup.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-rowgroup-bs4/js/rowGroup.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-rowreorder/js/dataTables.rowReorder.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-rowreorder-bs4/js/rowReorder.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-scroller/js/dataTables.scroller.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-scroller-bs4/js/scroller.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-select/js/dataTables.select.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-staterestore/js/dataTables.stateRestore.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/datatables.net-staterestore-bs4/js/stateRestore.bootstrap4.min.js?idcache={!! $fsc->id_cache !!}"></script>--}}
{{-- REMOTO: https://cdn.datatables.net/ --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.11.5/b-2.2.2/b-colvis-2.2.2/b-html5-2.2.2/b-print-2.2.2/cr-1.5.5/date-1.1.2/fc-4.0.2/fh-3.2.2/kt-2.6.4/r-2.2.9/rg-1.1.4/rr-1.2.8/sc-2.0.5/sb-1.3.2/sp-2.0.0/sl-1.3.4/sr-1.1.0/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.11.5/sorting/datetime-moment.js"></script>

{{--
Optimiza el espacio, ves lo que cabe en pantalla sin scroll de página, sólo de tabla
http://live.datatables.net/zelezobo/2/edit
--}}

<script type="text/javascript">
    let datatablesList = [];
    let defaultResponsive = false;
    let defaultScrollY = false;
    let defaultScroller = !defaultResponsive;
    let dtOptions = {
        language: {
            url: "{!! FS_PATH !!}Templates/plugins/datatables/es-ES.json?idcache={!! $fsc->id_cache !!}",
            "decimal": "{!! FS_NF1 !!}",
            "thousands": "{!! FS_NF2 !!}"
        },
        deferRender: true,
        responsive: defaultResponsive,
        fixedHeader: {
            header: true,
            headerOffset: ($('#page-topbar').height() || 0)
        },
        fixedColumns: false,
        colReorder: true,
        stateSave: true,
        select: true,
        paging: false,
        //autoWidth: true,
        //lengthMenu: [[10, 25, 50], [10, 25, 50]],
        ordering: false, {{-- TODO: O ordena el datatable, o ordena la página, pero jamás los 2 --}}
        filter: true,
        scrollX: defaultScroller,
        scrollY: defaultScrollY,
        scrollCollapse: true,
        //scroller: defaultScroller,
        info: false,
        //dom: 'Bfrtip',
        lengthChange: false,
        initComplete: function (settings, json) {
            datatables_add_buttons(settings);
        },
        buttons: [
            //'pageLength',
            {
                extend: 'colvis',
                postfixButtons: ['colvisRestore'],
                text: '<i class="fa-solid fa-columns fa-fw" title="Cambiar visibilidad"></i>',
                className: 'btn-dark',
                columnText: function (dt, idx, title) {
                    return (idx + 1) + ': ' + title;
                }
            },
            {
                text: '<i class="fa-solid fa-scroll fa-fw" title="Activar/desactivar scrollY"></i>',
                className: 'btn-dark',
                action: function (e, dt, node, config) {
                    let id = dt.id || dt.nodes()[0].id;
                    defaultScrollY = !defaultScrollY;
                    if (defaultScrollY) {
                        dtOptions.scrollY = '50vh';
                    } else {
                        dtOptions.scrollY = false;
                    }
                    dt.draw();
                    let thisTableID = '#' + id;
                    if (datatablesList[thisTableID]) {
                        try {
                            datatablesList[thisTableID].destroy();
                            delete datatablesList[thisTableID];
                        } catch (e) {
                            // Está siendo procesado
                        }
                        datatablesList[thisTableID] = $(thisTableID).DataTable(dtOptions);
                    }
                }
            },
            {
                text: '<i class="fa-solid fa-exchange-alt fa-fw" title="Activar/desactivar diseño responsive"></i>',
                className: 'btn-dark',
                action: function (e, dt, node, config) {
                    let id = dt.id || dt.nodes()[0].id;
                    defaultResponsive = !defaultResponsive;
                    //defaultScroller = !defaultScroller;
                    dtOptions.responsive = defaultResponsive;
                    //dtOptions.scroller = defaultScroller;
                    dtOptions.scrollX = defaultScroller;

                    let thisTableID = '#' + id;
                    if (datatablesList[thisTableID]) {
                        try {
                            datatablesList[thisTableID].destroy();
                            delete datatablesList[thisTableID];
                        } catch (e) {
                            // Está siendo procesado
                        }
                        let table = $(thisTableID + ' > tfoot');
                        if (defaultResponsive) {
                            table.hide();
                        } else {
                            table.show();
                        }
                        datatablesList[thisTableID] = $(thisTableID).DataTable(dtOptions);
                    }
                }
            },
            {
                text: '<i class="fa-solid fa-broom fa-fw" title="Limpiar cambios"></i>',
                className: 'btn-dark',
                action: function (e, dt, node, config) {
                    bootbox.confirm({
                        message: '¿Realmente desea eliminar los ajustes de esta tabla?<br>Se restablecerá a los ajustes por defecto.',
                        title: '<b>Atención</b>',
                        callback: function (result) {
                            if (result) {
                                dt.table().state.clear();
                                window.location.reload();
                            }
                        }
                    });
                }
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fa-regular fa-file-excel fa-fw"></i>',
                className: 'btn-dark',
                title: "Listado",
            }
        ]
    };

    /**
     * Inicializa los datatables basado en el selector y opciones dados.
     * En caso de no indicar parametros, se usarán los valores por defecto.
     *
     * @param selector
     * @param options
     */
    function datatables_initialisation(selector, options) {
        selector = (selector === undefined) ? 'table.table-datatable' : selector;
        options = (options === undefined) ? dtOptions : options;

        let tables = document.querySelectorAll(selector + ":not(.dataTable)");
        tables.forEach(function (node) {
            if (!node.id) {
                bootbox.alert({
                    message: 'Tabla encontrada sin ID en la página, el ID es necesario para el correcto funcionamiento del DataTable.',
                    title: '<b>ERROR</b>',
                    size: 'large'
                });
                return;
            }

            let tableID = '#' + node.id;
            if (!datatablesList[tableID]) {
                datatablesList[tableID] = $(tableID).DataTable(options);
            }
        });
    }

    /**
     * Añade los botones a la tabla.
     *
     * @param settings
     */
    function datatables_add_buttons(settings) {
        let id = '#' + settings.nTable.id;
        if (datatablesList[id]) {
            setTimeout(function () {
                datatablesList[id].buttons().container().appendTo(id + '_wrapper .col-md-6:eq(0)');
            }, 500);
        }
    }

    $(document).ready(function () {
        $.fn.dataTable.moment('DD-MM-YYYY');

        /*
         * Si existe, ejecuta una función especifica de página,
         * sinó existe, ejecuta una función genérica.
         */
        if (typeof datatables_page_{!! $fsc->page->name !!} === 'function') {
            {{--
            TODO: Si se quiere un datatables en especifico, hay que:
                1.- Definirlo para dicha página en Templates/block/javascripts/{!! $fsc->page->name !!}.blade.php
                    1.1.- Añadir @include('block/javascripts/common/datatables')
                2.- Crear una función llamada datatables_page_{!! $fsc->page->name !!}
                    2.1.- Desde donde se define el dtOptions local
                    2.2.- Se invoca a datatables_initialisation(selector, options)
                3.- El código JS incluido, se encarga de invocar a la función que has definido si se ha hecho correctamente.
            --}}
            datatables_page_{!! $fsc->page->name !!}();
        } else {
            datatables_initialisation();
        }

        // Al cambiar la pestaña, reajustamos anchos de las columnas de datatables
        $('a[role="tab"]').on('shown.bs.tab', function (e) {
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
        });
    });
</script>
