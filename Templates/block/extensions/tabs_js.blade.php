<!-- PhpStorm bug -->
{{--
Añade código JS para detectar el cambio de pestaña a través de una URL.

USO:
@include('block/extensions/tabs_js', ['tab_panel_id' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='tab')
        else if (window.location.hash.substring(1) == 'ext_{!! $extension->name !!}') {
        $('#{!! $tab_panel_id !!} a[href="#ext_{!! $extension->name !!}"]').tab('show');
        }
    @endif
@endforeach
