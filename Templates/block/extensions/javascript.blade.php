<!-- PhpStorm bug -->
{{--
Esta extensión permite cargar elementos JS.

USO:
@include('block/extensions/javascript')
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type === 'javascript')
        {!! $extension->text !!}
    @endif
@endforeach
