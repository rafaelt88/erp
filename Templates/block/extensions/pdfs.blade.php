<!-- PhpStorm bug -->
{{--
Añade un botón para generar el PDF de impresión.

USO:
@include('block/extensions/pdfs', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='pdf')
        <a href="index.php?page={!! $extension->from !!}{!! $extension->params !!}{!! $page_params ?? '' !!}" target="_blank" class="btn d-flex justify-content-center align-items-center btn-outline-secondary mb-2">
            {!! $extension->text !!}
        </a>
    @endif
@endforeach
