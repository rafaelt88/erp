<!-- PhpStorm bug -->
{{--
Carga un menú que posiblemente, tenga un submenú.
--}}
<div class="accordion-item @if(str_contains($name,"|")) ms-4 @endif">
    <h3 class="fw-light d-flex flex-nowrap accordion-header">
        @php
            $option=get_page_info($name);
            $referencia = str_replace("|", "-", str_replace(" ", "",$name));
        @endphp
        <button class="text-capitalize accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{!!  $referencia !!}" aria-expanded="true" aria-controls="{!!  $referencia !!}">

            @if($option['icon'])
                <i class="{!! $option['icon'] !!}"></i>
            @endif
            {!! $option['name'] !!}
        </button>
    </h3>
    <div id="{!!  $referencia !!}" class="collapse ">
        <ul class="list-group sortable accordion-collapse accordion-body">
            @php
                $submenus = get_arrays_from($data);
                $items = get_items_from($data)
            @endphp
            @if($submenus)
                @foreach ($submenus as $newmenu => $newdata)
                    @include('block/extensions/blockmenu', ['name'=>$newmenu, 'data'=>$newdata])
                @endforeach
            @endif
            @if($items)
                @foreach ($items as $key => $value)
                    <li class="list-group-item clickable">
                        <i class="fa-solid fa-arrows-v fa-fw"></i>{!! $value !!}
                        <input type="hidden" name="{!! $key !!}" value="{!! $value !!}"/>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
