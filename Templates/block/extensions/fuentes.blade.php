<!-- PhpStorm bug -->
{{--
Esta extensión se utiliza únicamente para escoger entre planes contables, aunque realmente puede usarse para otros fines del mismo tipo.

USO:
@include('block/extensions/fuentes', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='fuente')
        <div class="radio">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fuente" value="{!! $extension->params !!}" checked=""/>
                <label class="form-check-label">
                    {!! $extension->text !!}
                </label>
            </div>
        </div>
    @endif
@endforeach
