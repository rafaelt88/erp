<!-- PhpStorm bug -->
{{--
Añade un iframe no visible en la página. Comunmente se ha usado para invocar otras direcciones con acciones.

USO:
@include('block/extensions/hidden_iframes', ['page_params' => ''])
--}}
@if(!constant('FS_DEBUG'))
<div class="visually-hidden">
    @foreach ($fsc->extensions as $key1 => $extension)
        @if ($extension->type=='hidden_iframe')
            <iframe src="index.php?page={!! $extension->from !!}{!! $page_params ?? '' !!}{!! $extension->params !!}&hide_title_iframe=true&hide_footer_iframe=true"></iframe>
        @endif
    @endforeach
</div>
@endif