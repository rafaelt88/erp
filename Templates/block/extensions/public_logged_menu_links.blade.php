<!-- PhpStorm bug -->
{{--
Opciones de menú que ve el "cliente" cuando está identificado.

USO:
@include('block/extensions/public_logged_menu_links', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='public_menu_link')
        <li class="nav-item @if ($extension->from==$fsc->class_name) active @endif ">
            <a href="index.php?page={!! $extension->from !!}" class="nav-link d-flex flex-column align-items-center">
                {!! $extension->text !!}
                <span class="sr-only">(current)</span>
            </a>
        </li>
    @endif
@endforeach
