<div class="tab-pane fade" id="v-pills-email" role="tabpanel" aria-labelledby="v-pills-email-tab">
    <div class="row">
        <div class="col-12">
            <div class="card border">
                <div class="card-header mt-2 text-truncate">
                    <i class="fa-solid fa-envelope fa-fw"></i>
                    Email
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a href="#email_cfg" id="email_config" aria-controls="email_cfg" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center active">
                                <i class="fa-solid fa-gears fa-fw"></i>
                                Configuración
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content nav-content">
                        <div class="tab-pane fade show active" id="email_cfg" role="tabpanel" aria-labelledby="email_cfg-tab">
                            <div class="row">
                                <div class="col-12">
                                    <p class="form-text">
                                        Si configuras tu cuenta de email, podrás usarla para enviar documentos.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="mb-2">
                                        <div class="input-group">
                                            <div class="input-group-text"><i class="fa-solid fa-envelope fa-fw"></i>
                                            </div>
                                            <input class="form-control" type="email" name="email" value="{!! $fsc->empresa->email !!}" autocomplete="off" placeholder="Email" autofocus=""/>
                                        </div>
                                        <p class="form-text">
                                            ¿Quieres usar tunombre@tuempresa.com?
                                            Prueba gratis <a rel="noopener" href="https://goo.gl/bRFlmv" target="_blank" class="link-primary">Google Apps for Work</a>.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="mb-2">
                                        <div class="input-group">
                                            <div class="input-group-text"><i class="fa-solid fa-key fa-fw"></i></div>
                                            <input class="form-control" type="password" name="mail_password" value="{!! $fsc->empresa->email_config['mail_password'] !!}" autocomplete="new-password" placeholder="Contraseña"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="mb-2">
                                        <div class="input-group">
                                            <div class="input-group-text"><i class="fa-solid fa-copy fa-fw"></i></div>
                                            <input class="form-control" type="text" name="mail_bcc" value="{!! $fsc->empresa->email_config['mail_bcc'] !!}" placeholder="Enviar copias a (opcional)"/>
                                        </div>
                                        <p class="form-text">Puedes escribir un email para que se envíe copia de
                                            todo.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="d-flex justify-content-end">
                                        <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit">
                                            <i class="fa-solid fa-save fa-fw"></i>
                                            <span>Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="card border mt-3">
                                <div class="card-header mt-2 text-truncate">
                                    Si no usas Gmail o Google Apps, rellena <b>también</b> estos datos
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="mb-2">
                                                <label>
                                                    Host
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-text">
                                                        <i class="fa-solid fa-globe fa-fw"></i></div>
                                                    <input class="form-control" type="text" name="mail_host" value="{!! $fsc->empresa->email_config['mail_host'] !!}" autocomplete="off"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="mb-2">
                                                <label>
                                                    Puerto
                                                </label>
                                                <input class="form-control" type="number" name="mail_port" value="{!! $fsc->empresa->email_config['mail_port'] !!}" autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="mb-2">
                                                <label>
                                                    Encriptación
                                                </label>
                                                <select name="mail_enc" class="form-select select2">
                                                    <option data-comment="Placeholder"></option>
                                                    @foreach ($fsc->encriptaciones() as $key1 => $value1)
                                                        @if ($key1==$fsc->empresa->email_config['mail_enc'])
                                                            <option value="{!! $key1 !!}" selected="">{!! $value1 !!}</option>
                                                        @else
                                                            <option value="{!! $key1 !!}">{!! $value1 !!}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="mb-2">
                                                <label>
                                                    Envío por
                                                </label>
                                                <select name="mail_mailer" class="form-select select2">
                                                    <option data-comment="Placeholder"></option>
                                                    @foreach ($fsc->mailers() as $key1 => $value1)
                                                        @if ($key1==$fsc->empresa->email_config['mail_mailer'])
                                                            <option value="{!! $key1 !!}" selected="">{!! $value1 !!}</option>
                                                        @else
                                                            <option value="{!! $key1 !!}">{!! $value1 !!}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="mb-2">
                                                <label>
                                                    Usuario
                                                </label>
                                                <input class="form-control" type="text" name="mail_user" value="{!! $fsc->empresa->email_config['mail_user'] !!}" autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-2">
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox" role="switch" name="mail_low_security" value="TRUE" @if ($fsc->empresa->email_config['mail_low_security']) checked="" @endif />
                                                    <label class="form-check-label">
                                                        Permitir certificados de servidor poco seguros: los certificados
                                                        autofirmados son algo habitual en servidores dedicados,
                                                        aunque poco seguros. Activa esta opción si no puedes conectar
                                                        a tu servidor de correo aunque los datos sean correctos
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <p class="form-text">
                                        Si tienes problemas configurando el email de <b>loading.es</b>,
                                        usa el puerto 25 y la encriptación TLS.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
