@if ($fsc->allow_delete)
    <a href="#" id="b_eliminar_rol" class="btn btn-danger flex-grow-1 flex-sm-grow-0">
        <i class="fa-solid fa-trash-can fa-fw"></i>
        <span class="d-none d-sm-inline d-sm-none d-md-inline">Eliminar</span>
    </a>
@endif

<button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" onclick="this.disabled=true;this.form.submit();">
    <i class="fa-solid fa-save fa-fw"></i>
    <span>Guardar</span>
</button>
