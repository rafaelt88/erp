@if ($fsc->allow_delete)
    <a href="{!! $fsc->url() !!}&clean_cache=TRUE" class="d-flex justify-content-center align-items-center btn btn-warning" title="Limpiar la cache">
        <i class="fa-solid fa-broom fa-fw"></i>
        <spanº>Limpiar la cache</spanº>
    </a>
@endif
