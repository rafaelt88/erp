<a href="index.php?page=admin_users" class="d-flex justify-content-center align-items-center btn btn-outline-secondary">
    <i class="fa-solid fa-arrow-left fa-fw"></i>
    <span>Usuarios</span>
</a>
<a href="index.php?page=admin_users#nuevo" class="d-flex justify-content-center align-items-center btn btn-success" title="Nuevo usuario">
    <i class="fa-solid fa-plus fa-fw"></i>
</a>
