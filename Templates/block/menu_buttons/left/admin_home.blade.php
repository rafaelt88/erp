@include('block/menu_buttons/refresh-bookmark')

@if (!$fsc->plugin_manager->disable_mod_plugins)
    <div class="d-flex flex-wrap gap-1 p-0">
        <a href="#" class="d-flex justify-content-center align-items-center btn btn-success" data-bs-toggle="modal" data-bs-target="#modal_add_plugin">
            <i class="fa-solid fa-plus fa-fw"></i>
            <span>Añadir</span>
        </a>
        @if ($fsc->check_for_updates())
            @php
                $class_color = 'info';
            @endphp
        @else
            @php
                $class_color = 'secondary';
            @endphp
        @endif
        <a class='d-flex justify-content-center align-items-center btn btn-{!! $class_color !!}' href='updater.php' title='Actualizador'>
            <i class="fa-solid fa-upload fa-fw"></i>
            <span>Actualizador</span>
        </a>
    </div>
@endif
