<div class="d-flex btn-group" role="group">
    @include('block/menu_buttons/refresh')
    @if ($fsc->page->show_on_menu)
        @include('block/menu_buttons/bookmark')
    @endif

    @if (file_exists($fsc->helpfile))
        <a target=_blank href="{!! $fsc->helpfile !!}"
          class="d-flex justify-content-center align-items-center btn btn-outline-secondary {!! $class !!}"
           title="Obtener ayuda"
           aria-label="help">
            <i class="fa-solid fa-life-ring fa-fw text-success"></i>
        </a>
    @endif

</div>
{{--@if ($fsc->page->show_on_menu)
    @include('block/menu_buttons/favorite')
@endif--}}
