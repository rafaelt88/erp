<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        @if ($fsc->empresa->nombrecorto)
            {!! $fsc->empresa->nombrecorto !!}
        @else
            {!! $fsc->empresa->nombre !!}
        @endif
    </title>
    <meta name="description" content="MiFactura.eu es un software de facturación y contabilidad para pymes. Es software libre bajo licencia GNU/LGPL."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="generator" content="MiFactura.eu"/>
    @if (file_exists('images/favicon.ico'))
        <link rel="shortcut icon" href="{!! FS_PATH !!}images/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
    @else
        <link rel="shortcut icon" href="{!! FS_PATH !!}Templates/img/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
    @endif
    <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/@fortawesome/fontawesome-free/css/all.min.css?idcache={!! $fsc->id_cache !!}"/>

    <script type="text/javascript" src="{!! FS_PATH !!}node_modules/jquery/dist/jquery.min.js?idcache={!! $fsc->id_cache !!}"></script>
    <script type="text/javascript" src="{!! FS_PATH !!}node_modules/bootstrap/dist/js/bootstrap.bundle.min.js?idcache={!! $fsc->id_cache !!}"></script>
    <script type="text/javascript" src="{!! FS_PATH !!}Templates/js/jquery.ui.shake.js?idcache={!! $fsc->id_cache !!}"></script>
    <script type="text/javascript" src="{!! FS_PATH !!}node_modules/lazysizes/lazysizes.min.js?idcache={!! $fsc->id_cache !!}" async
    ""></script>
    <script type="text/javascript">
        function ChangeIt() {
            @if (file_exists(FS_MYDOCS.'images/dasboardbg.jpg'))
                @php
                    rename(FS_MYDOCS.'images/dasboardbg.jpg', FS_MYDOCS.'images/dashboardbg.jpg');
                @endphp
                @endif

                @if (file_exists(FS_MYDOCS.'images/dashboardbg.jpg'))
                document.body.background = 'images/dashboardbg.jpg';
            @else
                document.body.background = 'Templates/img/dashboardbg.jpg';
            @endif

                x = $(window).width();
            y = $(window).height();
            if (x / y > 1.6) {
                y = x * 640 / 1024;
            } else {
                x = y * 1024 / 640;
            }
            s = x + "px " + y + "px";
            document.body.style.backgroundSize = s;// Background repeat
        }

        $(document).ready(function () {
            $(window).resize(function () {
                ChangeIt();
            });
            ChangeIt();
            @if ($fsc->get_errors())
            $("#box_login").shake();
            @endif

            document.f_login.user.focus();

            $("#b_feedback").click(function (event) {
                event.preventDefault();
                $("#modal_feedback").modal('show');
                document.f_feedback.feedback_text.focus();
            });
            $("#b_new_password").click(function (event) {
                event.preventDefault();
                $("#modal_new_password").modal('show');
                document.f_new_password.user.focus();
            });
        });
    </script>

    @if (file_exists(FS_MYDOCS.'images/logo.png'))
        <style rel="stylesheet">
            .well {
                background-color: #222222;
                color: #fff;
                text-align: center;
            }

            .page-header {
                padding-bottom: 9.5px;
                margin: 40px 0 40px;
                border-bottom: none;
            }
        </style>
    @else
        <style rel="stylesheet">
            .well {
                background-color: #222222;
                color: #fff;
                text-align: center;
            }

            .thumbnail {
                display: block;
                padding: 4px;
                margin-bottom: 21px;
                line-height: 1.4;
                background-color: transparent;
                border: none;
                border-radius: 0;
                -webkit-transition: border .2s ease-in-out;
                -o-transition: border .2s ease-in-out;
                transition: border .2s ease-in-out;
            }

            .page-header {
                padding-bottom: 9.5px;
                margin: 40px 0 40px;
                border-bottom: none;
            }
        </style>
    @endif
</head>
<body>
<nav class="navbar navbar-dark" role="navigation" style="margin: 0;">
    <div class="container-fluid mt-3">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-bs-toggle="collapse" data-bs-target="#mf-navbar-collapse-1">
                <span class="sr-only">Menús</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php" class="navbar-brand">
                <i class="fa-solid fa-lock fa-fw"></i>
                @if (FS_DEMO)
                    DEMO MiFactura.eu
                @elseif ($fsc->empresa->nombrecorto)
                    {!! $fsc->empresa->nombrecorto !!}
                @else
                    {!! $fsc->empresa->nombre !!}
                @endif
            </a>
        </div>
    </div>
</nav>

@include('parts/header-messages')

<form name="f_login" action="index.php?nlogin={!! $nlogin !!}{!! $nurl !!}" method="post" class="form" role="form">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="well well-sm">
                    <br/>
                    <div class="container-fluid mt-3">
                        <div class="row">
                            <div class="col-sm-4 col-md-5">
                                <div class="img-thumbnail d-none d-sm-inline">
                                    @if (FS_DEMO)
                                        <img src="{!! FS_PATH !!}Templates/img/loading_spinner.gif" class="lazyload" data-src="{!! FS_PATH !!}Templates/img/logo.png" alt="logo"/>
                                    @elseif (file_exists(FS_MYDOCS.'images/logo.png'))
                                        <img src="{!! FS_PATH !!}Templates/img/loading_spinner.gif" class="lazyload" data-src="images/logo.png" alt="{!! $fsc->empresa->nombre !!}"/>
                                    @elseif (file_exists(FS_MYDOCS.'images/logo.jpg'))
                                        <img src="{!! FS_PATH !!}Templates/img/loading_spinner.gif" class="lazyload" data-src="{!! FS_PATH !!}images/logo.jpg" alt="{!! $fsc->empresa->nombre !!}"/>
                                    @else
                                        <img src="{!! FS_PATH !!}Templates/img/loading_spinner.gif" class="lazyload" data-src="{!! FS_PATH !!}Templates/img/logo.png" alt="logo"/>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-7">
                                @if (FS_DEMO)
                                    <div class="mb-2">
                                        <input type="text" name="user" class="form-control input-lg" maxlength="12" placeholder="Usuario" autocomplete="off"/>
                                    </div>
                                @else
                                    <div class="mb-2">
                                        <input type="text" name="user" class="form-control input-lg" maxlength="12" placeholder="Usuario" autocomplete="off"/>
                                    </div>
                                    <div class="mb-2">
                                        <input type="password" name="password" class="form-control input-lg" maxlength="32" required placeholder="Contraseña"/>
                                    </div>
                                @endif
                                <button class="btn d-flex justify-content-center align-items-center btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();">
                                    <i class="fa-solid fa-sign-in-alt fa-fw"></i>
                                    <span>Iniciar sesión</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="visible-md mb-3"></div>

@include('feedback')

@endsection
</body>
</html>
