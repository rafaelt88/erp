<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        @if ($fsc->empresa->nombrecorto)
            {!! $fsc->empresa->nombrecorto !!}
        @else
            {!! $fsc->empresa->nombre !!}
        @endif
    </title>
    <meta name="description" content="MiFactura.eu es un software de facturación y contabilidad para pymes. Es software libre bajo licencia GNU/LGPL."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="generator" content="MiFactura.eu"/>
    @if (file_exists('images/favicon.ico'))
        <link rel="shortcut icon" href="{!! FS_PATH !!}images/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
    @else
        <link rel="shortcut icon" href="{!! FS_PATH !!}Templates/img/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
    @endif
    @include('parts/header-styles')
</head>
<body class="auth-body-bg" style="height: 100vh">

<form name="f_login" action="index.php?nlogin={!! $nlogin !!}{!! $nurl !!}" method="post" class="form h-100" role="form">
    <div class="container-fluid h-100 p-0">
        <div class="row g-0 h-100 auth-body-bg">
            <div class="col-xl-9" style="overflow: hidden">
                <div class="auth-full-bg pt-lg-5 p-4">
                    <div class="w-100">
                        <div class="bg-overlay">
                        </div>
                        <div class="cont-video">
                            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                                <source src="https://gitlab.com/xnetdigital-public/static-assets/-/raw/main/videos/X-Net_Intro.m4v" type="video/mp4"
                                        style="">
                            </video>
                        </div>
                        <div class="container d-flex justify-content-center h-100 flex-md-column flex-lg-row">
                            <div class="p-4 mt-auto">
                                <div class="row justify-content-center">
                                    <div class="col-lg-7">
                                        <div class="text-center">
                                            <h4 class="mb-3"></h4>
                                            <div dir="ltr">
                                                <div class="owl-carousel owl-theme auth-review-carousel"
                                                     id="auth-review-carousel">
                                                    <div class="item">
                                                        <div class="py-3">
                                                            <div>
                                                                <h4 class="font-size-16 text-primary">EL TRABAJO MÁS PRODUCTIVO ES EL QUE SALE DE LAS MANOS DE UNA PERSONA CONTENTA.</h4>
                                                                <p class="font-size-14 mb-2">(Víctor Pauchet)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 auth-body-bg">
                <div class="auth-full-page-content p-md-5 p-4">
                    <div class="w-100">

                        <div class="w-100 h-100" id="box_login">
                            <div>
                                <div class="mb-4 mb-md-5">
                                    <a href="index.php" class="d-block auth-logo">
                                        <img src="{!! FS_PATH !!}Templates/dist/assets/images/logo-dark.svg" alt="" style="height: 30px;" class="auth-logo-dark">
                                        <img src="{!! FS_PATH !!}Templates/dist/assets/images/logo-light.svg" alt="" style="height: 30px;" class="auth-logo-light">
                                    </a>
                                </div>
                                <div>
                                    <h5 class="text-primary">Bienvenido</h5>
                                    <p class="text-muted">Por favor inicia sesión para continuar</p>
                                </div>

                                <div class="mt-4">
                                    <form class="form-horizontal" method="POST" action="">
                                        <input type="hidden" name="_token" value="acdEh13tuQZPmh6oUgkAatVRh6vWvOjpAq6nvX6o">
                                        <div class="mb-2">
                                            <label>
                                                Usuario
                                            </label>
                                            @if (FS_DEMO)
                                                <div class="mb-2">
                                                    <input type="text" name="user" class="form-control input-lg" maxlength="12" placeholder="Usuario" autocomplete="off" required/>
                                                </div>
                                            @else
                                                <div class="mb-2">
                                                    <input type="text" name="user" class="form-control input-lg" maxlength="12" placeholder="Usuario" autocomplete="off" required/>
                                                </div>
                                                <label>
                                                    Contraseña
                                                </label>
                                                <div class="mb-2">
                                                    <input type="password" name="password" class="form-control input-lg" maxlength="32" placeholder="Contraseña" required/>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="mt-3 d-grid">
                                            <button class="btn d-flex justify-content-center align-items-center btn-primary" type="submit" onclick="">
                                                Iniciar sesión
                                            </button>
                                        </div>
                                    </form>
                                    @include('parts/login-error-messages')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@include('parts/footer-javascripts')
<script type="text/javascript" src="{!! FS_PATH !!}Templates/js/jquery.ui.shake.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        @if ($fsc->get_errors())
        $("#box_login").shake();
        @endif

        document.f_login.user.focus();

        $("#b_feedback").click(function (event) {
            event.preventDefault();
            $("#modal_feedback").modal('show');
            document.f_feedback.feedback_text.focus();
        });
        $("#b_new_password").click(function (event) {
            event.preventDefault();
            $("#modal_new_password").modal('show');
            document.f_new_password.user.focus();
        });
    });
</script>
</body>
</html>
