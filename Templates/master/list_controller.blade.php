@extends('layouts/main')

@section('styles')
    @parent
    @include('block/style/common/datatables')
@endsection

@section('javascripts')
    @parent
    @include('block/javascripts/common/datatables')
@endsection

@section('main-content')
    @if ($fsc->template_top)
        @if (view_exists($fsc->template_top))
            @include($fsc->template_top)
        @else
            @include('master/template_header')
        @endif
    @else
        @include('master/template_header')
    @endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    @include('block/card_page_header')
                    <div class="card-body px-0">
                        <div class="row">
                            <div class="col-12">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    @foreach ($fsc->tabs as $key1 => $value1)
                                        <li class="nav-item" role="presentation">
                                            <a id="tab_{!! $fsc->page->name !!}_{!! $key1 !!}" class="nav-link d-flex flex-column align-items-center @if ($value1['name'] == $fsc->active_tab) active @endif "
                                               href="{!! $fsc->url() !!}&tab={!! $value1['name'] !!}">
                                                {!! get_icon_with_badge($value1["icon"] . ' fa-fw', $fsc->show_numero($value1['count'], 0)) !!}
                                                <span>{!! $value1['title'] !!}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content nav-content">
                                    <div class="row">
                                        <div class="col-12">
                                            <form name="f_custom_search" action="{!! $fsc->url() !!}&tab={!! $fsc->active_tab !!}" method="post" class="form">
                                                <div class="row my-3">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <input type="hidden" name="offset" value="{!! $fsc->offset !!}"/>
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    @if (count($fsc->get_current_tab('buttons')) > 0)
                                                                        <div class="d-flex justify-content-end gap-1">
                                                                            @foreach ($fsc->get_current_tab('buttons') as $key1 => $value1)
                                                                                <a href="{!! $value1['link'] !!}" target="{!! $value1['target'] !!}" id="{!! $value1['id'] !!}" class="btn {!! $value1['class'] !!}">
                                                                                    <i class="{!! $value1['icon'] !!}"></i>
                                                                                    {!! $value1['label'] !!}
                                                                                </a>
                                                                            @endforeach
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if (count($fsc->get_current_tab('buttons')) > 0)
                                                    <div class="row">
                                                        <div class="col-12">
                                                            @php
                                                                $search_columns = $fsc->get_current_tab('search_columns')
                                                            @endphp

                                                            <div class="accordion mb-3" id="searchFilters">
                                                                <div class="accordion-item">
                                                                    <h2 class="accordion-header" id="searchHeading">
                                                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSearchFilters" aria-expanded="true" aria-controls="collapseSearchFilters">
                                                                            <i class="fa-solid fa-filter fa-fw"></i>
                                                                            <span class="text-nowrap">Filtros de búsqueda</span>
                                                                        </button>
                                                                    </h2>
                                                                    <div id="collapseSearchFilters" class="accordion-collapse collapse show" aria-labelledby="searchHeading" data-bs-parent="#searchFilters">
                                                                        <div class="accordion-body">
                                                                            <div class="row">
                                                                                <div class="col-sm-6 col-md-3 mb-2">
                                                                                    <input class="form-control" type="text" name="query" value="{!! $fsc->query !!}" autocomplete="off" autofocus="" placeholder="Buscar"/>
                                                                                </div>
                                                                                @foreach ($fsc->get_current_tab('filters') as $key1 => $value1)
                                                                                    <div class="col-sm-6 col-md-3 mb-2">
                                                                                        {!! $value1->show() !!}
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                            <div class="row mt-3">
                                                                                <div class="col-sm-9">
                                                                                    <div class="row justify-content-start">
                                                                                        <div class="col-sm-8 col-md-6 col-xl-3">
                                                                                            @if (count($fsc->get_current_tab('sort_options')) > 0)
                                                                                                <select name="sort" class="form-select select2 input-sm" onchange="this.form.submit();">
                                                                                                    <option data-comment="Placeholder"></option>
                                                                                                    @foreach ($fsc->get_current_tab('sort_options') as $key1 => $value1)
                                                                                                        @if ($key1==$fsc->sort_option)
                                                                                                            <option value="{!! $key1 !!}" selected="">{!! $value1 !!}</option>
                                                                                                        @else
                                                                                                            <option value="{!! $key1 !!}">{!! $value1 !!}</option>
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            @endif
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3 d-sm-flex justify-content-end mt-2 mt-sm-0">
                                                                                    <div class="">
                                                                                        <button class="d-flex justify-content-center align-items-center btn btn-primary d-sm-flex text-nowrap w-100" type="submit" aria-label="Filtrar">
                                                                                            <i class="fa-solid fa-filter fa-fw"></i>
                                                                                            <span>Filtrar</span>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </form>
                                        </div>
                                    </div>
                                    @include('master/list_controller_table')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($fsc->template_bottom)
        @include($fsc->template_bottom)
    @endif
@endsection
