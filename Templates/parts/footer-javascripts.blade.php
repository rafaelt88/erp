<script type="text/javascript" src="{!! FS_PATH !!}node_modules/jquery/dist/jquery.min.js?idcache={!! $fsc->id_cache !!}"></script>
{{-- <script type="text/javascript" src="{!! FS_PATH !!}node_modules/popper.js/dist/umd/popper.min.js?idcache={!! $fsc->id_cache !!}"></script> --}}
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/bootstrap/dist/js/bootstrap.bundle.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/devbridge-autocomplete/dist/jquery.autocomplete.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/jquery-ui-dist/jquery-ui.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/bootbox/dist/bootbox.all.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/select2/dist/js/select2.full.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/select2/dist/js/i18n/es.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/lazysizes/lazysizes.min.js?idcache={!! $fsc->id_cache !!}" async></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/tinymce/tinymce.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/moment/min/moment.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/moment/locale/es.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript" src="{!! FS_PATH !!}node_modules/toastr/build/toastr.min.js?idcache={!! $fsc->id_cache !!}"></script>
<script type="text/javascript">
    @if (FS_DEBUG)
    let oldOnError = window.onerror;

    let newErrorHandler = function (msg, url, lineNo, columnNo, error) {
        let errorDetails = {
            msg: msg,
            url: url,
            lineNo: lineNo,
            columnNo: columnNo,
            error: error
        };

        console.log(errorDetails);
        let toastrOptions = {
            "escapeHtml": true,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        let message = msg + ' on line ' + lineNo + ' for ' + url;
        let title = 'JavaScript error';
        toastr.error(message, title, toastrOptions)


        if (oldOnError) {
            // Call previous handler.
            return oldOnError(msg, url, lineNo, columnNo, error);
        }

        // Just let default handler run.
        return false;
    }

    // Override previous handler.
    window.onerror = function (msg, url, lineNo, columnNo, error) {
        return newErrorHandler(msg, url, lineNo, columnNo, error);
    }
    @endif

        window.FontAwesomeConfig = {autoReplaceSvg: 'nest'}
</script>
<script type="text/javascript" defer src="{!! FS_PATH !!}node_modules/@fortawesome/fontawesome-free/js/all.min.js?idcache={!! $fsc->id_cache !!}"></script>

@switch(THEME)
    @case('skote')
        <script type="text/javascript" src="{!! FS_PATH !!}node_modules/metismenu/dist/metisMenu.min.js?idcache={!! $fsc->id_cache !!}"></script>
        <script type="text/javascript" src="{!! FS_PATH !!}node_modules/simplebar/dist/simplebar.min.js?idcache={!! $fsc->id_cache !!}"></script>
        <script type="text/javascript" src="{!! FS_PATH !!}node_modules/node-waves/dist/waves.min.js?idcache={!! $fsc->id_cache !!}"></script>
        <script type="text/javascript" src="{!! FS_PATH !!}node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js?idcache={!! $fsc->id_cache !!}"></script>
        <script type="text/javascript" src="{!! FS_PATH !!}node_modules/bootstrap-maxlength/dist/bootstrap-maxlength.min.js?idcache={!! $fsc->id_cache !!}"></script>
        <script type="text/javascript" src="{!! FS_PATH !!}Templates/dist/assets/js/app.js?idcache={!! $fsc->id_cache !!}"></script>
        @break

    @default
        @if ($fsc->use_sidebar_menu == true)
            <script type="text/javascript" src="{!! FS_PATH !!}node_modules/metismenu/dist/metisMenu.min.js?idcache={!! $fsc->id_cache !!}"></script>
        @else
            {{-- Por ahora nada aquí --}}
        @endif
@endswitch

<script type="text/javascript" src="{!! $fsc->get_js_location('base.js') !!}"></script>

<script type="text/javascript">
    bootbox.setLocale("es");

    const articulo_habilitar_ancho = Boolean({!! $fsc->empresa->articulo_habilitar_ancho !!});
    const articulo_habilitar_alto = Boolean({!! $fsc->empresa->articulo_habilitar_alto !!});
    const articulo_habilitar_profundo = Boolean({!! $fsc->empresa->articulo_habilitar_profundo !!});

    $('[data-bs-toggle="tooltip"]').tooltip();


    /**
     * TODO: Missing documentation.
     *
     * @param precio
     * @param coddivisa
     * @returns {string|*}
     */
    function show_precio(precio, coddivisa) {
        coddivisa || (coddivisa = '{!! $fsc->empresa->coddivisa !!}');

        if (coddivisa == '{!! $fsc->empresa->coddivisa !!}') {
            @if (FS_POS_DIVISA=='right')
                return number_format(precio, {!! FS_NF0 !!}, '{!! FS_NF1 !!}', '{!! FS_NF2 !!}') + ' {!! $fsc->simbolo_divisa() !!}';
            @else
                return '{!! $fsc->simbolo_divisa() !!}' + number_format(precio, {!! FS_NF0 !!}, '{!! FS_NF1 !!}', '{!! FS_NF2 !!}');
            @endif
        } else {
            return number_format(precio, {!! FS_NF0 !!}, '{!! FS_NF1 !!}', '{!! FS_NF2 !!}');
        }
    }

    /**
     * TODO: Missing documentation.
     *
     * @param numero
     * @returns {string|*}
     */
    function show_numero(numero) {
        return number_format(numero, {!! FS_NF0 !!}, '{!! FS_NF1 !!}', '{!! FS_NF2 !!}');
    }

    /**
     * Devuelve el impuesto asignado por defecto de la empresa.
     *
     * @returns {string}
     */
    function get_codimpuesto() {
        return '{!! $fsc->empresa->codimpuesto !!}';
    }

    /**
     * Devuelve el impuesto exento asignado por defecto de la empresa.
     *
     * @returns {string}
     */
    function get_codimpuesto_exento() {
        return '{!! $fsc->empresa->codimpuesto_sinimpuestos !!}';
    }

    /**
     * TODO: Missing documentation.
     *
     * @returns {string}
     */
    function get_impuesto_txt() {
        return '{!! FS_IVA !!}';
    }

    @if ($fsc->show_background)
    /**
     * TODO: Missing documentation.
     *
     * @constructor
     */
    function ChangeIt() {
        @if (file_exists(FS_MYDOCS.'images/dasboardbg.jpg'))
                @php
                    rename(FS_MYDOCS.'images/dasboardbg.jpg', FS_MYDOCS.'images/dashboardbg.jpg');
                @endphp
                @endif

                @if (file_exists(FS_MYDOCS.'images/dashboardbg.jpg'))
            document.body.background = 'images/dashboardbg.jpg';
        @else
            document.body.background = 'Templates/img/dashboardbg.jpg';
        @endif

            x = $(window).width();
        y = $(window).height();
        if (x / y > 1.6) {
            y = x * 640 / 1024;
        } else {
            x = y * 1024 / 640;
        }
        s = x + "px " + y + "px";
        document.body.style.backgroundSize = s;// Background repeat
    }

    @endif

    $(document).ready(function () {
        check_mobile_menu();
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
        };
        $.fn.modal.Constructor.prototype._enforceFocus = function () {
        };

        @if ($fsc->show_background)
        $(window).resize(function () {
            ChangeIt();
        });
        ChangeIt();
        @endif

        @switch(THEME)
        @case('skote')
        // Activamos el select2 para todos los selects
        $('select.select2').each(function () {
            $(this).select2({
                placeholder: "Selecciona una opción",
                language: 'es',
                theme: 'bootstrap-5',
                //allowClear: true,
                containerCssClass: ':all:',
                dropdownParent: $(this).parent()
            });
        });

        {{-- TODO: jQuery v3.6.0 introdujo este problema, con v3.5.1 no sucede --}}
        $(document).on("select2:open", () => {
            document.querySelector(".select2-container--open .select2-search__field").focus()
        });

        $("#side-menu").metisMenu({
            toggle: false
        });
        @break

        @default
        // Activamos el select2 para todos los selects
        $('select.select2').select2({
            placeholder: "Selecciona una opción",
            language: 'es',
            theme: 'bootstrap-5',
            allowClear: true,
            containerCssClass: ':all:'
        });
        @if ($fsc->use_sidebar_menu == true)
        $("#main-menu").metisMenu({
            toggle: false
        });
        @else
        {{-- Por ahora nada aquí --}}
        @endif
        @endswitch

        $.ajax({
            dataType: "json",
            url: 'miAJAX.php?action=register&codagente={!! $fsc->user->codagente !!}&url=' + window.location,
            success: function (data) {
                if (data.status == 'Ok') {
                    data = data.data;
                    if (data.plugins) {
                        for (c = 0; c < data.plugins.length; c++) {
                            $('.btn-info[href$="' + data.plugins[c] + '"').attr("href", "index.php?page=ayuda_soporte_plugin&codagente={!! $fsc->user->codagente !!}");
                        }
                    }

                    var tt = data.capacidad;
                    var p = data.ocupado * 100 / tt;
                    p = Math.min(p, 100); // Ajustamos de 0 a 100 para no ver % excedidos
                    p = p.toFixed(2) + '%';
                    var texto = 'Ocupados ' + data.ocupado + 'Mb de ' + tt + 'Mb';
                    if (tt == -1) {
                        texto = 'Espacio ilimitado';
                        p = '  ∞  ';
                    }
                    var col = '<div class="progress"><div class="progress-bar';
                    if (p <= 50) {
                        col += ' progress-bar-success';
                    }
                    if (p > 50) {
                        col += ' progress-bar-warning';
                    }
                    if (p > 80) {
                        col += ' progress-bar-danger';
                    }
                    col += '" role="progressbar" aria-valuenow="' + p + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + p + '%;"><b style="color: black;" title="' + texto + '">' + p + '</b></div></div>';

                    if (data.vermodal == 1) {
                        $("#modal_mf .modal-header h4").html(data.titulo);
                        $("#modal_mf .modal-body").html(data.texto);
                        $('#modal_mf').modal('show');
                    }
                    {{--
                    /* Para el header de FS*/
                    $("ul.nav:eq(1)").append('<li class="dropdown"><a class="dropdown-toggle" href="index.php?page=ayuda_soporte" style="height: 45px;">' + col + '</span></a></li>');
                    /* Para el header del plugin Menú Único*/
                    //$(".navbar-right").append('<li class="dropdown"><a class="dropdown-toggle" href="index.php?page=ayuda_soporte" style="height: 45px;">'+col+'</span></a></li>');

                    /* Para el header del plugin adminlte*/
                    $(".navbar-custom-menu .navbar-nav").append('<li class="dropdown"><a href="index.php?page=ayuda_soporte" class="dropdown-toggle" data-bs-toggle="dropdown" style="height: 50px;">' + col + '</span></a></li>');
                    --}}
                }
            }
        });
        //cambiamos el menu de ayuda, se supone que este plugin solo lo utilizaran nuestros clientes
        $("ul.navbar-right ul.dropdown-menu:eq(0)").html('<li> <a rel="noopener" href="https://www.mifactura.eu" target="_blank"><i class="fa-solid fa-book fa-fw"></i> X-Net</a></li><li> <a rel="noopener" href="https://www.mifactura.eu/contactar.php"  target="_blank"><i class="fa-solid fa-shield fa-fw"></i> Contactar</a></li><li><a href="index.php?page=ayuda_soporte_soporte"><i class="fa-solid fa-life-ring fa-fw"></i> Acceso Soporte</a></li><li class="divider"></li><li><a href="#" id="b_feedback"><i class="fa-solid fa-edit fa-fw"></i> Informar de error...</a></li>');

        $("#b_feedback").click(function (event) {
            event.preventDefault();
            $("#modal_feedback").modal("show");
            document.f_feedback.feedback_text.focus();
        });
        //Como este plugin solo lo tendrán los clientes de x-net, interceptamos el envio del formulario de ayuda para que nos lo envie a nosotros
        $("form[name='f_feedback']").attr('action', 'https://www.mifactura.eu/contactar.php?enviar=reportar');
        $("form[name='f_feedback']").attr('target', '_blank');

    });


    function check_mobile_menu() {
        let mobile_container = document.getElementsByClassName("mobile_page")[0];
        if (mobile_container !== undefined) {
            let container = document.getElementsByClassName("page-content")[0];
            container.style.cssText = 'padding: 0px !important';
        }
    }


    async function set_favorite_page() {
        let prueba = await call_set_favorite_page();
    }

    function call_set_favorite_page() {
        @php
            //$fsc->page->set_favorite_page(($fsc->page->folder));
        @endphp
        /**$.ajax({
            type: "POST",
            url: '/model/fs_page.php',
            dataType: 'json',
            success: function (obj, textstatus) {
                if( !('error' in obj) ) {
                    yourVariable = obj.result;
                    console.log(yourVariable);
                }
                else {
                    console.log(obj.error);
                }
            }
        });**/
    }
</script>

{{-- @include('parts/notifications_toastr') --}}

@if (FS_DEBUG)
    <!-- debugBar->getFooter -->
    {!! $fsc->debugBar->getFooter() !!}
@endif

@include('block/extensions/javascript')

@if (isset($fsc->protectClose) && ($fsc->protectClose))
    @include('block/javascripts/common/protect_close');
@endif

{{-- Si el template es un modal de ruptura del código (como acceso denegado), no se carga el JS de la página --}}
@if (!in_array($fsc->template, $fsc::BREAKING_PAGES))

    @if (view_exists('block/javascripts/' . $fsc->page->name))
        @include('block/javascripts/' . $fsc->page->name)
    @endif

    @section('javascripts')
        <!-- Sección javascripts -->
    @show

@endif

@if (isset($fsc->javascriptCode) && count($fsc->javascriptCode) > 0)
    <script type="text/javascript">
        $(document).ready(function () {
            @foreach($fsc->javascriptCode as $code)
                 {!! $code !!}
            @endforeach
        });
    </script>
@endif

@include('no_script')
