@php
    if (method_exists($fsc, 'get_errors')) {
        $errors = $fsc->get_errors();
        $messages = $fsc->get_messages();
        $advices = $fsc->get_advices();
    } else {
        $errors = $fsc->getErrors();
        $messages = $fsc->getMessages();
        $advices = $fsc->getAdvices();
    }
@endphp

@if ($errors || $messages || $advices)
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @endif

                @if ($errors)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Cerrar"></button>
                        <ul class="m-0">
                            @foreach ($errors as $key1 => $value1)
                                <li>{!! $value1 !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($messages)
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Cerrar"></button>
                        <ul class="m-0">
                            @foreach ($messages as $key1 => $value1)
                                <li>{!! $value1 !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($advices)
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Cerrar"></button>
                        <ul class="m-0">
                            @foreach ($advices as $key1 => $value1)
                                <li>{!! $value1 !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if ($errors || $messages || $advices)
            </div>
        </div>
    </div>
@endif
