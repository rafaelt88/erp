<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container-fluid">
        <a href="index.php" class="navbar-brand">
            <i class="fa-solid fa-home fa-fw"></i>
            @if (FS_DEMO)
                DEMO
            @elseif ($fsc->empresa->nombrecorto)
                {!! $fsc->empresa->nombrecorto !!}
            @else
                {!! $fsc->empresa->nombre !!}
            @endif
        </a>

        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#mf-navbar-collapse-1" aria-controls="mf-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="mf-navbar-collapse-1">
            <ul class="navbar-nav mb-md-0">
                @foreach ($fsc->folders() as $key1 => $value1)
                    <li class="nav-item dropdown">
                        <a href="#" aria-expanded="false" data-bs-toggle="dropdown" id="navbarDropdown_{!! $fsc->page->folder !!}" class="nav-link dropdown-toggle @if ($value1==$fsc->page->folder) active @endif ">
                            <span class="text-capitalize">{!! $value1 !!}</span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown_{!! $fsc->page->folder !!}">
                            @foreach ($fsc->pages($value1) as $key2 => $value2)
                                <li>
                                    <a href="{!! $value2->url() !!}" class="dropdown-item text-secondary @if ($value2->showing()) active @endif ">
                                        {!! $value2->title !!}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
                @if (count($GLOBALS['plugins'])>0)
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown_fast" class="nav-link dropdown-toggle" href="#" aria-expanded="false" data-bs-toggle="dropdown" title="Acceso rápido">
                            <i class="fa-solid fa-star fa-fw"></i>
                            <span class="d-block d-sm-none">Acceso rápido</span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown_fast">
                            @php
                                $menu_ar_vacio = true;
                            @endphp
                            @foreach ($fsc->user->get_menu() as $key1 => $value1)
                                @if ($value1->important)
                                    <li>
                                        <a href="{!! $value1->url() !!}" class="dropdown-item text-secondary">
                                            {!! $value1->title !!}
                                        </a>
                                    </li>
                                    @php
                                        $menu_ar_vacio = false;
                                    @endphp
                                @endif
                            @endforeach
                            @if ($menu_ar_vacio)
                                <li>
                                    <a href="#" class="dropdown-item text-secondary">
                                        Vacío
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
            </ul>
            <ul class="navbar-nav mb-md-0 ms-auto">
                <li class="nav-item" role="presentation">
                    <a rel="noopener" href="https://api.whatsapp.com/send?phone=34637582364&text=Necesito%20Soporte" target="_blank" class="nav-link d-flex flex-column align-items-center" title="Soporte vía WhatsApp">
                        <i style="background-size: auto 20px; height: 20px; overflow: hidden; padding-right: 0; width: 20px; background-image: url(https://static.whatsapp.net/rsrc.php/yv/r/-r3j-x8ZnM7.svg); background-repeat: no-repeat; display: block; float: left;"></i>
                        <span class="d-block d-sm-none">Soporte vía WhatsApp</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" data-bs-toggle="dropdown" id="navbarDropdown_ayuda" class="nav-link dropdown-toggle">
                        <i class="fa-solid fa-circle-question fa-fw"></i>
                        <span class="d-block d-sm-none">{!! $fsc->reseller_data->name !!}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown_ayuda">
                        @if (isset($reseller->url))
                            <li>
                                <a href="{!! $fsc->reseller_data->url !!}" target="_blank" class="dropdown-item text-secondary">
                                    <i class="fa-solid fa-book fa-fw"></i>{!! $fsc->reseller_data->name !!}
                                </a>
                            </li>
                        @endif
                        @if (isset($reseller->url_contacto))
                            <li>
                                <a href="{!! $fsc->reseller_data->url_contacto !!}" target="_blank" class="dropdown-item text-secondary">
                                    <i class="fa-solid fa-shield fa-fw"></i>
                                    Contactar
                                </a>
                            </li>
                        @endif
                        <li class="divider"></li>
                        <li>
                            <a href="#" id="b_feedback" class="dropdown-item text-secondary">
                                <i class="fa-solid fa-edit fa-fw"></i>
                                Informar de error...
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a id="navbarDropdown_usuario" class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" title="{!! $fsc->user->nick !!}">
                        <i class="fa-solid fa-user-circle fa-fw"></i>
                        <span class="d-block d-sm-none">Usuario</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" data-bs-popper="static" aria-labelledby="navbarDropdown_usuario">
                        <li>
                            <a href="{!! $fsc->user->url() !!}" class="dropdown-item text-secondary">
                                <i class="fa-solid fa-user-circle fa-fw"></i>{!! $fsc->user->nick !!}
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{!! $fsc->url() !!}&logout=TRUE" class="dropdown-item text-secondary">
                                <i class="fa-solid fa-sign-out fa-fw"></i>
                                Cerrar sesión
                            </a>
                        </li>
                    </ul>
                </li>

                {{--
                  @if ($fsc->check_for_updates())
                   <li>
                      <a href="updater.php" class="d-flex btn-danger" title="Hay actualizaciones disponibles">
                         <i class="fa-solid fa-cloud-upload fa-fw d-none d-sm-inline"></i>
                         <span class="d-block d-sm-none">Actualizaciones</span>
                      </a>
                   </li>
                   @endif

                  @if ($fsc->get_last_changes())
                   <li class="dropdown">
                      <a href="#" data-bs-toggle="dropdown" id="navbarDropdown_historial" class="nav-link dropdown-toggle">
                         <i class="fa-solid fa-clock fa-fw d-none d-sm-inline"></i>
                         <span class="d-block d-sm-none">Historial</span>
                      </a>
                      <ul class="dropdown-menu" aria-labelledby="navbarDropdown_historial">
                      @foreach ($fsc->get_last_changes() as $key1 => $value1)
                        @if ($value1['nuevo'])
                         <li title="creado el {!! $value1['cambio'] !!}">
                            <a href="{!! $value1['url'] !!}" class="link-primary">
                               <i class="fa-solid fa-file fa-fw"></i>{!! $value1['texto'] !!}
                            </a>
                         </li>
                          @else
                         <li title="modificado el {!! $value1['cambio'] !!}">
                            <a href="{!! $value1['url'] !!}" class="link-primary">
                               <i class="fa-solid fa-edit fa-fw"></i>{!! $value1['texto'] !!}
                            </a>
                         </li>
                         @endif
                      @endforeach
                      </ul>
                   </li>
                   @endif
                --}}
            </ul>
        </div>
    </div>
</nav>
