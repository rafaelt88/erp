<script type="text/javascript">

    $(document).ready(function () {
        let toastrOptions = {
            "escapeHtml": true,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        let titleToastr = '';
        let messageToastr = "";

        @foreach ($fsc->get_all_logs() as $key_log => $log)
            @switch($log['channel'])
            @case ('messages')
            toastrOptions.timeOut = 5000;
        messageToastr = '{!! str_replace(PHP_EOL,' ', $log['message']) !!}';
        // titleToastr = 'Confirmación';
        toastr.success(messageToastr, titleToastr, toastrOptions);
        @break

            @case ('advices')
            toastrOptions.timeOut = 5000;
        messageToastr = '{!! str_replace(PHP_EOL,' ', $log['message']) !!}';
        // titleToastr = 'Advertencia';
        toastr.info(messageToastr, titleToastr, toastrOptions);
        @break

            @case ('errors')
            toastrOptions.timeOut = 0;
        messageToastr = '{!! str_replace(PHP_EOL,' ', $log['message']) !!}';
        // titleToastr = 'Error';
        toastr.error(messageToastr, titleToastr, toastrOptions);
        @break
        {{--
        @case ('debug')
        @case ('sql')
        @default
            messageToastr = '{!! str_replace(PHP_EOL,' ', $log['message']) !!}';
            toastr.warning(messageToastr, titleToastr, toastrOptions);
        --}}
        @endswitch
        @endforeach
    });
</script>
