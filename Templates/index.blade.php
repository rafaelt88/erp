@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        <i class="fa-solid fa-exclamation-triangle fa-fw"></i>
                        Página no encontrada
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 text-justify">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        Ayuda
                    </div>
                    <div class="card-body">
                        <p class="form-text">
                            No se encuentra el controlador de esta página.
                            Consulta con el administrador.
                        </p>
                        <h3 class="fw-light">
                            Problemas frecuentes
                        </h3>
                        <ul>
                            <li>
                                <b>¿Has actualizado desde MiFactura.eu 2014 o anteriores?</b>
                                <ul>
                                    <li>
                                        El sistema ha cambiado mucho, ahora los plugins tienen un desarrollo
                                        independiente.
                                    </li>
                                    <li>
                                        Elimina los plugins antiguos y sigue el asistente de Admin &gt;
                                        <a href="index.php?page=admin_home" class="link-primary">Panel de control</a>.
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b>¿Has desactivado un plugin?</b>
                                <ul>
                                    <li>
                                        Es posible que fallase al eliminar las entradas del menú.
                                        Ve a admin &gt;
                                        <a href="index.php?page=admin_home" class="link-primary">Panel de control</a>
                                        > Menú y pulsa guardar. Eliminará las entradas del menú de las páginas
                                        que ya no estén activas.
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b>¿Has reinstalado a lo bestia?</b>
                                <ul>
                                    <li>
                                        Tendrás en el menú acceso a páginas de plugins que no están activos.
                                        Ve a admin &gt;
                                        <a href="index.php?page=admin_home" class="link-primary">Panel de control</a>
                                        > Menú y pulsa guardar. Eliminará las entradas del menú de las páginas
                                        que ya no estén activas.
                                    </li>
                                </ul>
                            </li>
                            @if (filter_input(INPUT_GET, 'page'))
                                <li>
                                    <b>¿Eres programador y estas intentando hacer un plugin?</b>
                                    <ul>
                                        <li>
                                            Entonces te comento que lo que ha pasado es que no encuentro
                                            el archivo
                                            <b>{!! filter_input(INPUT_GET, 'page') !!}.php</b>, que debería estar en
                                            el directorio <b>controller</b> de tu plugin.
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                        <p class="form-text">
                            Si crees que es un error de MiFactura.eu, no dudes en notificármelo.
                            <b>Haz clic en el botón de ayuda</b> <i class="fa-solid fa-circle-question fa-fw"></i>
                            y <b>notifícame el error</b>. Intenta no obviar información importante:
                            ¿Has actualizado desde MiFactura.eu 2014 o anteriores? ¿Vienes de Eneboo/Abanq?
                            ¿Has reinstalado a lo bestia? Son cuestiones importantes a la hora de buscar un error.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('parts/errors/broken')
@endsection
