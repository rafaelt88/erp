/*
Template Name: Skote - Admin & Dashboard Template
Author: Themesbrand
Version: 3.3.0.
Website: https://themesbrand.com/
Contact: themesbrand@gmail.com
File: Main Js File
*/

(function ($) {
    'use strict';

    let $body = $('body');
    let language = localStorage.getItem('language');
    // Default Language
    let default_lang = 'es';
    let working_notifications = true;

    /**
     * Set language to selected
     *
     * @param lang
     */
    function setLanguage(lang) {
        if (document.getElementById('header-lang-img')) {
            if (lang === 'en') {
                document.getElementById('header-lang-img').src = 'Templates/dist/assets/images/flags/us.jpg';
            } else if (lang === 'es') {
                document.getElementById('header-lang-img').src = 'Templates/dist/assets/images/flags/spain.jpg';
            } else if (lang === 'gr') {
                document.getElementById('header-lang-img').src = 'Templates/dist/assets/images/flags/germany.jpg';
            } else if (lang === 'it') {
                document.getElementById('header-lang-img').src = 'Templates/dist/assets/images/flags/italy.jpg';
            } else if (lang === 'ru') {
                document.getElementById('header-lang-img').src = 'Templates/dist/assets/images/flags/russia.jpg';
            }
            localStorage.setItem('language', lang);
            language = localStorage.getItem('language');
            getLanguage();
        }
    }

    /**
     * Multi language setting
     */
    function getLanguage() {
        language = language === null ? setLanguage(default_lang) : 'es';
        $.getJSON('Templates/dist/assets/lang/' + language + '.json', function (lang) {
            $('html').attr('lang', language);
            $.each(lang, function (index, val) {
                index === 'head' ? $(document).attr('title', val['title']) : false;
                $('[key="' + index + '"]').text(val);
            });
        });
    }

    /**
     * Initialize metismenu
     */
    function initMetisMenu() {
        //metis menu
        $('#side-menu').metisMenu();
    }

    /**
     * Initialize a sidebar that is collapsible
     */
    function initLeftMenuCollapse() {
        $('#vertical-menu-btn').on('click', function (event) {
            event.preventDefault();
            $body.toggleClass('sidebar-enable');
            if ($(window).width() >= 992) {
                $body.toggleClass('vertical-collapsed');
            } else {
                $body.removeClass('vertical-collapsed');
            }

            if (typeof $.fn.dataTable !== 'undefined') {
                $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            }
        });
    }

    /**
     * Initialize the active items on sidebar menu
     */
    function initActiveMenu() {
        // === following js will activate the menu in left side bar based on url ====
        $('#sidebar-menu a').each(function () {
            let pageUrl = window.location.href.split(/[?#]/)[0];
            if (this.href == pageUrl) {
                $(this).addClass('active');
                $(this).parent().addClass('mm-active'); // add active to li of the current link
                $(this).parent().parent().addClass('mm-show');
                $(this).parent().parent().prev().addClass('mm-active'); // add active class to an anchor
                $(this).parent().parent().parent().addClass('mm-active');
                $(this).parent().parent().parent().parent().addClass('mm-show'); // add active to li of the current link
                $(this)
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .addClass('mm-active');
            }
        });
    }

    /**
     * Initialize a scrollable sidebar menu
     */
    function initMenuItemScroll() {
        // focus active menu in left sidebar
        $(document).ready(function () {
            let sidebarMenuActive = $('#sidebar-menu .mm-active .active');
            if (
                $('#sidebar-menu').length > 0 &&
                sidebarMenuActive.length > 0
            ) {
                let activeMenu = sidebarMenuActive.offset().top;
                if (activeMenu > 300) {
                    activeMenu = activeMenu - 300;
                    $('.vertical-menu .simplebar-content-wrapper').animate(
                        {scrollTop: activeMenu},
                        'slow'
                    );
                }
            }
        });
    }

    /**
     * Initialize an horizontal menu
     */
    function initHorizontalMenuActive() {
        $('.navbar-nav a').each(function () {
            let pageUrl = window.location.href.split(/[?#]/)[0];
            if (this.href == pageUrl) {
                $(this).addClass('active');
                $(this).parent().addClass('active');
                $(this).parent().parent().addClass('active');
                $(this).parent().parent().parent().addClass('active');
                $(this).parent().parent().parent().parent().addClass('active');
                $(this).parent().parent().parent().parent().parent().addClass('active');
                $(this)
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .addClass('active');
            }
        });
    }

    /**
     * Initialize environment to fullscreen.
     */
    function initFullScreen() {
        $('[data-toggle="fullscreen"]').on('click', function (e) {
            e.preventDefault();
            $body.toggleClass('fullscreen-enable');
            if (
                !document.fullscreenElement &&
                /* alternative standard method */ !document.mozFullScreenElement &&
                !document.webkitFullscreenElement
            ) {
                // current working methods
                if (document.documentElement.requestFullscreen) {
                    document.documentElement.requestFullscreen();
                } else if (document.documentElement.mozRequestFullScreen) {
                    document.documentElement.mozRequestFullScreen();
                } else if (document.documentElement.webkitRequestFullscreen) {
                    document.documentElement.webkitRequestFullscreen(
                        Element.ALLOW_KEYBOARD_INPUT
                    );
                }
            } else {
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                }
            }
        });
        document.addEventListener('fullscreenchange', exitHandler);
        document.addEventListener('webkitfullscreenchange', exitHandler);
        document.addEventListener('mozfullscreenchange', exitHandler);

        function exitHandler() {
            if (
                !document.webkitIsFullScreen &&
                !document.mozFullScreen &&
                !document.msFullscreenElement
            ) {
                $body.removeClass('fullscreen-enable');
            }
        }
    }

    /**
     * Initialize a right sidebar
     */
    function initRightSidebar() {
        // right side-bar toggle
        $('.right-bar-toggle').on('click', function () {
            $body.toggleClass('right-bar-enabled');
        });
        $(document).on('click', 'body', function (e) {
            if ($(e.target).closest('.right-bar-toggle, .right-bar').length > 0) {
                return;
            }
            $body.removeClass('right-bar-enabled');
        });
    }

    /**
     * Initialize a dropdown menu
     */
    function initDropdownMenu() {
        if (document.getElementById('topnav-menu-content')) {
            let elements = document
                .getElementById('topnav-menu-content')
                .getElementsByTagName('a');
            for (let i = 0, len = elements.length; i < len; i++) {
                elements[i].onclick = function (elem) {
                    if (elem.target.getAttribute('href') === '#') {
                        elem.target.parentElement.classList.toggle('active');
                        elem.target.nextElementSibling.classList.toggle('show');
                    }
                };
            }
            window.addEventListener('resize', updateMenu);
        }
    }

    /**
     * Update menu content
     */
    function updateMenu() {
        let elements = document
            .getElementById('topnav-menu-content')
            .getElementsByTagName('a');
        for (let i = 0, len = elements.length; i < len; i++) {
            if (elements[i].parentElement.getAttribute('class') === 'nav-item dropdown active') {
                elements[i].parentElement.classList.remove('active');
                if (elements[i].nextElementSibling !== null) {
                    elements[i].nextElementSibling.classList.remove('show');
                }
            }
        }
    }

    /**
     * Initialize some common tooltip-like helpers
     */
    function initComponents() {
        let tooltipTriggerList = [].slice.call(
            document.querySelectorAll('[data-bs-toggle="tooltip"]'));
        tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl);
        });

        let popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
        popoverTriggerList.map(function (popoverTriggerEl) {
            return new bootstrap.Popover(popoverTriggerEl);
        });

        let offcanvasElementList = [].slice.call(
            document.querySelectorAll('.offcanvas')
        );
        let offcanvasList = offcanvasElementList.map(function (offcanvasEl) {
            return new bootstrap.Offcanvas(offcanvasEl);
        });
    }

    /**
     * Initialize a preloader if it's founded
     */
    function initPreloader() {
        $(window).on('load', function () {
            $('#status').fadeOut('fast');
            $('#preloader').fadeOut('fast');
        });
    }

    /**
     * TODO: Missing documentation
     */
    function initSettings() {
        if (window.sessionStorage) {
            let themeMode = sessionStorage.getItem('theme_mode');
            if (!themeMode) {
                sessionStorage.setItem('theme_mode', 'light-mode-switch');
            } else {
                $('.right-bar input:checkbox').prop('checked', false);
                $('#' + themeMode).prop('checked', true);
                updateThemeSetting(themeMode);
            }
        }
        $('#light-mode-switch, #dark-mode-switch').on('change', function (e) {
            updateThemeSetting(e.target.id);
        });

        // show password input value
        $('#password-addon').on('click', function () {
            if ($(this).siblings('input').length > 0) {
                $(this).siblings('input').attr('type') === 'password'
                    ? $(this).siblings('input').attr('type', 'input')
                    : $(this).siblings('input').attr('type', 'password');
            }
        });
    }

    /**
     * Update theme settings on page and session storage.
     *
     * @param id
     */
    function updateThemeSetting(id) {
        let lightModeSwitch = $('#light-mode-switch');
        let darkModeSwitch = $('#dark-mode-switch');
        if (lightModeSwitch.prop('checked') === true && id === 'light-mode-switch' && $body.attr('data-theme') !== 'light') {
            $('html').removeAttr('dir');
            darkModeSwitch.prop('checked', false);
            $('#bootstrap-style').attr('href', 'Templates/dist/assets/css/bootstrap.min.css');
            $('#app-style').attr('href', 'Templates/dist/assets/css/app.min.css');
            $body.attr('data-theme', 'light');
            sessionStorage.setItem('theme_mode', 'light-mode-switch');
            updateThemeOptions();
        } else if (darkModeSwitch.prop('checked') === true && id === 'dark-mode-switch' && $body.attr('data-theme') !== 'dark') {
            $('html').removeAttr('dir');
            lightModeSwitch.prop('checked', false);
            $('#bootstrap-style').attr('href', 'Templates/dist/assets/css/bootstrap-dark.min.css');
            $('#app-style').attr('href', 'Templates/dist/assets/css/app-dark.min.css');
            $body.attr('data-theme', 'dark');
            sessionStorage.setItem('theme_mode', 'dark-mode-switch');
            updateThemeOptions();
        }
    }

    /**
     * Update theme options in realtime using AJAX.
     */
    function updateThemeOptions() {
        let userProfile = $('#user_profile_link');
        if (!userProfile) {
            bootbox.alert({
                size: 'medium',
                title: "<b>Atención:</b>",
                message: 'No se ha podido encontrar el enlace al perfil del usuario.'
            });
            return;
        }

        $.ajax({
            url: userProfile[0].href + "&action=update_theme_options",
            method: "POST",
            data: {
                theme_topbar: 'light',
                theme_sidebar: 'dark',
                theme_body: $body.attr('data-theme'),
            },
            dataType: "json"
        }).done(function (response) {
            if (!response.updated) {
                bootbox.alert({
                    size: 'medium',
                    title: "<b>Atención:</b>",
                    message: 'Error al actualizar los ajustes del tema.'
                });
            }
        }).fail(function () {
            bootbox.alert({
                size: 'medium',
                title: '<b>Atención:</b>',
                message: 'Error al intentar actualizar los ajustes del tema.'
            });
        });
    }

    /**
     * Initialize environment language
     */
    function initLanguage() {
        // Auto Loader
        if (language !== null && language !== default_lang) setLanguage(language);
        $('.language').on('click', function (e) {
            setLanguage($(this).attr('data-lang'));
        });
    }

    /**
     * Initialize a common check all option for checkboxes
     */
    function initCheckAll() {
        $('#checkAll').on('change', function () {
            $('.table-check .form-check-input').prop('checked', $(this).prop('checked'));
        });
        $('.table-check .form-check-input').change(function () {
            if ($('.table-check .form-check-input:checked').length === $('.table-check .form-check-input').length) {
                $('#checkAll').prop('checked', true);
            } else {
                $('#checkAll').prop('checked', false);
            }
        });
    }

    /**
     * Set the notifications received in JSON to screen in HTML.
     * @param response
     */
    function updateNotifications(response) {
        let notificaciones = response.notificaciones;
        let tipos_notificaciones = response.tipos_notificaciones;
        let content = '';

        if (notificaciones.length > 0) {
            notificaciones.forEach(function (item) {
                content += '<div class="notification-item text-reset">\n' +
                    '        <div class="d-flex py-0 px-2">\n' +
                    '            <a href="index.php?page=admin_notificacion&action=detalle&id=' + item.id + '" class="d-flex flex-grow-1 p-0">\n' +
                    '                <div class="d-flex flex-column justify-content-center align-items-center p-0">\n' +
                    '                    <div>\n' +
                    '                        <span class="' + tipos_notificaciones[item.idtipo_notificacion].estilo + '">\n' +
                    '                            <i class="' + tipos_notificaciones[item.idtipo_notificacion].icono + ' fa-2x"></i>\n' +
                    '                        </span>\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '                <div class="d-flex flex-grow-1 p-0 justify-content-between">\n' +
                    '                    <div class="d-flex flex-column">\n' +
                    '                        <h6 class="mb-1 text-truncate">' + item.mensaje + '</h6>\n' +
                    '                        <div class="font-size-12 text-muted">\n' +
                    '                            <p class="mb-1">' + (item.nombempleado ? item.nombempleado : '') + '</p>\n' +
                    '                            <p class="mb-0">\n' +
                    '                                <i class="mdi mdi-clock-outline"></i>\n' +
                    '                                <span class="moment-from-now">' + item.tiempo_transcurrido + '</span>\n' +
                    '                            </p>\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '            </a>\n';
                if (item.enlace == '') {
                    content += '                <div class="d-flex flex-column justify-content-center align-items-center p-0">\n' +
                        '                    <span>\n' +
                        '                        <a href="index.php?page=admin_notificacion&action=enlace&id=' + item.id + '">\n' +
                        '                            <i class="fa-xl fa-fw fa-regular fa-circle-right text-primary"></i>\n' +
                        '                        </a>\n' +
                        '                    </span>\n' +
                        '                </div>\n';
                }
                content += '        </div>\n' +
                    '    </div>';
            });
        } else {
            content = '    <div class="notification-item text-reset">\n' +
                '        <div class="d-flex py-0 px-2">\n' +
                '            <a href="#" class="d-flex flex-grow-1 p-0">\n' +
                '                <div class="d-flex flex-column justify-content-center align-items-center p-0">\n' +
                '                    <div>\n' +
                '                        <span class="text-success">\n' +
                '                            <i class="fa-solid fa-broom fa-fw fa-2x"></i>\n' +
                '                        </span>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="d-flex flex-grow-1 p-0 justify-content-between">\n' +
                '                    <div class="d-flex flex-column">\n' +
                '                        <h6 class="mb-1">\n' +
                '                            Ninguna notificación pendiente.\n' +
                '                        </h6>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </a>\n' +
                '        </div>\n' +
                '    </div>';
        }
        $('#div_notificaciones').html(content);
    }

    /**
     * Initialize notification system
     */
    function initNotifications() {
        let userProfile = $('#user_profile_link');
        if (userProfile[0] === undefined) {
            return;
        }

        if (working_notifications) {
            $.ajax({
                url: 'index.php?page=admin_notificacion&codagente=' + userProfile.data('codagente') + '&action=arrayNotificaciones',
                method: 'GET',
                dataType: 'json'
            }).done(function (response) {
                updateNotifications(response);
            }).fail(function (e, exp) {
                disable_notifications();
                bootbox.alert({
                    size: 'medium',
                    title: '<b>Atención:</b>',
                    message: 'No se ha podido acceder a las notificaciones del usuario.'
                });
            });

            $.ajax({
                url: 'index.php?page=admin_notificacion&codagente=' + userProfile.data('codagente') + '&action=totalNotificaciones',
                method: 'GET',
                dataType: 'json'
            }).done(function (response) {
                $('#user_total_notifications').html(response.total);
                if (response.total > 0) {
                    $('#user_notifications > svg').addClass('fa-shake');
                    $('#user_notifications > svg').addClass('fa-regular');
                    $('#user_total_notifications').removeClass('visually-hidden');
                } else {
                    $('#user_notifications > svg').removeClass('fa-shake');
                    $('#user_total_notifications').addClass('visually-hidden');
                }
            }).fail(function () {
                disable_notifications();
                bootbox.alert({
                    size: 'medium',
                    title: "<b>Atención:</b>",
                    message: 'No se ha podido acceder al total de notificaciones del usuario.'
                });
            });
        }
    }

    /**
     * Se desactivan las notificaciones y se notifica al usuario.
     */
    function disable_notifications() {
        working_notifications = false;
        bootbox.alert({
            size: 'medium',
            title: '<b>Atención:</b>',
            message: 'Se han desactivado temporalmente las notificaciones del usuario.'
        });
    }
    /**
     * Se encarga de escuchar los eventos click relacionados a las búsquedas
     * Para posteriormente identificar el formulario al cual debe realizar el submit
     */
    function fix_submit_filter() {
        $('body').on('click','#collapseSearchFilters [type="submit"]', function (e) { 
            let $this = $(this);
            /* Si el botón esta dentro de un formulario o posee el atributo form detenemos la ejecución del script */
            if ($this.closest('form').length>0 || $this.attr('form')) {
                return;
            }
            /* Buscamos si existe un formulario dentro del #collapseSearchFilters para realizar el submit */
            if ($this.closest('#collapseSearchFilters').find('form')) {
                $this.closest('#collapseSearchFilters').find('form').submit();
            }
        });
    }

    /**
     * Initialize all common components of the page
     */
    function init() {
        initMetisMenu();
        initLeftMenuCollapse();
        initActiveMenu();
        initMenuItemScroll();
        initHorizontalMenuActive();
        initFullScreen();
        initRightSidebar();
        initDropdownMenu();
        initComponents();
        initSettings();
        initLanguage();
        initPreloader();
        Waves.init();
        initCheckAll();
        initNotifications();
        fix_submit_filter();
    }

    $(document).ready(function () {
        init();
    });
})(jQuery);
