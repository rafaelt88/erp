@extends('layouts/main')

@section('main-content')
    @if ($fsc->rol)
        <form action="{!! $fsc->rol->url() !!}" method="post" class="form" id="f_rol_pages">
            @include('master/template_header')

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card border">
                            @include('block/card_page_header')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-2">
                                            <input type="text" name="descripcion" value="{!! $fsc->rol->descripcion !!}" class="form-control" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <a id="tab_admin_rol_autorizar" href="#autorizar" aria-controls="usuarios" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center active">
                                                    <i class="fa-solid fa-check-square fa-fw"></i>
                                                    <span>Autorizar</span>
                                                </a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a id="tab_admin_rol_usuarios" href="#usuarios" aria-controls="usuarios" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center">
                                                    <i class="fa-solid fa-users fa-fw"></i>
                                                    <span>Usuarios</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content nav-content">
                                            <div class="tab-pane fade show active" id="autorizar" role="tabpanel" aria-labelledby="autorizar-tab">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="table-responsive-datatable">
                                                            <table id="table_admin_rol_autorizaciones" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                                <thead class="table-dark">
                                                                <tr>
                                                                    <th class="text-start">Página</th>
                                                                    <th class="text-start">Menú</th>
                                                                    <th class="text-center">Ver / Modificar</th>
                                                                    <th class="text-center">Permiso de eliminación</th>
                                                                </tr>
                                                                <tr class="table-warning">
                                                                    <td class="text-start text-nowrap text-truncate"></td>
                                                                    <td class="text-start text-nowrap text-truncate"></td>
                                                                    <td class="text-center td-switch">
                                                                        <div class="h-100 d-flex justify-content-center align-items-center" title="Marcar/desmarcar todos">
                                                                            <div class="form-check form-switch">
                                                                                <input class="form-check-input" type="checkbox" role="switch" id="marcar_todo_ver" name="p_ver_modificar" value=""/>
                                                                                <label class="form-check-label" for="marcar_todo_ver"> </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-center td-switch">
                                                                        <div class="h-100 d-flex justify-content-center align-items-center" title="Marcar/desmarcar todos">
                                                                            <div class="form-check form-switch">
                                                                                <input class="form-check-input" type="checkbox" role="switch" id="marcar_todo_eliminar" name="p_eliminar" value=""/>
                                                                                <label class="form-check-label" for="marcar_todo_eliminar"> </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($fsc->all_pages() as $key1 => $value1)
                                                                    <tr>
                                                                        <td class="text-start text-nowrap text-truncate">{!! $value1->name !!}</td>
                                                                        <td class="text-start text-nowrap text-truncate">
                                                                            @if ($value1->important)
                                                                                <i class="fa-solid fa-star fa-fw"></i>» {!! $value1->title !!}
                                                                            @elseif ($value1->show_on_menu)
                                                                                <span class="text-capitalize">{!! $value1->folder !!}</span>
                                                                                » {!! $value1->title !!}
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-center td-switch">
                                                                            <div class="h-100 d-flex justify-content-center align-items-center">
                                                                                <div class="form-check form-switch">
                                                                                    <input class="form-check-input" type="checkbox" role="switch" id="enabled_{!! $key1 !!}" name="enabled[]" value="{!! $value1->name !!}" onclick="check_allow_delete('{!! $key1 !!}')" @if ($value1->enabled) checked="" @endif />
                                                                                    <label class="form-check-label">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center td-switch">
                                                                            <div class="h-100 d-flex justify-content-center align-items-center">
                                                                                <div class="form-check form-switch">
                                                                                    <input class="form-check-input" type="checkbox" role="switch" id="allow_delete_{!! $key1 !!}" name="allow_delete[]" value="{!! $value1->name !!}" @if ($value1->allow_delete) checked="" @endif title="El usuario tiene permisos para eliminar en esta página"/>
                                                                                    <label class="form-check-label">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="usuarios" role="tabpanel" aria-labelledby="usuarios-tab">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="table-responsive-datatable">
                                                            <table id="table_admin_rol_usuarios" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                                <thead class="table-dark">
                                                                <tr>
                                                                    <th style="max-width: 40px;"></th>
                                                                    <th class="text-start">Nick</th>
                                                                    <th class="text-start">Email</th>
                                                                    <th class="text-end">Último login</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($fsc->all_users() as $key1 => $value1)
                                                                    <tr @if ($value1->included) class="table-success" @endif >
                                                                        <td class="text-center text-nowrap text-truncate">
                                                                            @if ($value1->admin)
                                                                                <i class="fa-solid fa-lock fa-fw"></i>
                                                                            @else
                                                                                <div class="form-check form-switch">
                                                                                    <input class="form-check-input" type="checkbox" role="switch" id="enabled_{!! $key1 !!}" name="iuser[{!! $key1 !!}]" value="{!! $value1->nick !!}" @if ($value1->included) checked="" @endif />
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-start text-nowrap text-truncate">
                                                                            <a href="{!! $value1->url() !!}" class="link-primary">{!! $value1->nick !!}</a>
                                                                            @if ($value1->admin)
                                                                                <span class="badge bg-warning">administrador</span>
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-start text-nowrap text-truncate">{!! $value1->email !!}</td>
                                                                        <td class="text-end text-nowrap text-truncate">{!! $value1->show_last_login() !!}</td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @else
        @include('parts/errors/coming-soon')
    @endif
@endsection
