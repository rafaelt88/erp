@if ($fsc->hide_title == true)
    @include('parts/header_sin_menu')
@else
    @switch(THEME)
        @case('skote')
            @include('parts/header-menu-left-skote')
            @break

        @default
            @if ($fsc->use_sidebar_menu == true)
                @include('parts/header-menu-left')
            @else
                @include('parts/header')
            @endif
    @endswitch
@endif
