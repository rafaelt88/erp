@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="card border">
            @include('block/card_page_header')
            <div class="card-body">
                <form action="{!! $fsc->url() !!}" method="post" class="form">
                    <input type="hidden" name="guardar" value="TRUE"/>
                    <div class="row">
                        <div class="col-12">
                            <h3 class="fw-light">
                                <i class="fa-solid fa-folder-open fa-fw"></i>
                                <span class="text-capitalize">Menú principal</span>
                            </h3>
                            @php
                                $submenus = get_arrays_from($fsc->folders_array());
                            @endphp
                            @if($submenus)
                                <div class="row">
                                    @foreach ($submenus as $newmenu => $newdata)
                                        <div class="col-sm-12 col-md-6 col-xl-4 mt-2 accordion">
                                            @include('block/extensions/blockmenu', ['name'=>$newmenu, 'data'=>$newdata])
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-2 d-flex justify-content-end">
                            <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" title="Guardar">
                                <i class="fa-solid fa-save fa-fw"></i>
                                <span>Guardar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
