#!/bin/bash
# Generate a list of commits in the project

mkdir -p cache

COMMITS_FILE="$(pwd)/cache/commits.csv"

echo 'repo,commit id,author,date,comment,changed files,lines added,lines deleted' > ${COMMITS_FILE}
git log --submodule=log --date=local --all --pretty='@mifactura,%h,%an,%cI,"%s",' --shortstat | tr '\n' ' ' | tr '@' '\n' >> ${COMMITS_FILE}
git submodule foreach "REPO=$(echo $name); git log --submodule=log --date=local --all --pretty='@$REPO,%h,%an,%cI,\"%s\"' --shortstat | tr '\n' ' ' | tr '@' '\n' >> ${COMMITS_FILE}"
sed -i 's/ files changed//g' ${COMMITS_FILE}
sed -i 's/ file changed//g' ${COMMITS_FILE}
sed -i 's/ insertions(+)//g' ${COMMITS_FILE}
sed -i 's/ insertion(+)//g' ${COMMITS_FILE}
sed -i 's/ deletions(-)//g' ${COMMITS_FILE}
sed -i 's/ deletion(-)//g' ${COMMITS_FILE}