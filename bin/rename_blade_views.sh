#!/bin/bash

# Renombra todas las vistas html a blade.php dentro de la carpeta Templates para poder migrar de RainTPL a Blade

PWD="$(pwd)"

files=$(find . -path "./*Templates/*.html" -type f -exec ls -lspah {} \; | awk '{print $10}')

for file in ${files}; do
  new=$(echo "$file" | sed "s/\.html/\.blade\.php/g")
  mv "${file}" "${new}"
done

cd "${PWD}"