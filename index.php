<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
if (array_key_exists('__debug', $_REQUEST)) {
    define('FS_DEBUG', true);
}

$autoload_file = __DIR__ . '/vendor/autoload.php';
if (!file_exists($autoload_file)) {
    die('<h1>COMPOSER ERROR</h1><p>You need to run: composer install</p>');
}
require_once $autoload_file;

$min_version = 7.2;
$max_version = 8.1;
if ((float) substr(phpversion(), 0, 3) < $min_version && (float) substr(phpversion(), 0, 3) > $max_version) {
    /// comprobamos la versión de PHP
    die('MiFactura.eu necesita PHP ' . $min_version . ' hasta ' . $max_version . ', y se ha detectado la versión de PHP ' . phpversion());
}

if (!file_exists('config.php')) {
    /// si no hay config.php redirigimos al instalador
    header('Location: install.php');
    die('Redireccionando al instalador...');
}

$extensions = ['bcmath', 'curl', 'dom', 'gd', 'json', 'mbstring', 'simplexml', 'tokenizer', 'zip'];
$errors = [];
foreach ($extensions as $extension) {
    if (!extension_loaded($extension)) {
        $errors[] = $extension;
    }
}
if (!empty($errors)) {
    die('Necesitas instalar las extensiones <b>' . implode($errors, ', ') . '</b> para PHP ' . phpversion());
}

/**
 * Contiene la ruta de la instalación que está siendo ejecutada
 *
 * TODO: Las constantes a mantener son:
 *      - FOLDER_DATA que contiene la ruta de la instalación que está siendo utilizada
 *      - BASE_PATH que contiene la ruta de la instalación principal.
 *
 * Si no se está ejecutando en multi-instalación, ambas son iguales.
 *
 * Eliminar el resto de constantes... FS_FOLDER, XFS_FOLDER, FS_FOLDER_DATA, XFS_FOLDER_DATA, PATH...
 */
if (!defined('BASE_PATH')) {    // Se define en src/Xnet/Functions cargada en autoload
    define('BASE_PATH', __DIR__);
}
if (constant('BASE_PATH') !== __DIR__) {
    dump('Directorio actual: ' . __DIR__);
    dump('BASE_PATH: ' . constant('BASE_PATH'));
    die('Hay un problema con la definición de BASE_PATH en src/Xnet/Functions');
}
define('FS_FOLDER', constant('BASE_PATH'));
define('XFS_FOLDER', constant('BASE_PATH'));
if (!defined('FOLDER_DATA')) {
    define('FOLDER_DATA', __DIR__);
    define('FS_FOLDER_DATA', __DIR__);
    define('XFS_FOLDER_DATA', __DIR__);
}
// Esto es necesario para ejecutar el cron.
chdir(constant('BASE_PATH'));

/**
 * Ruta al archivo para gestionar datos en periodo de mantenimiento
 *
 * TODO: Este nombre de archivo tendrá que ser el mismo que se haya utilizado en el núcleo.
 */
define('MAINTENANCE_FILE', constant('BASE_PATH') . DIRECTORY_SEPARATOR . 'in_maintenance');
define('UPDATING_DATABASE_FILE', constant('BASE_PATH') . DIRECTORY_SEPARATOR . 'in_db_maintenance');
define('DATABASE_UPDATE_FLAG', constant('BASE_PATH') . DIRECTORY_SEPARATOR . 'database_update_flag');

/**
 * Recordad que desde /base/xfs_constants.php se puede cambiar el flujo de ejecución.
 * Nótese que el procedimiento habitual de ejecución es mediante la función xfs_functions::execute_tasks()
 * que se invoca desde xfs_constants.php.
 */
//require_once constant('BASE_PATH') . '/Alxarafe/Base/constants.php';
//require_once constant('BASE_PATH') . '/base/xfs_constants.php';
//require_once constant('BASE_PATH') . '/base/fs_functions.php';

/**
 * Registramos la función para capturar los "fatal error".
 * Información importante a la hora de depurar errores.
 */
register_shutdown_function("fatal_handler");

// Si se ejecuta desde la consola, pasar los parámetros que se le han pasado a $_GET
define('CONSOLE_MODE', (bool) fs_filter_input_get('console', false) || PHP_SAPI === 'cli');
if (PHP_SAPI === 'cli') {
    parse_str(implode('&', array_slice($argv, 1)), $_GET);
    define('EOL', "\n");
} else {
    define('EOL', constant('CONSOLE_MODE') ? "\n" : '<br>');
}

if (fs_filter_input_get('task') == 'cron') {
    require_once constant('BASE_PATH') . '/base/fs_cron.php';
    die(EOL . 'Fin de ejecución de CRON' . EOL);
}

// Carga los parámetros de configuración (incluyendo el config.php)
require_once(constant('BASE_PATH') . '/base/fs_config.php');

require_once constant('BASE_PATH') . '/base/fs_index.php';
