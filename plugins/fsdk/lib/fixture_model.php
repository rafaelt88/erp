<?php
use Illuminate\Support\Facades\Lang;

require 'fixture_methods.php';

class fixture_model
{

    use fixture_methods;

    /**
     *
     * @param string $resource
     * @param int $size
     * @return array|void
     */
    public function fixtures(string $resource, int $size = 10)
    {
        if (class_exists($resource) && is_subclass_of($resource, fs_model::class) && method_exists($resource, 'test')) {
            $ref = new \ReflectionClass($resource = new $resource());
            $filename = dirname($ref->getFileName()) . '/table/' . $resource->table_name() . '.xml';
            if (file_exists($filename)) {
                $schema = simplexml_load_file($filename);
                for ($i = 0; $i < abs($size); $i ++) {
                    $record = $this->test_data($resource, $schema);
                    $ok = $record->test();
                    $results[$ok ? 'ok' : 'error'][] = $ok ? $record : [
                        $record,
                        $record->get_errors()
                    ];
                }
            }
            return $results ?? [];
        }
    }

    /**
     *
     * @param fs_model $resource
     * @param \SimpleXMLElement $schema
     * @return fs_model
     */
    private function test_data(fs_model $resource, \SimpleXMLElement $schema)
    {
        foreach ($schema->restriccion as $column) {
            $this->reference($column->consulta, $resource);
        }
        foreach ($schema->columna as $column) {
            if ($this->nullable()) {
                continue;
            }
            $params = [];
            $name = strval($column->nombre);
            if (property_exists($resource, $name) || ! is_null($resource->$name)) {
                $rule = null;
                switch (get_class($resource)) {
                    case 'pais':
                        switch ($name) {
                            case 'nombre':
                                $params = [
                                    'country',
                                    $this->locale()
                                ];
                                $resource->codiso = preg_replace('#.*\b([A-Z])$#u', '$1', $params[1]);
                                break;
                        }
                        break;
                    case 'notificacion_tipo':
                        switch ($name) {
                            case 'estilo':
                                $params[0] = 'css_style';
                                break;
                            case 'icono':
                                $params[0] = 'css_icon';
                                break;
                        }
                        break;
                    case 'almacen':
                    case 'contacto':
                    case 'ejercicio':
                        switch ($name) {
                            case 'created_at':
                            case 'updated_at':
                            case 'deleted_at':
                                $params[0] = 'datetime';
                                break;
                            case 'fecha':
                            case 'fechainicio':
                            case 'fechafin':
                                $params[0] = 'date';
                                break;
                            case 'fax':
                                $params[0] = 'telefono';
                                break;
                            case 'poblacion':
                                $params[0] = 'ciudad';
                                break;
                            case 'codpostal':
                                $params[0] = 'zip_code';
                                break;
                            case 'codpais':
                                $rule = 'FOREIGN KEY (codpais) REFERENCES paises (codpais)';
                        }
                        break;
                }
                if (is_string($rule)) {
                    $value = $this->reference($rule);
                    if ($value !== $rule) {
                        $resource->$name = $value;
                        continue;
                    }
                }
                $resource->$name = $this->fill_field($name, $column->tipo, $params);
            }
        }
        dd($resource, $schema);
        return $resource;
    }

    private function fill_field(string $name, string $tipo, array $params = [])
    {
        if (! isset($params[0])) {
            if (method_exists($this, $name)) {
                $params[0] = $name;
            }
        }
        preg_replace_callback_array([
            '#varying\((\d+)\)$#' => function ($match) use (&$params) {
                if (! isset($params[0])) {
                    $params[0] = 'random_string';
                }
                array_push($params, $match[1]);
            },
            '#^(boolean|integer|double)\b#' => function ($match) use (&$params) {
                $params = [
                    $match[0]
                ];
            }
        ], $tipo, 1);
        return call_user_func_array([
            $this,
            array_shift($params) ?: 'random_string'
        ], $params);
    }

    /**
     *
     * @var array
     */
    private $_references = [];

    /**
     *
     * @param string $rule
     * @param fs_model $resource
     * @return mixed
     */
    private function reference(string $rule, fs_model &$resource)
    {
        $db = $this->db;
        $references = &$this->_references;
        preg_replace_callback('#^FOREIGN KEY\s+\(([^)]+)\)\s+REFERENCES\s+\b(\w+)\b\s+\(([^)]+)\).*#', function ($match) use ($db, &$references, &$resource) {
            if (! array_key_exists($match[2], $references)) {
                $rows = $db->select(strtr('SELECT t.:column FROM :table t ORDER BY RAND() LIMIT 20', [
                    ':table' => $match[2],
                    ':column' => $match[3]
                ]));
                foreach ($rows as &$value) {
                    $value = array_shift($value);
                }
                $references[$match[2]] = $rows;
            }
            $rows = $references[$match[2]] ?: [];
            $resource->$match[1] = $rows[rand(0, count($rows) - 1)];
        }, $rule);
    }
}