<?php

trait fixture_methods
{

    /**
     *
     * @return bool
     */
    protected function boolean(): bool
    {
        return boolval(rand(0, 1));
    }

    /**
     *
     * @return int
     */
    protected function integer(): int
    {
        return intval($this->cantidad(0, floor(rand() * pow(4, 4)), floor(rand() * pow(8, 8))));
    }

    /**
     *
     * @param int $size
     */
    protected function double(int $decimals = 3): float
    {
        return floatval(preg_replace("#(\d{{$decimals}})$#", '.$1', strval($this->integer())));
    }

    /**
     *
     * @return string
     */
    protected function date(): string
    {
        return mt_rand(1, 28) . '-' . mt_rand(1, 12) . '-' . mt_rand(2020, date('Y'));
    }

    /**
     *
     * @return string
     */
    protected function time(): string
    {
        return mt_rand(0, 23) . ':' . mt_rand(0, 59) . ':' . mt_rand(0, 59);
    }

    /**
     *
     * @param string $format
     * @return string
     */
    protected function datetime(string $format = 'Y-m-d H:i:s'): string

    {
        return date($format, $this->timestamp());
    }

    /**
     *
     * @return int
     */
    protected function timestamp(): int
    {
        return mt_rand(1, time());
    }

    /**
     *
     * @return string
     */
    protected function telefono(): string
    {
        return mt_rand(555555555, 999999999);
    }

    /**
     *
     * @param int $size
     * @return string
     */
    protected function zip_code(): string
    {
        return mt_rand(11111, 99999);
    }

    /**
     *
     * @return string
     */
    protected function contacto(): string
    {
        return $this->nombre() . ' ' . $this->apellidos();
    }

    /**
     *
     * @return string
     */
    protected function locale(): string
    {
        $language = [
            'en',
            'es',
            'pt'
        ];
        $country = [
            'US',
            'GB',
            'ES',
            'AR',
            'VE',
            'BR',
            'CO'
        ];
        return $language[rand(0, count($language) - 1)] . '-' . $country[rand(0, count($language) - 1)];
    }

    /**
     *
     * @param string $locale
     * @return string
     */
    protected function country(string $locale = null): string
    {
        return locale_get_display_region($locale ?: $this->locale());
    }

    /**
     *
     * @param int $max
     * @param int $mod
     * @return bool
     */
    protected function nullable(int $max = 3, int $mod = 2): bool
    {
        return boolval(rand(0, $max) % rand(1, $mod));
    }

    /**
     *
     * @return string
     */
    protected function css_style(): string
    {
        $data = [
            'fa-warning',
            'fa-info',
            'fa-success',
            'fa-danger'
        ];
        return $data[rand(0, count($data) - 1)];
    }

    /**
     *
     * @return string
     */
    protected function css_icon(): string
    {
        $data = [
            'fa-blog',
            'fa-user-ninja',
            'fa-user',
            'fa-shop',
            'fa-save',
            'fa-maximize',
            'fa-crop',
            'fa-exclamation',
            'fa-check'
        ];
        return $data[rand(0, count($data) - 1)];
    }

    /**
     *
     * @param int $max
     * @param int $mod
     * @return bool
     */
    protected function cargo()
    {
        $data = [
            'Supervisor',
            'Operador',
            'Analista',
            'Gerente',
            'Vigilante',
            'Cocinero'
        ];
        return $data[rand(0, count($data) - 1)];
    }

    /**
     *
     * @return string
     */
    protected function genrecibos()
    {
        $data = [
            'Emitido',
            'Anulado',
            'Pagado'
        ];
        return $data[rand(0, count($data) - 1)];
    }
    
    
    /**
     *
     * @return string
     */
    protected function estado()
    {
        $data = [
            'ABIERTO',
            'EN CURSO',
            'FINALIZADO'
        ];
        return $data[rand(0, count($data) - 1)];
    }

    /**
     *
     * @return string
     */
    protected function vencimiento(int $max = 2): string
    {
        $data = [
            'day',
            'week',
            'month'
        ];
        return rand($max * - 1, $max) . $data[rand(0, count($data) - 1)];
    }
}