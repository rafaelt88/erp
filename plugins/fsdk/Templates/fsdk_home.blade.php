@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        <i class="fa-solid fa-rocket fa-fw"></i>
                        {!! $fsc->page->title !!}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="#" class="btn d-flex justify-content-center align-items-center btn-warning" data-bs-toggle="modal" data-bs-target="#modal_datos_prueba">
                                    <i class="fa-solid fa-magic fa-fw"></i>
                                    <span>Generar datos de prueba</span>
                                </a>
                            </div>
                            <div class="col-sm-9">
                                <p class="form-text">
                                    Genera datos de prueba aleatorios para probar tus modificaciones.
                                    Puedes generar infinidad de artículos, clientes, proveedores y
                                    {!! FS_ALBARANES !!} de tamaños aleatorios.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="#" class="btn d-flex justify-content-center align-items-center btn-secondary" data-bs-toggle="modal" data-bs-target="#modal_probar_modelos">
                                    <i class="fa-solid fa-circle-exclamation fa-fw"></i>
                                    <span>Probar modelos</span>
                                </a>
                            </div>
                            <div class="col-sm-9">
                                <p class="form-text">
                                    Permite realizar pruebas de integridad.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <br>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="#" class="btn d-flex justify-content-center align-items-center btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#modal_tablas">
                                    <i class="fa-solid fa-database fa-fw"></i>
                                    <span>Tablas</span>
                                </a>
                            </div>
                            <div class="col-sm-9">
                                <p class="form-text">
                                    Selecciona una tabla de la base de datos para generar el <b>xml</b>
                                    y/o el <b>modelo</b> necesarios para crearla e interactuar con ella.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="#" class="btn d-flex justify-content-center align-items-center btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#modal_nuevo_plugin">
                                    <i class="fa-solid fa-puzzle-piece fa-fw"></i>
                                    <span>Generar nuevo plugin</span>
                                </a>
                            </div>
                            <div class="col-sm-9">
                                <p class="form-text">
                                    Crea un plugin básico con "hola mundo" a partir de unos datos básicos.
                                </p>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="page-header">
                                    <h2 class="fw-light">
                                        Otros
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="index.php?page=fsdk_plan_contable" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                    <i class="fa-solid fa-balance-scale fa-fw"></i>
                                    <span>CSV =&gt; Plan contable</span>
                                </a>
                            </div>
                            <div class="col-sm-9">
                                <p class="form-text">
                                    Permite generar un plan contable a partir de un CSV o <b>excel</b> con el listado
                                    de cuentas y sus descripciones.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_datos_prueba" role="dialog">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fa-solid fa-magic fa-fw"></i>
                        Generar datos de prueba
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <p class="form-text">
                        Selecciona qué datos quieres generar.
                    </p>
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! $fsc->url() !!}&gdp=notificaciones" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-bell fa-fw"></i>
                                <span>Notificaciones</span>
                            </a>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! $fsc->url() !!}&gdp=fabricantes" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-industry fa-fw"></i>
                                <span>Fabricantes</span>
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=familias" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-folder fa-fw"></i>
                                <span>Familias</span>
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=articulos" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-cubes fa-fw"></i>
                                <span>Artículos</span>
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=agentes" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-users fa-fw"></i>
                                <span>Empleados</span>
                            </a>
                            <br>
                        </div>
                        <div class="col-sm-4">
                            <a href="{!! $fsc->url() !!}&gdp=proveedores" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-users fa-fw"></i>
                                <span>Proveedores</span>
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=albaranesprov" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-copy fa-fw"></i>{!! ucfirst(constant('FS_ALBARANES')) !!} de compra
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=pedidosprov" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-copy fa-fw"></i>{!! ucfirst(constant('FS_PEDIDOS')) !!} de compra
                            </a>
                            <br>
                        </div>
                        <div class="col-sm-4">
                            <a href="{!! $fsc->url() !!}&gdp=gruposcli" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-folder fa-fw"></i>
                                <span>Grupos de clientes</span>
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=clientes" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-users fa-fw"></i>
                                <span>Clientes</span>
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=albaranescli" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-copy fa-fw"></i>{!! ucfirst(constant('FS_ALBARANES')) !!} de venta
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=pedidoscli" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-copy fa-fw"></i>{!! ucfirst(constant('FS_PEDIDOS')) !!} de venta
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=presupuestoscli" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-copy fa-fw"></i>{!! ucfirst(constant('FS_PRESUPUESTOS')) !!} de venta
                            </a>
                            <br>
                            <a href="{!! $fsc->url() !!}&gdp=servicioscli" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                <i class="fa-solid fa-copy fa-fw"></i>{!! ucfirst(constant('FS_SERVICIOS')) !!} a clientes
                            </a>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        @include('block/extensions/generates', ['page_params' => ''])
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-warning">
                                <i class="fa-solid fa-warning fa-fw"></i>
                                La generación de datos de prueba sirve para demos y para probar los
                                plugins con datos masivos. No continues si no sabes lo que haces.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_probar_modelos" role="dialog">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fa-solid fa-squere-check fa-fw"></i>
                        Probar modelos
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="alert alert-info">
                            Los datos utilizados al momento de hacer las pruebas <b class="text-uppercase">no serán almacenados</b>.
                        </div>
                    </div>

                    <script>
                    async function loadFixtureResource(resource) {
                    	 await fetch('index.php?page=fsdk_home&__debug&resource=' + resource)
						  .then(response => response.text())
						  .then(text => {
							  try {
							      const data = JSON.parse(text);
							      console.log(data);
							  } catch(err) {
								  console.error(text);
							  }
						  });
					}
                    </script>

                    <main id="fixture-container">
                    	<div class="resources row">
                        	@php
                    		$resources = explode(",", "almacen,contacto,cuenta_banco,divisa,ejercicio,forma_pago,impuesto,notificacion_tipo,pais,serie");
                        	@endphp
                            @foreach ($resources as $res)
                            <div class="col-sm-6 col-md-4 col-lg-3 pb-2">
                                <button class="btn btn-outline-secondary w-100" onclick="loadFixtureResource('{!! $res !!}')">
                                    {!! $res !!}
                                </button>
                            </div>
                            @endforeach
                        </div>
                    	<div class="results">
                    		
                    	</div>
                    </main>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_tablas" role="dialog">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fa-solid fa-database fa-fw"></i>
                        Tablas
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <p class="form-text">
                        Selecciona una tabla para ver las opciones de generación.
                    </p>
                    <div class="row">
                        @foreach ($fsc->tablas as $key1 => $value1)
                            <div class="col-sm-4">
                                <a href="index.php?page=fsdk_tabla&table={!! $value1['name'] !!}" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                                    <i class="fa-solid fa-folder fa-fw"></i>{!! $value1['name'] !!}
                                </a>
                                <br>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form class="form" role="form" name="f_nuevo_plugin" action="{!! $fsc->url() !!}" method="post">
        <input type="hidden" name="generarplugin" value="1">
        <div class="modal" id="modal_nuevo_plugin" role="dialog">
            <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            <i class="fa-solid fa-puzzle-piece fa-fw"></i>
                            Nuevo plugin
                        </h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                    </div>
                    <div class="modal-body">
                        <p class="form-text">
                            Genera un nuevo plugin para que puedas empezar a modificarlo.
                        </p>
                        <div class="mb-2">
                            <label>
                                Nombre
                            </label>
                            <input type="text" name="nombre" value="holamundo" class="form-control" autofocus="" autocomplete="off"/>
                        </div>
                        <div class="mb-2">
                            <label>
                                Descripción
                            </label>
                            <textarea name="descripcion" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit">
                            <i class="fa-solid fa-puzzle-piece fa-fw"></i>
                            <span>Generar</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
