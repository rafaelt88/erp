@if ($fsc->tabla)
    <script src="https://cdn.rawgit.com/zenorocha/clipboard.js/v1.5.12/dist/clipboard.min.js?idcache={!! $fsc->id_cache !!}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            new Clipboard('.btn');
        });
    </script>
@endif
