<?php

/*
 * This file is part of fsdk for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once __DIR__ . '/../lib/generar_datos_prueba.php';

if (!defined('FS_SERVICIOS')) {
    /**
     * Nombre traducible para "Servicios"
     */
    define('FS_SERVICIOS', 'servicios');
}

/**
 * Description of fsdk_home
 */
class fsdk_home extends fs_controller
{
    /**
     * TODO: Missing documentation
     *
     * @var false|array
     */
    public $tablas;

    /**
     * TODO: Missing documentation
     *
     * @var string
     */
    public $url_recarga;

    /**
     * fsdk_home constructor.
     */
    public function __construct()
    {
        parent::__construct(__CLASS__, 'FSDK', ['admin', 'dashboard', 'config']);
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        $this->url_recarga = false;

        if (isset($_GET['gdp'])) {
            $gdp = new generar_datos_prueba($this->db, $this->empresa);
            switch ($_GET['gdp']) {
                case 'notificaciones':
                    if (class_exists('notificacion')) {
                        $num = $gdp->notificaciones();
                        $this->new_message('Generados ' . $num . ' notificaciones.', true);
                    } else {
                        $this->new_error_msg('Error no controlado, las notificaciones están integradas en el núcleo.');
                    }
                    break;

                case 'fabricantes':
                    if (class_exists('fabricante')) {
                        $num = $gdp->fabricantes();
                        $this->new_message('Generados ' . $num . ' fabricantes.', true);
                    } else {
                        $this->new_error_msg('Instala el plugin facturacion_base.');
                    }
                    break;

                case 'familias':
                    if (class_exists('familia')) {
                        $num = $gdp->familias();
                        $this->new_message('Generadas ' . $num . ' familias.', true);
                    } else {
                        $this->new_error_msg('Instala el plugin facturacion_base.');
                    }
                    break;

                case 'articulos':
                    if (class_exists('articulo')) {
                        $num = $gdp->articulos();
                        $this->new_message('Generados ' . $num . ' artículos.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=articulos';
                    } else {
                        $this->new_error_msg('Instala el plugin facturacion_base.');
                    }
                    break;

                case 'gruposcli':
                    if (class_exists('cliente')) {
                        $num = $gdp->grupos_clientes();
                        $this->new_message('Generados ' . $num . ' grupos de clientes.', true);
                    }
                    break;

                case 'clientes':
                    if (class_exists('cliente')) {
                        $num = $gdp->clientes();
                        $this->new_message('Generados ' . $num . ' clientes.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=clientes';
                    } else {
                        $this->new_error_msg('Instala el plugin facturacion_base.');
                    }
                    break;

                case 'agentes':
                    $num = $gdp->agentes();
                    $this->new_message('Generados ' . $num . ' empleados.', true);
                    break;

                case 'proveedores':
                    if (class_exists('proveedor')) {
                        $num = $gdp->proveedores();
                        $this->new_message('Generados ' . $num . ' proveedores.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=proveedores';
                    } else {
                        $this->new_error_msg('Instala el plugin facturacion_base.');
                    }
                    break;

                case 'presupuestoscli':
                    if (class_exists('presupuesto_cliente')) {
                        $num = $gdp->presupuestoscli();
                        $this->new_message('Generados ' . $num . ' ' . constant('FS_PRESUPUESTOS') . ' de venta.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=presupuestoscli';
                    } else {
                        $this->new_error_msg('Instala el plugin presupuestos_y_pedidos.');
                    }
                    break;

                case 'pedidosprov':
                    if (class_exists('pedido_proveedor')) {
                        $num = $gdp->pedidosprov();
                        $this->new_message('Generados ' . $num . ' ' . constant('FS_PEDIDOS') . ' de compra.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=pedidosprov';
                    } else {
                        $this->new_error_msg('Instala el plugin presupuestos_y_pedidos.');
                    }
                    break;

                case 'pedidoscli':
                    if (class_exists('pedido_cliente')) {
                        $num = $gdp->pedidoscli();
                        $this->new_message('Generados ' . $num . ' ' . constant('FS_PEDIDOS') . ' de venta.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=pedidoscli';
                    } else {
                        $this->new_error_msg('Instala el plugin presupuestos_y_pedidos.');
                    }
                    break;

                case 'albaranesprov':
                    if (class_exists('albaran_proveedor')) {
                        $num = $gdp->albaranesprov();
                        $this->new_message('Generados ' . $num . ' ' . constant('FS_ALBARANES') . ' de compra.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=albaranesprov';
                    } else {
                        $this->new_error_msg('Instala el plugin facturacion_base.');
                    }
                    break;

                case 'albaranescli':
                    if (class_exists('albaran_cliente')) {
                        $num = $gdp->albaranescli();
                        $this->new_message('Generados ' . $num . ' ' . constant('FS_ALBARANES') . ' de venta.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=albaranescli';
                    } else {
                        $this->new_error_msg('Instala el plugin facturacion_base.');
                    }
                    break;

                case 'servicioscli':
                    if (class_exists('servicio_cliente')) {
                        $num = $gdp->servicioscli();
                        $this->new_message('Generados ' . $num . ' ' . constant('FS_SERVICIOS') . ' a clientes.', true);
                        $this->new_message('Recargando... &nbsp; <i class="fa-solid fa-refresh fa-fw fa-spin"></i>');

                        $this->url_recarga = $this->url() . '&gdp=servicioscli';
                    } else {
                        $this->new_error_msg('Instala el plugin servicios.');
                    }
                    break;
            }
        } elseif(array_key_exists('resource', $_GET)) {
            $gdp = new generar_datos_prueba($this->db, $this->empresa);
            if (is_array($result = $gdp->fixtures($_GET['resource'], $_REQUEST['limit'] ?: 50))) {
                dd($result);
                header('Content-Type: applpication/json');
                die(json_encode($result ?? [], JSON_PRETTY_PRINT));
            }
            die('Invalid or inadmissible resource.');
        } elseif (isset($_POST['generarplugin'])) {
            if (!is_writable("plugins")) {
                $this->new_message('No tienes permisos de escritura en la carpeta plugins.');
            } elseif ($this->generar_plugin($_POST['nombre'], $_POST['descripcion'])) {
                $this->new_message("Se generó el plugin " . $_POST['nombre'] . " en el directorio plugins."
                    . " Puedes activarlo desde el <a href='index.php?page=admin_home#plugins'>panel de control</a>.");
            } else {
                $this->new_error_msg("Hubo un problema al generar el plugin " . $_POST['nombre'] . ": Revise los logs de su servidor web.");
            }
        }

        $this->tablas = $this->db->list_tables();
    }

    /**
     * TODO: Missing documentation
     *
     * @param string $nombre
     * @param string $descripcion
     *
     * @return bool
     */
    private function generar_plugin($nombre, $descripcion)
    {
        if (!$this->crea_estructura($nombre)) {
            return false;
        } elseif (!$this->genera_ficheros($nombre, $descripcion)) {
            return false;
        }

        return true;
    }

    /**
     * TODO: Missing documentation
     *
     * @param string $nombre
     *
     * @return bool
     */
    private function crea_estructura($nombre)
    {
        $ok = false;

        // creamos el dir del plugin
        if (mkdir("plugins/" . $nombre)) {
            $ok = true;

            // creamos los directorios
            $dirs = ["controller", "view", "model"];
            foreach ($dirs as $dir) {
                if (!mkdir("plugins/" . $nombre . "/" . $dir)) {
                    $ok = false;
                }
            }
        }

        return $ok;
    }

    /**
     * TODO: Missing documentation
     *
     * @param string $nombre
     * @param string $descripcion
     *
     * @return bool
     */
    private function genera_ficheros($nombre, $descripcion)
    {
        $descripcion .= "\n<br/>Accesible desde Admin &gt; " . $nombre;

        if (!file_put_contents("plugins/" . $nombre . "/description", $descripcion)) {
            return false;
        }

        if (!file_put_contents("plugins/" . $nombre . "/mifactura.ini", "version = 1")) {
            return false;
        }

        $textcontroller = str_replace("holamundo", $nombre, file_get_contents(__DIR__ . '/../tmpls/controller.php'));
        $textvista = file_get_contents(__DIR__ . '/../tmpls/view.html');

        if (!file_put_contents("plugins/" . $nombre . "/controller/" . $nombre . ".php", $textcontroller)) {
            return false;
        } elseif (!file_put_contents("plugins/" . $nombre . "/view/" . $nombre . ".html", $textvista)) {
            return false;
        }

        return true;
    }
}
