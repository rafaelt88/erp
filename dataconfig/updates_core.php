<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use Xnet\Core\XnetUpdateController;
use Xnet\Model\Version;

/**
 * Class updates_core
 *
 * Actualizaciones de bases de datos del núcleo según la versión a la que se actualice.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0615
 *
 */
class updates_core extends XnetUpdateController
{
    /**
     * Instala y activa los plugins soporte_empresas y facturación (vacíos), si no existen.
     * También actualiza la lista de plugins desde la tabla enabled_plugins.list
     * TODO: Tendría que eliminar el archivo enabled_plugins.list y actualizar los plugins en Versions.
     *
     * TODO: Quitar los tres guiones del final y ajustar el nombre del método cuando se quiera proceder a la instalación del plugin.
     * TODO: También podría eliminar los plugins que ya no hacen falta.
     *
     * @return array
     */
    public function update_20220628___(): array
    {
        $result['status'] = true;
        foreach (['soporte_empresas', 'facturacion'] as $plugin) {
            $folder = 'plugins/' . $plugin;
            if (!is_dir($folder) && !mkdir($folder, 0777, true)) {
                $result['status'] = false;
                $result['error'][] = 'No se ha podido instalar el plugin ' . $folder;
                return $result;
            }
            $filename = $folder . '/mifactura.ini';
            if (!file_exists($filename)) {
                if (false === file_put_contents($filename, "name=$plugin\nversion=0")) {
                    $result['status'] = false;
                    $result['error'][] = 'Error al intentar crear el archivo ' . $filename;
                    return $result;
                }
            }
        }

        /// Cargamos la lista de plugins activos
        $GLOBALS['plugins'] = [];
        // TODO: Se puede utilizar Version::getPlugins para obtener la relación de plugins sin usar enabled_plugins.list;
        $filename = constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list';
        if (file_exists($filename)) {
            $list = explode(',', file_get_contents($filename));
            if (!empty($list)) {
                foreach ($list as $f) {
                    if (file_exists('plugins/' . $f)) {
                        $GLOBALS['plugins'][] = $f;
                    }
                }
                $list = array_reverse($list);
                $newList = [];
                $lastItem = '';
                $insertado = false;
                foreach ($list as $item) {
                    if ($item !== $lastItem) {
                        $lastItem = $item;
                        if (!in_array($item, ['xnetcommonhelpers', 'ayuda_soporte']) && !$insertado) {
                            $insertado = true;
                            $newList[] = 'soporte_empresas';
                            $newList[] = 'facturacion';
                        }
                        $newList[] = $item;
                    }
                }
                if (false === file_put_contents($filename, implode(',', array_unique(array_reverse($newList))))) {
                    $result['status'] = false;
                    $result['error'][] = 'Error al intentar guardar el archivo ' . $filename;
                    return $result;
                }
            }
        }

        return $result;
    }

    /**
     * Crea los campos que dan fallo al actualizar a la versión BLADE.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0706
     *
     * @return array
     */
    public function update_20220706(): array
    {
        // Se revisan los campos que podrían faltar en fs_tables
        $data = [
            'fs_pages' => [
                'title' => 'VARCHAR (50) NOT NULL DEFAULT ""',
                'alias' => 'VARCHAR (90) NOT NULL DEFAULT ""',
                'description' => 'VARCHAR (255) NOT NULL DEFAULT ""',
                'show_on_menu' => 'TINYINT(1) NOT NULL DEFAULT 0',
                'favorite' => 'TINYINT(1) NOT NULL DEFAULT 0',
            ],
            'fs_users' => [
                'use_sidebar_menu' => 'TINYINT(1) NOT NULL DEFAULT 0',
                // Se añaden estos a posterior porqué se ha detectado como problema migrando
                'theme_topbar' => 'VARCHAR (20) NOT NULL DEFAULT "light"',
                'theme_sidebar' => 'VARCHAR (20) NOT NULL DEFAULT "dark"',
                'theme_body' => 'VARCHAR (20) NOT NULL DEFAULT "light"',
            ],
        ];

        return $this->addNewTableField($data);
    }

    /**
     * Elimina constraints antiguas, ahora tienen otro nombre o no se usan con constraint.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0711
     *
     * @return array
     */
    public function update_20220711(): array
    {
        // Se revisan los campos que podrían faltar en fs_tables
        $data = [
            'fs_pages' => [
                'ca_fs_users_pages',
            ],
            'fs_roles_access' => [
                'fs_roles_access_page',
            ],
            'fs_extensions2' => [
                'ca_fs_extensions2_fs_pages',
                'ca_fs_extensions2_fs_pages2',
            ],
        ];

        return $this->removeOldConstraint($data);
    }

    /**
     * Crea o actualiza la tabla de versiones (versions), traspasa la secuencia de activación de "enabled_plugins.list"
     * al campo sequence de dicha tabla, y elimina el archivo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0825
     *
     * @return array
     */
    public function update_20220825(): array
    {
        $result['status'] = true;

        // Éste es el único sitio en el que debe de aparecer "enabled_plugins.list" al subir los cambios.
        $filename = constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list';
        if (!file_exists($filename)) {
            $result['advice'][] = 'No se encuentra el archivo `enabled_plugins.list`. Es posible que se haya ejecutado ya esta actualización.';
            return $result; // Retorna true, para marcarlo como actualizado y que no vuelva a ejecutarse
        }

        $list = explode(',', file_get_contents($filename));
        if (empty($list)) {
            $result['status'] = false;
            $result['error'][] = 'El archivo `enabled_plugins.list` está vacío. Revise su contenido y vuelva a actualizar.';
            return $result;
        }

        if (!Version::checkPlugins(array_reverse($list))) {
            $result['status'] = false;
            $result['error'][] = 'Se ha producido un error al tratar de crear el archivo de versiones. Solicite asistencia técnica para actualizar.';
            return $result;
        }

        if (file_exists($filename) && !unlink($filename)) {
            $result['status'] = false;
            $result['error'][] = 'Se ha producido un error al tratar de eliminar el archivo de plugins activos obsoleto. Solicite asistencia técnica para actualizar.';
            return $result;
        }

        return $result;
    }

    /**
     * Correcciones a la tabla notificaciones.
     *
     * @author  Francesc Pineda Segarra
     * @version 2022.0901
     *
     * @return array
     */
    public function update_20220901(): array
    {
        // Se revisan los campos que podrían faltar en fs_tables
        $data = [
            'notificaciones' => [
                'idtipo_notificacion',
                'tipo_notificacion',
            ],
        ];
        return $this->removeOldConstraintAndIndex($data);
    }
}
