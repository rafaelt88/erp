<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
error_reporting(E_ALL);
date_default_timezone_set('Europe/Madrid');
/**
 * URL de la comunidad.
 */
define('FS_COMMUNITY_URL', 'https://community.mifactura.eu');

$errors = [];
$errors2 = [];

/**
 * TODO: Missing documentation
 *
 * @param string      $name
 * @param false|mixed $default
 *
 * @return mixed
 */
function fs_filter_input_req($name, $default = false)
{
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
}

/**
 * TODO: Missing documentation
 *
 * @param array  $errors
 * @param string $nombre_archivo
 */
function guarda_config(&$errors, $nombre_archivo = 'config.php')
{
    $archivo = fopen(__DIR__ . '/' . $nombre_archivo, "w");
    if ($archivo) {
        fwrite($archivo, "<?php\n");

        $fields = [
            'DB_TYPE',
            'DB_HOST',
            'DB_PORT',
            'DB_NAME',
            'DB_USER',
            'DB_PASS',
            'CACHE_HOST',
            'CACHE_PORT',
            'CACHE_PREFIX',
        ];
        foreach ($fields as $name) {
            fwrite($archivo, "define('FS_" . $name . "', '" . fs_filter_input_req(mb_strtolower($name)) . "');\n");
        }

        if (fs_filter_input_req('db_type') == 'MYSQL' && fs_filter_input_req('mysql_socket') != '') {
            fwrite($archivo, "ini_set('mysqli.default_socket', '" . fs_filter_input_req('mysql_socket') . "');\n");
        }

        fwrite($archivo, "define('FS_TMP_NAME', '" . random_string(20) . "/');\n");
        fwrite($archivo, "define('FS_COOKIES_EXPIRE', 31536000);\n");
        fwrite($archivo, "define('FS_ITEM_LIMIT', 50);\n");

        $fieldsFalse = ['DB_HISTORY', 'DEMO', 'DISABLE_MOD_PLUGINS', 'DISABLE_ADD_PLUGINS', 'DISABLE_RM_PLUGINS'];
        foreach ($fieldsFalse as $name) {
            fwrite($archivo, "define('FS_" . $name . "', FALSE);\n");
        }

        if (fs_filter_input_req('proxy_type')) {
            fwrite($archivo, "define('FS_PROXY_TYPE', '" . fs_filter_input_req('proxy_type') . "');\n");
            fwrite($archivo, "define('FS_PROXY_HOST', '" . fs_filter_input_req('proxy_host') . "');\n");
            fwrite($archivo, "define('FS_PROXY_PORT', '" . fs_filter_input_req('proxy_port') . "');\n");
        }

        fclose($archivo);

        if (fs_filter_input_req('unattended') == true) {
            die('Ok');
        }

        header("Location: index.php");
        exit();
    }

    $errors[] = "permisos";
}

/**
 * TODO: Missing documentation
 *
 * @param array $errors
 * @param array $errors2
 */
function test_mysql(&$errors, &$errors2)
{
    if (!class_exists('mysqli')) {
        $errors[] = "db_mysql";
        $errors2[] = 'No tienes instalada la extensión de PHP para MySQL.';
        return;
    }

    if (fs_filter_input_req('mysql_socket') != '') {
        ini_set('mysqli.default_socket', fs_filter_input_req('mysql_socket'));
    }

    // Omitimos el valor del nombre de la BD porque lo comprobaremos más tarde
    $connection = @new mysqli(fs_filter_input_req('db_host'), fs_filter_input_req('db_user'), fs_filter_input_req('db_pass'), '', intval(fs_filter_input_req('db_port')));
    if ($connection->connect_error) {
        $errors[] = "db_mysql";
        $errors2[] = $connection->connect_errno . ': ' . $connection->connect_error;
        return;
    }

    // Comprobamos que la BD exista, de lo contrario la creamos
    $db_selected = mysqli_select_db($connection, fs_filter_input_req('db_name'));
    if ($db_selected) {
        guarda_config($errors);
        return;
    }

    $sqlCrearBD = "CREATE DATABASE `" . fs_filter_input_req('db_name') . "` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;";
    if (mysqli_query($connection, $sqlCrearBD)) {
        guarda_config($errors);
        return;
    }

    $errors[] = "db_mysql";
    $errors2[] = mysqli_error($connection);
}

/**
 * TODO: Missing documentation
 *
 * @param array $errors
 * @param array $errors2
 */
function test_postgresql(&$errors, &$errors2)
{
    if (!function_exists('pg_connect')) {
        $errors[] = "db_postgresql";
        $errors2[] = 'No tienes instalada la extensión de PHP para PostgreSQL.';
        return;
    }

    $connection = @pg_connect('host=' . fs_filter_input_req('db_host') . ' port=' . fs_filter_input_req('db_port') . ' user=' . fs_filter_input_req('db_user') . ' password=' . fs_filter_input_req('db_pass'));

    if (!$connection) {
        $errors[] = "db_postgresql";
        $errors2[] = 'No se puede conectar a la base de datos. Revisa los datos de usuario y contraseña.';
        return;
    }

    // Comprobamos que la BD exista, de lo contrario la creamos
    $connection2 = @pg_connect('host=' . fs_filter_input_req('db_host') . ' port=' . fs_filter_input_req('db_port') . ' dbname=' . fs_filter_input_req('db_name') . ' user=' . fs_filter_input_req('db_user') . ' password=' . fs_filter_input_req('db_pass'));

    if ($connection2) {
        guarda_config($errors);
        return;
    }

    $sqlCrearBD = 'CREATE DATABASE "' . fs_filter_input_req('db_name') . '" ENCODING "UTF8";';
    if (pg_query($connection, $sqlCrearBD)) {
        guarda_config($errors);
        return;
    }

    $errors[] = "db_postgresql";
    $errors2[] = 'Error al crear la base de datos . ';
}

/**
 * TODO: Missing documentation
 *
 * @param int $length
 *
 * @return false|string
 */
function random_string($length = 20)
{
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

$name = random_string(8);
if (PHP_OS_FAMILY == "Windows") {
    $name = mb_strtolower($name);
}
$db_type = fs_filter_input_req('db_type', 'MYSQL');
$db_host = fs_filter_input_req('db_host', 'localhost');
$db_port = fs_filter_input_req('db_port', '3306');
$db_name = fs_filter_input_req('db_name', 'mf_' . $name);
$db_user = fs_filter_input_req('db_user', '');
$unattended = fs_filter_input_req('unattended', false);
if ($unattended) {
    guarda_config($errors);
}

/**
 * Buscamos errores
 */
if (file_exists('config.php')) {
    header('Location: index.php');
} elseif (floatval(substr(phpversion(), 0, 3)) < 7.2) {
    $errors[] = 'php';
} elseif (floatval('3,1') >= floatval('3.1')) {
    $errors[] = "floatval";
    $errors2[] = 'El separador de decimales de esta versión de PHP no es el punto, como sucede en las instalaciones estándar. Debes corregirlo.';
} elseif (!function_exists('mb_substr')) {
    $errors[] = "mb_substr";
} elseif (!extension_loaded('simplexml')) {
    $errors[] = "simplexml";
    $errors2[] = 'No se encuentra la extensión simplexml en tu instalación de PHP. Debes instalarla o activarla.';
    $errors2[] = 'Linux: instala el paquete <b>php-xml </b> y reinicia el Apache.';
    // } elseif (!extension_loaded('openssl')) {
    // $errors[] = "openssl";
} elseif (!extension_loaded('zip')) {
    $errors[] = "ziparchive";
} elseif (!is_writable(__DIR__)) {
    $errors[] = "permisos";
} elseif (fs_filter_input_req('action') === 'save') {
    if (fs_filter_input_req('db_type') == 'MYSQL') {
        test_mysql($errors, $errors2);
    } elseif (fs_filter_input_req('db_type') == 'POSTGRESQL') {
        test_postgresql($errors, $errors2);
    }

    $db_type = fs_filter_input_req('db_type', 'MYSQL');
    $db_host = fs_filter_input_req('db_host', 'localhost');
    $db_port = fs_filter_input_req('db_port', '3306');
    $db_name = fs_filter_input_req('db_name', 'mifactura');
    $db_user = fs_filter_input_req('db_user', '');
}

$system_info = 'MiFactura: ' . trim(@file_get_contents('MiVersion')) . "\n";
$system_info .= 'os: ' . php_uname() . "\n";
$system_info .= 'php: ' . phpversion() . "\n";

if (isset($_SERVER['REQUEST_URI'])) {
    $system_info .= 'url: ' . $_SERVER['REQUEST_URI'] . "\n------";
}
foreach ($errors as $e) {
    $system_info .= "\n" . $e;
}

$system_info = str_replace('"', "'", $system_info);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>MiFactura.eu</title>
    <meta name="description" content="MiFactura.eu es un software de facturación y contabilidad para pymes. Es software libre bajo licencia GNU/LGPL."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="Templates/img/favicon.ico"/>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css"/>
    <link rel="stylesheet" href="Templates/css/custom.css"/>
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css"/>
    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="node_modules/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="node_modules/bootbox/dist/bootbox.min.js"></script>
    <script type="text/javascript" src="Templates/js/base.js"></script>
    <script type="text/javascript" src="node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="node_modules/lazysizes/lazysizes.min.js" async=""></script>
    <script type="text/javascript" src="node_modules/tinymce/tinymce.min.js"></script>
    <style>
        .license {
            width: 100%;
            height: 480px;
            scrolling: "yes";
            overflow-y: auto;
            text-align: justify;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">MiFactura.eu</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                        <span class="d-none d-sm-inline">
                            <i class="fa-solid fa-circle-question fa-fw"></i>
                        </span>
                        <span class="visible-xs">Ayuda</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="dropdown-item" href="<?php echo FS_COMMUNITY_URL; ?>/doc/2" target="_blank">
                                <i class="fa-solid fa-book fa-fw"></i>Documentación
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?php echo FS_COMMUNITY_URL; ?>/contacto" target="_blank">
                                <i class="fa-solid fa-shield fa-fw"></i>Soporte oficial
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="dropdown-item" href="#" id="b_feedback">
                                <i class="fa-solid fa-edit fa-fw"></i>Informar de error...
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<form name="f_feedback" action="<?php echo FS_COMMUNITY_URL; ?>/feedback" method="post" target="_blank" class="form"
      role="form">
    <input type="hidden" name="feedback_info" value="<?php echo $system_info; ?>"/>
    <input type="hidden" name="feedback_type" value="error"/>
    <div class="modal" id="modal_feedback">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        <i class="fa-solid fa-edit fa-fw"></i>Informar de error...
                    </h4>
                    <p class="form-text">
                        Usa este formulario para informarnos de cualquier error o duda que hayas encontrado.
                        Para facilitarnos el trabajo este formulario también nos informa de la versión de
                        MiFactura.eu que usas, versión de php, etc...
                    </p>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <textarea class="form-control" name="feedback_text" rows="6" placeholder="Detalla tu duda o problema..."></textarea>
                    </div>
                    <div class="mb-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa-solid fa-envelope fa-fw"></i>
                            </span>
                            <input type="email" class="form-control" name="feedback_email"
                                   placeholder="Introduce tu email"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa-solid fa-send fa-fw"></i>Enviar
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    function change_db_type() {
        if (document.f_configuracion_inicial.db_type.value == 'POSTGRESQL') {
            document.f_configuracion_inicial.db_port.value = '5432';
            if (document.f_configuracion_inicial.db_user.value == '') {
                document.f_configuracion_inicial.db_user.value = 'postgres';
            }
            $("#mysql_socket").hide();
        } else {
            document.f_configuracion_inicial.db_port.value = '3306';
            $("#mysql_socket").show();
        }
    }

    $(document).ready(function () {
        $("#f_configuracion_inicial").validate({
            rules: {
                db_type: {required: false},
                db_host: {required: true, minlength: 2},
                db_port: {required: true, minlength: 2},
                db_name: {required: true, minlength: 2},
                db_user: {required: true, minlength: 2},
                db_pass: {required: false},
                cache_host: {required: true, minlength: 2},
                cache_port: {required: true, minlength: 2},
                cache_prefix: {required: false, minlength: 2}
            },
            messages: {
                db_host: {
                    required: "El campo es obligatorio.",
                    minlength: $.validator.format("Requiere mínimo {0} caracteres!")
                },
                db_port: {
                    required: "El campo es obligatorio.",
                    minlength: $.validator.format("Requiere mínimo {0} caracteres!")
                },
                db_name: {
                    required: "El campo es obligatorio.",
                    minlength: $.validator.format("Requiere mínimo {0} caracteres!")
                },
                db_user: {
                    required: "El campo es obligatorio.",
                    minlength: $.validator.format("Requiere mínimo {0} caracteres!")
                },
                cache_host: {
                    required: "El campo es obligatorio.",
                    minlength: $.validator.format("Requiere mínimo {0} caracteres!")
                },
                cache_port: {
                    required: "El campo es obligatorio.",
                    minlength: $.validator.format("Requiere mínimo {0} caracteres!")
                }
            }
        });
    });
</script>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card border mt-3">
                <h5 class="card-header text-truncate">
                    <i class="fa-solid fa-cloud-upload fa-fw"></i>
                    Bienvenido al instalador de MiFactura.eu
                    <small><?php echo trim(@file_get_contents('MiVersion')); ?></small>
                </h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <b>Antes de empezar...</b>
                            <p class="form-text">
                                Recuerda que tienes el
                                <b>menú de ayuda</b> arriba a la derecha. Si encuentras cualquier problema,
                                haz clic en
                                <b>informar de error...</b> y describe tu duda, sugerencia o el error que has encontrado.
                                No sabemos hacer software perfecto, pero con tu ayuda nos podemos acercar cada vez más ;-)
                                <br/><br/>
                                Y recuerda que tienes una sección especialmente dedicada a la
                                <b>instalación</b> en nuestra
                                documentación oficial:
                            </p>
                            <a href="<?php echo FS_COMMUNITY_URL; ?>/doc/2/instalacion" target="_blank" class="btn btn-info">
                                <i class="fa-solid fa-book fa-fw"></i>
                                Documentación
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-12">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
            foreach ($errors as $err) {
                if ($err == 'permisos') {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            Permisos de escritura
                        </h5>
                        <div class="card-body">
                            <p class="form-text">
                                La carpeta de MiFactura.eu no tiene permisos de escritura.
                                Estos permisos son necesarios para el sistema de plantillas,
                                instalar plugins, actualizaciones, etc...
                            </p>
                            <h3>
                                <i class="fa-solid fa-linux fa-fw"></i>Linux
                            </h3>
                            <pre>sudo chmod -R o+w <?php echo dirname(__FILE__); ?></pre>
                            <p class="form-text">
                                Este comando soluciona el problema en el 95% de los casos, pero
                                puedes optar por una solución más restrictiva, simplemente es necesario
                                que Apache (o PHP) pueda leer y escribir en la carpeta.
                            </p>
                            <h3>
                                <i class="fa-solid fa-lock fa-fw"></i>Fedora / CentOS / Red Hat
                            </h3>
                            <p class="form-text">
                                La configuración por defecto de estas distribuciones, en concreto SELinux,
                                bloquea cualquier intento de comprobar si la carpeta tiene permisos de escritura.
                                Desactiva o modifica la configuración de SELinux para el correcto funcionamiento
                                de MiFactura.eu.
                            </p>
                            <h3>
                                <i class="fa-solid fa-globe fa-fw"></i>Hosting
                            </h3>
                            <p class="form-text">
                                Intenta dar permisos de escritura desde el cliente <b>FTP</b> o desde el <b>cPanel</b>.
                            </p>
                        </div>
                    </div>
                    <?php
                } elseif ($err == 'php') {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            Versión de PHP obsoleta
                        </h5>
                        <div class="card-body">
                            <p class="form-text">
                                MiFactura.eu necesita PHP <b>5.6</b> o superior.
                                Tú estás usando la versión <b><?php echo phpversion() ?></b>.
                            </p>
                            <h3>Soluciones:</h3>
                            <ul>
                                <li>
                                    <p class="form-text">
                                        Muchos hostings ofrecen <b>varias versiones de PHP</b>. Ve al panel de control
                                        de tu hosting y selecciona la versión de PHP más alta.
                                    </p>
                                </li>
                                <li>
                                    <p class="form-text">
                                        Busca un proveedor de hosting más completo, que son la mayoría. Mira en nuestra
                                        sección de
                                        <a href="<?php echo FS_COMMUNITY_URL; ?>/descargar?nube=TRUE" target="_blank">Hostings
                                            recomendados</a>.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php
                } elseif ($err == 'mb_substr') {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            No se encuentra la función mb_substr()
                        </h5>
                        <div class="card-body">
                            <p class="form-text">
                                MiFactura.eu necesita la extensión mbstring para poder trabajar con caracteres
                                no europeos (chinos, coreanos, japonenes y rusos).
                            </p>
                            <h3>
                                <i class="fa-solid fa-linux fa-fw"></i>Linux
                            </h3>
                            <p class="form-text">
                                Instala el paquete <b>php-mbstring</b> y reinicia el Apache.
                            </p>
                            <h3>
                                <i class="fa-solid fa-globe fa-fw"></i>Hosting
                            </h3>
                            <p class="form-text">
                                Algunos proveedores de hosting ofrecen versiones de PHP demasiado recortadas.
                                Es mejor que busques un proveedor de hosting más completo, que son la mayoría.
                                Mira en nuestra sección de
                                <a href="<?php echo FS_COMMUNITY_URL; ?>/descargar?nube=TRUE" target="_blank">Hostings
                                    recomendados</a>.
                            </p>
                        </div>
                    </div>
                    <?php
                } elseif ($err == 'openssl') {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            No se encuentra la extensión OpenSSL
                        </h5>
                        <div class="card-body">
                            <p class="form-text">
                                MiFactura.eu necesita la extensión OpenSSL para poder descargar plugins,
                                actualizaciones y enviar emails.
                            </p>
                            <h3>
                                <i class="fa-solid fa-globe fa-fw"></i>Hosting
                            </h3>
                            <p class="form-text">
                                Algunos proveedores de hosting ofrecen versiones de PHP demasiado recortadas.
                                Es mejor que busques un proveedor de hosting más completo, que son la mayoría.
                                Mira en nuestra sección de
                                <a href="<?php echo FS_COMMUNITY_URL; ?>/descargar?nube=TRUE" target="_blank">Hostings
                                    recomendados</a>.
                            </p>
                            <h3>
                                <i class="fa-solid fa-windows fa-fw"></i>Windows
                            </h3>
                            <p class="form-text">
                                Ofrecemos una versión de MiFactura.eu para Windows <b>con todo</b> el software necesario
                                (como OpenSSL) ya incluido de serie. Puedes encontrala en nuestra sección de
                                <a href="<?php echo FS_COMMUNITY_URL; ?>/descargar?windows=TRUE" target="_blank">descargas</a>.
                                Si decides utilizar <b>un empaquetado distinto</b>, y este no incluye lo necesario,
                                deberás
                                buscar ayuda en los foros o el soporte de los creadores de ese empaquetado.
                            </p>
                            <h3>
                                <i class="fa-solid fa-linux fa-fw"></i>Linux
                            </h3>
                            <p class="form-text">
                                Es muy raro que una instalación propia de PHP en Linux no incluya OpenSSL.
                                Intenta instalar el paquete <b>php-openssl</b> con tu gestor de paquetes
                                y reinicia el Apache. Para más información consulta la ayuda o los foros
                                de la distribución Linux que utilices.
                            </p>
                            <h3>
                                <i class="fa-solid fa-apple fa-fw"></i>Mac
                            </h3>
                            <p class="form-text">
                                Es raro que un empaquetado Apache+PHP+MySQL para Mac no incluya OpenSSL.
                                Nosotros ofrecemos varios empaquetados con todo lo necesario en nuestra sección de
                                <a href="<?php echo FS_COMMUNITY_URL; ?>/descargar?mac=TRUE"
                                   target="_blank">descargas</a>.
                            </p>
                        </div>
                    </div>
                    <?php
                } elseif ($err == 'ziparchive') {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            No se encuentra la extensión ZipArchive
                        </h5>
                        <div class="card-body">
                            <p class="form-text">
                                MiFactura.eu necesita la extensión ZipArchive para poder
                                descomprimir plugins y actualizaciones.
                            </p>
                            <h3>
                                <i class="fa-solid fa-linux fa-fw"></i>Linux
                            </h3>
                            <p class="form-text">Instala el paquete <b>php-zip</b> y reinicia el Apache.</p>
                            <h3>
                                <i class="fa-solid fa-globe fa-fw"></i>Hosting
                            </h3>
                            <p class="form-text">
                                Algunos proveedores de hosting ofrecen versiones de PHP demasiado recortadas.
                                Es mejor que busques un proveedor de hosting más completo, que son la mayoría.
                                Mira en nuestra sección de
                                <a href="<?php echo FS_COMMUNITY_URL; ?>/descargar?nube=TRUE" target="_blank">Hostings
                                    recomendados</a>.
                            </p>
                        </div>
                    </div>
                    <?php
                } elseif ($err == 'db_mysql') {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            Acceso a base de datos MySQL
                        </h5>
                        <div class="card-body">
                            <ul>
                                <?php
                                foreach ($errors2 as $err2) {
                                    echo "<li>" . $err2 . "</li>";
                                }

                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php
                } elseif ($err == 'db_postgresql') {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            Acceso a base de datos PostgreSQL
                        </h5>
                        <div class="card-body">
                            <ul>
                                <?php
                                foreach ($errors2 as $err2) {
                                    echo "<li>" . $err2 . "</li>";
                                }

                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="card text-white bg-danger mt-3">
                        <h5 class="card-header">
                            Error
                        </h5>
                        <div class="card-body">
                            <ul>
                                <?php
                                if (!empty($errors2)) {
                                    foreach ($errors2 as $err2) {
                                        echo "<li>" . $err2 . "</li>";
                                    }
                                } else {
                                    echo "<li>Error desconocido.</li>";
                                }

                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <form name="f_configuracion_inicial" id="f_configuracion_inicial" action="install.php" class="form" role="form" method="post">
        <div class="row mt-3">
            <div class="col-sm-12">
                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    <li class="nav-item">
                        <a href="#db" class="nav-link active" aria-controls="db" role="tab" data-bs-toggle="tab" id="tab_database">
                            <i class="fa-solid fa-database fa-fw"></i>
                            Base de datos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#cache" class="nav-link" aria-controls="cache" role="tab" data-bs-toggle="tab" id="tab_advanced">
                            <i class="fa-solid fa-wrench fa-fw"></i>
                            Avanzado
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#licencia" class="nav-link" aria-controls="licencia" role="tab" data-bs-toggle="tab" id="tab_license">
                            <i class="fa-solid fa-file-alt fa-fw"></i>
                            Licencia
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content nav-content p-4">
            <div role="tabpanel" class="tab-pane fade show active" id="db">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label>Tipo de servidor SQL</label>
                            <select name="db_type" class="form-control select2" onchange="change_db_type()">
                                <option value="MYSQL"<?php echo ($db_type == 'MYSQL') ? ' selected = ""' : ''; ?> >MySQL</option>
                                <option value="POSTGRESQL"<?php echo ($db_type == 'POSTGRESQL') ? ' selected = ""' : ''; ?>>
                                    PostgreSQL
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label>Servidor</label>
                            <input class="form-control" type="text" name="db_host" value="<?php echo $db_host; ?>" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label>Puerto</label>
                            <input class="form-control" type="number" name="db_port" value="<?php echo $db_port; ?>" autocomplete="off"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label>Nombre base de datos</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="fa-solid fa-database fa-fw"></i>
                                </span>
                                <input class="form-control" type="text" name="db_name" value="<?php echo $db_name; ?>" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label>Usuario</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="fa-solid fa-user fa-fw"></i>
                                </span>
                                <input class="form-control" type="text" name="db_user" value="<?php echo $db_user; ?>" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label>Contraseña</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="fa-solid fa-key fa-fw"></i>
                                </span>
                                <input class="form-control" type="password" name="db_pass" value="" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div id="mysql_socket" class="form-group">
                            <label>Socket</label>
                            <input class="form-control" type="text" name="mysql_socket" value="" placeholder="Opcional" autocomplete="off"/>
                            <p class="form-text">
                                Solamente en algunos hostings es necesario especificar el socket de MySQL.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="cache">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card border mt-3">
                            <div class="card-header">
                                Memcached
                            </div>
                            <div class="card-body">
                                <p class="form-text">
                                    Este apartado es totalmente <b>opcional</b>. Si tienes instalado memcached,
                                    puedes especificar aquí la ruta, puerto y prefijo a utilizar. Si no,
                                    déjalo como está.
                                </p>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="mb-3">
                                            Servidor
                                            <input class="form-control" type="text" name="cache_host" value="localhost" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="mb-3">
                                            Puerto
                                            <input class="form-control" type="number" name="cache_port" value="11211" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="mb-3">
                                            Prefijo
                                            <input class="form-control" type="text" name="cache_prefix" value="<?php echo random_string(8); ?>_" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card border mt-3">
                            <div class="card-header">
                                Proxy
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="mb-3">
                                            Tipo de Proxy
                                            <select class='form-control select2' name="proxy_type">
                                                <option value="">Sin proxy</option>
                                                <option value="HTTP">HTTP</option>
                                                <option value="HTTPS">HTTPS</option>
                                                <option value="SOCKS5">SOCKS5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="mb-3">
                                            Servidor
                                            <input class="form-control" type="text" name="proxy_host" placeholder="192.168.1.1" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="mb-3">
                                            Puerto
                                            <input class="form-control" type="number" name="proxy_port" placeholder="8080" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="licencia">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-3">
                            <pre class="license"><?php echo file_get_contents('COPYING'); ?></pre>
                            <p class="form-text">
                                MiFactura.eu también incluye una versión modificada de <a href="https://github.com/feulf/raintpl/tree/65493157073ff0f313a67fe2ee42139b3eaa7f5a">RainTPL</a> que también tiene licencia <a href="raintpl/LICENSE.txt">LGPL</a>, así como <a href="https://github.com/PHPMailer/PHPMailer/">phpmailer</a> con la misma licencia <a href="extras/phpmailer/LICENSE">LGPL</a>.
                            </p>
                            <p class="form-text">
                                Para la parte gráfica se incluye el framewrowk <a href="http://getbootstrap.com">Bootstrap</a>, con licencia <a href="https://github.com/twbs/bootstrap/blob/master/LICENSE">MIT</a> y <a href="http://fontawesome.io">font-awesome</a> también con licencia <a href="http://fontawesome.io/license">MIT</a>.
                            </p>
                            <p class="form-text">
                                Y por último, pero no menos importante, también incluye <a href="https://github.com/jquery/jquery">jQuery</a>, con licencia <a href="https://github.com/jquery/jquery/blob/master/LICENSE.txt">MIT</a>.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-end">
                <button id="submit_button" class="btn btn-success" type="submit" name="action" value="save">
                    <i class="fa-solid fa-check fa-fw"></i>
                    Aceptar
                </button>
            </div>
        </div>
    </form>
    <div class="row mb-4">
        <div class="col-sm-12 text-center">
            <hr/>
            <small>
                &COPY; 2013-<?php echo date('Y'); ?>
                <a target="_blank" href="<?php echo FS_COMMUNITY_URL; ?>">MiFactura.eu</a>
            </small>
        </div>
    </div>
</div>
</body>
</html>
