<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Contiene la ruta principal del alojamiento.
 */
define('BASE_PATH', __DIR__); // Eliminar cuando no se use

/**
 * Contiene la ruta principal del alojamiento.
 *
 * @deprecated Utilice en su lugar BASE_PATH
 */
define('PATH', constant('BASE_PATH'));

/**
 * Contiene la ruta principal del alojamiento.
 *
 * No está deprecated, no es la misma ruta cuando es una multi instalación
 */
define('FS_FOLDER', constant('BASE_PATH'));

$min_version = 7.2;
$max_version = 8.1;
if ((float) substr(phpversion(), 0, 3) < $min_version && (float) substr(phpversion(), 0, 3) > $max_version) {
    /// comprobamos la versión de PHP
    die('MiFactura.eu necesita PHP ' . $min_version . ' hasta ' . $max_version . ', y se ha detectado la versión de PHP ' . phpversion());
}

if (!file_exists('config.php')) {
    /// si no hay config.php redirigimos al instalador
    header('Location: install.php');
    die('Redireccionando al instalador...');
}

$extensions = ['bcmath', 'curl', 'dom', 'gd', 'json', 'mbstring', 'simplexml', 'tokenizer', 'zip'];
$errors = [];
foreach ($extensions as $extension) {
    if (!extension_loaded($extension)) {
        $errors[] = $extension;
    }
}
if (!empty($errors)) {
    die('Necesitas instalar las extensiones <b>' . implode($errors, ', ') . '</b> para PHP ' . phpversion());
}

/// cargamos las constantes de configuración
require_once constant('BASE_PATH') . '/config.php';
require_once constant('BASE_PATH') . '/base/config2.php';
require_once constant('BASE_PATH') . '/base/fs_core_log.php';
require_once constant('BASE_PATH') . '/base/fs_db2.php';
$db = new fs_db2();

require_once constant('BASE_PATH') . '/base/fs_extended_model.php';
require_once constant('BASE_PATH') . '/base/fs_log_manager.php';
require_once constant('BASE_PATH') . '/base/fs_api.php';
require_all_models();

if ($db->connect() && $db->connected()) {
    $api = new fs_api();
    echo $api->run();

    /// guardamos los errores en el log
    $log_manager = new fs_log_manager();
    $log_manager->save();

    $db->close();
} else {
    echo 'ERROR al conectar a la base de datos';
}
